/*
 * Sistema de Gestão de Projetos Científicos - SGPC
 * 
 * 
 * Copyright  (C)  2010-2014  Instituto  de  Ciências  Matemáticas  e  de
 *                            Computação - ICMC/USP
 * 
 * 
 * This program is  free software; you can redistribute  it and/or modify
 * it under the  terms of the GNU General Public  License as published by
 * the Free Software Foundation; either  version 2 of the License, or (at
 * your option) any later version.
 * 
 * This program  is distributed in the  hope that it will  be useful, but
 * WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
 * MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
 * General Public License for more details.
 * 
 * You  should have received  a copy  of the  GNU General  Public License
 * along  with  this  program;  if   not,  write  to  the  Free  Software
 * Foundation,  Inc.,  51  Franklin   Street,  Fifth  Floor,  Boston,  MA
 * 02110-1301, USA.
 * 
 * 
 * EXCEPTION TO THE TERMS AND CONDITIONS OF GNU GENERAL PUBLIC LICENSE
 * 
 * The  LICENSE  file may  contain  an  additional  clause as  a  special
 * exception  to the  terms  and  conditions of  the  GNU General  Public
 * License. This  clause, if  present, gives you  the permission  to link
 * this  program with  certain third-part  software and  to obtain,  as a
 * result, a  work based  on this program  that can be  distributed using
 * other license than the GNU General Public License.  If you modify this
 * program, you may extend this exception to your version of the program,
 * but you  are not obligated  to do so.   If you do  not wish to  do so,
 * delete this exception statement from your version.
 * 
 */

package br.usp.icmc.sgpc.security.jsf;

import br.usp.icmc.sgpc.beans.PessoaBean;
import br.usp.icmc.sgpc.jsf.UserSessionManagedBean;
import br.usp.icmc.sgpc.security.RbacConstantes;
import br.usp.icmc.sgpc.security.beans.ObjetoProtegidoBean;
import br.usp.icmc.sgpc.security.beans.PapelBean;
import br.usp.icmc.sgpc.security.service.SecurityService;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIPanel;
import javax.faces.context.FacesContext;
import org.apache.log4j.Logger;
import org.primefaces.context.RequestContext;
import org.primefaces.model.DualListModel;

/**
 *
 * @author Artur
 */
@ManagedBean(name = "objetoProtegidoMB")
@ViewScoped
public class ObjetoProtegidoManagedBean implements Serializable {

    private static final Logger logger = Logger.getLogger(ObjetoProtegidoManagedBean.class);
    private ObjetoProtegidoBean objetoProtegido;
    private List<ObjetoProtegidoBean> listaObjetosProtegidos = new ArrayList<ObjetoProtegidoBean>();
    private UIPanel panelForm;
    private Integer currentRow;
    private String pesquisa;
    public static final String PESQUISAR_STATE = "pesquisar";
    public static final String ADICIONAR_STATE = "adicionar";
    public static final String EDITAR_STATE = "editar";
    private String currentState = PESQUISAR_STATE;
    private boolean podeEditarObjetoProtegido;
    private String imagemEditarRegistro = "/images/icons/editar.png";
    private String imagemExcluirRegistro = "/images/icons/delbw.png";
    private String imagemCriarRegistro = "/images/icons/add1bw.png";
    private DualListModel<PapelBean> papeisDualList;

    public String getImagemCriarRegistro() {
        return imagemCriarRegistro;
    }

    public void setImagemCriarRegistro(String imagemCriarRegistro) {
        this.imagemCriarRegistro = imagemCriarRegistro;
    }

    public String getImagemEditarRegistro() {
        if (podeEditarObjetoProtegido) {
            imagemEditarRegistro = "/images/icons/editar.png";
        } else {
            imagemEditarRegistro = "/images/icons/editarbw.png";
        }
        return imagemEditarRegistro;
    }

    public void setImagemEditarRegistro(String imagemEditarRegistro) {
        this.imagemEditarRegistro = imagemEditarRegistro;
    }

    public String getImagemExcluirRegistro() {
        return imagemExcluirRegistro;
    }

    public void setImagemExcluirRegistro(String imagemExcluirRegistro) {
        this.imagemExcluirRegistro = imagemExcluirRegistro;
    }

    public boolean isPodeEditarObjetoProtegido() {
        return podeEditarObjetoProtegido;
    }

    public void setPodeEditarObjetoProtegido(boolean podeEditarObjetoProtegido) {
        this.podeEditarObjetoProtegido = podeEditarObjetoProtegido;
    }

    public ObjetoProtegidoManagedBean() {
        logger.debug("Instanciando classe");
        this.pesquisar();

        UserSessionManagedBean userSessionMB = (UserSessionManagedBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userSessionMB");
        PessoaBean usuarioLogado = userSessionMB.getLoggedUser();

        podeEditarObjetoProtegido = SecurityService.getInstance().verificarPermissao(usuarioLogado, RbacConstantes.OBJETO_PROTEGIDO_EDITAR);

        papeisDualList = new DualListModel<PapelBean>();
    }

    public List<ObjetoProtegidoBean> getListaObjetosProtegidos() {
        return listaObjetosProtegidos;
    }

    public void setListaObjetosProtegidos(List<ObjetoProtegidoBean> listaObjetosProtegidos) {
        this.listaObjetosProtegidos = listaObjetosProtegidos;
    }

    public ObjetoProtegidoBean getObjetoProtegido() {
        return objetoProtegido;
    }

    public void setObjetoProtegido(ObjetoProtegidoBean objetoProtegido) {
        this.objetoProtegido = objetoProtegido;
    }

    public String getPesquisa() {
        return pesquisa;
    }

    public void setPesquisa(String pesquisa) {
        this.pesquisa = pesquisa;
    }

    public Integer getCurrentRow() {
        return currentRow;
    }

    public void setCurrentRow(Integer currentRow) {
        this.currentRow = currentRow;
    }

    public UIPanel getPanelForm() {
        return panelForm;
    }

    public void setPanelForm(UIPanel panelForm) {
        this.panelForm = panelForm;
    }

    public String getCurrentState() {
        return currentState;
    }

    public void setCurrentState(String currentState) {
        this.currentState = currentState;
    }

    public boolean isAdicionarState() {
        return ADICIONAR_STATE.equals(this.getCurrentState());
    }

    public void prepareAdicionar() {
        this.clear();
        this.setCurrentState(ADICIONAR_STATE);
        this.objetoProtegido = new ObjetoProtegidoBean();
        List<PapelBean> todosPapeis = SecurityService.getInstance().listarPapeis();
        papeisDualList = new DualListModel<PapelBean>(todosPapeis, objetoProtegido.getPapeis());
        RequestContext.getCurrentInstance().execute("PF('addEditPopup').show();");
    }

    public void prepareEditar(ObjetoProtegidoBean obj) {
        this.setCurrentState(EDITAR_STATE);
        this.objetoProtegido = obj;
        List<PapelBean> todosPapeis = SecurityService.getInstance().listarPapeis();
        todosPapeis.removeAll(objetoProtegido.getPapeis());
        papeisDualList = new DualListModel<PapelBean>(todosPapeis, objetoProtegido.getPapeis());
        RequestContext.getCurrentInstance().execute("PF('addEditPopup').show();");
    }
    public void prepareExcluir(ObjetoProtegidoBean obj) {
        this.setCurrentState(EDITAR_STATE);
        this.objetoProtegido = obj;
        RequestContext.getCurrentInstance().execute("PF('deletePopup').show();");
    }

    public boolean isEditarState() {
        return EDITAR_STATE.equals(this.getCurrentState());
    }

    public boolean isPesquisarState() {
        String state = this.getCurrentState();
        return (state == null || PESQUISAR_STATE.equals(state));
    }

    public void clear() {
        this.objetoProtegido = new ObjetoProtegidoBean();
    }

    /*
     * Exclui registro
     */
    public void excluir() {
        logger.debug("EXCLUIR REGISTRO");
        SecurityService.getInstance().excluirObjetoProtegido(objetoProtegido);
        this.pesquisar();
        RequestContext.getCurrentInstance().execute("PF('deletePopup').hide();");
    }

    public void pesquisar() {
        logger.debug("PESQUISAR");
        this.objetoProtegido = new ObjetoProtegidoBean();
        if ("".equals(this.pesquisa) || this.pesquisa == null) {
            logger.debug("PESQUISAR TODOS");
            this.listaObjetosProtegidos = SecurityService.getInstance().listarObjetosProtegidos();
        } else {
            logger.debug("PESQUISAR COM TERMO");
            this.listaObjetosProtegidos = SecurityService.getInstance().pesquisarObjetosProtegidos(pesquisa);
        }
    }

    public void gravar() {
        if (ADICIONAR_STATE.equals(this.currentState)) {
            logger.debug("ADICIONAR REGISTRO");
            SecurityService.getInstance().cadastraObjetoProtegido(objetoProtegido);
            objetoProtegido.setPapeis(papeisDualList.getTarget());
            SecurityService.getInstance().atualizarObjetoProtegido(objetoProtegido);
        } else if (EDITAR_STATE.equals(this.currentState)) {
            logger.debug("EDITAR REGISTRO");
            objetoProtegido.setPapeis(papeisDualList.getTarget());
            SecurityService.getInstance().atualizarObjetoProtegido(objetoProtegido);
        }
        this.pesquisar();
        RequestContext.getCurrentInstance().execute("PF('addEditPopup').hide();");
    }

    public DualListModel<PapelBean> getPapeisDualList() {
        return papeisDualList;
    }

    public void setPapeisDualList(DualListModel<PapelBean> papeisDualList) {
        this.papeisDualList = papeisDualList;
    }
}
