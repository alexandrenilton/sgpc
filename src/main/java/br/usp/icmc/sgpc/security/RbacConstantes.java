/*
 * Sistema de Gestão de Projetos Científicos - SGPC
 * 
 * 
 * Copyright  (C)  2010-2014  Instituto  de  Ciências  Matemáticas  e  de
 *                            Computação - ICMC/USP
 * 
 * 
 * This program is  free software; you can redistribute  it and/or modify
 * it under the  terms of the GNU General Public  License as published by
 * the Free Software Foundation; either  version 2 of the License, or (at
 * your option) any later version.
 * 
 * This program  is distributed in the  hope that it will  be useful, but
 * WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
 * MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
 * General Public License for more details.
 * 
 * You  should have received  a copy  of the  GNU General  Public License
 * along  with  this  program;  if   not,  write  to  the  Free  Software
 * Foundation,  Inc.,  51  Franklin   Street,  Fifth  Floor,  Boston,  MA
 * 02110-1301, USA.
 * 
 * 
 * EXCEPTION TO THE TERMS AND CONDITIONS OF GNU GENERAL PUBLIC LICENSE
 * 
 * The  LICENSE  file may  contain  an  additional  clause as  a  special
 * exception  to the  terms  and  conditions of  the  GNU General  Public
 * License. This  clause, if  present, gives you  the permission  to link
 * this  program with  certain third-part  software and  to obtain,  as a
 * result, a  work based  on this program  that can be  distributed using
 * other license than the GNU General Public License.  If you modify this
 * program, you may extend this exception to your version of the program,
 * but you  are not obligated  to do so.   If you do  not wish to  do so,
 * delete this exception statement from your version.
 * 
 */

package br.usp.icmc.sgpc.security;

/**
 *
 * @author Artur
 */
public class RbacConstantes {
    public static final String CONTA_CORRENTE_CONSULTAR_TERCEIROS = "CONTA_CORRENTE_CONSULTAR_TERCEIROS";
    public static final String CONTA_CORRENTE_EDITAR_TERCEIROS = "CONTA_CORRENTE_EDITAR_TERCEIROS";
    public static final String CONTA_CORRENTE_EXCLUIR_TERCEIROS = "CONTA_CORRENTE_EXCLUIR_TERCEIROS";

    public static final String PROJETO_EDITAR_TERCEIROS = "PROJETO_EDITAR_TERCEIROS";
    public static final String PROJETO_EXCLUIR_TERCEIROS = "PROJETO_EXCLUIR_TERCEIROS";
    public static final String PROJETO_CONSULTAR_TERCEIROS = "PROJETO_CONSULTAR_TERCEIROS";
    public static final String PROJETO_CRIAR_PROPRIO = "PROJETO_CRIAR_PROPRIO";
    public static final String PROJETO_CRIAR_TERCEIROS = "PROJETO_CRIAR_TERCEIROS";

    public static final String PESSOA_EDITAR_TERCEIROS = "PESSOA_EDITAR_TERCEIROS";
    public static final String PESSOA_EXCLUIR_TERCEIROS = "PESSOA_EXCLUIR_TERCEIROS";
    public static final String PESSOA_CRIAR_REGISTRO = "PESSOA_CRIAR_REGISTRO";
    public static final String PESSOA_EXIBIR_TERCEIROS = "PESSOA_EXIBIR_TERCEIROS";
    public static final String PESSOA_GERENCIAR_GRUPOS = "PESSOA_GERENCIAR_GRUPOS";

    public static final String AUXILIO_CRIAR_TERCEIROS = "AUXILIO_CRIAR_TERCEIROS";
    public static final String AUXILIO_CONSULTAR_TERCEIROS = "AUXILIO_CONSULTAR_TERCEIROS";
    public static final String AUXILIO_EXCLUIR = "AUXILIO_EXCLUIR";

    public static final String ACESSA_PROJETO_LOGIN = "ACESSA_PROJETO_LOGIN";

    public static final String INSTITUICAO_CRIAR = "INSTITUICAO_CRIAR";
    public static final String INSTITUICAO_EDITAR = "INSTITUICAO_EDITAR";
    public static final String INSTITUICAO_EXCLUIR = "INSTITUICAO_EXCLUIR";
    
    public static final String UNIDADE_CRIAR = "UNIDADE_CRIAR";
    public static final String UNIDADE_EDITAR = "UNIDADE_EDITAR";
    public static final String UNIDADE_EXCLUIR = "UNIDADE_EXCLUIR";

    public static final String DEPARTAMENTO_CRIAR = "DEPARTAMENTO_CRIAR";
    public static final String DEPARTAMENTO_EDITAR = "DEPARTAMENTO_EDITAR";

    public static final String CENTROS_DESPESA_CRIAR = "CENTROS_DESPESA_CRIAR";
    public static final String CENTROS_DESPESA_EDITAR = "CENTROS_DESPESA_EDITAR";
    public static final String CENTROS_DESPESA_EXCLUIR = "CENTROS_DESPESA_EXCLUIR";

    public static final String FINACIADOR_CRIAR = "FINANCIADOR_CRIAR";
    public static final String FINANCIADOR_EDITAR = "FINANCIADOR_EDITAR";
    public static final String FINANCIADOR_EXCLUIR = "FINANCIADOR_EXCLUIR";

    public static final String INSTITUICAO_BANCARIA_CRIAR = "INSTITUICAO_BANCARIA_CRIAR";
    public static final String INSTITUICAO_BANCARIA_EDITAR = "INSTITUICAO_BANCARIA_EDITAR";
    public static final String INSTITUICAO_BANCARIA_EXCLUIR = "INSTITUICAO_BANCARIA_EXCLUIR";

    public static final String PAPEL_CRIAR = "PAPEL_CRIAR";
    public static final String PAPEL_EDITAR = "PAPEL_EDITAR";

    public static final String OBJETO_PROTEGIDO_EDITAR = "OBJETO_PROTEGIDO_EDITAR";
    
    public static final String AUXILIO_CRIAR = "AUXILIO_CRIAR";
    public static final String AUXILIO_EDITAR = "AUXILIO_EDITAR";
    public static final String AUXILIO_REMANEJAR = "AUXILIO_REMANEJAR";

    public static final String AUXILIO_ALTERAR_STATUS_FINANCEIRO = "AUXILIO_ALTERAR_STATUS_FINANCEIRO";

    public static final String AUDITORIA_ACESSAR = "AUDITORIA_ACESSAR";

    public static final String DESPESA_LISTAR = "DESPESA_LISTAR";

    public static final String SOLICITA_COTACAO = "SOLICITA_COTACAO";

    public static final String FORNECEDOR_CRIAR = "FORNECEDOR_CRIAR";
    public static final String FORNECEDOR_EDITAR = "FORNECEDOR_EDITAR";

    public static final String PRODUTO_CRIAR = "PRODUTO_CRIAR";
    public static final String PRODUTO_EDITAR = "PRODUTO_CRIAR";

    public static final String GRUPO_FORNECIMENTO_CRIAR = "GRUPO_FORNECIMENTO_CRIAR";
    public static final String GRUPO_FORNECIMENTO_EDITAR = "GRUPO_FORNECIMENTO_EDITAR";

    public static final String TIPO_AUXILIO_CRIAR = "TIPO_AUXILIO_CRIAR";
    public static final String TIPO_AUXILIO_EDITAR = "TIPO_AUXILIO_CRIAR";
    public static final String TIPO_AUXILIO_EXCLUIR = "TIPO_AUXILIO_EXCLUIR";

    public static final String ALINEA_CRIAR = "ALINEA_CRIAR";
    public static final String ALINEA_EDITAR = "ALINEA_EDITAR";
    public static final String ALINEA_EXCLUIR = "ALINEA_EXCLUIR";

    public static final String FINANCIAMENTO_ESPECIAL_CRIAR = "FINANCIAMENTO_ESPECIAL_CRIAR";
    public static final String FINANCIAMENTO_ESPECIAL_EDITAR = "FINANCIAMENTO_ESPECIAL_EDITAR";
    public static final String FINANCIAMENTO_ESPECIAL_EXCLUIR = "FINANCIAMENTO_ESPECIAL_EXCLUIR";

    public static final String BEM_PATRIMONIADO_CRIAR = "BEM_PATRIMONIADO_CRIAR";
    public static final String BEM_PATRIMONIADO_EDITAR = "BEM_PATRIMONIADO_EDITAR";

    public static final String ACESSA_CONFIGURACOES = "ACESSA_CONFIGURACOES";
    public static final String DESPESA_EXCLUIR = "DESPESA_EXCLUIR";
}
