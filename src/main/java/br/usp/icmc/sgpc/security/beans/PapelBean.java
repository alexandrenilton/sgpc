/*
 * Sistema de Gestão de Projetos Científicos - SGPC
 * 
 * 
 * Copyright  (C)  2010-2014  Instituto  de  Ciências  Matemáticas  e  de
 *                            Computação - ICMC/USP
 * 
 * 
 * This program is  free software; you can redistribute  it and/or modify
 * it under the  terms of the GNU General Public  License as published by
 * the Free Software Foundation; either  version 2 of the License, or (at
 * your option) any later version.
 * 
 * This program  is distributed in the  hope that it will  be useful, but
 * WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
 * MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
 * General Public License for more details.
 * 
 * You  should have received  a copy  of the  GNU General  Public License
 * along  with  this  program;  if   not,  write  to  the  Free  Software
 * Foundation,  Inc.,  51  Franklin   Street,  Fifth  Floor,  Boston,  MA
 * 02110-1301, USA.
 * 
 * 
 * EXCEPTION TO THE TERMS AND CONDITIONS OF GNU GENERAL PUBLIC LICENSE
 * 
 * The  LICENSE  file may  contain  an  additional  clause as  a  special
 * exception  to the  terms  and  conditions of  the  GNU General  Public
 * License. This  clause, if  present, gives you  the permission  to link
 * this  program with  certain third-part  software and  to obtain,  as a
 * result, a  work based  on this program  that can be  distributed using
 * other license than the GNU General Public License.  If you modify this
 * program, you may extend this exception to your version of the program,
 * but you  are not obligated  to do so.   If you do  not wish to  do so,
 * delete this exception statement from your version.
 * 
 */

package br.usp.icmc.sgpc.security.beans;

import br.usp.icmc.sgpc.beans.PessoaBean;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OrderBy;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 *
 * @author Artur
 */
@Entity
@Table(name = "papel")
@NamedQueries({
    @NamedQuery(name = "PapelBean.findAll", query = "SELECT p FROM PapelBean p"),
    @NamedQuery(name = "PapelBean.findById", query = "SELECT p FROM PapelBean p WHERE p.id = :id"),
    @NamedQuery(name = "PapelBean.findByNome", query = "SELECT p FROM PapelBean p WHERE p.nome = :nome"),
    @NamedQuery(name = "PapelBean.findByKeyword", query = "SELECT p FROM PapelBean p WHERE p.nome LIKE :nome"),
    @NamedQuery(name = "PapelBean.findByPodeSerResponsavel", query = "SELECT p FROM PapelBean p WHERE p.podeSerResponsavel = :podeSerResponsavel")})
    
public class PapelBean implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Basic(optional = false)
    @SequenceGenerator(name="id", sequenceName="papel_id_papel_seq")
    @GeneratedValue (strategy=GenerationType.AUTO, generator="id")
    @Column(name = "id_papel", nullable = false)
    private Integer id;

    @Basic(optional = false)
    @Column(name = "nome", nullable = false, length = 100)
    private String nome;

    @ManyToMany(mappedBy="papeis", cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @OrderBy("nome")
    private List<PessoaBean> pessoas;

    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH}, fetch = FetchType.LAZY)
    @JoinTable(name = "permissao",
        joinColumns =
            @JoinColumn(name = "fk_papel", referencedColumnName = "id_papel"),
        inverseJoinColumns =
            @JoinColumn(name = "fk_objeto_protegido", referencedColumnName = "id_objeto_protegido")
    )
    private List<ObjetoProtegidoBean> objetosProtegidos;
    
    @Column(name = "pode_ser_responsavel", nullable=false, columnDefinition="boolean default false")
    private boolean podeSerResponsavel;

    public PapelBean() {
        this.objetosProtegidos = new ArrayList<ObjetoProtegidoBean>();
        this.pessoas = new ArrayList<PessoaBean>();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public List<PessoaBean> getPessoas() {
        return pessoas;
    }

    public void setPessoas(List<PessoaBean> pessoas) {
        this.pessoas = pessoas;
    }

    public List<ObjetoProtegidoBean> getObjetosProtegidos() {
        return objetosProtegidos;
    }

    public void setObjetosProtegidos(List<ObjetoProtegidoBean> objetosProtegidos) {
        this.objetosProtegidos = objetosProtegidos;
    }

    @Override
    public String toString() {
        return this.getNome();
    }

    @Override
    public boolean equals(Object obj) {
         // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(obj instanceof PapelBean)) {
            return false;
        }
        PapelBean other = (PapelBean) obj;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    public boolean isPodeSerResponsavel() {
        return podeSerResponsavel;
    }

    public void setPodeSerResponsavel(boolean podeSerResponsavel) {
        this.podeSerResponsavel = podeSerResponsavel;
    }
}
