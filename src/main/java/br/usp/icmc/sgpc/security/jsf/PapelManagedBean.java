/*
 * Sistema de Gestão de Projetos Científicos - SGPC
 * 
 * 
 * Copyright  (C)  2010-2014  Instituto  de  Ciências  Matemáticas  e  de
 *                            Computação - ICMC/USP
 * 
 * 
 * This program is  free software; you can redistribute  it and/or modify
 * it under the  terms of the GNU General Public  License as published by
 * the Free Software Foundation; either  version 2 of the License, or (at
 * your option) any later version.
 * 
 * This program  is distributed in the  hope that it will  be useful, but
 * WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
 * MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
 * General Public License for more details.
 * 
 * You  should have received  a copy  of the  GNU General  Public License
 * along  with  this  program;  if   not,  write  to  the  Free  Software
 * Foundation,  Inc.,  51  Franklin   Street,  Fifth  Floor,  Boston,  MA
 * 02110-1301, USA.
 * 
 * 
 * EXCEPTION TO THE TERMS AND CONDITIONS OF GNU GENERAL PUBLIC LICENSE
 * 
 * The  LICENSE  file may  contain  an  additional  clause as  a  special
 * exception  to the  terms  and  conditions of  the  GNU General  Public
 * License. This  clause, if  present, gives you  the permission  to link
 * this  program with  certain third-part  software and  to obtain,  as a
 * result, a  work based  on this program  that can be  distributed using
 * other license than the GNU General Public License.  If you modify this
 * program, you may extend this exception to your version of the program,
 * but you  are not obligated  to do so.   If you do  not wish to  do so,
 * delete this exception statement from your version.
 * 
 */

package br.usp.icmc.sgpc.security.jsf;

import br.usp.icmc.sgpc.beans.PessoaBean;
import br.usp.icmc.sgpc.jsf.UserSessionManagedBean;
import br.usp.icmc.sgpc.security.RbacConstantes;
import br.usp.icmc.sgpc.security.beans.ObjetoProtegidoBean;
import br.usp.icmc.sgpc.security.beans.PapelBean;
import br.usp.icmc.sgpc.security.service.SecurityService;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIPanel;
import javax.faces.context.FacesContext;
import org.apache.log4j.Logger;
import org.primefaces.context.RequestContext;
import org.primefaces.model.DualListModel;

/**
 *
 * @author Artur
 */
@ManagedBean(name = "papelMB")
@ViewScoped
public class PapelManagedBean implements Serializable {

    private static final Logger logger = Logger.getLogger(PapelManagedBean.class);
    private PapelBean papel;
    private List<PapelBean> listaPapeis = new ArrayList<PapelBean>();
    private UIPanel panelForm;
    private Integer currentRow;
    private String pesquisa;
    public static final String PESQUISAR_STATE = "pesquisar";
    public static final String ADICIONAR_STATE = "adicionar";
    public static final String EDITAR_STATE = "editar";
    private String currentState = PESQUISAR_STATE;
    private DualListModel<ObjetoProtegidoBean> objetosProtegidosDisponiveis;
    private boolean podeCriarPapel;
    private boolean podeEditarPapel;
    private String imagemEditarRegistro = "/images/icons/editar.png";
    private String imagemExcluirRegistro = "/images/icons/delbw.png";
    private String imagemCriarRegistro = "/images/icons/add1.png";

    public String getImagemCriarRegistro() {
        if (podeCriarPapel) {
            imagemCriarRegistro = "/images/icons/add1.png";
        } else {
            imagemCriarRegistro = "/images/icons/add1bw.png";
        }
        return imagemCriarRegistro;
    }

    public void setImagemCriarRegistro(String imagemCriarRegistro) {
        this.imagemCriarRegistro = imagemCriarRegistro;
    }

    public String getImagemEditarRegistro() {
        if (podeEditarPapel) {
            imagemEditarRegistro = "/images/icons/editar.png";
        } else {
            imagemEditarRegistro = "/images/icons/editarbw.png";
        }
        return imagemEditarRegistro;
    }

    public void setImagemEditarRegistro(String imagemEditarRegistro) {
        this.imagemEditarRegistro = imagemEditarRegistro;
    }

    public String getImagemExcluirRegistro() {
        return imagemExcluirRegistro;
    }

    public void setImagemExcluirRegistro(String imagemExcluirRegistro) {
        this.imagemExcluirRegistro = imagemExcluirRegistro;
    }

    public boolean isPodeEditarPapel() {
        return podeEditarPapel;
    }

    public void setPodeEditarPapel(boolean podeEditarPapel) {
        this.podeEditarPapel = podeEditarPapel;
    }

    public boolean isPodeCriarPapel() {
        return podeCriarPapel;
    }

    public void setPodeCriarPapel(boolean podeCriarPapel) {
        this.podeCriarPapel = podeCriarPapel;
    }

    public PapelManagedBean() {
        logger.debug("Instanciando classe");
        this.pesquisar();

        UserSessionManagedBean userSessionMB = (UserSessionManagedBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("userSessionMB");
        PessoaBean usuarioLogado = userSessionMB.getLoggedUser();

        podeCriarPapel = SecurityService.getInstance().verificarPermissao(usuarioLogado, RbacConstantes.PAPEL_CRIAR);
        podeEditarPapel = SecurityService.getInstance().verificarPermissao(usuarioLogado, RbacConstantes.PAPEL_EDITAR);

        objetosProtegidosDisponiveis = new DualListModel<ObjetoProtegidoBean>();
    }

    public List<PapelBean> getListaPapeis() {
        return listaPapeis;
    }

    public void setListaPapeis(List<PapelBean> listaPapeis) {
        this.listaPapeis = listaPapeis;
    }

    public PapelBean getPapel() {
        return papel;
    }

    public void setPapel(PapelBean papel) {
        this.papel = papel;
    }

    public String getPesquisa() {
        return pesquisa;
    }

    public void setPesquisa(String pesquisa) {
        this.pesquisa = pesquisa;
    }

    public Integer getCurrentRow() {
        return currentRow;
    }

    public void setCurrentRow(Integer currentRow) {
        this.currentRow = currentRow;
    }

    public UIPanel getPanelForm() {
        return panelForm;
    }

    public void setPanelForm(UIPanel panelForm) {
        this.panelForm = panelForm;
    }

    public String getCurrentState() {
        return currentState;
    }

    public void setCurrentState(String currentState) {
        this.currentState = currentState;
    }

    public boolean isAdicionarState() {
        return ADICIONAR_STATE.equals(this.getCurrentState());
    }

    public void prepareAdicionar() {
        this.clear();
        this.setCurrentState(ADICIONAR_STATE);
        this.papel = new PapelBean();
        List<ObjetoProtegidoBean> todosObjetosProtegidos = SecurityService.getInstance().listarObjetosProtegidos();
        objetosProtegidosDisponiveis = new DualListModel<ObjetoProtegidoBean>(todosObjetosProtegidos, papel.getObjetosProtegidos());
        RequestContext.getCurrentInstance().execute("PF('addEditPopup').show();");
    }

    public void prepareEditar(PapelBean papel) {
        this.setCurrentState(EDITAR_STATE);
        this.papel = papel;
        List<ObjetoProtegidoBean> todosObjetosProtegidos = SecurityService.getInstance().listarObjetosProtegidos();
        todosObjetosProtegidos.removeAll(papel.getObjetosProtegidos());
        objetosProtegidosDisponiveis = new DualListModel<ObjetoProtegidoBean>(todosObjetosProtegidos, papel.getObjetosProtegidos());
        RequestContext.getCurrentInstance().execute("PF('addEditPopup').show();");
    }
    
    public void prepareExcluir(PapelBean papel) {
        this.papel = papel;
        RequestContext.getCurrentInstance().execute("PF('deletePopup').show();");
    }

    public boolean isEditarState() {
        return EDITAR_STATE.equals(this.getCurrentState());
    }

    public boolean isPesquisarState() {
        String state = this.getCurrentState();
        return (state == null || PESQUISAR_STATE.equals(state));
    }

    public void clear() {
        this.papel = new PapelBean();
    }

    /*
     * Exclui registro
     */
    public void excluir() {
        logger.debug("EXCLUIR REGISTRO");
        SecurityService.getInstance().excluirPapel(papel);
        this.pesquisar();
        RequestContext.getCurrentInstance().execute("PF('deletePopup').hide();");
    }

    public void pesquisar() {
        logger.debug("PESQUISAR");
        this.papel = new PapelBean();
        if ("".equals(this.pesquisa) || this.pesquisa == null) {
            logger.debug("PESQUISAR TODOS");
            this.listaPapeis = SecurityService.getInstance().listarPapeis();
        } else {
            logger.debug("PESQUISAR COM TERMO");
            this.listaPapeis = SecurityService.getInstance().pesquisarPapel(pesquisa);
        }
    }

    public void gravar() {
        if (ADICIONAR_STATE.equals(this.currentState)) {
            logger.debug("ADICIONAR REGISTRO");
            SecurityService.getInstance().cadastraPapel(papel);
            papel.setObjetosProtegidos(objetosProtegidosDisponiveis.getTarget());
            SecurityService.getInstance().atualizarPapel(papel);
        } else if (EDITAR_STATE.equals(this.currentState)) {
            logger.debug("EDITAR REGISTRO");
            papel.setObjetosProtegidos(objetosProtegidosDisponiveis.getTarget());
            SecurityService.getInstance().atualizarPapel(papel);
        }
        this.pesquisar();
        RequestContext.getCurrentInstance().execute("PF('addEditPopup').hide();");
    }

    public DualListModel<ObjetoProtegidoBean> getObjetosProtegidosDisponiveis() {
        return objetosProtegidosDisponiveis;
    }

    public void setObjetosProtegidosDisponiveis(DualListModel<ObjetoProtegidoBean> objetosProtegidosDisponiveis) {
        this.objetosProtegidosDisponiveis = objetosProtegidosDisponiveis;
    }
}
