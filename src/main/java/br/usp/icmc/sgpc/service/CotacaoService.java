/*
 * Sistema de Gestão de Projetos Científicos - SGPC
 * 
 * 
 * Copyright  (C)  2010-2014  Instituto  de  Ciências  Matemáticas  e  de
 *                            Computação - ICMC/USP
 * 
 * 
 * This program is  free software; you can redistribute  it and/or modify
 * it under the  terms of the GNU General Public  License as published by
 * the Free Software Foundation; either  version 2 of the License, or (at
 * your option) any later version.
 * 
 * This program  is distributed in the  hope that it will  be useful, but
 * WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
 * MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
 * General Public License for more details.
 * 
 * You  should have received  a copy  of the  GNU General  Public License
 * along  with  this  program;  if   not,  write  to  the  Free  Software
 * Foundation,  Inc.,  51  Franklin   Street,  Fifth  Floor,  Boston,  MA
 * 02110-1301, USA.
 * 
 * 
 * EXCEPTION TO THE TERMS AND CONDITIONS OF GNU GENERAL PUBLIC LICENSE
 * 
 * The  LICENSE  file may  contain  an  additional  clause as  a  special
 * exception  to the  terms  and  conditions of  the  GNU General  Public
 * License. This  clause, if  present, gives you  the permission  to link
 * this  program with  certain third-part  software and  to obtain,  as a
 * result, a  work based  on this program  that can be  distributed using
 * other license than the GNU General Public License.  If you modify this
 * program, you may extend this exception to your version of the program,
 * but you  are not obligated  to do so.   If you do  not wish to  do so,
 * delete this exception statement from your version.
 * 
 */

package br.usp.icmc.sgpc.service;

import br.usp.icmc.sgpc.beans.CategoriaFornecedorBean;
import br.usp.icmc.sgpc.beans.CotacaoBean;
import br.usp.icmc.sgpc.beans.ItemBean;
import br.usp.icmc.sgpc.beans.PessoaBean;
import br.usp.icmc.sgpc.beans.SolicitaOrcamentoBean;
import br.usp.icmc.sgpc.beans.SolicitacaoCotacaoBean;
import br.usp.icmc.sgpc.fmw.ApplicationLog;
import br.usp.icmc.sgpc.fmw.Base64Coder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author herick
 */
public class CotacaoService {

    private static CotacaoService instance = new CotacaoService();
    Logger logger = Logger.getLogger(this.getClass());

    private CotacaoService() {
    }

    public static CotacaoService getInstance() {
        return instance;
    }

    public void enviaCotacaoFinanceiro(CotacaoBean cotacao) {
        logger.debug("----> Enviar Cotacao Financeiro");
        cotacao.setStatus("Em Andamento");
        Service.getInstance().atualizarCotacao(cotacao);
        //Enviar e-mail financeiro avisando da alteracao de status da cotação e para análise da mesma

        String subject = "Solicitação de cotação - SGPC";
        StringBuilder recipient = new StringBuilder();
        StringBuilder message = null;

        List<PessoaBean> listaPessoasFinanceiro = Service.getInstance().pesquisarPessoasFinanceiro();
        logger.debug("Quantidade de pessoas na lista do financeiro:" + listaPessoasFinanceiro.size());

        recipient.append("herick.marques@gmail.com");
        recipient.append(",");
        recipient.append("talarico@icmc.usp.br");
        /* for (PessoaBean pessoa : listaPessoasFinanceiro) {
        recipient.append(",");
        recipient.append(pessoa.getEmail());
        }*/

        message = new StringBuilder();

        message.append("Foi solicitado um pedido de cotação.");
        message.append("\n");
        message.append("----------------------------------------------------------------------\n");
        message.append("Projeto: ").append(cotacao.getFkProjeto().getTitulo());
        message.append("\n");
        message.append("Responsável: ").append(cotacao.getFkProjeto().getFkResponsavel().getNome());
        message.append("\n");
        message.append("----------------------------------------------------------------------\n");


        MailService.getInstance().sendMail(subject, recipient.toString(), message.toString());
    }

    public void solicitaCotacaoFornecedores(ItemBean item, SolicitacaoCotacaoBean solicitacao) {
        logger.debug("----> Enviar Cotacao Fornecedor");
        //Montar a solicitação para envio aos fornecedores
        CotacaoBean cotacao = item.getFkCotacao();
        cotacao.setStatus("Para Cotacao");
        Service.getInstance().atualizarCotacao(cotacao);

        solicitacao.setFkItem(item);
        solicitacao.setFkGrupoFornecimento(item.getFkProduto().getFkGrupoFornecimento());
        item.setStatus("Solicitação Enviada");
        item.getSolicitacoesCotacoes().add(solicitacao);
        Service.getInstance().atualizarItem(item);

        item = Service.getInstance().buscarItem(item.getId());
        solicitacao = item.getSolicitacoesCotacoes().get(item.getSolicitacoesCotacoes().size() - 1);
        solicitarCotacao(solicitacao);
    }

    private void solicitarCotacao(SolicitacaoCotacaoBean solicitacao) {

        List<CategoriaFornecedorBean> listaFornecedores = solicitacao.getFkGrupoFornecimento().getFornecedores();
        String subject = "Solicitação de Orçamento - SGPC";

        for (int i = 0; i < listaFornecedores.size(); i++) {
            //Verificar se fornecedor está ativo
            if (listaFornecedores.get(i).getFkFornecedor().getStatus().equals("Ativo")) {
                StringBuilder recipient = new StringBuilder();
                StringBuilder message = new StringBuilder();
                String email = "herick.marques@gmail.com";
                recipient.append("herick.marques@gmail.com");
                recipient.append(",");
                recipient.append("talarico@icmc.usp.br");
//                if (listaFornecedores.get(i).getFkRepresentante() != null) {
//                    email = listaFornecedores.get(i).getFkRepresentante().getEmail();
//                } else {
//                    email = listaFornecedores.get(i).getFkFornecedor().getEmail();
//                }

                SolicitaOrcamentoBean solOrcamento = new SolicitaOrcamentoBean();
                solOrcamento.setFkFornecedor(listaFornecedores.get(i).getFkFornecedor());
                solOrcamento.setFkSolicitacaoCotacao(solicitacao);
                String chave = gerarChave(solicitacao.getFkItem().getId() + System.currentTimeMillis() + email);
                solOrcamento.setChaveAcesso(chave);

                Service.getInstance().cadastrarSolicitaOrcamento(solOrcamento);
                //Enviar e-mail para Fornecedor ou Representante aviso sobre cotação
                logger.debug("-----> Fornecedor: " + listaFornecedores.get(i).getFkFornecedor().getRazaoSocial() + " -- Item para orçamento: " + solicitacao.getFkItem().getFkProduto().getDescricao());

                message.append("Foi solicitado um pedido de orçamento.");
                message.append("\n");
                message.append("----------------------------------------------------------------------\n");
                message.append("Item: ").append(solicitacao.getFkItem().getFkProduto().getDescricao());
                message.append("\n");
                message.append("Quantidade: ").append(solicitacao.getFkItem().getQuantidade());
                message.append("\n");
                message.append("Unidade: ").append(solicitacao.getFkItem().getUnidade());
                message.append("\n");
                message.append("Observação: ").append(solicitacao.getFkItem().getObservacao());
                message.append("\n");
                message.append("Data Limite: ").append(solicitacao.getDataLimiteResposta());
                message.append("\n");
                message.append("\n");
                message.append("----------------------------------------------------------------------\n");
                message.append("Chave de Acesso à Licitação: ").append(chave);
                message.append("\n");
                message.append("Endereço para Acesso: ").append("http://143.107.183.168:8080/sgpcteste/faces/licitacao.xhtml");
                message.append("\n");
                message.append("----------------------------------------------------------------------\n");
                MailService.getInstance().sendMail(subject, recipient.toString(), message.toString());
            }
        }
    }

    public synchronized String gerarChave(String plaintext) {
        MessageDigest md = null;
        try {
            md = MessageDigest.getInstance("MD5"); //step 2
        } catch (NoSuchAlgorithmException e) {
            e.getMessage();
        }
        try {
            md.reset();
            md.update(plaintext.getBytes("UTF-8")); //step 3
        } catch (Exception e) {
            e.getMessage();
        }
        byte raw[] = md.digest(); //step 4

        String hash = new String(Base64Coder.encode(raw));
        ApplicationLog.debug("Chave encryptada: " + hash);
        return hash; //step 6
    }
}
