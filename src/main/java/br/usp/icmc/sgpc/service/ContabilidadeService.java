/*
 * Sistema de Gestão de Projetos Científicos - SGPC
 * 
 * 
 * Copyright  (C)  2010-2014  Instituto  de  Ciências  Matemáticas  e  de
 *                            Computação - ICMC/USP
 * 
 * 
 * This program is  free software; you can redistribute  it and/or modify
 * it under the  terms of the GNU General Public  License as published by
 * the Free Software Foundation; either  version 2 of the License, or (at
 * your option) any later version.
 * 
 * This program  is distributed in the  hope that it will  be useful, but
 * WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
 * MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
 * General Public License for more details.
 * 
 * You  should have received  a copy  of the  GNU General  Public License
 * along  with  this  program;  if   not,  write  to  the  Free  Software
 * Foundation,  Inc.,  51  Franklin   Street,  Fifth  Floor,  Boston,  MA
 * 02110-1301, USA.
 * 
 * 
 * EXCEPTION TO THE TERMS AND CONDITIONS OF GNU GENERAL PUBLIC LICENSE
 * 
 * The  LICENSE  file may  contain  an  additional  clause as  a  special
 * exception  to the  terms  and  conditions of  the  GNU General  Public
 * License. This  clause, if  present, gives you  the permission  to link
 * this  program with  certain third-part  software and  to obtain,  as a
 * result, a  work based  on this program  that can be  distributed using
 * other license than the GNU General Public License.  If you modify this
 * program, you may extend this exception to your version of the program,
 * but you  are not obligated  to do so.   If you do  not wish to  do so,
 * delete this exception statement from your version.
 * 
 */

package br.usp.icmc.sgpc.service;

import br.usp.icmc.sgpc.beans.AlineaBean;
import br.usp.icmc.sgpc.beans.AuxilioBean;
import br.usp.icmc.sgpc.beans.DespesaBean;
import br.usp.icmc.sgpc.beans.LancamentoContaCorrenteBean;
import br.usp.icmc.sgpc.beans.ProjetoBean;
import br.usp.icmc.sgpc.beans.SubcentroDespesasBean;
import br.usp.icmc.sgpc.dao.DespesaJpaDao;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author herick
 */
public class ContabilidadeService {

    private static ContabilidadeService instance = new ContabilidadeService();
    Logger logger = Logger.getLogger(this.getClass());

    private ContabilidadeService() {
    }

    public static ContabilidadeService getInstance() {
        return instance;
    }

    public BigDecimal verbaAprovadaProjeto(ProjetoBean projeto, int moeda) {
        BigDecimal result = new BigDecimal(0);
        List<AuxilioBean> auxilios = projeto.getAuxilios();
        for(AuxilioBean a : auxilios) {
            result = result.add(verbaAprovadaAuxilio(a,moeda));
        }
        return result;
    }

    public BigDecimal totalDespesaProjeto(ProjetoBean projeto, int moeda) {
        BigDecimal result = new BigDecimal(0);
        List<AuxilioBean> auxilios = projeto.getAuxilios();
        for (AuxilioBean a : auxilios){
            result = result.add(totalDespesaAuxilio(a, moeda));
        }
        return result;
    }

    public BigDecimal saldoProjeto(ProjetoBean projeto, int moeda) {
        return verbaAprovadaProjeto(projeto,moeda).subtract(totalDespesaProjeto(projeto,moeda));
    }

    public BigDecimal verbaAprovadaAuxilio(AuxilioBean auxilio, int moeda) {
        BigDecimal result = new BigDecimal(0);
        List<AlineaBean> alineas = auxilio.getAlineas();
        for(AlineaBean a : alineas){
            if(moeda == a.getMoeda()) result = result.add(a.getVerbaAprovada());
        }
        return result;
    }

    public BigDecimal totalDespesaAuxilio(AuxilioBean auxilio, int moeda) {
        BigDecimal result = new BigDecimal(0);
        List<AlineaBean> alineas = auxilio.getAlineas();
        for (AlineaBean alinea : alineas){
            if(alinea.getMoeda() == moeda){
                List<DespesaBean> despesas = alinea.getDespesas();
                if(despesas != null){
                    for (int j = 0; j < despesas.size(); j++) {
                        result = result.add(despesas.get(j).getValor());
                    }
                }
            }
        }
        return result;
    }

    public BigDecimal saldoAuxilio(AuxilioBean auxilio, int moeda) {
        return verbaAprovadaAuxilio(auxilio,moeda).subtract(totalDespesaAuxilio(auxilio,moeda));
    }

    public BigDecimal totalDespesaAlinea(AlineaBean alinea){
        BigDecimal result = new BigDecimal(0);
        List<DespesaBean> despesas = alinea.getDespesas();
        if(despesas != null){
            for(int i=0; i<despesas.size(); i++){
                DespesaBean desp = despesas.get(i);
                result = result.add(desp.getValor());
            }
        }
        return result;
    }

    public BigDecimal saldoAlinea(AlineaBean alinea) {
        if(alinea == null) return new BigDecimal(0);
        return alinea.getVerbaAprovada().subtract(totalDespesaAlinea(alinea));
    }

    public List<LancamentoContaCorrenteBean> getLancamentosContaCorrente(AuxilioBean auxilio){
        List<LancamentoContaCorrenteBean> listaRetorno = new ArrayList<LancamentoContaCorrenteBean>();
        List<AlineaBean> alineas = auxilio.getAlineas();
        List<DespesaBean> despesas = new ArrayList<DespesaBean>();
        LancamentoContaCorrenteBean lancamento;
        BigDecimal valorTotal = new BigDecimal(0);
        for(AlineaBean alinea : alineas){
            //despesas.addAll(alinea.getDespesas());
            valorTotal = valorTotal.add(alinea.getVerbaAprovada());
            despesas = alinea.getDespesas();
            if(despesas != null){
                for(DespesaBean despesa : despesas){
                    lancamento = new LancamentoContaCorrenteBean();
                    lancamento.setData(despesa.getDataRealizada());
                    lancamento.setDebito(despesa.getValor().doubleValue());
                    lancamento.setDescricao(despesa.getObservacao());

                    listaRetorno.add(lancamento);
                }
            }
        }
        lancamento = new LancamentoContaCorrenteBean();
        lancamento.setData(auxilio.getDataInicial());
        lancamento.setCredito(valorTotal.doubleValue());
        lancamento.setDescricao("Concessão");
        lancamento.setSaldoAnterior(0);
        listaRetorno.add(lancamento);

        Collections.sort (listaRetorno, new Comparator() {
            public int compare(Object o1, Object o2) {
                LancamentoContaCorrenteBean p1 = (LancamentoContaCorrenteBean) o1;
                LancamentoContaCorrenteBean p2 = (LancamentoContaCorrenteBean) o2;
                if(p1!=null && p2!=null && p1.getData()!=null && p2.getData()!=null){
                    return p1.getData().before(p2.getData()) ? -1 : p1.getData().after(p2.getData()) ? +1 : 0;
                }else {
                    return 0;
                }
            }
        });

        double saldoTemp = listaRetorno.get(0).getSaldoAnterior();
        for(LancamentoContaCorrenteBean lancamentoTemp : listaRetorno ){
            lancamentoTemp.setSaldoAnterior(saldoTemp);
            saldoTemp += lancamentoTemp.getCredito();
            saldoTemp -= lancamentoTemp.getDebito();
            lancamentoTemp.setSaldoAtual(saldoTemp);
        }

        return listaRetorno;
    }
    
    public BigDecimal totalDespesaSubcentroPorAlinea(SubcentroDespesasBean subcentro) {
        return new DespesaJpaDao().totalDespesaSubcentroPorAlinea(subcentro);
    }

    public BigDecimal saldoSubcentro(SubcentroDespesasBean subcentro) {
        if(subcentro == null) return new BigDecimal(0);
        if(subcentro.getValor() == null) subcentro.setValor(new BigDecimal(0));
        BigDecimal totalDespesas = totalDespesaSubcentroPorAlinea(subcentro);
        if(totalDespesas == null) totalDespesas = new BigDecimal(0);
        return subcentro.getValor().subtract(totalDespesas);
    }
}
