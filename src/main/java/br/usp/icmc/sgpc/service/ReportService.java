/*
 * Sistema de Gestão de Projetos Científicos - SGPC
 * 
 * 
 * Copyright  (C)  2010-2014  Instituto  de  Ciências  Matemáticas  e  de
 *                            Computação - ICMC/USP
 * 
 * 
 * This program is  free software; you can redistribute  it and/or modify
 * it under the  terms of the GNU General Public  License as published by
 * the Free Software Foundation; either  version 2 of the License, or (at
 * your option) any later version.
 * 
 * This program  is distributed in the  hope that it will  be useful, but
 * WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
 * MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
 * General Public License for more details.
 * 
 * You  should have received  a copy  of the  GNU General  Public License
 * along  with  this  program;  if   not,  write  to  the  Free  Software
 * Foundation,  Inc.,  51  Franklin   Street,  Fifth  Floor,  Boston,  MA
 * 02110-1301, USA.
 * 
 * 
 * EXCEPTION TO THE TERMS AND CONDITIONS OF GNU GENERAL PUBLIC LICENSE
 * 
 * The  LICENSE  file may  contain  an  additional  clause as  a  special
 * exception  to the  terms  and  conditions of  the  GNU General  Public
 * License. This  clause, if  present, gives you  the permission  to link
 * this  program with  certain third-part  software and  to obtain,  as a
 * result, a  work based  on this program  that can be  distributed using
 * other license than the GNU General Public License.  If you modify this
 * program, you may extend this exception to your version of the program,
 * but you  are not obligated  to do so.   If you do  not wish to  do so,
 * delete this exception statement from your version.
 * 
 */

package br.usp.icmc.sgpc.service;

import br.usp.icmc.sgpc.beans.*;
import br.usp.icmc.sgpc.security.ConfigConstantes;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.*;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JExcelApiExporterParameter;
import net.sf.jasperreports.engine.export.JRRtfExporter;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import net.sf.jasperreports.engine.export.JRXlsExporterParameter;
import net.sf.jasperreports.engine.type.WhenNoDataTypeEnum;
import net.sf.jasperreports.engine.util.JRLoader;
import org.apache.log4j.Logger;

/**
 *
 * @author Artur
 */
public class ReportService {

    private static ReportService instance = new ReportService();
    Logger logger = Logger.getLogger(this.getClass());
    public static final int FORMATO_PDF = 1;
    public static final int FORMATO_XLS = 2;
    public static final int FORMATO_RTF = 3;

    private ReportService() {
    }

    public static ReportService getInstance() {
        return instance;
    }

    public InputStream emiteRelatorioProjetos(List<ProjetoBean> listaProjetos, int formato, List<String> filtrosAtivos) {
        Map parametros = new HashMap();
        parametros.put("filtrosAtivos", geraCabecalhoFiltros(filtrosAtivos));
        return this.emiteRelatorio(listaProjetos, "listaProjetos.jasper", parametros, formato);
    }

    public InputStream emiteRelatorioAuxilios(List<AuxilioBean> listaAuxilios, int formato, List<String> filtrosAtivos) {
        Map parametros = new HashMap();
        parametros.put("filtrosAtivos", geraCabecalhoFiltros(filtrosAtivos));
        return this.emiteRelatorio(listaAuxilios, "listaAuxilios2.jasper", parametros, formato);
    }

    public InputStream emiteRelatorioPrestacao(List<PrestacaoContasBean> listaPrestacaoContas, int formato, List<String> filtrosAtivos) {
        Map parametros = new HashMap();
        parametros.put("filtrosAtivos", geraCabecalhoFiltros(filtrosAtivos));
        return this.emiteRelatorio(listaPrestacaoContas, "listaPrestacaoContas.jasper", parametros, formato);
    }

    public InputStream emiteRelatorioRelatorioCientifico(List<RelatorioCientificoBean> listaRelatoriosCientificos, int formato, List<String> filtrosAtivos) {
        Map parametros = new HashMap();
        parametros.put("filtrosAtivos", geraCabecalhoFiltros(filtrosAtivos));
        return this.emiteRelatorio(listaRelatoriosCientificos, "listaRelatoriosCientificos.jasper", parametros, formato);
    }

    public InputStream emiteRelatorioPessoas(List<PessoaBean> listaPessoas, int formato, List<String> filtrosAtivos) {
        Map parametros = new HashMap();
        parametros.put("filtrosAtivos", geraCabecalhoFiltros(filtrosAtivos));
        return this.emiteRelatorio(listaPessoas, "listaPessoas.jasper", parametros, formato);
    }

    private InputStream emiteRelatorio(Object objeto, String arquivoJasper, Map parametros, int formato) {
        List<Object> listaObjetos = new ArrayList<Object>();
        listaObjetos.add(objeto);
        return emiteRelatorio(listaObjetos, arquivoJasper, parametros, formato);
    }

    private InputStream emiteRelatorio(List listaObjetos, String arquivoJasper, Map parametros, int formato) {
        arquivoJasper = "../reports/" + arquivoJasper;
        JasperReport jr;
        JasperPrint jasperPrint = null;
        InputStream relatorio = null;

        //Object[] registros = listaObjetos.toArray(new Object[listaObjetos.size()]);
        //JRBeanArrayDataSource ds = new JRBeanArrayDataSource(registros);
        JRDataSource ds = new JRBeanCollectionDataSource(listaObjetos);
        
        parametros.put(JRParameter.REPORT_LOCALE, new Locale("pt","BR"));

        try {
            jr = (JasperReport) JRLoader.loadObject(getClass().getClassLoader().getResourceAsStream(arquivoJasper));
            jr.setWhenNoDataType(WhenNoDataTypeEnum.ALL_SECTIONS_NO_DETAIL);

            jasperPrint = JasperFillManager.fillReport(jr, parametros, ds);

            ByteArrayOutputStream arrayOutputStream = new ByteArrayOutputStream();
            switch (formato) {
                case 1:
                    JasperExportManager.exportReportToPdfStream(jasperPrint, arrayOutputStream);
                    break;
                case 2:
                    JRXlsExporter exporterXls = new JRXlsExporter();
                    //exporterXls.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
                    //exporterXls.setParameter(JRExporterParameter.OUTPUT_FILE_NAME, "C:/jasperRepots/demo.xls");
                    //exporterXls.setParameter(JRExporterParameter.OUTPUT_STREAM, arrayOutputStream);
                    exporterXls.setParameter(JRXlsExporterParameter.JASPER_PRINT, jasperPrint);
                    exporterXls.setParameter(JRXlsExporterParameter.OUTPUT_STREAM, arrayOutputStream);
                    exporterXls.setParameter(JRXlsExporterParameter.IS_ONE_PAGE_PER_SHEET, Boolean.FALSE);
                    //exporterXls.setParameter(JRXlsExporterParameter.IS_AUTO_DETECT_CELL_TYPE, Boolean.TRUE);
                    exporterXls.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.FALSE);
                    exporterXls.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
                    exporterXls.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE);
//                    exporterXls.setParameter(JExcelApiExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE);
                    exporterXls.exportReport();
                    break;
                case 3:
                    JRRtfExporter exporterRtf = new JRRtfExporter();
                    exporterRtf.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
                    exporterRtf.setParameter(JRExporterParameter.OUTPUT_STREAM, arrayOutputStream);
                    exporterRtf.exportReport();
                    break;
                default:
                    JasperExportManager.exportReportToPdfStream(jasperPrint, arrayOutputStream);
                    break;
            }

            relatorio = new ByteArrayInputStream(arrayOutputStream.toByteArray());

        } catch (JRException ex) {
            logger.debug(ex);
        }
        return relatorio;
    }

    public InputStream emiteRelatorioInstituicoesBancarias(List<InstituicaoBancariaBean> listaInstituicoes, int formato) {
        Map parametros = new HashMap();
        return this.emiteRelatorio(listaInstituicoes, "listaInstituicoesBancarias.jasper", parametros, formato);
    }

    public InputStream emiteRelatorioInstituicoes(List<InstituicaoBean> listaInstituicoes, int formato) {
        Map parametros = new HashMap();
        return this.emiteRelatorio(listaInstituicoes, "listaInstituicoes.jasper", parametros, formato);
    }

    public InputStream emiteRelatorioDespesas(List<DespesaBean> listaDespesas, int formato, List<String> filtrosAtivos) {
        Map parametros = new HashMap();
        parametros.put("filtrosAtivos", geraCabecalhoFiltros(filtrosAtivos));
        return this.emiteRelatorio(listaDespesas, "listaDespesas.jasper", parametros, formato);
    }

    public InputStream emiteRelatorioFinanciadores(List<FinanciadorBean> listaFinanciadores, int formato) {
        Map parametros = new HashMap();
        return this.emiteRelatorio(listaFinanciadores, "listaFinanciadores.jasper", parametros, formato);
    }

    public InputStream emiteRelatorioCategoriasAlineas(List<CategoriaAlineaBean> listaCategoriasAlineas, int formato) {
        Map parametros = new HashMap();
        return this.emiteRelatorio(listaCategoriasAlineas, "listaAlineas.jasper", parametros, formato);
    }

    public InputStream emiteRelatorioCentrosDespesas(List<CentroDespesaBean> listaCentrosDespesas, int formato, List<String> filtrosAtivos) {
        Map parametros = new HashMap();
        parametros.put("filtrosAtivos", geraCabecalhoFiltros(filtrosAtivos));
        return this.emiteRelatorio(listaCentrosDespesas, "listaCentrosDespesa.jasper", parametros, formato);
    }

    public InputStream emiteBalancete(AuxilioBean auxilio, int formato) {
        Map parametros = new HashMap();
        return this.emiteRelatorio(auxilio, "balancetePrestacaoContas.jasper", parametros, formato);
    }

    public InputStream emiteRelatorioProgramasEspeciais(List<ProgramaEspecialFinanciamentoBean> listaProgramasEspeciais, int formato) {
        Map parametros = new HashMap();
        return this.emiteRelatorio(listaProgramasEspeciais, "listaProgramasEspeciais.jasper", parametros, formato);
    }


    public InputStream emiteRelatorioReciboDespesa(ReciboBean r, int formato) {
        ConfiguracaoBean corpoTextoRecibo = ConfiguracaoService.getInstance().getConfiguracao(ConfigConstantes.CONFIG_RECIBO_TEXTO);
        DecimalFormat df = new DecimalFormat("#,###,##0.00", new DecimalFormatSymbols(new Locale("pt", "BR")));
        df.setParseBigDecimal(true);
        df.setRoundingMode(RoundingMode.DOWN);

        String textoRecibo = corpoTextoRecibo.getValor();
        textoRecibo = textoRecibo.replaceAll("NOME_PESSOA", r.getFkDespesa().getFavorecidoNome());
        textoRecibo = textoRecibo.replaceAll("CPF_PESSOA", r.getFkDespesa().getFavorecidoCpf());
        textoRecibo = textoRecibo.replaceAll("CPF_PESSOA", r.getFkDespesa().getFavorecidoCpf());
        textoRecibo = textoRecibo.replaceAll("VALOR_DESPESA", df.format(r.getValor()));
        textoRecibo = textoRecibo.replaceAll("QTD_DIARIAS", r.getFkDespesa().getNumeroDiarias().toString());
        Map parametros = new HashMap();
        parametros.put("textoRecibo", textoRecibo);

        //return this.emiteRelatorio(r, "reciboDiaria.jasper", parametros, formato);
        return this.emiteRelatorio(r, "teste_sistema.jasper", parametros, formato);
    }
    
    private String geraCabecalhoFiltros(List<String> filtrosAtivos){
        if(filtrosAtivos!= null && !filtrosAtivos.isEmpty()){
            StringBuilder str = new StringBuilder();
            str.append("Filtros ativos:\n");
            for(String filtro : filtrosAtivos){
                str.append(filtro);
                str.append("\n");
            }
            return str.toString();
        }else{
            return null;
        }
    }
    
//    public InputStream emitePrestacaoDeContas(PrestacaoContasBean prestacao, int formato){
//        JasperReport jasperReport;
//        InputStream relatorio = null;
//        
//        Map parameters = new HashMap();
//        javax.persistence.EntityManager entityManager = new DespesaJpaDao().getEntityManager();
//        parameters.put(JRJpaQueryExecuterFactory.PARAMETER_JPA_ENTITY_MANAGER,entityManager);
//        
//        Date dataInicial = Service.getInstance().calcularDataInicialPrestacao(prestacao);
//        Date dataFinal = prestacao.getDataLimite();
//        List<DespesaBean> lista = Service.getInstance().pesquisar(prestacao.getFkAuxilio(), dataInicial, dataFinal);
//        
//        try{        
//            jasperReport = (JasperReport) JRLoader.loadObject(getClass().getClassLoader().getResourceAsStream("../reports/relatorio_prestacao_contas.jasper"));
//            JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters);
//            ByteArrayOutputStream arrayOutputStream = new ByteArrayOutputStream();
//            switch (formato) {
//                case 1:
//                    JasperExportManager.exportReportToPdfStream(jasperPrint, arrayOutputStream);
//                    break;
//                case 2:
//                    JRXlsExporter exporterXls = new JRXlsExporter();
//                    exporterXls.setParameter(JRXlsExporterParameter.JASPER_PRINT, jasperPrint);
//                    exporterXls.setParameter(JRXlsExporterParameter.OUTPUT_STREAM, arrayOutputStream);
//                    exporterXls.setParameter(JRXlsExporterParameter.IS_ONE_PAGE_PER_SHEET, Boolean.FALSE);
//                    exporterXls.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.FALSE);
//                    exporterXls.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
//                    exporterXls.exportReport();
//                    break;
//                case 3:
//                    JRRtfExporter exporterRtf = new JRRtfExporter();
//                    exporterRtf.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
//                    exporterRtf.setParameter(JRExporterParameter.OUTPUT_STREAM, arrayOutputStream);
//                    exporterRtf.exportReport();
//                    break;
//                default:
//                    JasperExportManager.exportReportToPdfStream(jasperPrint, arrayOutputStream);
//                    break;
//            }
//            relatorio = new ByteArrayInputStream(arrayOutputStream.toByteArray());
//        } catch (JRException ex) {
//            logger.debug(ex);
//        }
//
//        return relatorio;
//    }
    
    public InputStream emitePrestacaoDeContas(List<DespesaBean> listaDespesas, Date dataInicial, Date dataFinal, int formato) {
        Map parametros = new HashMap();
        parametros.put("dataInicial", dataInicial);
        parametros.put("dataFinal", dataFinal);

        ModeloRelatorioPrestacaoContasBean modelo = null;
        if(!listaDespesas.isEmpty()){
            modelo = listaDespesas.get(0).getFkAlinea().getFkAuxilio().getFkModalidade().getFkTipoAuxilio().getFkFinanciador().getFkModeloRelatorio();
        }
        if(modelo == null){
            modelo = Service.getInstance().getModeloRelatorioDefault();
        }
        String arquivo = "relatorio_prestacao_contas_FAPESP";
        if(modelo!=null){
            arquivo = modelo.getArquivo();
        }
        arquivo = arquivo+".jasper";
        return this.emiteRelatorio(listaDespesas, arquivo, parametros, formato);

    }
    
    public InputStream emiteRelatorioDespesasSubcentro(SubcentroDespesasBean subcentro, int formato) {
        Map parametros = new HashMap();
        return this.emiteRelatorio(subcentro, "relatorio_despesas_subcentro.jasper", parametros, formato);
    }
    
    public InputStream emiteRelatorioHistoricoAuxilio(List<HistoricoAuxilioBean> listaHistorico, int formato, List<String> filtrosAtivos) {
        Map parametros = new HashMap();
        parametros.put("filtrosAtivos", geraCabecalhoFiltros(filtrosAtivos));
        return this.emiteRelatorio(listaHistorico, "relatorio_historico_auxilio.jasper", parametros, formato);
    }
    
    public InputStream emiteRelatorioCheques(List<DocumentoComprovacaoPagamentoBean> listaCheques, int formato, List<String> filtrosAtivos) {
        Map parametros = new HashMap();
        parametros.put("filtrosAtivos", geraCabecalhoFiltros(filtrosAtivos));
        return this.emiteRelatorio(listaCheques, "listaCheques.jasper", parametros, formato);
    }
}
