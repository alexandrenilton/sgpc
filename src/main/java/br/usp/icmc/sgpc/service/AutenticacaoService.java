/*
 * Sistema de Gestão de Projetos Científicos - SGPC
 * 
 * 
 * Copyright  (C)  2010-2014  Instituto  de  Ciências  Matemáticas  e  de
 *                            Computação - ICMC/USP
 * 
 * 
 * This program is  free software; you can redistribute  it and/or modify
 * it under the  terms of the GNU General Public  License as published by
 * the Free Software Foundation; either  version 2 of the License, or (at
 * your option) any later version.
 * 
 * This program  is distributed in the  hope that it will  be useful, but
 * WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
 * MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
 * General Public License for more details.
 * 
 * You  should have received  a copy  of the  GNU General  Public License
 * along  with  this  program;  if   not,  write  to  the  Free  Software
 * Foundation,  Inc.,  51  Franklin   Street,  Fifth  Floor,  Boston,  MA
 * 02110-1301, USA.
 * 
 * 
 * EXCEPTION TO THE TERMS AND CONDITIONS OF GNU GENERAL PUBLIC LICENSE
 * 
 * The  LICENSE  file may  contain  an  additional  clause as  a  special
 * exception  to the  terms  and  conditions of  the  GNU General  Public
 * License. This  clause, if  present, gives you  the permission  to link
 * this  program with  certain third-part  software and  to obtain,  as a
 * result, a  work based  on this program  that can be  distributed using
 * other license than the GNU General Public License.  If you modify this
 * program, you may extend this exception to your version of the program,
 * but you  are not obligated  to do so.   If you do  not wish to  do so,
 * delete this exception statement from your version.
 * 
 */

package br.usp.icmc.sgpc.service;

import br.usp.icmc.sgpc.beans.PessoaBean;
import br.usp.icmc.sgpc.dao.PessoaJpaDao;
import br.usp.icmc.sgpc.fmw.ApplicationLog;
import br.usp.icmc.sgpc.fmw.Base64Coder;
import br.usp.icmc.sgpc.fmw.USPException;
import br.usp.icmc.sgpc.security.ConfigConstantes;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Random;
import org.apache.log4j.Logger;

/**
 *
 * @author Artur
 */
public class AutenticacaoService {
    private static AutenticacaoService instance = new AutenticacaoService();
    Logger logger = Logger.getLogger(this.getClass());
    
    /*
     * Metodo construtor privado, para evitar
     * multipla instanciacao
     */
    private AutenticacaoService() {
    }

    /*
     * Metodo que retorna a unica instancia da classe
     */
    public static AutenticacaoService getInstance() {
        return instance;
    }

    public String encryptPassword(String input) {
        MessageDigest md = null;
        String result = input;
        if (input != null) {
            try {
                md = MessageDigest.getInstance("MD5");
                md.update(input.getBytes());
                BigInteger hash = new BigInteger(1, md.digest());
                result = hash.toString(16);
            } catch (NoSuchAlgorithmException ex) {
                logger.debug(ex);
            }
            while (result.length() < 32) {
                result = "0" + result;
            }
        }
        return result;
    }

    public PessoaBean realizaLogin(String username, String password) throws USPException {
        if ("".equals(username)) {
            logger.debug("Username n\u00e3o pode ser vazio");
            throw new USPException("Username n\u00e3o pode ser vazio");
        }
        if ("".equals(password)) {
            logger.debug("Senha n\u00e3o pode ser vazia");
            throw new USPException("Senha n\u00e3o pode ser vazia");
        }
        PessoaBean cadastrada = new PessoaJpaDao().findByUsername(username);
        String encryptedPassword = encryptPassword(password);
        if (encryptedPassword.equals(cadastrada.getPassword())) {
            logger.debug("Login realizado com sucesso");
            AuditoriaService.getInstance().gravarAcaoUsuario(cadastrada, "Login no Sistema. Autentica\u00e7\u00e3o local bem sucedida", "Login", ConfigConstantes.CONFIG_AUDITORIA_LOGIN);
        } else {
            AuditoriaService.getInstance().gravarAcaoUsuario(cadastrada, "Erro no Login.", "Login", "Senha incorreta.", ConfigConstantes.CONFIG_AUDITORIA_ERRO_LOGIN);
            throw new USPException("Senha inv\u00e1lida");
        }
        return cadastrada;
    }
    /*
     * Gera senha randômica da combinação de números e caracteres
     * maíusculas e minúsculas.
     * Parâmetro de passagem é o tamanho da senha a ser gerada
     */

    public String getRandomPassword(int length) {
        char[] ALL_CHARS = new char[62];
        Random RANDOM = new Random();
        for (int i = 48, j = 0; i < 123; i++) {
            if (Character.isLetterOrDigit(i)) {
                ALL_CHARS[j] = (char) i;
                j++;
            }
        }
        char[] result = new char[length];
        for (int i = 0; i < length; i++) {
            result[i] = ALL_CHARS[RANDOM.nextInt(ALL_CHARS.length)];
        }
        return new String(result);
    }
    
}
