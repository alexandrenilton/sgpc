/*
 * Sistema de Gestão de Projetos Científicos - SGPC
 * 
 * 
 * Copyright  (C)  2010-2014  Instituto  de  Ciências  Matemáticas  e  de
 *                            Computação - ICMC/USP
 * 
 * 
 * This program is  free software; you can redistribute  it and/or modify
 * it under the  terms of the GNU General Public  License as published by
 * the Free Software Foundation; either  version 2 of the License, or (at
 * your option) any later version.
 * 
 * This program  is distributed in the  hope that it will  be useful, but
 * WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
 * MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
 * General Public License for more details.
 * 
 * You  should have received  a copy  of the  GNU General  Public License
 * along  with  this  program;  if   not,  write  to  the  Free  Software
 * Foundation,  Inc.,  51  Franklin   Street,  Fifth  Floor,  Boston,  MA
 * 02110-1301, USA.
 * 
 * 
 * EXCEPTION TO THE TERMS AND CONDITIONS OF GNU GENERAL PUBLIC LICENSE
 * 
 * The  LICENSE  file may  contain  an  additional  clause as  a  special
 * exception  to the  terms  and  conditions of  the  GNU General  Public
 * License. This  clause, if  present, gives you  the permission  to link
 * this  program with  certain third-part  software and  to obtain,  as a
 * result, a  work based  on this program  that can be  distributed using
 * other license than the GNU General Public License.  If you modify this
 * program, you may extend this exception to your version of the program,
 * but you  are not obligated  to do so.   If you do  not wish to  do so,
 * delete this exception statement from your version.
 * 
 */

package br.usp.icmc.sgpc.beans;

import br.usp.icmc.sgpc.interfaces.SgpcBeanInterface;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.*;

/**
 *
 * @author Artur
 */
@Entity
@Table(name = "despesa")
@Inheritance(strategy=InheritanceType.JOINED)
@NamedQueries({
    @NamedQuery(name = "DespesaBean.findAll", query = "SELECT d FROM DespesaBean d"),
    @NamedQuery(name = "DespesaBean.findById", query = "SELECT d FROM DespesaBean d WHERE d.id = :id"),
    @NamedQuery(name = "DespesaBean.findByAuxilio", query = "SELECT d FROM DespesaBean d WHERE d.fkAlinea.fkAuxilio = :auxilio ORDER BY d.dataRealizada"),
    @NamedQuery(name = "DespesaBean.findByBolsa", query = "SELECT d FROM DespesaBean d WHERE d.fkBolsaAlocada = :bolsa"),
    @NamedQuery(name = "DespesaBean.findByDataRealizada", query = "SELECT d FROM DespesaBean d WHERE d.dataRealizada = :dataRealizada"),
    @NamedQuery(name = "DespesaBean.findByStatus", query = "SELECT d FROM DespesaBean d WHERE d.status = :status"),
    @NamedQuery(name = "DespesaBean.findByObservacao", query = "SELECT d FROM DespesaBean d WHERE d.observacao = :observacao"),
    @NamedQuery(name = "DespesaBean.findByKeyword", query = "SELECT d FROM DespesaBean d WHERE upper(d.observacao) LIKE upper(:keyword)"),
    @NamedQuery(name = "DespesaBean.findByOwner", query = "SELECT d FROM DespesaBean d WHERE d.fkAlinea.fkAuxilio.fkProjeto.fkResponsavel = :responsavel"),
    @NamedQuery(name = "DespesaBean.findByValor", query = "SELECT d FROM DespesaBean d WHERE d.valor = :valor"),
    @NamedQuery(name = "DespesaBean.relatorioPrestacaoContas", query = "SELECT d FROM DespesaBean d, DocumentoComprovacaoDespesaBean dcd WHERE d.fkAlinea.fkAuxilio = :auxilio AND dcd.fkDespesa = d AND dcd.dataEmissao >= :dataInicialPesquisa AND dcd.dataEmissao <= :dataFinalPesquisa ORDER BY d.tipoDespesa, d.fkAlinea.fkCategoriaAlinea.nome, d.subalinea, dcd.dataEmissao")})
public class DespesaBean implements Serializable, SgpcBeanInterface {
    public static final int GENERO_NOTA_FISCAL = 0;
    public static final int GENERO_RECIBO = 1;
    public static final int GENERO_DIARIA = 2;
    public static final int GENERO_RECOLHIMENTO = 3;

    protected static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @SequenceGenerator(name="id", sequenceName="despesa_id_despesa_seq")
    @GeneratedValue (strategy=GenerationType.AUTO, generator="id")
    @Column(name = "id_despesa", nullable = false)
    protected Integer id;

    @Column(name = "data_realizada")
    @Temporal(TemporalType.DATE)
    protected Date dataRealizada;

    @Basic(optional = false)
    @Column(name = "status", nullable = false, length = 20)
    protected String status;

    @Column(name = "observacao", length = 1000)
    protected String observacao;

    @Basic(optional = false)
    @Column(name = "valor", nullable = false)
    protected BigDecimal valor;

    @OneToMany(mappedBy = "fkDespesa", fetch = FetchType.LAZY, orphanRemoval = true)
    private List<ItemBean> itens;

    @OneToMany(mappedBy = "fkDespesa", fetch = FetchType.LAZY, orphanRemoval = true)
    private List<ChequeBean> cheques;

    @JoinColumn(name = "fk_alinea", referencedColumnName = "id_alinea", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    protected AlineaBean fkAlinea;

    @JoinColumn(name = "fk_prestacao_contas", referencedColumnName = "id_prestacao_contas", nullable = true)
    @ManyToOne(optional = true, fetch = FetchType.LAZY)
    protected PrestacaoContasBean fkPrestacaoContas;

    @OneToMany(mappedBy = "fkDespesa", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    @OrderBy("dataEmissao, numero")
    private List<DocumentoComprovacaoDespesaBean> documentosComprovacaoDespesas;

    @OneToMany(mappedBy = "fkDespesa", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    @OrderBy("dataEmissao, numero")
    private List<DocumentoComprovacaoPagamentoBean> documentosComprovacaoPagamentos;


    @OneToMany(cascade = CascadeType.ALL, mappedBy = "fkDespesa", fetch = FetchType.LAZY, orphanRemoval = true)
    private List<BemPatrimoniadoBean> bensPatrimoniados;

    @Basic(optional = false)
    @Column(name = "tipo_despesa", nullable = false)
    protected String tipoDespesa;

    @JoinColumn(name = "fk_bolsa_alocada", referencedColumnName = "id_bolsa_alocada", nullable = true)
    @ManyToOne(optional = true, fetch = FetchType.LAZY)
    protected BolsaAlocadaBean fkBolsaAlocada;

    @JoinColumn(name = "fk_fornecedor", referencedColumnName = "id_fornecedor", nullable = true)
    @ManyToOne(optional = true, fetch = FetchType.LAZY)
    private FornecedorBean fkFornecedor;

    @Column(name = "descricao", length = 1000)
    protected String descricao;

    @Basic(optional = false)
    @Column(name = "genero", nullable = false)
    private int genero;

    @ElementCollection
    @CollectionTable(name = "arquivo_anexo_despesa", joinColumns =
    @JoinColumn(name = "fk_despesa"))
    @OrderColumn
    private List<ArquivoAnexoDespesaBean> arquivosAnexos = new ArrayList<ArquivoAnexoDespesaBean>();

    @Column(name = "data_inicio_periodo")
    @Temporal(TemporalType.DATE)
    protected Date dataInicioPeriodo;

    @Column(name = "data_fim_periodo")
    @Temporal(TemporalType.DATE)
    protected Date dataFimPeriodo;

    @Column(name = "numero_diarias")
    private Integer numeroDiarias;

    @Column(name = "favorecido_nome", length = 1000)
    protected String favorecidoNome;

    @Column(name = "favorecido_cpf", length = 1000)
    protected String favorecidoCpf;

    @Column(name = "data_liberacao_pagamento")
    @Temporal(TemporalType.DATE)
    protected Date dataLiberacaoPagamento;
    
    @Column(name = "subalinea", length = 100)
    protected String subalinea;
    
    
    @JoinColumn(name = "fk_subcentro", referencedColumnName = "id_subcentrodespesas", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    protected SubcentroDespesasBean fkSubcentro;
    
    @JoinColumn(name = "fk_pessoa", referencedColumnName = "id_pessoa", nullable = true)
    @ManyToOne(optional = true, fetch = FetchType.LAZY)
    private PessoaBean fkPessoa;
    
    @Column(name = "verba_liberada", nullable=false, columnDefinition="boolean default false")
    private boolean verbaLiberada;

    public DespesaBean() {
        bensPatrimoniados = new ArrayList<BemPatrimoniadoBean>();
        documentosComprovacaoDespesas = new ArrayList<DocumentoComprovacaoDespesaBean>();
        documentosComprovacaoPagamentos = new ArrayList<DocumentoComprovacaoPagamentoBean>();
        arquivosAnexos = new ArrayList<ArquivoAnexoDespesaBean>();
        valor = new BigDecimal(0);
    }

    public DespesaBean(Integer id) {
        this.id = id;
    }

    public BolsaAlocadaBean getFkBolsaAlocada() {
        return fkBolsaAlocada;
    }

    public void setFkBolsaAlocada(BolsaAlocadaBean fkBolsaAlocada) {
        this.fkBolsaAlocada = fkBolsaAlocada;
    }

    public String getTipoDespesa() {
        return tipoDespesa;
    }

    public void setTipoDespesa(String tipoDespesa) {
        this.tipoDespesa = tipoDespesa;
    }

    public List<BemPatrimoniadoBean> getBensPatrimoniados() {
        return bensPatrimoniados;
    }

    public void setBensPatrimoniados(List<BemPatrimoniadoBean> bensPatrimoniados) {
        this.bensPatrimoniados = bensPatrimoniados;
    }

    public List<DocumentoComprovacaoDespesaBean> getDocumentosComprovacaoDespesas() {
        if(documentosComprovacaoDespesas==null) documentosComprovacaoDespesas = new ArrayList<DocumentoComprovacaoDespesaBean>();
        return documentosComprovacaoDespesas;
    }

    public void setDocumentosComprovacaoDespesas(List<DocumentoComprovacaoDespesaBean> documentosComprovacaoDespesas) {
        this.documentosComprovacaoDespesas = documentosComprovacaoDespesas;
    }

    public List<DocumentoComprovacaoPagamentoBean> getDocumentosComprovacaoPagamentos() {
        if(documentosComprovacaoPagamentos==null) documentosComprovacaoPagamentos = new ArrayList<DocumentoComprovacaoPagamentoBean>();
        return documentosComprovacaoPagamentos;
    }

    public void setDocumentosComprovacaoPagamentos(List<DocumentoComprovacaoPagamentoBean> documentosComprovacaoPagamentos) {
        this.documentosComprovacaoPagamentos = documentosComprovacaoPagamentos;
    }

    public DespesaBean(Integer id, String status, BigDecimal valor) {
        this.id = id;
        this.status = status;
        this.valor = valor;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getDataRealizada() {
        return dataRealizada;
    }

    public void setDataRealizada(Date dataRealizada) {
        this.dataRealizada = dataRealizada;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public BigDecimal getValor() {
        return valor;
    }

    public void setValor(BigDecimal valor) {
        this.valor = valor;
    }

    public AlineaBean getFkAlinea() {
        return fkAlinea;
    }

    public void setFkAlinea(AlineaBean fkAlinea) {
        this.fkAlinea = fkAlinea;
    }

    public List<ChequeBean> getCheques() {
        return cheques;
    }

    public void setCheques(List<ChequeBean> cheques) {
        this.cheques = cheques;
    }

    public List<ItemBean> getItens() {
        return itens;
    }

    public void setItens(List<ItemBean> itens) {
        this.itens = itens;
    }

  /*  public DocumentoComprovacaoDespesaBean getFkComprovacaoDespesa() {
        return fkComprovacaoDespesa;
    }

    public void setFkComprovacaoDespesa(DocumentoComprovacaoDespesaBean fkComprovacaoDespesa) {
        this.fkComprovacaoDespesa = fkComprovacaoDespesa;
    }
*/
    public PrestacaoContasBean getFkPrestacaoContas() {
        return fkPrestacaoContas;
    }

    public void setFkPrestacaoContas(PrestacaoContasBean fkPrestacaoContas) {
        this.fkPrestacaoContas = fkPrestacaoContas;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DespesaBean)) {
            return false;
        }
        DespesaBean other = (DespesaBean) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.usp.icmc.sgpc.Despesa[id=" + id + "]";
    }

    public FornecedorBean getFkFornecedor() {
        return fkFornecedor;
    }

    public void setFkFornecedor(FornecedorBean fkFornecedor) {
        this.fkFornecedor = fkFornecedor;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public int getGenero() {
        return genero;
    }

    public void setGenero(int genero) {
        this.genero = genero;
    }

    public List<ArquivoAnexoDespesaBean> getArquivosAnexos() {
        return arquivosAnexos;
    }

    public void setArquivosAnexos(List<ArquivoAnexoDespesaBean> arquivosAnexos) {
        this.arquivosAnexos = arquivosAnexos;
    }

    public Date getDataFimPeriodo() {
        return dataFimPeriodo;
    }

    public void setDataFimPeriodo(Date dataFimPeriodo) {
        this.dataFimPeriodo = dataFimPeriodo;
    }

    public Date getDataInicioPeriodo() {
        return dataInicioPeriodo;
    }

    public void setDataInicioPeriodo(Date dataInicioPeriodo) {
        this.dataInicioPeriodo = dataInicioPeriodo;
    }

    public String getFavorecidoCpf() {
        return favorecidoCpf;
    }

    public void setFavorecidoCpf(String favorecidoCpf) {
        this.favorecidoCpf = favorecidoCpf;
    }

    public String getFavorecidoNome() {
        return favorecidoNome;
    }

    public void setFavorecidoNome(String favorecidoNome) {
        this.favorecidoNome = favorecidoNome;
    }

    public Integer getNumeroDiarias() {
        return numeroDiarias;
    }

    public void setNumeroDiarias(Integer numeroDiarias) {
        this.numeroDiarias = numeroDiarias;
    }

    public Date getDataLiberacaoPagamento() {
        return dataLiberacaoPagamento;
    }

    public void setDataLiberacaoPagamento(Date dataLiberacaoPagamento) {
        this.dataLiberacaoPagamento = dataLiberacaoPagamento;
    }

    public String getSubalinea() {
        return subalinea;
    }

    public void setSubalinea(String subalinea) {
        this.subalinea = subalinea;
    }

    public SubcentroDespesasBean getFkSubcentro() {
        return fkSubcentro;
    }

    public void setFkSubcentro(SubcentroDespesasBean fkSubcentro) {
        this.fkSubcentro = fkSubcentro;
    }

    public PessoaBean getFkPessoa() {
        return fkPessoa;
    }

    public void setFkPessoa(PessoaBean fkPessoa) {
        this.fkPessoa = fkPessoa;
    }

    public boolean isVerbaLiberada() {
        return verbaLiberada;
    }

    public void setVerbaLiberada(boolean verbaLiberada) {
        this.verbaLiberada = verbaLiberada;
    }

}
