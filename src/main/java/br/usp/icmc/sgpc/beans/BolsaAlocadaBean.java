/*
 * Sistema de Gestão de Projetos Científicos - SGPC
 * 
 * 
 * Copyright  (C)  2010-2014  Instituto  de  Ciências  Matemáticas  e  de
 *                            Computação - ICMC/USP
 * 
 * 
 * This program is  free software; you can redistribute  it and/or modify
 * it under the  terms of the GNU General Public  License as published by
 * the Free Software Foundation; either  version 2 of the License, or (at
 * your option) any later version.
 * 
 * This program  is distributed in the  hope that it will  be useful, but
 * WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
 * MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
 * General Public License for more details.
 * 
 * You  should have received  a copy  of the  GNU General  Public License
 * along  with  this  program;  if   not,  write  to  the  Free  Software
 * Foundation,  Inc.,  51  Franklin   Street,  Fifth  Floor,  Boston,  MA
 * 02110-1301, USA.
 * 
 * 
 * EXCEPTION TO THE TERMS AND CONDITIONS OF GNU GENERAL PUBLIC LICENSE
 * 
 * The  LICENSE  file may  contain  an  additional  clause as  a  special
 * exception  to the  terms  and  conditions of  the  GNU General  Public
 * License. This  clause, if  present, gives you  the permission  to link
 * this  program with  certain third-part  software and  to obtain,  as a
 * result, a  work based  on this program  that can be  distributed using
 * other license than the GNU General Public License.  If you modify this
 * program, you may extend this exception to your version of the program,
 * but you  are not obligated  to do so.   If you do  not wish to  do so,
 * delete this exception statement from your version.
 * 
 */

package br.usp.icmc.sgpc.beans;

import br.usp.icmc.sgpc.interfaces.SgpcBeanInterface;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author herick
 */
@Entity
@Table(name = "bolsa_alocada")
@NamedQueries({
    @NamedQuery(name = "BolsaAlocadaBean.findAll", query = "SELECT b FROM BolsaAlocadaBean b"),
    @NamedQuery(name = "BolsaAlocadaBean.findById", query = "SELECT b FROM BolsaAlocadaBean b WHERE b.id = :id")})
public class BolsaAlocadaBean implements Serializable, SgpcBeanInterface {

        private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @SequenceGenerator(name = "id", sequenceName = "bolsa_alocada_id_bolsa_alocada_seq")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "id")
    @Column(name = "id_bolsa_alocada", nullable = false)
    private Integer id;

    @Basic(optional = false)
    @Column(name = "data_inicio", nullable = false)
    @Temporal(TemporalType.DATE)
    private Date dataInicio;

    @Basic(optional = false)
    @Column(name = "data_fim", nullable = false)
    @Temporal(TemporalType.DATE)
    private Date dataFim;

    @Basic(optional = false)
    @Column(name = "valor", nullable = false)
    private BigDecimal valor;

    @JoinColumn(name = "fk_pessoa", referencedColumnName = "id_pessoa", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private PessoaBean fkPessoa;

    @JoinColumn(name = "fk_alinea", referencedColumnName = "id_alinea", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    protected AlineaBean fkAlinea;

    //Status possíveis: Vigente, Cancelada, Finalizada
    @Basic(optional = false)
    @Column(name = "status", nullable = false)
    private String status;
    
    public BolsaAlocadaBean() {
    }

    public Integer getId() {
        return id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getDataFim() {
        return dataFim;
    }

    public void setDataFim(Date dataFim) {
        this.dataFim = dataFim;
    }

    public Date getDataInicio() {
        return dataInicio;
    }

    public void setDataInicio(Date dataInicio) {
        this.dataInicio = dataInicio;
    }

    public AlineaBean getFkAlinea() {
        return fkAlinea;
    }

    public void setFkAlinea(AlineaBean fkAlinea) {
        this.fkAlinea = fkAlinea;
    }

    public PessoaBean getFkPessoa() {
        return fkPessoa;
    }

    public void setFkPessoa(PessoaBean fkPessoa) {
        this.fkPessoa = fkPessoa;
    }

    public BigDecimal getValor() {
        return valor;
    }

    public void setValor(BigDecimal valor) {
        this.valor = valor;
    }


    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BolsaAlocadaBean)) {
            return false;
        }
        BolsaAlocadaBean other = (BolsaAlocadaBean) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.usp.icmc.sgpc.BolsaAlocadaBean[id=" + id + "]";
    }

}
