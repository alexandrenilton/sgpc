/*
 * Sistema de Gestão de Projetos Científicos - SGPC
 * 
 * 
 * Copyright  (C)  2010-2014  Instituto  de  Ciências  Matemáticas  e  de
 *                            Computação - ICMC/USP
 * 
 * 
 * This program is  free software; you can redistribute  it and/or modify
 * it under the  terms of the GNU General Public  License as published by
 * the Free Software Foundation; either  version 2 of the License, or (at
 * your option) any later version.
 * 
 * This program  is distributed in the  hope that it will  be useful, but
 * WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
 * MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
 * General Public License for more details.
 * 
 * You  should have received  a copy  of the  GNU General  Public License
 * along  with  this  program;  if   not,  write  to  the  Free  Software
 * Foundation,  Inc.,  51  Franklin   Street,  Fifth  Floor,  Boston,  MA
 * 02110-1301, USA.
 * 
 * 
 * EXCEPTION TO THE TERMS AND CONDITIONS OF GNU GENERAL PUBLIC LICENSE
 * 
 * The  LICENSE  file may  contain  an  additional  clause as  a  special
 * exception  to the  terms  and  conditions of  the  GNU General  Public
 * License. This  clause, if  present, gives you  the permission  to link
 * this  program with  certain third-part  software and  to obtain,  as a
 * result, a  work based  on this program  that can be  distributed using
 * other license than the GNU General Public License.  If you modify this
 * program, you may extend this exception to your version of the program,
 * but you  are not obligated  to do so.   If you do  not wish to  do so,
 * delete this exception statement from your version.
 * 
 */

package br.usp.icmc.sgpc.beans;

import br.usp.icmc.sgpc.interfaces.SgpcBeanInterface;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 *
 * @author David
 */
@Entity
@Table(name = "fornecedor")
@NamedQueries({
    @NamedQuery(name = "FornecedorBean.findAll", query = "SELECT i FROM FornecedorBean i"),
    @NamedQuery(name = "FornecedorBean.findByKeyword", query = "SELECT i FROM FornecedorBean i WHERE upper(i.razaoSocial) LIKE upper(:keyword) OR upper(i.nomeFantasia) LIKE upper(:keyword)"
    + " OR upper(i.cnpj) LIKE upper(:keyword) OR upper(i.ie) LIKE upper(:keyword) OR upper(i.website) LIKE upper(:keyword) OR upper(i.fone) LIKE upper(:keyword) OR upper(i.fax) LIKE upper(:keyword)")})
public class FornecedorBean implements Serializable, SgpcBeanInterface {
	
	private static final long serialVersionUID = 1L;

    @Id
    @Basic(optional = false)
    @SequenceGenerator(name = "id", sequenceName = "fornecedor_id_fornecedor_seq")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "id")
    @Column(name = "id_fornecedor", nullable = false)
    private Integer id;

    @Basic(optional = false)
    @Column(name = "razao_social", nullable = false, length = 100)
    private String razaoSocial;

    @Basic(optional = false)
    @Column(name = "nome_fantasia", nullable = false, length = 100)
    private String nomeFantasia;

    @Basic(optional = true)
    @Column(name = "cnpj", nullable = true, length = 30)
    private String cnpj;

    @Basic(optional = true)
    @Column(name = "ie", nullable = true, length = 30)
    private String ie;

    @Basic(optional = false)
    @Column(name = "website", length = 100)
    private String website;

    @Basic(optional = false)
    @Column(name = "fone", length = 50)
    private String fone;

    @Basic(optional = false)
    @Column(name = "fax", length = 50)
    private String fax;

    @OneToOne
    @JoinColumn(name = "fk_endereco", referencedColumnName = "id_endereco", nullable = true)
    private EnderecoBean fkEndereco;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_fornecedor", referencedColumnName = "id_fornecedor")
    private List<CategoriaFornecedorBean> gruposFornecimento;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_fornecedor", referencedColumnName = "id_fornecedor", nullable = false)
    private List<RepresentanteVendasBean> representantes;

    @Basic(optional = false)
    @Column(name = "status", nullable = false, length = 50)
    protected String status;

    @Basic(optional = false)
    @Column(name = "email", nullable = false, length = 50)
    protected String email;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_fornecedor", referencedColumnName = "id_fornecedor")
    private List<CategoriaFornecedorBean> categorias;

    public FornecedorBean(){
        representantes = new ArrayList<RepresentanteVendasBean>();
    }

    public List<CategoriaFornecedorBean> getCategorias() {
        return categorias;
    }

    public void setCategorias(List<CategoriaFornecedorBean> categorias) {
        this.categorias = categorias;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<RepresentanteVendasBean> getRepresentantes() {
        return representantes;
    }

    public void setRepresentantes(List<RepresentanteVendasBean> representantes) {
        this.representantes = representantes;
    }

    public List<CategoriaFornecedorBean> getGruposFornecimento() {
        return gruposFornecimento;
    }

    public void setGruposFornecimento(List<CategoriaFornecedorBean> gruposFornecimento) {
        this.gruposFornecimento = gruposFornecimento;
    }

    public EnderecoBean getFkEndereco() {
        return fkEndereco;
    }

    public void setFkEndereco(EnderecoBean fkEndereco) {
        this.fkEndereco = fkEndereco;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getRazaoSocial() {
        return razaoSocial;
    }

    public void setRazaoSocial(String razaoSocial) {
        this.razaoSocial = razaoSocial;
    }

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getFone() {
        return fone;
    }

    public void setFone(String fone) {
        this.fone = fone;
    }

    public String getIe() {
        return ie;
    }

    public void setIe(String ie) {
        this.ie = ie;
    }

    public String getNomeFantasia() {
        return nomeFantasia;
    }

    public void setNomeFantasia(String nomeFantasia) {
        this.nomeFantasia = nomeFantasia;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FornecedorBean)) {
            return false;
        }
        FornecedorBean other = (FornecedorBean) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return this.razaoSocial;
    }
     
}
