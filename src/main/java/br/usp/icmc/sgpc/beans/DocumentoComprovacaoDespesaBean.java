/*
 * Sistema de Gestão de Projetos Científicos - SGPC
 * 
 * 
 * Copyright  (C)  2010-2014  Instituto  de  Ciências  Matemáticas  e  de
 *                            Computação - ICMC/USP
 * 
 * 
 * This program is  free software; you can redistribute  it and/or modify
 * it under the  terms of the GNU General Public  License as published by
 * the Free Software Foundation; either  version 2 of the License, or (at
 * your option) any later version.
 * 
 * This program  is distributed in the  hope that it will  be useful, but
 * WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
 * MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
 * General Public License for more details.
 * 
 * You  should have received  a copy  of the  GNU General  Public License
 * along  with  this  program;  if   not,  write  to  the  Free  Software
 * Foundation,  Inc.,  51  Franklin   Street,  Fifth  Floor,  Boston,  MA
 * 02110-1301, USA.
 * 
 * 
 * EXCEPTION TO THE TERMS AND CONDITIONS OF GNU GENERAL PUBLIC LICENSE
 * 
 * The  LICENSE  file may  contain  an  additional  clause as  a  special
 * exception  to the  terms  and  conditions of  the  GNU General  Public
 * License. This  clause, if  present, gives you  the permission  to link
 * this  program with  certain third-part  software and  to obtain,  as a
 * result, a  work based  on this program  that can be  distributed using
 * other license than the GNU General Public License.  If you modify this
 * program, you may extend this exception to your version of the program,
 * but you  are not obligated  to do so.   If you do  not wish to  do so,
 * delete this exception statement from your version.
 * 
 */

package br.usp.icmc.sgpc.beans;

import br.usp.icmc.sgpc.interfaces.SgpcBeanInterface;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.*;

/**
 *
 * @author Artur
 */
@Entity
@Table(name = "documento_comprovacao_despesa")
@Inheritance(strategy=InheritanceType.JOINED)
@NamedQueries({
    @NamedQuery(name = "DocumentoComprovacaoDespesaBean.findAll", query = "SELECT d FROM DocumentoComprovacaoDespesaBean d"),
    @NamedQuery(name = "DocumentoComprovacaoDespesaBean.findById", query = "SELECT d FROM DocumentoComprovacaoDespesaBean d WHERE d.id = :id"),
    @NamedQuery(name = "DocumentoComprovacaoDespesaBean.findByNumero", query = "SELECT d FROM DocumentoComprovacaoDespesaBean d WHERE d.numero = :numero"),
    @NamedQuery(name = "DocumentoComprovacaoDespesaBean.findByDataEmissao", query = "SELECT d FROM DocumentoComprovacaoDespesaBean d WHERE d.dataEmissao = :dataEmissao"),
    @NamedQuery(name = "DocumentoComprovacaoDespesaBean.findByValor", query = "SELECT d FROM DocumentoComprovacaoDespesaBean d WHERE d.valor = :valor")})
public abstract class DocumentoComprovacaoDespesaBean implements Serializable, SgpcBeanInterface {

    protected static final int NOTA_FISCAL = 0;
    protected static final int RECIBO = 1;

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @SequenceGenerator(name="id", sequenceName="documento_comprovacao_despesa_id_documento_comprovacao_desp_seq")
    @GeneratedValue (strategy=GenerationType.AUTO, generator="id")
    @Column(name = "id_documento_comprovacao_despesa", nullable = false)
    protected Integer id;

    @Basic(optional = false)
    @Column(name = "numero", nullable = false, length = 100)
    protected String numero;

    @Basic(optional = false)
    @Column(name = "data_emissao", nullable = false)
    @Temporal(TemporalType.DATE)
    protected Date dataEmissao;

    @Basic(optional = false)
    @Column(name = "valor", nullable = false)
    protected BigDecimal valor;
    

    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    @JoinColumn(name="fk_despesa", referencedColumnName = "id_despesa", nullable = false)
    protected DespesaBean fkDespesa;

    @Basic(optional = true)
    @Column(name = "observacao", nullable = true, length = 1000)
    protected String observacao;

    @ElementCollection
    @CollectionTable(name = "arquivo_despesa", joinColumns =
    @JoinColumn(name = "fk_doc_despesa"))
    private List<ArquivoDocDespesaBean> arquivosDespesa = new ArrayList<ArquivoDocDespesaBean>();

    public List<ArquivoDocDespesaBean> getArquivosDespesa() {
        return arquivosDespesa;
    }

    public void setArquivosDespesa(List<ArquivoDocDespesaBean> arquivosDespesa) {
        this.arquivosDespesa = arquivosDespesa;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public DocumentoComprovacaoDespesaBean() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getDataEmissao() {
        return dataEmissao;
    }

    public void setDataEmissao(Date dataEmissao) {
        this.dataEmissao = dataEmissao;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public BigDecimal getValor() {
        return valor;
    }

    public void setValor(BigDecimal valor) {
        this.valor = valor;
    }

    public DespesaBean getFkDespesa() {
        return fkDespesa;
    }

    public void setFkDespesa(DespesaBean fkDespesa) {
        this.fkDespesa = fkDespesa;
    }

    public abstract int getTipoDocumento();
}
