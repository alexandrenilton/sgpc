/*
 * Sistema de Gestão de Projetos Científicos - SGPC
 * 
 * 
 * Copyright  (C)  2010-2014  Instituto  de  Ciências  Matemáticas  e  de
 *                            Computação - ICMC/USP
 * 
 * 
 * This program is  free software; you can redistribute  it and/or modify
 * it under the  terms of the GNU General Public  License as published by
 * the Free Software Foundation; either  version 2 of the License, or (at
 * your option) any later version.
 * 
 * This program  is distributed in the  hope that it will  be useful, but
 * WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
 * MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
 * General Public License for more details.
 * 
 * You  should have received  a copy  of the  GNU General  Public License
 * along  with  this  program;  if   not,  write  to  the  Free  Software
 * Foundation,  Inc.,  51  Franklin   Street,  Fifth  Floor,  Boston,  MA
 * 02110-1301, USA.
 * 
 * 
 * EXCEPTION TO THE TERMS AND CONDITIONS OF GNU GENERAL PUBLIC LICENSE
 * 
 * The  LICENSE  file may  contain  an  additional  clause as  a  special
 * exception  to the  terms  and  conditions of  the  GNU General  Public
 * License. This  clause, if  present, gives you  the permission  to link
 * this  program with  certain third-part  software and  to obtain,  as a
 * result, a  work based  on this program  that can be  distributed using
 * other license than the GNU General Public License.  If you modify this
 * program, you may extend this exception to your version of the program,
 * but you  are not obligated  to do so.   If you do  not wish to  do so,
 * delete this exception statement from your version.
 * 
 */

package br.usp.icmc.sgpc.beans;

import br.usp.icmc.sgpc.interfaces.SgpcBeanInterface;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author herick
 */
@Entity
@Table(name = "pessoa_projeto")
@NamedQueries({
    @NamedQuery(name = "PessoaProjetoBean.findAll", query = "SELECT p FROM PessoaProjetoBean p"),
    @NamedQuery(name = "PessoaProjetoBean.findByProjeto", query = "SELECT p FROM PessoaProjetoBean p WHERE p.fkProjeto = :projeto"),
    @NamedQuery(name = "PessoaProjetoBean.findByPessoa", query = "SELECT p FROM PessoaProjetoBean p WHERE p.fkPessoa = :pessoa")})
public class PessoaProjetoBean implements Serializable, SgpcBeanInterface {

    private static final long serialVersionUID = 1L;

    @Id
    @Basic(optional = false)
    @SequenceGenerator(name = "id", sequenceName = "pessoa_projeto_id_pessoa_projeto_seq")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "id")
    @Column(name = "id_pessoa_projeto", nullable = false)
    private Integer id;

    @JoinColumn(name = "fk_projeto", referencedColumnName = "id_projeto", nullable = false)
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private ProjetoBean fkProjeto;

    @JoinColumn(name = "fk_pessoa", referencedColumnName = "id_pessoa", nullable = false)
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private PessoaBean fkPessoa;

    @Column(name = "data_associacao")
    @Temporal(TemporalType.DATE)
    private Date dataAssociacao;

    @Column(name = "tipo", length = 200, nullable = true)
    private String tipo;
    
    @OneToMany(mappedBy = "fkPessoaProjeto", fetch = FetchType.LAZY, cascade=CascadeType.ALL, orphanRemoval = true)
    private List<ParticipanteAuxilioBean> participantesAuxilio;

    public PessoaProjetoBean() {
        this.dataAssociacao = new Date();
    }

    public Date getDataAssociacao() {
        return dataAssociacao;
    }

    public void setDataAssociacao(Date dataAssociacao) {
        this.dataAssociacao = dataAssociacao;
    }

    public PessoaBean getFkPessoa() {
        return fkPessoa;
    }

    public void setFkPessoa(PessoaBean fkPessoa) {
        this.fkPessoa = fkPessoa;
    }

    public ProjetoBean getFkProjeto() {
        return fkProjeto;
    }

    public void setFkProjeto(ProjetoBean fkProjeto) {
        this.fkProjeto = fkProjeto;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    @Override
    public boolean equals(Object object) {
         // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PessoaProjetoBean)) {
            return false;
        }
        PessoaProjetoBean other = (PessoaProjetoBean) object;
        if ((this.fkPessoa.getId() == null && other.getFkPessoa().getId() != null) || (this.getFkPessoa().getId() != null && !this.fkPessoa.getId().equals(other.fkPessoa.getId()))) {
            return false;
        }
        return true;
    }
    
    public String toString(){
        return this.fkPessoa.getNome();
    }

    public List<ParticipanteAuxilioBean> getParticipantesAuxilio() {
        return participantesAuxilio;
    }

    public void setParticipantesAuxilio(List<ParticipanteAuxilioBean> participantesAuxilio) {
        this.participantesAuxilio = participantesAuxilio;
    }
}
