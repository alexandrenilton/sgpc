/*
 * Sistema de Gestão de Projetos Científicos - SGPC
 * 
 * 
 * Copyright  (C)  2010-2014  Instituto  de  Ciências  Matemáticas  e  de
 *                            Computação - ICMC/USP
 * 
 * 
 * This program is  free software; you can redistribute  it and/or modify
 * it under the  terms of the GNU General Public  License as published by
 * the Free Software Foundation; either  version 2 of the License, or (at
 * your option) any later version.
 * 
 * This program  is distributed in the  hope that it will  be useful, but
 * WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
 * MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
 * General Public License for more details.
 * 
 * You  should have received  a copy  of the  GNU General  Public License
 * along  with  this  program;  if   not,  write  to  the  Free  Software
 * Foundation,  Inc.,  51  Franklin   Street,  Fifth  Floor,  Boston,  MA
 * 02110-1301, USA.
 * 
 * 
 * EXCEPTION TO THE TERMS AND CONDITIONS OF GNU GENERAL PUBLIC LICENSE
 * 
 * The  LICENSE  file may  contain  an  additional  clause as  a  special
 * exception  to the  terms  and  conditions of  the  GNU General  Public
 * License. This  clause, if  present, gives you  the permission  to link
 * this  program with  certain third-part  software and  to obtain,  as a
 * result, a  work based  on this program  that can be  distributed using
 * other license than the GNU General Public License.  If you modify this
 * program, you may extend this exception to your version of the program,
 * but you  are not obligated  to do so.   If you do  not wish to  do so,
 * delete this exception statement from your version.
 * 
 */

package br.usp.icmc.sgpc.beans;

import br.usp.icmc.sgpc.interfaces.SgpcBeanInterface;
import java.io.Serializable;
import javax.persistence.*;

/**
 *
 * @author Artur
 */
@Entity
@Table(name = "cotas_bolsas")
@NamedQueries({
    @NamedQuery(name = "CotaBolsaBean.findAll", query = "SELECT a FROM CotaBolsaBean a"),
    @NamedQuery(name = "CotaBolsaBean.findById", query = "SELECT a FROM CotaBolsaBean a WHERE a.id = :id"),
    @NamedQuery(name = "CotaBolsaBean.findByObservacoes", query = "SELECT a FROM CotaBolsaBean a WHERE a.observacoes = :observacoes"),
    @NamedQuery(name = "CotaBolsaBean.findByKeyword", query = "SELECT a FROM CotaBolsaBean a WHERE upper(a.observacoes) LIKE upper(:keyword)")})
public class CotaBolsaBean implements Serializable, SgpcBeanInterface {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @SequenceGenerator(name = "id", sequenceName = "cotas_bolsas_id_cotas_bolsas_seq")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "id")
    @Column(name = "id_cotas_bolsas", nullable = false)
    private Integer id;
    
    @Basic(optional = true)
    @Column(name = "observacoes", nullable = true)
    private String observacoes;
    
    @JoinColumn(name = "fk_auxilio", referencedColumnName = "id_auxilio", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private AuxilioBean fkAuxilio;
    
    @Basic(optional = true)
    @Column(name = "qtd_concedida", nullable = true)
    private int quantidadeConcedida;
    
    @Basic(optional = true)
    @Column(name = "qtd_disponivel", nullable = true)
    private int quantidadeDisponivel;
    
    @JoinColumn(name = "fk_modalidade", referencedColumnName = "id_modalidade", nullable=false)
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private ModalidadeBean fkModalidade;

    public CotaBolsaBean() {
    }
    
    @Override
    public String toString() {
        return "br.usp.icmc.sgpc.CotaBolsa[idCotaBolsa=" + id + "]";
    }
    
    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CotaBolsaBean)) {
            return false;
        }
        CotaBolsaBean other = (CotaBolsaBean) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    public AuxilioBean getFkAuxilio() {
        return fkAuxilio;
    }

    public void setFkAuxilio(AuxilioBean fkAuxilio) {
        this.fkAuxilio = fkAuxilio;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getObservacoes() {
        return observacoes;
    }

    public void setObservacoes(String observacoes) {
        this.observacoes = observacoes;
    }

    public int getQuantidadeConcedida() {
        return quantidadeConcedida;
    }

    public void setQuantidadeConcedida(int quantidadeConcedida) {
        this.quantidadeConcedida = quantidadeConcedida;
    }

    public int getQuantidadeDisponivel() {
        return quantidadeDisponivel;
    }

    public void setQuantidadeDisponivel(int quantidadeDisponivel) {
        this.quantidadeDisponivel = quantidadeDisponivel;
    }

    public ModalidadeBean getFkModalidade() {
        return fkModalidade;
    }

    public void setFkModalidade(ModalidadeBean fkModalidade) {
        this.fkModalidade = fkModalidade;
    }
    
}
