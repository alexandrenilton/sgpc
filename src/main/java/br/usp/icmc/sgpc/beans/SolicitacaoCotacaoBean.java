/*
 * Sistema de Gestão de Projetos Científicos - SGPC
 * 
 * 
 * Copyright  (C)  2010-2014  Instituto  de  Ciências  Matemáticas  e  de
 *                            Computação - ICMC/USP
 * 
 * 
 * This program is  free software; you can redistribute  it and/or modify
 * it under the  terms of the GNU General Public  License as published by
 * the Free Software Foundation; either  version 2 of the License, or (at
 * your option) any later version.
 * 
 * This program  is distributed in the  hope that it will  be useful, but
 * WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
 * MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
 * General Public License for more details.
 * 
 * You  should have received  a copy  of the  GNU General  Public License
 * along  with  this  program;  if   not,  write  to  the  Free  Software
 * Foundation,  Inc.,  51  Franklin   Street,  Fifth  Floor,  Boston,  MA
 * 02110-1301, USA.
 * 
 * 
 * EXCEPTION TO THE TERMS AND CONDITIONS OF GNU GENERAL PUBLIC LICENSE
 * 
 * The  LICENSE  file may  contain  an  additional  clause as  a  special
 * exception  to the  terms  and  conditions of  the  GNU General  Public
 * License. This  clause, if  present, gives you  the permission  to link
 * this  program with  certain third-part  software and  to obtain,  as a
 * result, a  work based  on this program  that can be  distributed using
 * other license than the GNU General Public License.  If you modify this
 * program, you may extend this exception to your version of the program,
 * but you  are not obligated  to do so.   If you do  not wish to  do so,
 * delete this exception statement from your version.
 * 
 */

package br.usp.icmc.sgpc.beans;

import br.usp.icmc.sgpc.interfaces.SgpcBeanInterface;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Artur
 */
@Entity
@Table(name = "solicitacao_cotacao")
@NamedQueries({
    @NamedQuery(name = "SolicitacaoCotacaoBean.findAll", query = "SELECT s FROM SolicitacaoCotacaoBean s"),
    @NamedQuery(name = "SolicitacaoCotacaoBean.findById", query = "SELECT s FROM SolicitacaoCotacaoBean s WHERE s.id = :id"),
    @NamedQuery(name = "SolicitacaoCotacaoBean.findByDataEnvio", query = "SELECT s FROM SolicitacaoCotacaoBean s WHERE s.dataEnvio = :dataEnvio"),
    @NamedQuery(name = "SolicitacaoCotacaoBean.findByDataLimiteResposta", query = "SELECT s FROM SolicitacaoCotacaoBean s WHERE s.dataLimiteResposta = :dataLimiteResposta")})
public class SolicitacaoCotacaoBean implements Serializable, SgpcBeanInterface {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @SequenceGenerator(name = "id", sequenceName = "solicitacao_cotacao_id_solicitacao_cotacao_seq")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "id")
    @Column(name = "id_solicitacao_cotacao", nullable = false)
    private Integer id;

    @Column(name = "data_envio")
    @Temporal(TemporalType.DATE)
    private Date dataEnvio;

    @Column(name = "data_limite_resposta")
    @Temporal(TemporalType.DATE)
    private Date dataLimiteResposta;

    @JoinColumn(name = "fk_grupo_fornecimento", referencedColumnName = "id_grupo_fornecimento", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private GrupoFornecimentoBean fkGrupoFornecimento;

    @JoinColumn(name = "fk_item", referencedColumnName = "id_item", insertable = false, updatable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private ItemBean fkItem;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "fkSolicitacaoCotacao", fetch = FetchType.LAZY)
    private List<OrcamentoBean> orcamentos;

    public SolicitacaoCotacaoBean() {
        dataEnvio = new Date();
    }

    public SolicitacaoCotacaoBean(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getDataEnvio() {
        return dataEnvio;
    }

    public void setDataEnvio(Date dataEnvio) {
        this.dataEnvio = dataEnvio;
    }

    public Date getDataLimiteResposta() {
        return dataLimiteResposta;
    }

    public void setDataLimiteResposta(Date dataLimiteResposta) {
        this.dataLimiteResposta = dataLimiteResposta;
    }

    public GrupoFornecimentoBean getFkGrupoFornecimento() {
        return fkGrupoFornecimento;
    }

    public void setFkGrupoFornecimento(GrupoFornecimentoBean fkGrupoFornecimento) {
        this.fkGrupoFornecimento = fkGrupoFornecimento;
    }

    public ItemBean getFkItem() {
        return fkItem;
    }

    public void setFkItem(ItemBean fkItem) {
        this.fkItem = fkItem;
    }

    public List<OrcamentoBean> getOrcamentos() {
        return orcamentos;
    }

    public void setOrcamentos(List<OrcamentoBean> orcamentos) {
        this.orcamentos = orcamentos;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SolicitacaoCotacaoBean)) {
            return false;
        }
        SolicitacaoCotacaoBean other = (SolicitacaoCotacaoBean) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.usp.icmc.sgpc.Convite[id=" + id + "]";
    }

}
