/*
 * Sistema de Gestão de Projetos Científicos - SGPC
 * 
 * 
 * Copyright  (C)  2010-2014  Instituto  de  Ciências  Matemáticas  e  de
 *                            Computação - ICMC/USP
 * 
 * 
 * This program is  free software; you can redistribute  it and/or modify
 * it under the  terms of the GNU General Public  License as published by
 * the Free Software Foundation; either  version 2 of the License, or (at
 * your option) any later version.
 * 
 * This program  is distributed in the  hope that it will  be useful, but
 * WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
 * MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
 * General Public License for more details.
 * 
 * You  should have received  a copy  of the  GNU General  Public License
 * along  with  this  program;  if   not,  write  to  the  Free  Software
 * Foundation,  Inc.,  51  Franklin   Street,  Fifth  Floor,  Boston,  MA
 * 02110-1301, USA.
 * 
 * 
 * EXCEPTION TO THE TERMS AND CONDITIONS OF GNU GENERAL PUBLIC LICENSE
 * 
 * The  LICENSE  file may  contain  an  additional  clause as  a  special
 * exception  to the  terms  and  conditions of  the  GNU General  Public
 * License. This  clause, if  present, gives you  the permission  to link
 * this  program with  certain third-part  software and  to obtain,  as a
 * result, a  work based  on this program  that can be  distributed using
 * other license than the GNU General Public License.  If you modify this
 * program, you may extend this exception to your version of the program,
 * but you  are not obligated  to do so.   If you do  not wish to  do so,
 * delete this exception statement from your version.
 * 
 */

package br.usp.icmc.sgpc.beans;

import br.usp.icmc.sgpc.interfaces.SgpcBeanInterface;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.OrderBy;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Artur
 */
@Entity
@Table(name = "auxilio")
@NamedQueries({
    @NamedQuery(name = "AuxilioBean.findAll", query = "SELECT a FROM AuxilioBean a ORDER BY fkProjeto.fkResponsavel.nome, fkModalidade.fkTipoAuxilio.fkFinanciador.nome, numeroMercurio"),
    @NamedQuery(name = "AuxilioBean.findByStatus", query = "SELECT a FROM AuxilioBean a WHERE a.status = :status"),
    @NamedQuery(name = "AuxilioBean.findById", query = "SELECT a FROM AuxilioBean a WHERE a.id = :id"),
    @NamedQuery(name = "AuxilioBean.findByObservacoes", query = "SELECT a FROM AuxilioBean a WHERE a.observacoes = :observacoes"),
    @NamedQuery(name = "AuxilioBean.findByDataInicial", query = "SELECT a FROM AuxilioBean a WHERE a.dataInicial = :dataInicial"),
    @NamedQuery(name = "AuxilioBean.findByDataFinal", query = "SELECT a FROM AuxilioBean a WHERE a.dataFinal = :dataFinal"),
    @NamedQuery(name = "AuxilioBean.findByProtocolo", query = "SELECT a FROM AuxilioBean a WHERE a.protocolo = :protocolo"),
    @NamedQuery(name = "AuxilioBean.findByNumeroMercurio", query = "SELECT a FROM AuxilioBean a WHERE a.numeroMercurio = :numeroMercurio"),
    @NamedQuery(name = "AuxilioBean.findByDataSolicitacao", query = "SELECT a FROM AuxilioBean a WHERE a.dataSolicitacao = :dataSolicitacao"),
    @NamedQuery(name = "AuxilioBean.findByProtocoloFinanciador", query = "SELECT a FROM AuxilioBean a WHERE a.protocoloFinanciador= :protocoloFinanciador"),
    @NamedQuery(name = "AuxilioBean.findByNomeIdentificacao", query = "SELECT a FROM AuxilioBean a WHERE a.nomeIdentificacao = :nomeIdentificacao"),
    @NamedQuery(name = "AuxilioBean.findByTipo", query = "SELECT a FROM AuxilioBean a WHERE a.tipo = :tipo"),
    @NamedQuery(name = "AuxilioBean.findByOwner", query = "SELECT a FROM AuxilioBean a WHERE a.fkProjeto.fkResponsavel = :responsavel"),
    @NamedQuery(name = "AuxilioBean.findByOwnerTerm", query = "SELECT a FROM AuxilioBean a WHERE a.fkProjeto.fkResponsavel = :responsavel AND (upper(a.observacoes) LIKE upper(:keyword) OR a.protocolo LIKE :keyword OR a.numeroMercurio LIKE :keyword OR a.protocoloFinanciador LIKE :keyword OR upper(a.nomeIdentificacao) LIKE upper(:keyword))"),
    @NamedQuery(name = "AuxilioBean.findByKeyword", query = "SELECT a FROM AuxilioBean a WHERE upper(a.observacoes) LIKE upper(:keyword) OR a.protocolo LIKE :keyword OR a.numeroMercurio LIKE :keyword OR a.protocoloFinanciador LIKE :keyword OR upper(a.nomeIdentificacao) LIKE upper(:keyword) ORDER BY a.fkModalidade.fkTipoAuxilio.fkFinanciador.nome, a.numeroMercurio")})
public class AuxilioBean implements Serializable, SgpcBeanInterface {

    private static final long serialVersionUID = 1L;
    
    public static final String STATUS_EM_ELABORACAO = "Em Elaboração";
    public static final String STATUS_EM_ANALISE = "Em Análise";
    public static final String STATUS_APROVADO = "Aprovado";
    public static final String STATUS_NAO_APROVADO = "Não Aprovado";
    public static final String STATUS_EM_ANDAMENTO = "Em Andamento";
    public static final String STATUS_SUSPENSO = "Suspenso";
    public static final String STATUS_DESISTENCIA = "Desistência";
    public static final String STATUS_TRANSFERIDO = "Transferido";
    public static final String STATUS_CANCELADO = "Cancelado";
    public static final String STATUS_FINALIZADO = "Finalizado";
    public static final String STATUS_ATRASADO = "Atrasado";
    
    @Id
    @Basic(optional = false)
    @SequenceGenerator(name = "id", sequenceName = "auxilio_id_auxilio_seq")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "id")
    @Column(name = "id_auxilio", nullable = false)
    private Integer id;
    
    @Column(name = "status", length = 50)
    private String status;

    @Basic(optional = false)
    @Column(name = "observacoes", nullable = true, length = 1000)
    private String observacoes;

    @Basic(optional = false)
    @Column(name = "data_inicial")
    @Temporal(TemporalType.DATE)
    private Date dataInicial;

    @Column(name = "data_final")
    @Temporal(TemporalType.DATE)
    private Date dataFinal;

    @Column(name = "protocolo", length = 50)
    private String protocolo;

    @Column(name = "numero_mercurio", length = 50)
    private String numeroMercurio;

    @Column(name = "data_solicitacao")
    @Temporal(TemporalType.DATE)
    private Date dataSolicitacao;

    @Column(name = "protocolo_financiador", length = 50)
    private String protocoloFinanciador;

    @Column(name = "nome_identificacao", length = 500, nullable = false)
    private String nomeIdentificacao;

    @Column(name = "tipo", length = 20, nullable = true)
    private String tipo;

    @OneToMany(mappedBy = "fkAuxilio", fetch = FetchType.LAZY, cascade=CascadeType.ALL, orphanRemoval = true)
    private List<AlineaBean> alineas;

    @OneToMany(mappedBy = "fkAuxilio", fetch = FetchType.LAZY, cascade=CascadeType.ALL, orphanRemoval = true)
    @OrderBy(value="dataLimite")
    private List<PrestacaoContasBean> prestacaoContas;

    @OneToMany(mappedBy = "fkAuxilio", fetch = FetchType.LAZY, cascade=CascadeType.ALL, orphanRemoval = true)
    @OrderBy(value="dataLimite")
    private List<RelatorioCientificoBean> relatorioCientificos;
    //depois comentar esse relacionamento
    
    @JoinColumn(name = "fk_modalidade", referencedColumnName = "id_modalidade", nullable=false)
    @ManyToOne(optional = true, fetch = FetchType.LAZY)
    private ModalidadeBean fkModalidade;

    @JoinColumn(name = "fk_conta_corrente", referencedColumnName = "id_conta_corrente", nullable = true)
    @ManyToOne(optional = true, fetch = FetchType.LAZY)
    private ContaCorrenteBean fkContaCorrente;

    @JoinColumn(name = "fk_centro_despesa", referencedColumnName = "id_centro_despesa", nullable = true)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private CentroDespesaBean fkCentroDespesa;

    @JoinColumn(name = "fk_projeto", referencedColumnName = "id_projeto", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private ProjetoBean fkProjeto;

    @ElementCollection(fetch=FetchType.LAZY)
    @CollectionTable(name = "arquivo_auxilio", joinColumns =
    @JoinColumn(name = "fk_auxilio"))
    private List<ArquivoAnexoBean> arquivosAnexos;

    @JoinColumn(name = "fk_programa_especial_financiamento", referencedColumnName = "id_programa_especial_financiamento", nullable=true)
    @ManyToOne(optional = true, fetch = FetchType.LAZY)
    private ProgramaEspecialFinanciamentoBean fkProgramaEspecialFinanciamento;

    @JoinColumn(name = "fk_bolsista", referencedColumnName = "id_pessoa", nullable = true)
    @ManyToOne(optional = true, fetch = FetchType.LAZY)
    private PessoaBean fkBolsista;
    
    @OneToMany(mappedBy = "fkAuxilio", fetch = FetchType.LAZY, cascade=CascadeType.ALL, orphanRemoval = true)
    @OrderBy(value="id")
    private List<CotaBolsaBean> cotasBolsas;

    @JoinColumn(name = "fk_responsavel", referencedColumnName = "id_pessoa", nullable = true)
    @ManyToOne(optional = true, fetch = FetchType.EAGER)
    private PessoaBean fkResponsavel;
    
    @OneToMany(mappedBy = "fkAuxilio", fetch = FetchType.LAZY, cascade=CascadeType.ALL, orphanRemoval = true)
    private List<HistoricoAuxilioBean> historicosAuxilio;
    
    @OneToMany(mappedBy = "fkAuxilio", fetch = FetchType.LAZY, cascade=CascadeType.ALL, orphanRemoval = true)
    private List<ParticipanteAuxilioBean> participantesAuxilio;

    public AuxilioBean() {
        this.dataSolicitacao = new Date();
        this.alineas = new ArrayList<AlineaBean>();
        this.prestacaoContas = new ArrayList<PrestacaoContasBean>();
        this.relatorioCientificos = new ArrayList<RelatorioCientificoBean>();
        this.arquivosAnexos = new ArrayList<ArquivoAnexoBean>();
        this.cotasBolsas = new ArrayList<CotaBolsaBean>();
        this.historicosAuxilio = new ArrayList<HistoricoAuxilioBean>();
        this.participantesAuxilio = new ArrayList<ParticipanteAuxilioBean>();
    }

    public ProgramaEspecialFinanciamentoBean getFkProgramaEspecialFinanciamento() {
        return fkProgramaEspecialFinanciamento;
    }

    public void setFkProgramaEspecialFinanciamento(ProgramaEspecialFinanciamentoBean fkProgramaEspecialFinanciamento) {
        this.fkProgramaEspecialFinanciamento = fkProgramaEspecialFinanciamento;
    }

    public ModalidadeBean getFkModalidade() {
        return fkModalidade;
    }

    public void setFkModalidade(ModalidadeBean fkModalidade) {
        this.fkModalidade = fkModalidade;
    }

    
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getObservacoes() {
        return observacoes;
    }

    public void setObservacoes(String observacoes) {
        this.observacoes = observacoes;
    }

    public Date getDataInicial() {
        return dataInicial;
    }

    public void setDataInicial(Date dataInicial) {
        this.dataInicial = dataInicial;
    }

    public Date getDataFinal() {
        return dataFinal;
    }

    public void setDataFinal(Date dataFinal) {
        this.dataFinal = dataFinal;
    }

    public String getProtocolo() {
        return protocolo;
    }

    public void setProtocolo(String protocolo) {
        this.protocolo = protocolo;
    }

    public String getNumeroMercurio() {
        return numeroMercurio;
    }

    public void setNumeroMercurio(String numeroMercurio) {
        this.numeroMercurio = numeroMercurio;
    }

    public Date getDataSolicitacao() {
        return dataSolicitacao;
    }

    public void setDataSolicitacao(Date dataSolicitacao) {
        this.dataSolicitacao = dataSolicitacao;
    }

    public List<AlineaBean> getAlineas() {
        return alineas;
    }

    public void setAlineas(List<AlineaBean> alineas) {
        this.alineas = alineas;
    }

    public List<PrestacaoContasBean> getPrestacaoContas() {
        return prestacaoContas;
    }

    public void setPrestacaoContas(List<PrestacaoContasBean> prestacaoContas) {
        this.prestacaoContas = prestacaoContas;
    }

    public List<RelatorioCientificoBean> getRelatorioCientificos() {
        return relatorioCientificos;
    }

    public void setRelatorioCientificos(List<RelatorioCientificoBean> relatorioCientificos) {
        this.relatorioCientificos = relatorioCientificos;
    }
   
    public String getProtocoloFinanciador() {
        return protocoloFinanciador;
    }

    public void setProtocoloFinanciador(String protocoloFinanciador) {
        this.protocoloFinanciador = protocoloFinanciador;
    }

    public String getNomeIdentificacao() {
        return nomeIdentificacao;
    }

    public void setNomeIdentificacao(String nomeIdentificacao) {
        this.nomeIdentificacao = nomeIdentificacao;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public ContaCorrenteBean getFkContaCorrente() {
        return fkContaCorrente;
    }

    public void setFkContaCorrente(ContaCorrenteBean fkContaCorrente) {
        this.fkContaCorrente = fkContaCorrente;
    }

    public CentroDespesaBean getFkCentroDespesa() {
        return fkCentroDespesa;
    }

    public void setFkCentroDespesa(CentroDespesaBean fkCentroDespesa) {
        this.fkCentroDespesa = fkCentroDespesa;
    }

    public ProjetoBean getFkProjeto() {
        return fkProjeto;
    }

    public void setFkProjeto(ProjetoBean fkProjeto) {
        this.fkProjeto = fkProjeto;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AuxilioBean)) {
            return false;
        }
        AuxilioBean other = (AuxilioBean) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.usp.icmc.sgpc.Auxilio[idAuxilio=" + id + "]";
    }

    public List<ArquivoAnexoBean> getArquivosAnexos() {
        return arquivosAnexos;
    }

    public void setArquivosAnexos(List<ArquivoAnexoBean> arquivosAnexos) {
        this.arquivosAnexos = arquivosAnexos;
    }
    
    public BigDecimal getValorReceitasRs(){
        return getValorReceitas(AlineaBean.MOEDA_REAL);
    }
    public BigDecimal getValorReceitasUs(){
        return getValorReceitas(AlineaBean.MOEDA_DOLAR);
    }
    
    public BigDecimal getValorReceitas(int moeda){
        BigDecimal retorno = new BigDecimal(0);
        for (AlineaBean alinea : alineas){
            if(alinea.getMoeda()==moeda) retorno=retorno.add(alinea.getVerbaAprovada());
        }
        return retorno;
    }
    
    public BigDecimal getValorDespesasRs(){
        return getValorDespesas(AlineaBean.MOEDA_REAL);
    }
    public BigDecimal getValorDespesasUs(){
        return getValorDespesas(AlineaBean.MOEDA_DOLAR);
    }

    public BigDecimal getValorDespesas(int moeda){
        BigDecimal retorno = new BigDecimal(0);
        for (AlineaBean alinea : alineas){
            for(DespesaBean despesa : alinea.getDespesas()){
                if(alinea.getMoeda()==moeda) retorno = retorno.add(despesa.getValor());
            }
        }
        return retorno;
    }

    public PessoaBean getFkBolsista() {
        return fkBolsista;
    }

    public void setFkBolsista(PessoaBean fkBolsista) {
        this.fkBolsista = fkBolsista;
    }

    public List<CotaBolsaBean> getCotasBolsas() {
        return cotasBolsas;
    }

    public void setCotasBolsas(List<CotaBolsaBean> cotasBolsas) {
        this.cotasBolsas = cotasBolsas;
    }

    public PessoaBean getFkResponsavel() {
        return fkResponsavel;
    }

    public void setFkResponsavel(PessoaBean fkResponsavel) {
        this.fkResponsavel = fkResponsavel;
    }

    public List<HistoricoAuxilioBean> getHistoricosAuxilio() {
        return historicosAuxilio;
    }

    public void setHistoricosAuxilio(List<HistoricoAuxilioBean> historicosAuxilio) {
        this.historicosAuxilio = historicosAuxilio;
    }

    public List<ParticipanteAuxilioBean> getParticipantesAuxilio() {
        return participantesAuxilio;
    }

    public void setParticipantesAuxilio(List<ParticipanteAuxilioBean> participantesAuxilio) {
        this.participantesAuxilio = participantesAuxilio;
    }
    
}
