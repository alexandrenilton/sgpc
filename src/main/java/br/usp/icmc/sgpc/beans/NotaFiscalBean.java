/*
 * Sistema de Gestão de Projetos Científicos - SGPC
 * 
 * 
 * Copyright  (C)  2010-2014  Instituto  de  Ciências  Matemáticas  e  de
 *                            Computação - ICMC/USP
 * 
 * 
 * This program is  free software; you can redistribute  it and/or modify
 * it under the  terms of the GNU General Public  License as published by
 * the Free Software Foundation; either  version 2 of the License, or (at
 * your option) any later version.
 * 
 * This program  is distributed in the  hope that it will  be useful, but
 * WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
 * MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
 * General Public License for more details.
 * 
 * You  should have received  a copy  of the  GNU General  Public License
 * along  with  this  program;  if   not,  write  to  the  Free  Software
 * Foundation,  Inc.,  51  Franklin   Street,  Fifth  Floor,  Boston,  MA
 * 02110-1301, USA.
 * 
 * 
 * EXCEPTION TO THE TERMS AND CONDITIONS OF GNU GENERAL PUBLIC LICENSE
 * 
 * The  LICENSE  file may  contain  an  additional  clause as  a  special
 * exception  to the  terms  and  conditions of  the  GNU General  Public
 * License. This  clause, if  present, gives you  the permission  to link
 * this  program with  certain third-part  software and  to obtain,  as a
 * result, a  work based  on this program  that can be  distributed using
 * other license than the GNU General Public License.  If you modify this
 * program, you may extend this exception to your version of the program,
 * but you  are not obligated  to do so.   If you do  not wish to  do so,
 * delete this exception statement from your version.
 * 
 */

package br.usp.icmc.sgpc.beans;

import br.usp.icmc.sgpc.interfaces.SgpcBeanInterface;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author Artur
 */
@Entity
@Table(name = "nota_fiscal")
@NamedQueries({
    @NamedQuery(name = "NotaFiscalBean.findAll", query = "SELECT n FROM NotaFiscalBean n"),
    @NamedQuery(name = "NotaFiscalBean.findById", query = "SELECT n FROM NotaFiscalBean n WHERE n.id = :id"),
    @NamedQuery(name = "NotaFiscalBean.findByTipo", query = "SELECT n FROM NotaFiscalBean n WHERE n.tipo = :tipo"),
    @NamedQuery(name = "NotaFiscalBean.findByDanfe", query = "SELECT n FROM NotaFiscalBean n WHERE n.danfe = :danfe")})
public class NotaFiscalBean extends DocumentoComprovacaoDespesaBean  implements Serializable, SgpcBeanInterface {
    private static final long serialVersionUID = 1L;
        

    @Column(name = "tipo", length = 20, nullable = true)
    private String tipo;

    @Column(name = "danfe", length = 50)
    private String danfe;

    public NotaFiscalBean() {
    }

    public NotaFiscalBean(Integer id, String tipo, String danfe) {
        this.id = id;
        this.tipo = tipo;
        this.danfe = danfe;

    }

    public String getDanfe() {
        return danfe;
    }

    public void setDanfe(String danfe) {
        this.danfe = danfe;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof NotaFiscalBean)) {
            return false;
        }
        NotaFiscalBean other = (NotaFiscalBean) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        if(this.id != null && other.id != null && this.id.intValue() == other.id.intValue()) return true;
        
        if(this.getDataEmissao()!=null && other.getDataEmissao()!=null && this.getDataEmissao().equals(other.getDataEmissao()) &&
                this.getNumero()!=null && other.getNumero()!=null && this.getNumero().equals(other.getNumero()) &&
                this.getValor() == other.getValor() &&
                this.fkDespesa!=null && other.getFkDespesa()!=null && this.getFkDespesa().getId() == other.getFkDespesa().getId()) return true;
        return false;
    }

    @Override
    public String toString() {
        return "br.usp.icmc.sgpc.NotaFiscal[id=" + id + "]";
    }

    @Override
    public int getTipoDocumento() {
        return NOTA_FISCAL;
    }

}
