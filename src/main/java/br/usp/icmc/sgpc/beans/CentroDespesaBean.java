/*
 * Sistema de Gestão de Projetos Científicos - SGPC
 * 
 * 
 * Copyright  (C)  2010-2014  Instituto  de  Ciências  Matemáticas  e  de
 *                            Computação - ICMC/USP
 * 
 * 
 * This program is  free software; you can redistribute  it and/or modify
 * it under the  terms of the GNU General Public  License as published by
 * the Free Software Foundation; either  version 2 of the License, or (at
 * your option) any later version.
 * 
 * This program  is distributed in the  hope that it will  be useful, but
 * WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
 * MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
 * General Public License for more details.
 * 
 * You  should have received  a copy  of the  GNU General  Public License
 * along  with  this  program;  if   not,  write  to  the  Free  Software
 * Foundation,  Inc.,  51  Franklin   Street,  Fifth  Floor,  Boston,  MA
 * 02110-1301, USA.
 * 
 * 
 * EXCEPTION TO THE TERMS AND CONDITIONS OF GNU GENERAL PUBLIC LICENSE
 * 
 * The  LICENSE  file may  contain  an  additional  clause as  a  special
 * exception  to the  terms  and  conditions of  the  GNU General  Public
 * License. This  clause, if  present, gives you  the permission  to link
 * this  program with  certain third-part  software and  to obtain,  as a
 * result, a  work based  on this program  that can be  distributed using
 * other license than the GNU General Public License.  If you modify this
 * program, you may extend this exception to your version of the program,
 * but you  are not obligated  to do so.   If you do  not wish to  do so,
 * delete this exception statement from your version.
 * 
 */

package br.usp.icmc.sgpc.beans;

import br.usp.icmc.sgpc.interfaces.SgpcBeanInterface;
import java.io.Serializable;
import java.util.Comparator;
import java.util.List;
import javax.persistence.*;

@Entity
@Table(name = "centro_despesa")
@NamedQueries({
    @NamedQuery(name = "CentroDespesaBean.findAll", query = "SELECT c FROM CentroDespesaBean c"),
    @NamedQuery(name = "CentroDespesaBean.findById", query = "SELECT c FROM CentroDespesaBean c WHERE c.id = :id"),
    @NamedQuery(name = "CentroDespesaBean.findByNome", query = "SELECT c FROM CentroDespesaBean c WHERE c.nome = :nome"),
    @NamedQuery(name = "CentroDespesaBean.findByKeyword", query = "SELECT c FROM CentroDespesaBean c WHERE upper(c.nome) LIKE upper(:keyword)")})
public class CentroDespesaBean implements Serializable, SgpcBeanInterface {
    public static final Comparator<CentroDespesaBean> POR_NOME = new Comparator<CentroDespesaBean>(){
        public int compare(CentroDespesaBean p1, CentroDespesaBean p2){
            if(p1!=null && p2!=null && p1.nome!=null && p2.nome!=null){
                return p1.nome.compareTo(p2.nome);
            }
            return -1;
        }
    };
    
    public static final Comparator<CentroDespesaBean> POR_ESTRUTURA_HIERARQUICA = new Comparator<CentroDespesaBean>(){
        public int compare(CentroDespesaBean p1, CentroDespesaBean p2){
            if(p1!=null && p2!=null && p1.estruturaHierarquica!=null && p2.estruturaHierarquica!=null){
                return p1.estruturaHierarquica.compareTo(p2.estruturaHierarquica);
            }
            return -1;
        }
    };
    public static final int TIPO_AMBOS = 0;
    public static final int TIPO_FAVORECIDO = 1;
    public static final int TIPO_FINANCIADOR = 2;

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @SequenceGenerator(name = "id", sequenceName = "centro_despesa_id_centro_despesa_seq")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "id")
    @Column(name = "id_centro_despesa", nullable = false)
    private Integer id;
    @Column(name = "nome", nullable = false, length = 100)
    private String nome;
    @OneToMany(mappedBy = "fkCentroDespesa", fetch = FetchType.LAZY)
    private List<AuxilioBean> auxilios;
    
    @JoinColumn(name = "fk_unidade", referencedColumnName = "id_unidade", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private UnidadeBean fkUnidade;
    
    @JoinColumn(name = "fk_centro_despesa_pai", referencedColumnName = "id_centro_despesa", nullable = true)
    @ManyToOne(optional = true, fetch = FetchType.EAGER)
    private CentroDespesaBean centroDespesasPai;

    @OneToMany(mappedBy = "centroDespesasPai", fetch = FetchType.LAZY, orphanRemoval = true, cascade = CascadeType.ALL)
    @OrderBy("nome ASC")
    private List<CentroDespesaBean> centrosDespesaFilhos;
    
    @Column(name="tipo", nullable = false, columnDefinition="INTEGER default 0")
    private Integer tipo;
    
    @Column(name="codigo_hierarquia", nullable = true, columnDefinition="DECIMAL(30,0)")
    private Double codigoHierarquia;
    
    @Column(name = "estrutura_hierarquica", nullable = true, length = 512)
    private String estruturaHierarquica;
    
    @Column(name = "ordem_vertical_hierarquia", nullable = false, columnDefinition="INTEGER default 0")
    private int ordemVerticalHierarquia;

    public CentroDespesaBean() {
    }

    public List<AuxilioBean> getAuxilios() {
        return auxilios;
    }

    public void setAuxilios(List<AuxilioBean> auxilios) {
        this.auxilios = auxilios;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CentroDespesaBean)) {
            return false;
        }
        CentroDespesaBean other = (CentroDespesaBean) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        //return "br.usp.icmc.sgpc.CentroDespesa[id=" + id + "]";
        return this.nome;
    }

    public CentroDespesaBean getCentroDespesasPai() {
        return centroDespesasPai;
    }

    public void setCentroDespesasPai(CentroDespesaBean centroDespesasPai) {
        this.centroDespesasPai = centroDespesasPai;
    }

    public List<CentroDespesaBean> getCentrosDespesaFilhos() {
        return centrosDespesaFilhos;
    }

    public void setCentrosDespesaFilhos(List<CentroDespesaBean> centrosDespesaFilhos) {
        this.centrosDespesaFilhos = centrosDespesaFilhos;
    }

    public UnidadeBean getFkUnidade() {
        return fkUnidade;
    }

    public void setFkUnidade(UnidadeBean fkUnidade) {
        this.fkUnidade = fkUnidade;
    }

    public Integer getTipo() {
        return tipo;
    }

    public void setTipo(Integer tipo) {
        this.tipo = tipo;
    }

    public Double getCodigoHierarquia() {
        return codigoHierarquia;
    }

    public void setCodigoHierarquia(Double codigoHierarquia) {
        this.codigoHierarquia = codigoHierarquia;
    }

    public String getEstruturaHierarquica() {
        return estruturaHierarquica;
    }

    public void setEstruturaHierarquica(String estruturaHierarquica) {
        this.estruturaHierarquica = estruturaHierarquica;
    }

    public int getOrdemVerticalHierarquia() {
        return ordemVerticalHierarquia;
    }

    public void setOrdemVerticalHierarquia(int ordemVerticalHierarquia) {
        this.ordemVerticalHierarquia = ordemVerticalHierarquia;
    }
}
