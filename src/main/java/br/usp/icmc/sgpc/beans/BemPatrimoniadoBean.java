/*
 * Sistema de Gestão de Projetos Científicos - SGPC
 * 
 * 
 * Copyright  (C)  2010-2014  Instituto  de  Ciências  Matemáticas  e  de
 *                            Computação - ICMC/USP
 * 
 * 
 * This program is  free software; you can redistribute  it and/or modify
 * it under the  terms of the GNU General Public  License as published by
 * the Free Software Foundation; either  version 2 of the License, or (at
 * your option) any later version.
 * 
 * This program  is distributed in the  hope that it will  be useful, but
 * WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
 * MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
 * General Public License for more details.
 * 
 * You  should have received  a copy  of the  GNU General  Public License
 * along  with  this  program;  if   not,  write  to  the  Free  Software
 * Foundation,  Inc.,  51  Franklin   Street,  Fifth  Floor,  Boston,  MA
 * 02110-1301, USA.
 * 
 * 
 * EXCEPTION TO THE TERMS AND CONDITIONS OF GNU GENERAL PUBLIC LICENSE
 * 
 * The  LICENSE  file may  contain  an  additional  clause as  a  special
 * exception  to the  terms  and  conditions of  the  GNU General  Public
 * License. This  clause, if  present, gives you  the permission  to link
 * this  program with  certain third-part  software and  to obtain,  as a
 * result, a  work based  on this program  that can be  distributed using
 * other license than the GNU General Public License.  If you modify this
 * program, you may extend this exception to your version of the program,
 * but you  are not obligated  to do so.   If you do  not wish to  do so,
 * delete this exception statement from your version.
 * 
 */

package br.usp.icmc.sgpc.beans;

import br.usp.icmc.sgpc.interfaces.SgpcBeanInterface;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Artur
 */
@Entity
@Table(name = "bem_patrimoniado")
@NamedQueries({
    @NamedQuery(name = "BemPatrimoniadoBean.findAll", query = "SELECT b FROM BemPatrimoniadoBean b"),
    @NamedQuery(name = "BemPatrimoniadoBean.findById", query = "SELECT b FROM BemPatrimoniadoBean b WHERE b.id = :id"),
    @NamedQuery(name = "BemPatrimoniadoBean.findByNumero", query = "SELECT b FROM BemPatrimoniadoBean b WHERE b.numero = :numero"),
    @NamedQuery(name = "BemPatrimoniadoBean.findByDescricao", query = "SELECT b FROM BemPatrimoniadoBean b WHERE b.descricao = :descricao"),
    @NamedQuery(name = "BemPatrimoniadoBean.findByDataRegistro", query = "SELECT b FROM BemPatrimoniadoBean b WHERE b.dataRegistro = :dataRegistro"),
    @NamedQuery(name = "BemPatrimoniadoBean.findByKeyword", query = "SELECT b FROM BemPatrimoniadoBean b WHERE b.numero LIKE :keyword OR upper(b.descricao) LIKE upper(:keyword) OR b.dataRegistro LIKE :keyword")})
public class BemPatrimoniadoBean implements Serializable, SgpcBeanInterface {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @SequenceGenerator(name = "id", sequenceName = "bem_patrimoniado_id_bem_patrimoniado_seq")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "id")
    @Column(name = "id_bem_patrimoniado", nullable = false)
    private Integer id;

    @Basic(optional = false)
    @Column(name = "numero", nullable = false, length = 100)
    private String numero;

    @Basic(optional = false)
    @Column(name = "descricao", nullable = false, length = 1000)
    private String descricao;

    @Basic(optional = false)
    @Column(name = "data_registro", nullable = false)
    @Temporal(TemporalType.DATE)
    private Date dataRegistro;

    @JoinColumn(name = "fk_despesa", referencedColumnName = "id_despesa", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private DespesaBean fkDespesa;

    public BemPatrimoniadoBean() {
    }

    public BemPatrimoniadoBean(Integer id) {
        this.id = id;
    }

    public BemPatrimoniadoBean(Integer id, String numero, String descricao, Date dataRegistro) {
        this.id = id;
        this.numero = numero;
        this.descricao = descricao;
        this.dataRegistro = dataRegistro;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Date getDataRegistro() {
        return dataRegistro;
    }

    public void setDataRegistro(Date dataRegistro) {
        this.dataRegistro = dataRegistro;
    }

    public DespesaBean getFkDespesa() {
        return fkDespesa;
    }

    public void setFkDespesa(DespesaBean fkDespesa) {
        this.fkDespesa = fkDespesa;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BemPatrimoniadoBean)) {
            return false;
        }
        BemPatrimoniadoBean other = (BemPatrimoniadoBean) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.usp.icmc.sgpc.BemPatrimoniado[id=" + id + "]";
    }

}
