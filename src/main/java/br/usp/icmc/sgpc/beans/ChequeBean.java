/*
 * Sistema de Gestão de Projetos Científicos - SGPC
 * 
 * 
 * Copyright  (C)  2010-2014  Instituto  de  Ciências  Matemáticas  e  de
 *                            Computação - ICMC/USP
 * 
 * 
 * This program is  free software; you can redistribute  it and/or modify
 * it under the  terms of the GNU General Public  License as published by
 * the Free Software Foundation; either  version 2 of the License, or (at
 * your option) any later version.
 * 
 * This program  is distributed in the  hope that it will  be useful, but
 * WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
 * MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
 * General Public License for more details.
 * 
 * You  should have received  a copy  of the  GNU General  Public License
 * along  with  this  program;  if   not,  write  to  the  Free  Software
 * Foundation,  Inc.,  51  Franklin   Street,  Fifth  Floor,  Boston,  MA
 * 02110-1301, USA.
 * 
 * 
 * EXCEPTION TO THE TERMS AND CONDITIONS OF GNU GENERAL PUBLIC LICENSE
 * 
 * The  LICENSE  file may  contain  an  additional  clause as  a  special
 * exception  to the  terms  and  conditions of  the  GNU General  Public
 * License. This  clause, if  present, gives you  the permission  to link
 * this  program with  certain third-part  software and  to obtain,  as a
 * result, a  work based  on this program  that can be  distributed using
 * other license than the GNU General Public License.  If you modify this
 * program, you may extend this exception to your version of the program,
 * but you  are not obligated  to do so.   If you do  not wish to  do so,
 * delete this exception statement from your version.
 * 
 */

package br.usp.icmc.sgpc.beans;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Artur
 */
@Entity
@Table(name = "cheque")
@NamedQueries({
    @NamedQuery(name = "ChequeBean.findAll", query = "SELECT c FROM ChequeBean c"),
    @NamedQuery(name = "ChequeBean.findByNumero", query = "SELECT c FROM ChequeBean c WHERE c.numero = :numero"),
    @NamedQuery(name = "ChequeBean.findByValor", query = "SELECT c FROM ChequeBean c WHERE c.valor = :valor"),
    @NamedQuery(name = "ChequeBean.findByDataEmissao", query = "SELECT c FROM ChequeBean c WHERE c.dataEmissao = :dataEmissao"),
    @NamedQuery(name = "ChequeBean.findByStatus", query = "SELECT c FROM ChequeBean c WHERE c.status = :status"),
    @NamedQuery(name = "ChequeBean.findById", query = "SELECT c FROM ChequeBean c WHERE c.id = :id")})
public class ChequeBean implements Serializable {
    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @Column(name = "numero", nullable = false, length = 50)
    private String numero;

    @Basic(optional = false)
    @Column(name = "valor", nullable = false)
    private BigDecimal valor;

    @Column(name = "data_emissao")
    @Temporal(TemporalType.DATE)
    private Date dataEmissao;

    @Basic(optional = false)
    @Column(name = "status", nullable = false, length = 50)
    private String status;

    @Id
    @Basic(optional = false)
    @Column(name = "id_cheque", nullable = false)
    private Integer id;
    
    @JoinColumn(name = "fk_conta_corrente", referencedColumnName = "id_conta_corrente", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private ContaCorrenteBean fkContaCorrente;

    @JoinColumn(name = "fk_despesa", referencedColumnName = "id_despesa", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private DespesaBean fkDespesa;

    public ChequeBean() {
    }

    public ChequeBean(Integer id) {
        this.id = id;
    }

    public ChequeBean(Integer id, String numero, BigDecimal valor, String status) {
        this.id = id;
        this.numero = numero;
        this.valor = valor;
        this.status = status;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public BigDecimal getValor() {
        return valor;
    }

    public void setValor(BigDecimal valor) {
        this.valor = valor;
    }

    public Date getDataEmissao() {
        return dataEmissao;
    }

    public void setDataEmissao(Date dataEmissao) {
        this.dataEmissao = dataEmissao;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public ContaCorrenteBean getFkContaCorrente() {
        return fkContaCorrente;
    }

    public void setFkContaCorrente(ContaCorrenteBean fkContaCorrente) {
        this.fkContaCorrente = fkContaCorrente;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ChequeBean)) {
            return false;
        }
        ChequeBean other = (ChequeBean) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.usp.icmc.sgpc.Cheque[id=" + id + "]";
    }

}
