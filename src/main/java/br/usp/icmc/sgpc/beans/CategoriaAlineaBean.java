/*
 * Sistema de Gestão de Projetos Científicos - SGPC
 * 
 * 
 * Copyright  (C)  2010-2014  Instituto  de  Ciências  Matemáticas  e  de
 *                            Computação - ICMC/USP
 * 
 * 
 * This program is  free software; you can redistribute  it and/or modify
 * it under the  terms of the GNU General Public  License as published by
 * the Free Software Foundation; either  version 2 of the License, or (at
 * your option) any later version.
 * 
 * This program  is distributed in the  hope that it will  be useful, but
 * WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
 * MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
 * General Public License for more details.
 * 
 * You  should have received  a copy  of the  GNU General  Public License
 * along  with  this  program;  if   not,  write  to  the  Free  Software
 * Foundation,  Inc.,  51  Franklin   Street,  Fifth  Floor,  Boston,  MA
 * 02110-1301, USA.
 * 
 * 
 * EXCEPTION TO THE TERMS AND CONDITIONS OF GNU GENERAL PUBLIC LICENSE
 * 
 * The  LICENSE  file may  contain  an  additional  clause as  a  special
 * exception  to the  terms  and  conditions of  the  GNU General  Public
 * License. This  clause, if  present, gives you  the permission  to link
 * this  program with  certain third-part  software and  to obtain,  as a
 * result, a  work based  on this program  that can be  distributed using
 * other license than the GNU General Public License.  If you modify this
 * program, you may extend this exception to your version of the program,
 * but you  are not obligated  to do so.   If you do  not wish to  do so,
 * delete this exception statement from your version.
 * 
 */

package br.usp.icmc.sgpc.beans;

import br.usp.icmc.sgpc.interfaces.SgpcBeanInterface;
import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 *
 * @author Herick
 */
@Entity
@Table(name = "categoria_alinea")
@NamedQueries({
    @NamedQuery(name = "CategoriaAlineaBean.findAll", query = "SELECT a FROM CategoriaAlineaBean a ORDER BY a.nome"),
    @NamedQuery(name = "CategoriaAlineaBean.findByKeyword", query = "SELECT a FROM CategoriaAlineaBean a WHERE upper(a.nome) LIKE upper(:keyword) OR upper(a.descricao) LIKE upper(:keyword)"),
    @NamedQuery(name = "CategoriaAlineaBean.findById", query = "SELECT a FROM CategoriaAlineaBean a WHERE a.id = :id")})
public class CategoriaAlineaBean implements Serializable, SgpcBeanInterface {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @SequenceGenerator(name = "id", sequenceName = "categoria_alinea_id_categoria_alinea_seq")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "id")
    @Column(name = "id_categoria_alinea", nullable = false)
    private Integer id;

    @Basic(optional = false)
    @Column(name = "nome", nullable = false, length = 100)
    private String nome;

    @Basic(optional = false)
    @Column(name = "descricao", nullable = true, length = 200)
    private String descricao;
    
    @Column(name = "capital", nullable=false, columnDefinition="boolean default false")
    private boolean capital;

    @Column(name = "bolsa", nullable=false, columnDefinition="boolean default false")
    private boolean bolsa;

    @Column(name = "custeio", nullable=false, columnDefinition="boolean default false")
    private boolean custeio;

    @Column(name = "reserva_tecnica", nullable=false, columnDefinition="boolean default false")
    private boolean reservaTecnica;
    
    @Column(name = "beneficio_complementar", nullable=false, columnDefinition="boolean default false")
    private boolean beneficioComplementar;

    public CategoriaAlineaBean() {
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public boolean isCapital() {
        return capital;
    }

    public void setCapital(boolean capital) {
        this.capital = capital;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public boolean isBolsa() {
        return bolsa;
    }

    public void setBolsa(boolean bolsa) {
        this.bolsa = bolsa;
    }


    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CategoriaAlineaBean)) {
            return false;
        }
        CategoriaAlineaBean other = (CategoriaAlineaBean) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return this.getNome();
    }

    public boolean isCusteio() {
        return custeio;
    }

    public void setCusteio(boolean custeio) {
        this.custeio = custeio;
    }

    public boolean isReservaTecnica() {
        return reservaTecnica;
    }

    public void setReservaTecnica(boolean reservaTecnica) {
        this.reservaTecnica = reservaTecnica;
    }

    public boolean isBeneficioComplementar() {
        return beneficioComplementar;
    }

    public void setBeneficioComplementar(boolean beneficioComplementar) {
        this.beneficioComplementar = beneficioComplementar;
    }
}
