/*
 * Sistema de Gestão de Projetos Científicos - SGPC
 * 
 * 
 * Copyright  (C)  2010-2014  Instituto  de  Ciências  Matemáticas  e  de
 *                            Computação - ICMC/USP
 * 
 * 
 * This program is  free software; you can redistribute  it and/or modify
 * it under the  terms of the GNU General Public  License as published by
 * the Free Software Foundation; either  version 2 of the License, or (at
 * your option) any later version.
 * 
 * This program  is distributed in the  hope that it will  be useful, but
 * WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
 * MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
 * General Public License for more details.
 * 
 * You  should have received  a copy  of the  GNU General  Public License
 * along  with  this  program;  if   not,  write  to  the  Free  Software
 * Foundation,  Inc.,  51  Franklin   Street,  Fifth  Floor,  Boston,  MA
 * 02110-1301, USA.
 * 
 * 
 * EXCEPTION TO THE TERMS AND CONDITIONS OF GNU GENERAL PUBLIC LICENSE
 * 
 * The  LICENSE  file may  contain  an  additional  clause as  a  special
 * exception  to the  terms  and  conditions of  the  GNU General  Public
 * License. This  clause, if  present, gives you  the permission  to link
 * this  program with  certain third-part  software and  to obtain,  as a
 * result, a  work based  on this program  that can be  distributed using
 * other license than the GNU General Public License.  If you modify this
 * program, you may extend this exception to your version of the program,
 * but you  are not obligated  to do so.   If you do  not wish to  do so,
 * delete this exception statement from your version.
 * 
 */

package br.usp.icmc.sgpc.beans;

import br.usp.icmc.sgpc.interfaces.SgpcBeanInterface;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Artur
 */
@Entity
@Table(name = "conta_corrente")
@NamedQueries({
    @NamedQuery(name = "ContaCorrenteBean.findAll", query = "SELECT c FROM ContaCorrenteBean c"),
    @NamedQuery(name = "ContaCorrenteBean.findByAgencia", query = "SELECT c FROM ContaCorrenteBean c WHERE c.agencia = :agencia"),
    @NamedQuery(name = "ContaCorrenteBean.findByNumero", query = "SELECT c FROM ContaCorrenteBean c WHERE c.numero = :numero"),
    @NamedQuery(name = "ContaCorrenteBean.findByDataAbertura", query = "SELECT c FROM ContaCorrenteBean c WHERE c.dataAbertura = :dataAbertura"),
    @NamedQuery(name = "ContaCorrenteBean.findById", query = "SELECT c FROM ContaCorrenteBean c WHERE c.id = :id"),
    @NamedQuery(name = "ContaCorrenteBean.findByOwnerTerm", query = "SELECT c FROM ContaCorrenteBean c WHERE c.fkPessoa = :responsavel AND (c.numero LIKE :keyword OR c.agencia LIKE :keyword)"),
    @NamedQuery(name = "ContaCorrenteBean.findByOwner", query = "SELECT c FROM ContaCorrenteBean c WHERE c.fkPessoa = :responsavel"),
    @NamedQuery(name = "ContaCorrenteBean.findByKeyword", query = "SELECT c FROM ContaCorrenteBean c WHERE upper(c.numero) LIKE upper(:keyword) OR upper(c.agencia) LIKE upper(:keyword) OR upper(c.fkPessoa.nome) LIKE upper(:keyword)")})
public class ContaCorrenteBean implements Serializable, SgpcBeanInterface {
    private static final long serialVersionUID = 1L;

    @Basic(optional = false)
    @Column(name = "agencia", nullable = false, length = 50)
    private String agencia;

    @Basic(optional = false)
    @Column(name = "numero", nullable = false, length = 50)
    private String numero;

    @Basic(optional = true)
    @Column(name = "data_abertura", nullable = true)
    @Temporal(TemporalType.DATE)
    private Date dataAbertura;

    @Id
    @Basic(optional = false)
    @SequenceGenerator(name="id", sequenceName="conta_corrente_id_conta_corrente_seq")
    @GeneratedValue (strategy=GenerationType.AUTO, generator="id")
    @Column(name = "id_conta_corrente", nullable = false)
    private Integer id;

    @JoinColumn(name = "fk_instituicao_bancaria", referencedColumnName = "id_instituicao_bancaria", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private InstituicaoBancariaBean fkBanco;

    @JoinColumn(name = "fk_pessoa", referencedColumnName = "id_pessoa", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private PessoaBean fkPessoa;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "fkContaCorrente", fetch = FetchType.LAZY)
    private List<ChequeBean> cheques;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "fkContaCorrente", fetch = FetchType.LAZY)
    private List<AuxilioBean> auxilios;
    
    @Column(name = "ativa", nullable=false, columnDefinition="boolean default true")
    private boolean ativa;

    public ContaCorrenteBean() {
    }

    public ContaCorrenteBean(Integer id) {
        this.id = id;
    }

    public ContaCorrenteBean(Integer id, String agencia, String numero, Date dataAbertura) {
        this.id = id;
        this.agencia = agencia;
        this.numero = numero;
        this.dataAbertura = dataAbertura;
    }

    public String getAgencia() {
        return agencia;
    }

    public void setAgencia(String agencia) {
        this.agencia = agencia;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public Date getDataAbertura() {
        return dataAbertura;
    }

    public void setDataAbertura(Date dataAbertura) {
        this.dataAbertura = dataAbertura;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public InstituicaoBancariaBean getFkBanco() {
        return fkBanco;
    }

    public void setFkBanco(InstituicaoBancariaBean fkBanco) {
        this.fkBanco = fkBanco;
    }

    public List<AuxilioBean> getAuxilios() {
        return auxilios;
    }

    public void setAuxilios(List<AuxilioBean> auxilios) {
        this.auxilios = auxilios;
    }

    public List<ChequeBean> getCheques() {
        return cheques;
    }

    public void setCheques(List<ChequeBean> cheques) {
        this.cheques = cheques;
    }

    public PessoaBean getFkPessoa() {
        return fkPessoa;
    }

    public void setFkPessoa(PessoaBean fkPessoa) {
        this.fkPessoa = fkPessoa;
    }
    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ContaCorrenteBean)) {
            return false;
        }
        ContaCorrenteBean other = (ContaCorrenteBean) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        //return "br.usp.icmc.sgpc.ContaCorrente[id=" + id + "]";
        return this.fkBanco.getNome() + " - Ag.:" + this.agencia+" - CC.:"+this.numero;
    }

    public boolean isAtiva() {
        return ativa;
    }

    public void setAtiva(boolean ativa) {
        this.ativa = ativa;
    }

}
