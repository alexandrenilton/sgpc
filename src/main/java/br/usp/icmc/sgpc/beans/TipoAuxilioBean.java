/*
 * Sistema de Gestão de Projetos Científicos - SGPC
 * 
 * 
 * Copyright  (C)  2010-2014  Instituto  de  Ciências  Matemáticas  e  de
 *                            Computação - ICMC/USP
 * 
 * 
 * This program is  free software; you can redistribute  it and/or modify
 * it under the  terms of the GNU General Public  License as published by
 * the Free Software Foundation; either  version 2 of the License, or (at
 * your option) any later version.
 * 
 * This program  is distributed in the  hope that it will  be useful, but
 * WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
 * MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
 * General Public License for more details.
 * 
 * You  should have received  a copy  of the  GNU General  Public License
 * along  with  this  program;  if   not,  write  to  the  Free  Software
 * Foundation,  Inc.,  51  Franklin   Street,  Fifth  Floor,  Boston,  MA
 * 02110-1301, USA.
 * 
 * 
 * EXCEPTION TO THE TERMS AND CONDITIONS OF GNU GENERAL PUBLIC LICENSE
 * 
 * The  LICENSE  file may  contain  an  additional  clause as  a  special
 * exception  to the  terms  and  conditions of  the  GNU General  Public
 * License. This  clause, if  present, gives you  the permission  to link
 * this  program with  certain third-part  software and  to obtain,  as a
 * result, a  work based  on this program  that can be  distributed using
 * other license than the GNU General Public License.  If you modify this
 * program, you may extend this exception to your version of the program,
 * but you  are not obligated  to do so.   If you do  not wish to  do so,
 * delete this exception statement from your version.
 * 
 */

package br.usp.icmc.sgpc.beans;

import br.usp.icmc.sgpc.interfaces.SgpcBeanInterface;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 *
 * @author david
 */
@Entity
@Table(name = "tipo_auxilio")
@NamedQueries({
    @NamedQuery(name = "TipoAuxilioBean.findAll", query = "SELECT i FROM TipoAuxilioBean i"),
    @NamedQuery(name = "TipoAuxilioBean.findById", query = "SELECT i FROM TipoAuxilioBean i WHERE i.id = :id"),
    @NamedQuery(name = "TipoAuxilioBean.findByNome", query = "SELECT i FROM TipoAuxilioBean i WHERE i.nome = :nome"),
    @NamedQuery(name = "TipoAuxilioBean.findByFinanciador", query = "SELECT i FROM TipoAuxilioBean i WHERE i.fkFinanciador = :financiador ORDER BY i.nome"),
    @NamedQuery(name = "TipoAuxilioBean.findByTermos", query = "SELECT i FROM TipoAuxilioBean i WHERE i.fkFinanciador = :financiador AND (upper(i.nome) LIKE upper(:keyword) OR upper(i.sigla) LIKE upper(:keyword)) ORDER BY i.nome"),
    @NamedQuery(name = "TipoAuxilioBean.findByKeyword", query = "SELECT i FROM TipoAuxilioBean i WHERE upper(i.nome) LIKE upper(:keyword) OR upper(i.sigla) LIKE upper(:keyword)")})
public class TipoAuxilioBean implements Serializable, SgpcBeanInterface {
    
    private static final long serialVersionUID = 1L;

    @Id
    @Basic(optional = false)
    @SequenceGenerator(name = "id", sequenceName = "tipo_auxilio_id_tipo_auxilio_seq") //-> nao sei?!
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "id") //informa que o id será gerado automaticamente pelo banco
    @Column(name = "id_tipo_auxilio", nullable = false)
    private Integer id;
    @Basic(optional = false)
    @Column(name = "nome", nullable = false, length = 100)
    private String nome;

    @Column(name = "sigla", length = 20)
    private String sigla;

    @OneToMany(mappedBy = "fkTipoAuxilio", fetch = FetchType.LAZY, cascade=CascadeType.ALL)
    private List<ModalidadeBean> modalidades = new ArrayList<ModalidadeBean>();

    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_id_financiador", referencedColumnName = "id_financiador", nullable = false)
    private FinanciadorBean fkFinanciador;

    public TipoAuxilioBean() {
        //this.nome = new ArrayList<>;
        this.modalidades = new ArrayList<ModalidadeBean>();
    }

    public String getSigla() {
        return sigla;
    }

    public void setSigla(String sigla) {
        this.sigla = sigla;
    }

    public FinanciadorBean getFkFinanciador() {
        return fkFinanciador;
    }

    public void setFkFinanciador(FinanciadorBean fkFinanciador) {
        this.fkFinanciador = fkFinanciador;
    }

    public List<ModalidadeBean> getModalidades() {
        return modalidades;
    }

    public void setModalidades(List<ModalidadeBean> modalidades) {
        this.modalidades = modalidades;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TipoAuxilioBean)) {
            return false;
        }
        TipoAuxilioBean other = (TipoAuxilioBean) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return this.getNome();
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }
}
