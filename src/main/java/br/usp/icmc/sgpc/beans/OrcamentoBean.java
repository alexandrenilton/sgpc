/*
 * Sistema de Gestão de Projetos Científicos - SGPC
 * 
 * 
 * Copyright  (C)  2010-2014  Instituto  de  Ciências  Matemáticas  e  de
 *                            Computação - ICMC/USP
 * 
 * 
 * This program is  free software; you can redistribute  it and/or modify
 * it under the  terms of the GNU General Public  License as published by
 * the Free Software Foundation; either  version 2 of the License, or (at
 * your option) any later version.
 * 
 * This program  is distributed in the  hope that it will  be useful, but
 * WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
 * MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
 * General Public License for more details.
 * 
 * You  should have received  a copy  of the  GNU General  Public License
 * along  with  this  program;  if   not,  write  to  the  Free  Software
 * Foundation,  Inc.,  51  Franklin   Street,  Fifth  Floor,  Boston,  MA
 * 02110-1301, USA.
 * 
 * 
 * EXCEPTION TO THE TERMS AND CONDITIONS OF GNU GENERAL PUBLIC LICENSE
 * 
 * The  LICENSE  file may  contain  an  additional  clause as  a  special
 * exception  to the  terms  and  conditions of  the  GNU General  Public
 * License. This  clause, if  present, gives you  the permission  to link
 * this  program with  certain third-part  software and  to obtain,  as a
 * result, a  work based  on this program  that can be  distributed using
 * other license than the GNU General Public License.  If you modify this
 * program, you may extend this exception to your version of the program,
 * but you  are not obligated  to do so.   If you do  not wish to  do so,
 * delete this exception statement from your version.
 * 
 */

package br.usp.icmc.sgpc.beans;

import br.usp.icmc.sgpc.interfaces.SgpcBeanInterface;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Artur
 */
@Entity
@Table(name = "orcamento")
@NamedQueries({
    @NamedQuery(name = "OrcamentoBean.findAll", query = "SELECT o FROM OrcamentoBean o"),
    @NamedQuery(name = "OrcamentoBean.findById", query = "SELECT o FROM OrcamentoBean o WHERE o.id = :id"),
    @NamedQuery(name = "OrcamentoBean.findByValor", query = "SELECT o FROM OrcamentoBean o WHERE o.valor = :valor"),
    @NamedQuery(name = "OrcamentoBean.findByDataValidade", query = "SELECT o FROM OrcamentoBean o WHERE o.dataValidade = :dataValidade")})
public class OrcamentoBean implements Serializable, SgpcBeanInterface {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @SequenceGenerator(name = "id", sequenceName = "orcamento_id_orcamento_seq")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "id")
    @Column(name = "id_orcamento", nullable = false)
    private Integer id;

    @Basic(optional = false)
    @Column(name = "valor", nullable = false)
    private BigDecimal valor;

    @Basic(optional = false)
    @Column(name = "data_validade", nullable = false)
    @Temporal(TemporalType.DATE)
    private Date dataValidade;

    @JoinColumn(name = "fk_fornecedor", referencedColumnName = "id_fornecedor", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private FornecedorBean fkFornecedor;

    @JoinColumn(name = "fk_solicitacao_cotacao", referencedColumnName = "id_solicitacao_cotacao", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private SolicitacaoCotacaoBean fkSolicitacaoCotacao;

    @OneToOne(optional = true, fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_solicita_orcamento", referencedColumnName = "id_solicita_orcamento", nullable = false)
    private SolicitaOrcamentoBean fkSolicitaOrcamento;

    @Basic(optional = false)
    @Column(name = "prazo", nullable = false)
    private Integer prazo;

    public OrcamentoBean() {
    }

    public OrcamentoBean(Integer id) {
        this.id = id;
    }

    public OrcamentoBean(Integer id, BigDecimal valor, Date dataValidade) {
        this.id = id;
        this.valor = valor;
        this.dataValidade = dataValidade;
    }

    public Integer getPrazo() {
        return prazo;
    }

    public void setPrazo(Integer prazo) {
        this.prazo = prazo;
    }

    public SolicitaOrcamentoBean getFkSolicitaOrcamento() {
        return fkSolicitaOrcamento;
    }

    public void setFkSolicitaOrcamento(SolicitaOrcamentoBean fkSolicitaOrcamento) {
        this.fkSolicitaOrcamento = fkSolicitaOrcamento;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public BigDecimal getValor() {
        return valor;
    }

    public void setValor(BigDecimal valor) {
        this.valor = valor;
    }

    public Date getDataValidade() {
        return dataValidade;
    }

    public void setDataValidade(Date dataValidade) {
        this.dataValidade = dataValidade;
    }

    public FornecedorBean getFkFornecedor() {
        return fkFornecedor;
    }

    public void setFkFornecedor(FornecedorBean fkFornecedor) {
        this.fkFornecedor = fkFornecedor;
    }

    public SolicitacaoCotacaoBean getFkSolicitacaoCotacao() {
        return fkSolicitacaoCotacao;
    }

    public void setFkSolicitacaoCotacao(SolicitacaoCotacaoBean fkSolicitacaoCotacao) {
        this.fkSolicitacaoCotacao = fkSolicitacaoCotacao;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof OrcamentoBean)) {
            return false;
        }
        OrcamentoBean other = (OrcamentoBean) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.usp.icmc.sgpc.Orcamento[id=" + id + "]";
    }

}
