/*
 * Sistema de Gestão de Projetos Científicos - SGPC
 * 
 * 
 * Copyright  (C)  2010-2014  Instituto  de  Ciências  Matemáticas  e  de
 *                            Computação - ICMC/USP
 * 
 * 
 * This program is  free software; you can redistribute  it and/or modify
 * it under the  terms of the GNU General Public  License as published by
 * the Free Software Foundation; either  version 2 of the License, or (at
 * your option) any later version.
 * 
 * This program  is distributed in the  hope that it will  be useful, but
 * WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
 * MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
 * General Public License for more details.
 * 
 * You  should have received  a copy  of the  GNU General  Public License
 * along  with  this  program;  if   not,  write  to  the  Free  Software
 * Foundation,  Inc.,  51  Franklin   Street,  Fifth  Floor,  Boston,  MA
 * 02110-1301, USA.
 * 
 * 
 * EXCEPTION TO THE TERMS AND CONDITIONS OF GNU GENERAL PUBLIC LICENSE
 * 
 * The  LICENSE  file may  contain  an  additional  clause as  a  special
 * exception  to the  terms  and  conditions of  the  GNU General  Public
 * License. This  clause, if  present, gives you  the permission  to link
 * this  program with  certain third-part  software and  to obtain,  as a
 * result, a  work based  on this program  that can be  distributed using
 * other license than the GNU General Public License.  If you modify this
 * program, you may extend this exception to your version of the program,
 * but you  are not obligated  to do so.   If you do  not wish to  do so,
 * delete this exception statement from your version.
 * 
 */

package br.usp.icmc.sgpc.beans;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author Artur
 */
@Entity
@Table(name = "cities")
@NamedQueries({
    @NamedQuery(name = "MunicipioBean.findAll", query = "SELECT m FROM MunicipioBean m"),
    @NamedQuery(name = "MunicipioBean.findByNome", query = "SELECT m FROM MunicipioBean m WHERE m.nome = :nome"),
    @NamedQuery(name = "MunicipioBean.findByNomePopular", query = "SELECT m FROM MunicipioBean m WHERE m.nomePopular = :nomePopular")})
public class MunicipioBean implements Serializable {
    private static final long serialVersionUID = 1L;
    
    @Id
    @Basic(optional = false)
    @Column(name = "id_city", nullable = false)
    private Integer id;

    @Column(name = "city", length = 100)
    private String nome;

    @Column(name = "accent_city", length = 100)
    private String nomePopular;

    @JoinColumn(name = "fk_region", referencedColumnName = "code", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private EstadoBean fkEstado;

    @Column(name = "population")
    private BigDecimal populacao;

    @Column(name = "latitude")
    private BigDecimal latitude;

    @Column(name = "longitude")
    private BigDecimal longitude;

    public MunicipioBean() {
    }

    public EstadoBean getEstado() {
        return fkEstado;
    }

    public void setEstado(EstadoBean fkEstado) {
        this.fkEstado = fkEstado;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public BigDecimal getLatitude() {
        return latitude;
    }

    public void setLatitude(BigDecimal latitude) {
        this.latitude = latitude;
    }

    public BigDecimal getLongitude() {
        return longitude;
    }

    public void setLongitude(BigDecimal longitude) {
        this.longitude = longitude;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getNomePopular() {
        return nomePopular;
    }

    public void setNomePopular(String nomePopular) {
        this.nomePopular = nomePopular;
    }

    public BigDecimal getPopulacao() {
        return populacao;
    }

    public void setPopulacao(BigDecimal populacao) {
        this.populacao = populacao;
    }

    @Override
    public String toString() {
        return "br.usp.icmc.sgpc.MunicipioBean[id=" + id + "]";
    }

}
