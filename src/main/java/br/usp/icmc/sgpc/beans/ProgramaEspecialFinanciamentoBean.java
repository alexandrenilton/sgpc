/*
 * Sistema de Gestão de Projetos Científicos - SGPC
 * 
 * 
 * Copyright  (C)  2010-2014  Instituto  de  Ciências  Matemáticas  e  de
 *                            Computação - ICMC/USP
 * 
 * 
 * This program is  free software; you can redistribute  it and/or modify
 * it under the  terms of the GNU General Public  License as published by
 * the Free Software Foundation; either  version 2 of the License, or (at
 * your option) any later version.
 * 
 * This program  is distributed in the  hope that it will  be useful, but
 * WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
 * MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
 * General Public License for more details.
 * 
 * You  should have received  a copy  of the  GNU General  Public License
 * along  with  this  program;  if   not,  write  to  the  Free  Software
 * Foundation,  Inc.,  51  Franklin   Street,  Fifth  Floor,  Boston,  MA
 * 02110-1301, USA.
 * 
 * 
 * EXCEPTION TO THE TERMS AND CONDITIONS OF GNU GENERAL PUBLIC LICENSE
 * 
 * The  LICENSE  file may  contain  an  additional  clause as  a  special
 * exception  to the  terms  and  conditions of  the  GNU General  Public
 * License. This  clause, if  present, gives you  the permission  to link
 * this  program with  certain third-part  software and  to obtain,  as a
 * result, a  work based  on this program  that can be  distributed using
 * other license than the GNU General Public License.  If you modify this
 * program, you may extend this exception to your version of the program,
 * but you  are not obligated  to do so.   If you do  not wish to  do so,
 * delete this exception statement from your version.
 * 
 */

package br.usp.icmc.sgpc.beans;

import br.usp.icmc.sgpc.interfaces.SgpcBeanInterface;
import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 *
 * @author herick
 */
@Entity
@Table(name = "programas_especiais_financiamento")
@NamedQueries({
    @NamedQuery(name = "ProgramaEspecialFinanciamentoBean.findAll", query = "SELECT a FROM ProgramaEspecialFinanciamentoBean a"),
    @NamedQuery(name = "ProgramaEspecialFinanciamentoBean.findByNome", query = "SELECT a FROM ProgramaEspecialFinanciamentoBean a WHERE a.nome = :nome"),
    @NamedQuery(name = "ProgramaEspecialFinanciamentoBean.findByKeyword", query = "SELECT a FROM ProgramaEspecialFinanciamentoBean a WHERE upper(a.nome) LIKE upper(:keyword) OR upper(a.descricao) LIKE upper(:keyword)"),
    @NamedQuery(name = "ProgramaEspecialFinanciamentoBean.findById", query = "SELECT a FROM ProgramaEspecialFinanciamentoBean a WHERE a.id = :id")})
public class ProgramaEspecialFinanciamentoBean implements Serializable, SgpcBeanInterface{

    @Id
    @Basic(optional = false)
    @SequenceGenerator(name = "id", sequenceName = "programas_especiais_financiam_id_programa_especial_financia_seq")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "id")
    @Column(name = "id_programa_especial_financiamento", nullable = false)
    private Integer id;

    @Basic(optional = false)
    @Column(name = "nome", nullable = false, length = 100)
    private String nome;

    @Column(name = "descricao", nullable = true, length = 100)
    private String descricao;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public ProgramaEspecialFinanciamentoBean(){
    }
    
    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProgramaEspecialFinanciamentoBean)) {
            return false;
        }
        ProgramaEspecialFinanciamentoBean other = (ProgramaEspecialFinanciamentoBean) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return this.getNome();
    }
}
