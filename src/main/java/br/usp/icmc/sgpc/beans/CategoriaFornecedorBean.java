/*
 * Sistema de Gestão de Projetos Científicos - SGPC
 * 
 * 
 * Copyright  (C)  2010-2014  Instituto  de  Ciências  Matemáticas  e  de
 *                            Computação - ICMC/USP
 * 
 * 
 * This program is  free software; you can redistribute  it and/or modify
 * it under the  terms of the GNU General Public  License as published by
 * the Free Software Foundation; either  version 2 of the License, or (at
 * your option) any later version.
 * 
 * This program  is distributed in the  hope that it will  be useful, but
 * WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
 * MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
 * General Public License for more details.
 * 
 * You  should have received  a copy  of the  GNU General  Public License
 * along  with  this  program;  if   not,  write  to  the  Free  Software
 * Foundation,  Inc.,  51  Franklin   Street,  Fifth  Floor,  Boston,  MA
 * 02110-1301, USA.
 * 
 * 
 * EXCEPTION TO THE TERMS AND CONDITIONS OF GNU GENERAL PUBLIC LICENSE
 * 
 * The  LICENSE  file may  contain  an  additional  clause as  a  special
 * exception  to the  terms  and  conditions of  the  GNU General  Public
 * License. This  clause, if  present, gives you  the permission  to link
 * this  program with  certain third-part  software and  to obtain,  as a
 * result, a  work based  on this program  that can be  distributed using
 * other license than the GNU General Public License.  If you modify this
 * program, you may extend this exception to your version of the program,
 * but you  are not obligated  to do so.   If you do  not wish to  do so,
 * delete this exception statement from your version.
 * 
 */

package br.usp.icmc.sgpc.beans;

import br.usp.icmc.sgpc.interfaces.SgpcBeanInterface;
import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 *
 * @author ademilson
 */
@Entity
@Table(name = "categoria_fornecedor")
@NamedQueries({
    @NamedQuery(name = "CategoriaFornecedorBean.findAll", query = "SELECT c FROM CategoriaFornecedorBean c"),
    @NamedQuery(name = "CategoriaFornecedorBean.findById", query = "SELECT c FROM CategoriaFornecedorBean c WHERE c.id = :id")})
public class CategoriaFornecedorBean implements Serializable, SgpcBeanInterface{
    private static final long serialVersionUID = 1L;

    @Id
    @Basic(optional = false)
    @SequenceGenerator(name = "id", sequenceName = "categoria_fornecedor_id_categoria_fornecedor_seq")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "id")
    @Column(name = "id_categoria_fornecedor", nullable = false)
    private Integer id;

    @JoinColumn(name = "fk_fornecedor", referencedColumnName = "id_fornecedor", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private FornecedorBean fkFornecedor;

    @JoinColumn(name = "fk_grupo_fornecimento", referencedColumnName = "id_grupo_fornecimento", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private GrupoFornecimentoBean fkGrupoFornecimento;

    @OneToOne(optional= true, fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_representante", referencedColumnName = "id_representante_vendas", nullable = true)
    private RepresentanteVendasBean fkRepresentante;

    public CategoriaFornecedorBean() {
    }

    public RepresentanteVendasBean getFkRepresentante() {
        return fkRepresentante;
    }

    public void setFkRepresentante(RepresentanteVendasBean fkRepresentante) {
        this.fkRepresentante = fkRepresentante;
    }

    public FornecedorBean getFkFornecedor() {
        return fkFornecedor;
    }

    public void setFkFornecedor(FornecedorBean fkFornecedor) {
        this.fkFornecedor = fkFornecedor;
    }

    public GrupoFornecimentoBean getFkGrupoFornecimento() {
        return fkGrupoFornecimento;
    }

    public void setFkGrupoFornecimento(GrupoFornecimentoBean fkGrupoFornecimento) {
        this.fkGrupoFornecimento = fkGrupoFornecimento;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    
    @Override
    public String toString() {
        return "br.usp.icmc.sgpc.CategoriaFornecedor[id=" + id + "]";
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CategoriaFornecedorBean)) {
            return false;
        }
        CategoriaFornecedorBean other = (CategoriaFornecedorBean) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }
}
