/*
 * Sistema de Gestão de Projetos Científicos - SGPC
 * 
 * 
 * Copyright  (C)  2010-2014  Instituto  de  Ciências  Matemáticas  e  de
 *                            Computação - ICMC/USP
 * 
 * 
 * This program is  free software; you can redistribute  it and/or modify
 * it under the  terms of the GNU General Public  License as published by
 * the Free Software Foundation; either  version 2 of the License, or (at
 * your option) any later version.
 * 
 * This program  is distributed in the  hope that it will  be useful, but
 * WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
 * MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
 * General Public License for more details.
 * 
 * You  should have received  a copy  of the  GNU General  Public License
 * along  with  this  program;  if   not,  write  to  the  Free  Software
 * Foundation,  Inc.,  51  Franklin   Street,  Fifth  Floor,  Boston,  MA
 * 02110-1301, USA.
 * 
 * 
 * EXCEPTION TO THE TERMS AND CONDITIONS OF GNU GENERAL PUBLIC LICENSE
 * 
 * The  LICENSE  file may  contain  an  additional  clause as  a  special
 * exception  to the  terms  and  conditions of  the  GNU General  Public
 * License. This  clause, if  present, gives you  the permission  to link
 * this  program with  certain third-part  software and  to obtain,  as a
 * result, a  work based  on this program  that can be  distributed using
 * other license than the GNU General Public License.  If you modify this
 * program, you may extend this exception to your version of the program,
 * but you  are not obligated  to do so.   If you do  not wish to  do so,
 * delete this exception statement from your version.
 * 
 */

package br.usp.icmc.sgpc.beans;

import br.usp.icmc.sgpc.interfaces.SgpcBeanInterface;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Artur
 */
@Entity
@Table(name = "atividade")
@NamedQueries({
    @NamedQuery(name = "AtividadeBean.findAll", query = "SELECT a FROM AtividadeBean a"),
    @NamedQuery(name = "AtividadeBean.findByTitulo", query = "SELECT a FROM AtividadeBean a WHERE a.titulo = :titulo"),
    @NamedQuery(name = "AtividadeBean.findByDescricao", query = "SELECT a FROM AtividadeBean a WHERE a.descricao = :descricao"),
    @NamedQuery(name = "AtividadeBean.findByHoraInicial", query = "SELECT a FROM AtividadeBean a WHERE a.horaInicial = :horaInicial"),
    @NamedQuery(name = "AtividadeBean.findByEvento", query = "SELECT a FROM AtividadeBean a WHERE a.fkEvento = :fkEvento"),
    @NamedQuery(name = "AtividadeBean.findByHoraFinal", query = "SELECT a FROM AtividadeBean a WHERE a.horaFinal = :horaFinal"),
    @NamedQuery(name = "AtividadeBean.findByKeyword", query = "SELECT a FROM AtividadeBean a WHERE upper(a.titulo) LIKE upper (:keyword) OR upper (a.descricao) LIKE upper (:keyword)"),
    @NamedQuery(name = "AtividadeBean.findById", query = "SELECT a FROM AtividadeBean a WHERE a.id = :id")})
public class AtividadeBean implements Serializable, SgpcBeanInterface {
    private static final long serialVersionUID = 1L;

    @Basic(optional = false)
    @Column(name = "titulo", nullable = false, length = 50)
    private String titulo;

    @Basic(optional = false)
    @Column(name = "descricao", nullable = false, length = 1000)
    private String descricao;

    @Basic(optional = false)
    @Column(name = "hora_inicial", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date horaInicial;

    @Basic(optional = false)
    @Column(name = "hora_final", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date horaFinal;

    @Id
    @Basic(optional = false)
    @SequenceGenerator(name="id", sequenceName="atividade_id_atividade_seq")
    @GeneratedValue (strategy=GenerationType.AUTO, generator="id")
    @Column(name = "id_atividade", nullable = false)
    private Integer id;

    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH}, fetch = FetchType.LAZY)
    @JoinTable(name = "inscricao_atividade",
        joinColumns =
            @JoinColumn(name = "fk_atividade", referencedColumnName = "id_atividade"),
        inverseJoinColumns =
            @JoinColumn(name = "fk_pessoa", referencedColumnName = "id_pessoa")
    )
    private List<PessoaBean> pessoasInscritas;

    @JoinColumn(name = "fk_organizacao_evento", referencedColumnName = "id_projeto", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private EventoBean fkEvento;

    public AtividadeBean() {
        pessoasInscritas = new ArrayList<PessoaBean>();
    }

    public List<PessoaBean> getPessoasInscritas() {
        return pessoasInscritas;
    }

    public void setPessoasInscritas(List<PessoaBean> pessoasInscritas) {
        this.pessoasInscritas = pessoasInscritas;
    }
    
    public AtividadeBean(Integer id) {
        this.id = id;
    }

    public AtividadeBean(Integer id, String titulo, String descricao, Date horaInicial, Date horaFinal) {
        this.id = id;
        this.titulo = titulo;
        this.descricao = descricao;
        this.horaInicial = horaInicial;
        this.horaFinal = horaFinal;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Date getHoraInicial() {
        return horaInicial;
    }

    public void setHoraInicial(Date horaInicial) {
        this.horaInicial = horaInicial;
    }

    public Date getHoraFinal() {
        return horaFinal;
    }

    public void setHoraFinal(Date horaFinal) {
        this.horaFinal = horaFinal;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public EventoBean getFkEvento() {
        return fkEvento;
    }

    public void setFkEvento(EventoBean fkEvento) {
        this.fkEvento = fkEvento;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AtividadeBean)) {
            return false;
        }
        AtividadeBean other = (AtividadeBean) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return this.titulo;
    }

}
