/*
 * Sistema de Gestão de Projetos Científicos - SGPC
 * 
 * 
 * Copyright  (C)  2010-2014  Instituto  de  Ciências  Matemáticas  e  de
 *                            Computação - ICMC/USP
 * 
 * 
 * This program is  free software; you can redistribute  it and/or modify
 * it under the  terms of the GNU General Public  License as published by
 * the Free Software Foundation; either  version 2 of the License, or (at
 * your option) any later version.
 * 
 * This program  is distributed in the  hope that it will  be useful, but
 * WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
 * MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
 * General Public License for more details.
 * 
 * You  should have received  a copy  of the  GNU General  Public License
 * along  with  this  program;  if   not,  write  to  the  Free  Software
 * Foundation,  Inc.,  51  Franklin   Street,  Fifth  Floor,  Boston,  MA
 * 02110-1301, USA.
 * 
 * 
 * EXCEPTION TO THE TERMS AND CONDITIONS OF GNU GENERAL PUBLIC LICENSE
 * 
 * The  LICENSE  file may  contain  an  additional  clause as  a  special
 * exception  to the  terms  and  conditions of  the  GNU General  Public
 * License. This  clause, if  present, gives you  the permission  to link
 * this  program with  certain third-part  software and  to obtain,  as a
 * result, a  work based  on this program  that can be  distributed using
 * other license than the GNU General Public License.  If you modify this
 * program, you may extend this exception to your version of the program,
 * but you  are not obligated  to do so.   If you do  not wish to  do so,
 * delete this exception statement from your version.
 * 
 */

package br.usp.icmc.sgpc.fmw;

import java.util.List;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.orm.jpa.EntityManagerFactoryUtils;

/**
 *
 * @author Artur
 */
public class GenericJpaDao<tipo> {

    private Class<? extends tipo> clazz = null;
    //@PersistenceContext
    private EntityManager entityManager = null;
    protected Logger logger = null;

    public GenericJpaDao(Class<? extends tipo> clazz) {
        init(clazz,"sgpcEntityManagerFactory");
    }

    public GenericJpaDao(Class<? extends tipo> clazz, String EMF) {
        init(clazz,EMF);
    }
    
    private void init(Class<? extends tipo> clazz, String EMF){
        this.clazz = clazz;
        FacesContext fc = FacesContext.getCurrentInstance();
        ApplicationContext ctx = ApplicationContextProvider.getApplicationContext();
        EntityManagerFactory emf = (EntityManagerFactory) ctx.getBean(EMF);
        entityManager = emf.createEntityManager();
        EntityManagerFactoryUtils.getTransactionalEntityManager(emf);
        logger = Logger.getLogger(this.getClass());
    }

    public EntityManager getEntityManager() {
        return this.entityManager;
    }

    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public tipo create(tipo p) {
        EntityManager em = null;

        em = getEntityManager();
        em.getTransaction().begin();
        //em.persist(p);
        p = em.merge(p);
        em.getTransaction().commit();
        return p;
    }

    public tipo update(tipo p) {
        EntityManager em = null;

        em = getEntityManager();
        em.getTransaction().begin();
        p = em.merge(p);
        em.getTransaction().commit();
        return p;

    }

    public void delete(tipo p) {
        EntityManager em = null;

        em = getEntityManager();
        em.getTransaction().begin();
        Object c = em.merge(p);
        em.remove(c);
        em.getTransaction().commit();

    }

    public tipo findEntity(Object id) {
        if(id==null) return null;
        EntityManager em = getEntityManager();

        em.getTransaction().begin();
        tipo retorno = em.find(clazz, id);
        em.getTransaction().commit();
        return retorno;

    }

    public List<tipo> findEntities() {
        return findEntities(true, -1, -1);
    }

    public List<tipo> findEntities(int maxResults, int firstResult) {
        return findEntities(false, maxResults, firstResult);
    }

    public List<tipo> findEntities(boolean all, int maxResults, int firstResult, String... ordenacoes) {
        EntityManager em = getEntityManager();

        em.getTransaction().begin();
        StringBuffer sql = new StringBuffer();
        sql.append("select o from " + clazz.getSimpleName() + " o ");
        if (ordenacoes != null && ordenacoes.length > 0) {
            sql.append("ORDER BY");
            for (int i = 0; i < ordenacoes.length; i++) {
                sql.append(" " + ordenacoes[i]);
                if (i < ordenacoes.length - 1) {
                    sql.append(",");
                }
            }
        }
        Query q = em.createQuery(sql.toString());
        if (!all) {
            q.setMaxResults(maxResults);
            q.setFirstResult(firstResult);
        }
        em.getTransaction().commit();
        return q.getResultList();

    }
    
    public void deleteAll(){
        EntityManager em = null;

        em = getEntityManager();
        em.getTransaction().begin();
        Query query=em.createQuery("DELETE FROM " + clazz.getSimpleName() + " o");
        int deleteRecords=query.executeUpdate();
        em.getTransaction().commit();
        logger.debug("Deleted records:" + deleteRecords);
    }
}
