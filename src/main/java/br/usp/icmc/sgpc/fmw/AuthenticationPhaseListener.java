/*
 * Sistema de Gestão de Projetos Científicos - SGPC
 * 
 * 
 * Copyright  (C)  2010-2014  Instituto  de  Ciências  Matemáticas  e  de
 *                            Computação - ICMC/USP
 * 
 * 
 * This program is  free software; you can redistribute  it and/or modify
 * it under the  terms of the GNU General Public  License as published by
 * the Free Software Foundation; either  version 2 of the License, or (at
 * your option) any later version.
 * 
 * This program  is distributed in the  hope that it will  be useful, but
 * WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
 * MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
 * General Public License for more details.
 * 
 * You  should have received  a copy  of the  GNU General  Public License
 * along  with  this  program;  if   not,  write  to  the  Free  Software
 * Foundation,  Inc.,  51  Franklin   Street,  Fifth  Floor,  Boston,  MA
 * 02110-1301, USA.
 * 
 * 
 * EXCEPTION TO THE TERMS AND CONDITIONS OF GNU GENERAL PUBLIC LICENSE
 * 
 * The  LICENSE  file may  contain  an  additional  clause as  a  special
 * exception  to the  terms  and  conditions of  the  GNU General  Public
 * License. This  clause, if  present, gives you  the permission  to link
 * this  program with  certain third-part  software and  to obtain,  as a
 * result, a  work based  on this program  that can be  distributed using
 * other license than the GNU General Public License.  If you modify this
 * program, you may extend this exception to your version of the program,
 * but you  are not obligated  to do so.   If you do  not wish to  do so,
 * delete this exception statement from your version.
 * 
 */

package br.usp.icmc.sgpc.fmw;

import br.usp.icmc.sgpc.beans.FornecedorBean;
import br.usp.icmc.sgpc.beans.PessoaBean;
import br.usp.icmc.sgpc.jsf.UserSessionManagedBean;
import java.util.Map;
import javax.faces.application.NavigationHandler;
import javax.faces.context.FacesContext;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;
import org.apache.log4j.Logger;

/**
 *
 * @author Artur
 *
 * http://www.rodrigolazoti.com.br/pt/2008/09/01/filtrando-usuarios-logados-em-jsf-com-phaselistener/
 */
public class AuthenticationPhaseListener implements PhaseListener {

    private static final Logger logger = Logger.getLogger(AuthenticationPhaseListener.class);

    public void afterPhase(PhaseEvent event) {
        logger.debug("Verificando se usuário está logado...");

        //FacesContext ctx = FacesContext.getCurrentInstance();
        FacesContext facesContext = event.getFacesContext();
        String currentPage = facesContext.getViewRoot().getViewId();

        boolean isLoginPage = (currentPage.lastIndexOf("login.xhtml") > -1);
        boolean isLicitacaoPage = (currentPage.lastIndexOf("licitacao.xhtml") > -1);
        boolean isOrcamentoPage = (currentPage.lastIndexOf("orcamento.xhtml") > -1);
        boolean isOauthPage = (currentPage.lastIndexOf("retornoOauth.xhtml") > -1);

//        HttpSession session = (HttpSession) facesContext.getExternalContext().getSession(true);
//        Object currentUser = session.getAttribute("currentUser");

        Map sessionMap = facesContext.getExternalContext().getSessionMap();
        UserSessionManagedBean userSessionMB = (UserSessionManagedBean) sessionMap.get("userSessionMB");
        PessoaBean pessoa = null;
        FornecedorBean fornecedor = null;

        if (userSessionMB != null) {
            pessoa = userSessionMB.getLoggedUser();
            fornecedor = userSessionMB.getFornecedor();
        }
        
        if(isOauthPage) return;

        if (!isOrcamentoPage && !isLicitacaoPage && !isLoginPage && pessoa == null) {
            NavigationHandler nh = facesContext.getApplication().getNavigationHandler();
            nh.handleNavigation(facesContext, null, "loginPage");
        }

        if (isOrcamentoPage && fornecedor == null) {
            NavigationHandler nh = facesContext.getApplication().getNavigationHandler();
            nh.handleNavigation(facesContext, null, "licitacaoPage");
        }

    }

    public void beforePhase(PhaseEvent event) {
    }

    public PhaseId getPhaseId() {
        return PhaseId.RESTORE_VIEW;
    }
}
