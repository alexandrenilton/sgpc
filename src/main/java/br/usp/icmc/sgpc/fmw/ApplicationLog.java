/*
 * Sistema de Gestão de Projetos Científicos - SGPC
 * 
 * 
 * Copyright  (C)  2010-2014  Instituto  de  Ciências  Matemáticas  e  de
 *                            Computação - ICMC/USP
 * 
 * 
 * This program is  free software; you can redistribute  it and/or modify
 * it under the  terms of the GNU General Public  License as published by
 * the Free Software Foundation; either  version 2 of the License, or (at
 * your option) any later version.
 * 
 * This program  is distributed in the  hope that it will  be useful, but
 * WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
 * MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
 * General Public License for more details.
 * 
 * You  should have received  a copy  of the  GNU General  Public License
 * along  with  this  program;  if   not,  write  to  the  Free  Software
 * Foundation,  Inc.,  51  Franklin   Street,  Fifth  Floor,  Boston,  MA
 * 02110-1301, USA.
 * 
 * 
 * EXCEPTION TO THE TERMS AND CONDITIONS OF GNU GENERAL PUBLIC LICENSE
 * 
 * The  LICENSE  file may  contain  an  additional  clause as  a  special
 * exception  to the  terms  and  conditions of  the  GNU General  Public
 * License. This  clause, if  present, gives you  the permission  to link
 * this  program with  certain third-part  software and  to obtain,  as a
 * result, a  work based  on this program  that can be  distributed using
 * other license than the GNU General Public License.  If you modify this
 * program, you may extend this exception to your version of the program,
 * but you  are not obligated  to do so.   If you do  not wish to  do so,
 * delete this exception statement from your version.
 * 
 */

/**
 * Instituto de Ciencias Matematicas e de Computacao
 * Universidade de Sao Paulo
 * ICMC/USP 2010 - http://www.icmc.usp.br/
 *
 * Artur Sampaio
 * Analista de Sistemas - STI
 * artur@icmc.usp.br
 * 01/04/2010
 *
 */
package br.usp.icmc.sgpc.fmw;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.InetAddress;
import java.net.UnknownHostException;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

/**
 *
 * @author Artur
 */
public class ApplicationLog {
    private static String hostName;

    static{
        try {
//            System.out.println("Inicializando ApplicationLog...");
            InetAddress host = InetAddress.getLocalHost();
            hostName = host.getHostName();

//            System.out.println("Lendo log4j.properties...");
//            Properties props = new Properties();
//            try {
//                props.load(new FileInputStream("log4j.properties"));
//                PropertyConfigurator.configure(props);
//            }catch(FileNotFoundException e){
//                e.printStackTrace();
//            }catch(IOException e){
//                e.printStackTrace();
//            }

            //PropertyConfigurator.configure("log4j.properties");
        } catch (UnknownHostException e) {
            hostName = "";
            System.out.println("Erro buscando nome do Host para o ApplicationLog!");
        }
    }

    /*
     * <p>Identificar nome da classe, nome do metodo e linha que gerou a excecao.</p>
     * @created 01/04/2010
     * @since 1.0
     * @param emissao   EmissaoMcoBean[] MCO's a serem consultados
     * @return String   StackTrace
     * @throws br.usp.icmc.fmw.exception.USPException
     */
    public static String getStackTrace(Throwable e) {
        if (e == null) {
            return null;
        }
        String strStackTrace;
        try {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            strStackTrace = sw.toString();
            sw.close();
            return strStackTrace;
        } catch (Exception e2) {
            return "Error getting StackTrace:" + e2.toString();
        }
    }

    public static void fatal(String str) {
        String classe = identificaClasse(new Throwable());
        String cmn = getClassMethodAndNumber();
        String conteudo = cmn + " - " + str;
        log(Level.FATAL, conteudo, classe);
    }

    public static void error(String str) {
        String classe = identificaClasse(new Throwable());
        String cmn = getClassMethodAndNumber();
        String conteudo = cmn + " - " + str;
        log(Level.ERROR, conteudo, classe);
    }

    public static void warn(String str) {
        String classe = identificaClasse(new Throwable());
        String cmn = getClassMethodAndNumber();
        String conteudo = cmn + " - " + str;
        log(Level.WARN, conteudo, classe);
    }

    public static void info(String str) {
        String classe = identificaClasse(new Throwable());
        String cmn = getClassMethodAndNumber();
        //String conteudo = cmn + " - " + str;
        String conteudo = str;
        log(Level.INFO, conteudo, classe);
    }

    public static void debug(String str) {
        String classe = identificaClasse(new Throwable());
        String cmn = getClassMethodAndNumber();
        String conteudo = cmn + " - " + str;
        log(Level.DEBUG, conteudo, classe);
    }

    public static void fatal(Throwable e) {
        String classe = identificaClasse(e);
        String conteudo = montaConteudo(e);
        log(Level.FATAL, conteudo, classe);
    }

    public static void error(Throwable e) {
        String classe = identificaClasse(e);
        String conteudo = montaConteudo(e);
        log(Level.ERROR, conteudo, classe);
    }

    public static void warn(Throwable e) {
        String classe = identificaClasse(e);
        String conteudo = montaConteudo(e);
        log(Level.WARN, conteudo, classe);
    }
    
    private static String identificaClasse(Throwable e){
        String classe = null;
        StackTraceElement elements[] = e.getStackTrace();
        if (elements != null && elements.length > 0) {
            StackTraceElement element = elements[1];
            classe = element.getClassName();
        }
        return classe;
    }

    private static String montaConteudo(Throwable e){
        String cmn = getClassMethodAndNumber(e);
        String conteudo = hostName + " - " + cmn + " - " + getStackTrace(e);
        return conteudo;
    }

    private static String getClassMethodAndNumber() {
        return getClassMethodAndNumber(new Throwable());
    }

    /*
     * <p>Identificar nome da classe, nome do metodo e linha que gerou a excecao.</p>
     * @created 01/04/2010
     * @since 1.0
     * @param emissao   EmissaoMcoBean[] MCO's a serem consultados
     * @return String   Nome da classe.nome do metodo:numero da linha
     * @throws br.usp.icmc.fmw.exception.USPException
     */
    public static String getClassMethodAndNumber(Throwable t) {
        if (t == null) {
            t = new Throwable();
        }
        StackTraceElement elements[] = t.getStackTrace();
        if (elements != null && elements.length > 0) {
            StackTraceElement element = elements[4];
            StringBuffer sb =
                    (new StringBuffer(element.getClassName())).append(".").append(element.getMethodName()).append(":").append(element.getLineNumber());
            System.out.println("getClassMethodAndNumber: " + sb.toString());
            return sb.toString();
        } else {
            return "ERROR! Stack trace is null!";
        }
    }

    private static void log(Level level, String mensagem, String classe){
        Logger logger = Logger.getLogger("br.usp.icmc.fmw.util.ApplicationLog");
        if(classe != null){
            logger = Logger.getLogger(classe);
        }
        if (logger.isEnabledFor(level)) {
            switch(level.toInt()){
                case Level.DEBUG_INT:
                    logger.debug(mensagem);
                    break;
                case Level.INFO_INT:
                    logger.info(mensagem);
                    break;
                case Level.WARN_INT:
                    logger.warn(mensagem);
                    break;
                case Level.ERROR_INT:
                    logger.error(mensagem);
                    break;
                case Level.FATAL_INT:
                    logger.fatal(mensagem);
                    break;
                default:
                    break;
            }
        }
    }
}