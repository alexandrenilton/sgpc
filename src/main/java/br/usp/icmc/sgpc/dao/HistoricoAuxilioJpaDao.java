/*
 * Sistema de Gestão de Projetos Científicos - SGPC
 * 
 * 
 * Copyright  (C)  2010-2014  Instituto  de  Ciências  Matemáticas  e  de
 *                            Computação - ICMC/USP
 * 
 * 
 * This program is  free software; you can redistribute  it and/or modify
 * it under the  terms of the GNU General Public  License as published by
 * the Free Software Foundation; either  version 2 of the License, or (at
 * your option) any later version.
 * 
 * This program  is distributed in the  hope that it will  be useful, but
 * WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
 * MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
 * General Public License for more details.
 * 
 * You  should have received  a copy  of the  GNU General  Public License
 * along  with  this  program;  if   not,  write  to  the  Free  Software
 * Foundation,  Inc.,  51  Franklin   Street,  Fifth  Floor,  Boston,  MA
 * 02110-1301, USA.
 * 
 * 
 * EXCEPTION TO THE TERMS AND CONDITIONS OF GNU GENERAL PUBLIC LICENSE
 * 
 * The  LICENSE  file may  contain  an  additional  clause as  a  special
 * exception  to the  terms  and  conditions of  the  GNU General  Public
 * License. This  clause, if  present, gives you  the permission  to link
 * this  program with  certain third-part  software and  to obtain,  as a
 * result, a  work based  on this program  that can be  distributed using
 * other license than the GNU General Public License.  If you modify this
 * program, you may extend this exception to your version of the program,
 * but you  are not obligated  to do so.   If you do  not wish to  do so,
 * delete this exception statement from your version.
 * 
 */

package br.usp.icmc.sgpc.dao;

import br.usp.icmc.sgpc.beans.AuxilioBean;
import br.usp.icmc.sgpc.beans.AuxilioBean_;
import br.usp.icmc.sgpc.beans.HistoricoAuxilioBean;
import br.usp.icmc.sgpc.beans.HistoricoAuxilioBean_;
import br.usp.icmc.sgpc.beans.PessoaBean;
import br.usp.icmc.sgpc.beans.PessoaBean_;
import br.usp.icmc.sgpc.fmw.GenericJpaDao;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

/**
 *
 * @author Artur
 */
public class HistoricoAuxilioJpaDao extends GenericJpaDao<HistoricoAuxilioBean> {

    public HistoricoAuxilioJpaDao() {
        super(HistoricoAuxilioBean.class);
    }
    
    public List<String> getUniqueActions(){
        List<String> retorno = null;
        EntityManager em = getEntityManager();
        try {
            Query queryUniqueActions = em.createQuery("SELECT DISTINCT(h.acao) FROM HistoricoAuxilioBean h");
            retorno = queryUniqueActions.getResultList();
        } catch (NoResultException nre) {
            logger.debug("Registros não encontrados");
        } catch (Exception e) {
            logger.error(e);
        }
        return retorno;
    }
    
    public List<HistoricoAuxilioBean> pesquisar(int first, int pageSize, AuxilioBean auxilio, Date dataInicial, Date dataFinal, String alinea, String subcentro, int operacao, PessoaBean responsavel, String acao){
        List<HistoricoAuxilioBean> historicos = null;
        EntityManager em = getEntityManager();
        CriteriaBuilder builder = em.getCriteriaBuilder();
        
        CriteriaQuery<HistoricoAuxilioBean> criteria = builder.createQuery(HistoricoAuxilioBean.class);
        Root<HistoricoAuxilioBean> root = criteria.from(HistoricoAuxilioBean.class);
        Join<HistoricoAuxilioBean, PessoaBean> pessoaResponsavelHistorico = root.join(HistoricoAuxilioBean_.fkResponsavel);
        Join<HistoricoAuxilioBean, AuxilioBean> auxilioAssociado = root.join(HistoricoAuxilioBean_.fkAuxilio);
        
        List<Predicate> predicados = new ArrayList<Predicate>();
        
        if(auxilio!=null){
            Predicate auxilioP = builder.equal(auxilioAssociado.get(AuxilioBean_.id), auxilio.getId());
            predicados.add(auxilioP);
        }
        
        if(dataInicial != null){
            Predicate dataIP = builder.greaterThanOrEqualTo(root.get(HistoricoAuxilioBean_.dataAlteracao), dataInicial);
            predicados.add(dataIP);
        }
        
        if(dataFinal != null){
            Calendar calendar=Calendar.getInstance();
            calendar.setTime(dataFinal);
            calendar.set(Calendar.HOUR_OF_DAY, 23);
            calendar.set(Calendar.MINUTE, 59);
            dataFinal=calendar.getTime();
            Predicate dataFP = builder.lessThanOrEqualTo(root.get(HistoricoAuxilioBean_.dataAlteracao), dataFinal);
            predicados.add(dataFP);
        }
        
        if(alinea != null){
            Predicate nomeAlineaP = builder.like(builder.upper(root.get(HistoricoAuxilioBean_.nomeAlinea)), (alinea).toUpperCase());
            predicados.add(nomeAlineaP);
        }
        
        if(subcentro != null){
            Predicate nomeSubcentroP = builder.like(builder.upper(root.get(HistoricoAuxilioBean_.nomeCentroDespesas)), (subcentro).toUpperCase());
            predicados.add(nomeSubcentroP);
        }
        
        if(operacao != 0){
            Predicate operacaoP = builder.equal(root.get(HistoricoAuxilioBean_.operacao), operacao);
            predicados.add(operacaoP);
        }
        
        if(responsavel != null){
            Predicate responsavelP = builder.equal(pessoaResponsavelHistorico.get(PessoaBean_.id), responsavel.getId());
            predicados.add(responsavelP);
        }
        
        if(acao!=null){
            Predicate acaoP = builder.equal(root.get(HistoricoAuxilioBean_.acao) , acao);
            predicados.add(acaoP);
        }
        predicados.add(builder.equal(root.get(HistoricoAuxilioBean_.editado), false));
        
        criteria.where(builder.and(predicados.toArray(new Predicate[]{})));
        criteria.orderBy(builder.desc(root.get(HistoricoAuxilioBean_.dataAlteracao)),builder.desc(root.get(HistoricoAuxilioBean_.id)));
        
        Query queryHistorico = em.createQuery(criteria);
        queryHistorico.setFirstResult(first);
        if(pageSize>0){
            queryHistorico.setMaxResults(pageSize);
        }
        historicos = queryHistorico.getResultList();
        
        return historicos;
    }
    
}
