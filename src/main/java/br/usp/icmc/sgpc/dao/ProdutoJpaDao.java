/*
 * Sistema de Gestão de Projetos Científicos - SGPC
 * 
 * 
 * Copyright  (C)  2010-2014  Instituto  de  Ciências  Matemáticas  e  de
 *                            Computação - ICMC/USP
 * 
 * 
 * This program is  free software; you can redistribute  it and/or modify
 * it under the  terms of the GNU General Public  License as published by
 * the Free Software Foundation; either  version 2 of the License, or (at
 * your option) any later version.
 * 
 * This program  is distributed in the  hope that it will  be useful, but
 * WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
 * MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
 * General Public License for more details.
 * 
 * You  should have received  a copy  of the  GNU General  Public License
 * along  with  this  program;  if   not,  write  to  the  Free  Software
 * Foundation,  Inc.,  51  Franklin   Street,  Fifth  Floor,  Boston,  MA
 * 02110-1301, USA.
 * 
 * 
 * EXCEPTION TO THE TERMS AND CONDITIONS OF GNU GENERAL PUBLIC LICENSE
 * 
 * The  LICENSE  file may  contain  an  additional  clause as  a  special
 * exception  to the  terms  and  conditions of  the  GNU General  Public
 * License. This  clause, if  present, gives you  the permission  to link
 * this  program with  certain third-part  software and  to obtain,  as a
 * result, a  work based  on this program  that can be  distributed using
 * other license than the GNU General Public License.  If you modify this
 * program, you may extend this exception to your version of the program,
 * but you  are not obligated  to do so.   If you do  not wish to  do so,
 * delete this exception statement from your version.
 * 
 */

package br.usp.icmc.sgpc.dao;

import br.usp.icmc.sgpc.beans.GrupoFornecimentoBean;
import br.usp.icmc.sgpc.beans.ProdutoBean;
import br.usp.icmc.sgpc.beans.ProdutoBean_;
import br.usp.icmc.sgpc.fmw.GenericJpaDao;
import javax.persistence.NoResultException;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

/**
 *
 * @author david
 */
public class ProdutoJpaDao extends GenericJpaDao<ProdutoBean> {

    public ProdutoJpaDao() {
        super(ProdutoBean.class);
    }

    public List<ProdutoBean> pesquisar(String termoPesquisa) {
        List<ProdutoBean> produtos = null;
        EntityManager em = getEntityManager();

        try {
            Query queryProdutosByKeyword = em.createNamedQuery("ProdutoBean.findByKeyword");
            queryProdutosByKeyword.setParameter("keyword", "%" + termoPesquisa + "%");
            produtos = queryProdutosByKeyword.getResultList();
        } catch (NoResultException nre) {
            logger.debug("Produto não cadastrado");
//            throw new USPException("Usuário não cadastrado");
        } catch (Exception e) {
            logger.error(e);
//            throw new USPException(e);
        }
        return produtos;
    }

    public List<ProdutoBean> pesquisarCriteria(String termoPesquisa, GrupoFornecimentoBean grupo) {
        List<ProdutoBean> produtos = null;
        EntityManager em = getEntityManager();
        CriteriaBuilder builder = em.getCriteriaBuilder();

        CriteriaQuery<ProdutoBean> criteria = builder.createQuery(ProdutoBean.class);
        Root<ProdutoBean> root = criteria.from(ProdutoBean.class);
        
        List<Predicate> predicados = new ArrayList<Predicate>();

        if (!"".equals(termoPesquisa) && termoPesquisa != null) {            
            Predicate desc = builder.like(builder.upper(root.get(ProdutoBean_.descricao)), ("%" + termoPesquisa + "%").toUpperCase());
            Predicate marca = builder.like(builder.upper(root.get(ProdutoBean_.marca)), ("%" + termoPesquisa + "%").toUpperCase());
            Predicate modelo = builder.like(builder.upper(root.get(ProdutoBean_.modelo)), ("%" + termoPesquisa + "%").toUpperCase());

            Predicate soma = builder.or(desc,marca,modelo);
            predicados.add(soma);
        }
        if (grupo != null) {
            Predicate group = builder.equal(root.get(ProdutoBean_.fkGrupoFornecimento), grupo);
            predicados.add(group);
        }

        criteria.where(builder.and(predicados.toArray(new Predicate[]{})));

        Query queryProjetoByOwner = em.createQuery(criteria);
        produtos = queryProjetoByOwner.getResultList();


        return produtos;
    }
}
