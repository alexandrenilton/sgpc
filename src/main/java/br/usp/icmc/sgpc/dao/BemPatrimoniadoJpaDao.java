/*
 * Sistema de Gestão de Projetos Científicos - SGPC
 * 
 * 
 * Copyright  (C)  2010-2014  Instituto  de  Ciências  Matemáticas  e  de
 *                            Computação - ICMC/USP
 * 
 * 
 * This program is  free software; you can redistribute  it and/or modify
 * it under the  terms of the GNU General Public  License as published by
 * the Free Software Foundation; either  version 2 of the License, or (at
 * your option) any later version.
 * 
 * This program  is distributed in the  hope that it will  be useful, but
 * WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
 * MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
 * General Public License for more details.
 * 
 * You  should have received  a copy  of the  GNU General  Public License
 * along  with  this  program;  if   not,  write  to  the  Free  Software
 * Foundation,  Inc.,  51  Franklin   Street,  Fifth  Floor,  Boston,  MA
 * 02110-1301, USA.
 * 
 * 
 * EXCEPTION TO THE TERMS AND CONDITIONS OF GNU GENERAL PUBLIC LICENSE
 * 
 * The  LICENSE  file may  contain  an  additional  clause as  a  special
 * exception  to the  terms  and  conditions of  the  GNU General  Public
 * License. This  clause, if  present, gives you  the permission  to link
 * this  program with  certain third-part  software and  to obtain,  as a
 * result, a  work based  on this program  that can be  distributed using
 * other license than the GNU General Public License.  If you modify this
 * program, you may extend this exception to your version of the program,
 * but you  are not obligated  to do so.   If you do  not wish to  do so,
 * delete this exception statement from your version.
 * 
 */

package br.usp.icmc.sgpc.dao;

import br.usp.icmc.sgpc.beans.AlineaBean;
import br.usp.icmc.sgpc.beans.AlineaBean_;
import br.usp.icmc.sgpc.beans.AuxilioBean;
import br.usp.icmc.sgpc.beans.AuxilioBean_;
import br.usp.icmc.sgpc.fmw.GenericJpaDao;
import br.usp.icmc.sgpc.beans.BemPatrimoniadoBean;
import br.usp.icmc.sgpc.beans.BemPatrimoniadoBean_;
import br.usp.icmc.sgpc.beans.DespesaBean;
import br.usp.icmc.sgpc.beans.DespesaBean_;
import br.usp.icmc.sgpc.beans.FinanciadorBean;
import br.usp.icmc.sgpc.beans.ModalidadeBean;
import br.usp.icmc.sgpc.beans.ModalidadeBean_;
import br.usp.icmc.sgpc.beans.TipoAuxilioBean;
import br.usp.icmc.sgpc.beans.TipoAuxilioBean_;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

/**
 *
 * @author Artur
 */
public class BemPatrimoniadoJpaDao extends GenericJpaDao<BemPatrimoniadoBean> {

    public BemPatrimoniadoJpaDao() {
        super(BemPatrimoniadoBean.class);
    }

    public List<BemPatrimoniadoBean> pesquisar(String termoPesquisa) {
        List<BemPatrimoniadoBean> bensPatrimoniados = null;
        EntityManager em = getEntityManager();
        try {            
            Query queryBemPatrimoniadoByKeyword = em.createNamedQuery("BemPatrimoniadoBean.findByKeyword");
            queryBemPatrimoniadoByKeyword.setParameter("keyword", "%" + termoPesquisa + "%");
            bensPatrimoniados = queryBemPatrimoniadoByKeyword.getResultList();
        } catch (NoResultException nre) {
            logger.debug("Usuário não cadastrado");
//            throw new USPException("Usuário não cadastrado");
        } catch (Exception e) {
            logger.error(e);
//            throw new USPException(e);
        }

        return bensPatrimoniados;
    }

    public List<BemPatrimoniadoBean> pesquisarCriteria(String termoPesquisa, Date dataInicio, Date dataFim, FinanciadorBean financiador) {
        List<BemPatrimoniadoBean> bemPatrimoniado = null;
        EntityManager em = getEntityManager();
        CriteriaBuilder builder = em.getCriteriaBuilder();

        CriteriaQuery<BemPatrimoniadoBean> criteria = builder.createQuery(BemPatrimoniadoBean.class);
        Root<BemPatrimoniadoBean> root = criteria.from(BemPatrimoniadoBean.class);
        Join<BemPatrimoniadoBean, DespesaBean> despesaP = root.join(BemPatrimoniadoBean_.fkDespesa);
        Join<DespesaBean, AlineaBean> alineaP = despesaP.join(DespesaBean_.fkAlinea);
        Join<AlineaBean, AuxilioBean> auxilioP = alineaP.join(AlineaBean_.fkAuxilio);

        Join<AuxilioBean, ModalidadeBean> modalidade = auxilioP.join(AuxilioBean_.fkModalidade);
        Join<ModalidadeBean, TipoAuxilioBean> tipoAuxilio = modalidade.join(ModalidadeBean_.fkTipoAuxilio);

        List<Predicate> predicados = new ArrayList<Predicate>();


        if (!"".equals(termoPesquisa) && termoPesquisa != null) {
            Predicate descricao = builder.like(builder.upper(root.get(BemPatrimoniadoBean_.descricao)), ("%" + termoPesquisa + "%").toUpperCase());
            Predicate procUsp = builder.like(builder.upper(auxilioP.get(AuxilioBean_.protocolo)), ("%" + termoPesquisa + "%").toUpperCase());
            Predicate procFinanc = builder.like(builder.upper(auxilioP.get(AuxilioBean_.protocoloFinanciador)), ("%" + termoPesquisa + "%").toUpperCase());

            Predicate soma = builder.or(descricao, procFinanc, procUsp);
            predicados.add(soma);
        }

        if (dataInicio != null) {
            Predicate dataL = builder.greaterThanOrEqualTo(root.get(BemPatrimoniadoBean_.dataRegistro), dataInicio);
            predicados.add(dataL);
        }

        if (dataFim != null) {
            Predicate dataL = builder.lessThanOrEqualTo(root.get(BemPatrimoniadoBean_.dataRegistro), dataFim);
            predicados.add(dataL);
        }

        if (financiador != null) {
            Predicate finan = builder.equal(tipoAuxilio.get(TipoAuxilioBean_.fkFinanciador), financiador);
            predicados.add(finan);
        }

        //sql.append(" ORDER BY p.descricao");
        criteria.where(builder.and(predicados.toArray(new Predicate[]{})));
        criteria.orderBy(builder.asc(root.get(BemPatrimoniadoBean_.descricao)));

        Query queryContas = em.createQuery(criteria);
        bemPatrimoniado = queryContas.getResultList();

        return bemPatrimoniado;
    }
}
