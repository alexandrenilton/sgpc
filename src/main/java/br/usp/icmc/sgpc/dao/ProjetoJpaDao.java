/*
 * Sistema de Gestão de Projetos Científicos - SGPC
 * 
 * 
 * Copyright  (C)  2010-2014  Instituto  de  Ciências  Matemáticas  e  de
 *                            Computação - ICMC/USP
 * 
 * 
 * This program is  free software; you can redistribute  it and/or modify
 * it under the  terms of the GNU General Public  License as published by
 * the Free Software Foundation; either  version 2 of the License, or (at
 * your option) any later version.
 * 
 * This program  is distributed in the  hope that it will  be useful, but
 * WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
 * MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
 * General Public License for more details.
 * 
 * You  should have received  a copy  of the  GNU General  Public License
 * along  with  this  program;  if   not,  write  to  the  Free  Software
 * Foundation,  Inc.,  51  Franklin   Street,  Fifth  Floor,  Boston,  MA
 * 02110-1301, USA.
 * 
 * 
 * EXCEPTION TO THE TERMS AND CONDITIONS OF GNU GENERAL PUBLIC LICENSE
 * 
 * The  LICENSE  file may  contain  an  additional  clause as  a  special
 * exception  to the  terms  and  conditions of  the  GNU General  Public
 * License. This  clause, if  present, gives you  the permission  to link
 * this  program with  certain third-part  software and  to obtain,  as a
 * result, a  work based  on this program  that can be  distributed using
 * other license than the GNU General Public License.  If you modify this
 * program, you may extend this exception to your version of the program,
 * but you  are not obligated  to do so.   If you do  not wish to  do so,
 * delete this exception statement from your version.
 * 
 */

package br.usp.icmc.sgpc.dao;

import br.usp.icmc.sgpc.beans.DepartamentoBean;
import br.usp.icmc.sgpc.beans.PessoaBean;
import br.usp.icmc.sgpc.beans.PessoaBean_;
import br.usp.icmc.sgpc.fmw.GenericJpaDao;
import br.usp.icmc.sgpc.beans.ProjetoBean;
import br.usp.icmc.sgpc.beans.ProjetoBean_;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

/**
 *
 * @author Artur
 */
public class ProjetoJpaDao extends GenericJpaDao<ProjetoBean> {

    public ProjetoJpaDao() {
        super(ProjetoBean.class);
    }

    public List<ProjetoBean> pesquisar() {
        List<ProjetoBean> projetos = null;
        EntityManager em = getEntityManager();
        try {
            Query queryProjetoByKeyword = em.createNamedQuery("ProjetoBean.findAll");
            projetos = queryProjetoByKeyword.getResultList();

        } catch (NoResultException nre) {
            logger.debug("Usuário não cadastrado");
        } catch (Exception e) {
            logger.error(e);
        }

        return projetos;
    }

    public List<ProjetoBean> pesquisar(String termoPesquisa) {
        List<ProjetoBean> projetos = null;
        EntityManager em = getEntityManager();
        try {
            Query queryProjetoByKeyword = em.createNamedQuery("ProjetoBean.findByKeyword");
            queryProjetoByKeyword.setParameter("keyword", "%" + termoPesquisa + "%");
            projetos = queryProjetoByKeyword.getResultList();

        } catch (NoResultException nre) {
            logger.debug("Usuário não cadastrado");
        } catch (Exception e) {
            logger.error(e);
        }

        return projetos;
    }

    public List<ProjetoBean> pesquisarOwner(PessoaBean responsavel) {
        List<ProjetoBean> projetos = null;
        EntityManager em = getEntityManager();
        try {
            Query queryProjetoByOwner = em.createNamedQuery("ProjetoBean.findByOwner");
            queryProjetoByOwner.setParameter("responsavel", responsavel);
            projetos = queryProjetoByOwner.getResultList();

        } catch (NoResultException nre) {
            logger.debug("Usuário não cadastrado");
        } catch (Exception e) {
            logger.error(e);
        }

        return projetos;
    }

    public List<ProjetoBean> pesquisarOwner(PessoaBean responsavel, String termoPesquisa) {
        List<ProjetoBean> projetos = null;
        EntityManager em = getEntityManager();
        try {
            Query queryProjetoByOwner = em.createNamedQuery("ProjetoBean.findByOwnerTerm");
            queryProjetoByOwner.setParameter("responsavel", responsavel);
            queryProjetoByOwner.setParameter("keyword", "%" + termoPesquisa + "%");
            projetos = queryProjetoByOwner.getResultList();

        } catch (NoResultException nre) {
            logger.debug("Usuário não cadastrado");
//            throw new USPException("Usuário não cadastrado");
        } catch (Exception e) {
            logger.error(e);
//            throw new USPException(e);
        }

        return projetos;
    }

    public List<ProjetoBean> pesquisarOwnerCriteria(PessoaBean user, String termoPesquisa, DepartamentoBean departamento, String status, Date dataInicial, Date dataFinal, PessoaBean responsavel) {
        List<ProjetoBean> projetos = null;
        EntityManager em = getEntityManager();
        CriteriaBuilder builder = em.getCriteriaBuilder();

        CriteriaQuery<ProjetoBean> criteria = builder.createQuery(ProjetoBean.class);
        Root<ProjetoBean> root = criteria.from(ProjetoBean.class);
        Join<ProjetoBean, PessoaBean> pessoaResponsavel = root.join(ProjetoBean_.fkResponsavel);

        List<Predicate> predicados = new ArrayList<Predicate>();
        if (user != null) {
            Predicate responsavelP = builder.equal(root.get(ProjetoBean_.fkResponsavel), user);
            predicados.add(responsavelP);
        }
        if (!"".equals(termoPesquisa) && termoPesquisa != null) {
            Predicate titulo = builder.like(builder.upper(root.get(ProjetoBean_.titulo)), ("%" + termoPesquisa + "%").toUpperCase());
            Predicate nomeResponsavel = builder.like(builder.upper(pessoaResponsavel.get(PessoaBean_.nome)), ("%" + termoPesquisa + "%").toUpperCase());
            Predicate descricao = builder.like(builder.upper(root.get(ProjetoBean_.descricao)), ("%" + termoPesquisa + "%").toUpperCase());

            Predicate soma = builder.or(titulo, nomeResponsavel, descricao);
            predicados.add(soma);
        }
        if (departamento != null) {
            Predicate departP = builder.equal(root.get(ProjetoBean_.fkDepartamento), departamento);
            predicados.add(departP);
        }
        if (!"".equals(status) && status != null) {
            Predicate statusP = builder.equal(root.get(ProjetoBean_.status), status);
            predicados.add(statusP);
        }
        if (dataInicial != null) {
            Predicate dataIP = builder.greaterThanOrEqualTo(root.get(ProjetoBean_.dataInicial), dataInicial);
            predicados.add(dataIP);
        }
        if (dataFinal != null) {
            Predicate dataFP = builder.lessThanOrEqualTo(root.get(ProjetoBean_.dataFinal), dataFinal);
            predicados.add(dataFP);
        }
        if(responsavel != null){
            Predicate respProjetoP = builder.equal(root.get(ProjetoBean_.fkResponsavel), responsavel);
            predicados.add(respProjetoP);
        }

        criteria.where(builder.and(predicados.toArray(new Predicate[]{})));
        criteria.orderBy(builder.asc(pessoaResponsavel.get(PessoaBean_.nome)));
        Query queryProjetoByOwner = em.createQuery(criteria);
        projetos = queryProjetoByOwner.getResultList();


        return projetos;
    }
}
