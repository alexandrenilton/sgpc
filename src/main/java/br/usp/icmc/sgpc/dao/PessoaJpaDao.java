/*
 * Sistema de Gestão de Projetos Científicos - SGPC
 * 
 * 
 * Copyright  (C)  2010-2014  Instituto  de  Ciências  Matemáticas  e  de
 *                            Computação - ICMC/USP
 * 
 * 
 * This program is  free software; you can redistribute  it and/or modify
 * it under the  terms of the GNU General Public  License as published by
 * the Free Software Foundation; either  version 2 of the License, or (at
 * your option) any later version.
 * 
 * This program  is distributed in the  hope that it will  be useful, but
 * WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
 * MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
 * General Public License for more details.
 * 
 * You  should have received  a copy  of the  GNU General  Public License
 * along  with  this  program;  if   not,  write  to  the  Free  Software
 * Foundation,  Inc.,  51  Franklin   Street,  Fifth  Floor,  Boston,  MA
 * 02110-1301, USA.
 * 
 * 
 * EXCEPTION TO THE TERMS AND CONDITIONS OF GNU GENERAL PUBLIC LICENSE
 * 
 * The  LICENSE  file may  contain  an  additional  clause as  a  special
 * exception  to the  terms  and  conditions of  the  GNU General  Public
 * License. This  clause, if  present, gives you  the permission  to link
 * this  program with  certain third-part  software and  to obtain,  as a
 * result, a  work based  on this program  that can be  distributed using
 * other license than the GNU General Public License.  If you modify this
 * program, you may extend this exception to your version of the program,
 * but you  are not obligated  to do so.   If you do  not wish to  do so,
 * delete this exception statement from your version.
 * 
 */

package br.usp.icmc.sgpc.dao;

import br.usp.icmc.sgpc.beans.DepartamentoBean;
import br.usp.icmc.sgpc.beans.DepartamentoBean_;
import br.usp.icmc.sgpc.beans.InstituicaoBean;
import br.usp.icmc.sgpc.fmw.GenericJpaDao;
import br.usp.icmc.sgpc.beans.PessoaBean;
import br.usp.icmc.sgpc.beans.PessoaBean_;
import br.usp.icmc.sgpc.beans.UnidadeBean;
import br.usp.icmc.sgpc.beans.UnidadeBean_;
import br.usp.icmc.sgpc.fmw.USPException;
import br.usp.icmc.sgpc.security.beans.PapelBean;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.hibernate.Session;

/**
 *
 * @author Artur
 */
public class PessoaJpaDao extends GenericJpaDao<PessoaBean> {

    public PessoaJpaDao() {
        super(PessoaBean.class);
    }

    public PessoaBean findByUsername(String username) throws USPException {
        PessoaBean pessoa = null;
        EntityManager em = getEntityManager();
        try {
            Query queryPessoaByUsername = em.createNamedQuery("PessoaBean.findByUsername");
            queryPessoaByUsername.setParameter("username", username);
            pessoa = (PessoaBean) queryPessoaByUsername.getSingleResult();
        } catch (NoResultException nre) {
            logger.debug("Usuário não cadastrado");
            throw new USPException("Usuário não cadastrado");
        } catch (Exception e) {
            logger.error(e);
            throw new USPException(e);
        }

        return pessoa;
    }

    public List<PessoaBean> pesquisar() {
        List<PessoaBean> lstPessoas = null;
        EntityManager em = getEntityManager();
        try {
            Query queryPessoa = em.createNamedQuery("PessoaBean.findAll");
            lstPessoas = queryPessoa.getResultList();
        } catch (NoResultException nre) {
            logger.debug("Usuário não cadastrado");
//            throw new USPException("Usuário não cadastrado");
        } catch (Exception e) {
            logger.error(e);
//            throw new USPException(e);
        }
        return lstPessoas;
    }

    public List<PessoaBean> pesquisar(String termoPesquisa) {
        List<PessoaBean> lstPessoas = null;
        EntityManager em = getEntityManager();
        try {
            Query queryPessoa = em.createNamedQuery("PessoaBean.findByKeyword");
            queryPessoa.setParameter("keyword", "%" + termoPesquisa + "%");
            lstPessoas = queryPessoa.getResultList();
        } catch (NoResultException nre) {
            logger.debug("Usuário não cadastrado");
//            throw new USPException("Usuário não cadastrado");
        } catch (Exception e) {
            logger.error(e);
//            throw new USPException(e);
        }
        return lstPessoas;
    }

    public PessoaBean findByEmail(String email) throws USPException {
        PessoaBean pessoa = null;
        EntityManager em = getEntityManager();
        try {
            Query queryPessoaByUsername = em.createNamedQuery("PessoaBean.findByEmail");
            queryPessoaByUsername.setParameter("email", email);
            pessoa = (PessoaBean) queryPessoaByUsername.getSingleResult();
        } catch (NoResultException nre) {
            logger.debug("E-mail não cadastrado no sistema");
            throw new USPException("E-mail não cadastrado no sistema");
        } catch (Exception e) {
            logger.error(e);
            throw new USPException(e);
        }

        return pessoa;
    }

    public PessoaBean findByCpf(String cpf) throws USPException {
        PessoaBean pessoa = null;
        EntityManager em = getEntityManager();
        try {
            Query queryPessoaByUsername = em.createNamedQuery("PessoaBean.findByCpf");
            queryPessoaByUsername.setParameter("cpf", cpf);
            pessoa = (PessoaBean) queryPessoaByUsername.getSingleResult();

        } catch (NoResultException nre) {
            logger.debug("CPF não cadastrado no sistema");
            throw new USPException("CPF não cadastrado no sistema");
        } catch (Exception e) {
            logger.error(e);
            throw new USPException(e);
        }

        return pessoa;
    }

    public List<PessoaBean> pesquisarGrupo(PapelBean papel) {
        List<PessoaBean> lstPessoas = null;
        EntityManager em = getEntityManager();

////        CriteriaBuilder builder = em.getCriteriaBuilder();
////
////        CriteriaQuery<PessoaBean> criteria = builder.createQuery(PessoaBean.class);
////        Root<PessoaBean> root = criteria.from(PessoaBean.class);
////        Join<PessoaBean, PapelBean> papelJoin = root.join(PessoaBean_.papeis);
////
////        List<Predicate> predicados = new ArrayList<Predicate>();
////
////        if(papel!=null){
////            Predicate predicadoPapel = builder.isMember(papel,pessoa.get(PessoaBean_.papeis));// equal(papel.get(PapelBean_.pessoas), papel);
////            predicados.add(predicadoPapel);
////        }
////
////        criteria.where(builder.and(predicados.toArray(new Predicate[]{})));
////
////        Query queryPessoasByPapel = em.createQuery(criteria);
////        lstPessoas = queryPessoasByPapel.getResultList();
////
////        return lstPessoas;

        try {
            Query queryPessoa = em.createNamedQuery("PessoaBean.findByGroup");
            queryPessoa.setParameter("papel", papel);
            lstPessoas = queryPessoa.getResultList();
        } catch (NoResultException nre) {
            logger.debug("Usuário não cadastrado");
//            throw new USPException("Usuário não cadastrado");
        } catch (Exception e) {
            logger.error(e);
//            throw new USPException(e);
        }
        return lstPessoas;
    }

    public List<PessoaBean> pesquisarCriteria(String termoPesquisa, InstituicaoBean instituicao, UnidadeBean unidade, DepartamentoBean departamento, PapelBean papel) {

        List<PessoaBean> lstPessoas = null;

        EntityManager em = getEntityManager();
        CriteriaBuilder builder = em.getCriteriaBuilder();

        CriteriaQuery<PessoaBean> criteria = builder.createQuery(PessoaBean.class);
        Root<PessoaBean> root = criteria.from(PessoaBean.class);

        List<Predicate> predicados = new ArrayList<Predicate>();

        if (!"".equals(termoPesquisa) && termoPesquisa != null) {
            Predicate username = builder.like(builder.upper(root.get(PessoaBean_.username)), ("%" + termoPesquisa + "%").toUpperCase());
            Predicate nome = builder.like(builder.upper(root.get(PessoaBean_.nome)), ("%" + termoPesquisa + "%").toUpperCase());
            Predicate rg = builder.like(root.get(PessoaBean_.rg), ("%" + termoPesquisa + "%"));
            Predicate cpf = builder.like(root.get(PessoaBean_.cpf), ("%" + termoPesquisa + "%"));
            Predicate email = builder.like(root.get(PessoaBean_.email), ("%" + termoPesquisa + "%"));

            Predicate soma = builder.or(username, nome, rg, cpf, email);
            predicados.add(soma);
        }
        if (instituicao != null) {
            Join<PessoaBean, DepartamentoBean> deptPessoa = root.join(PessoaBean_.fkDepartamento);
            Join<DepartamentoBean, UnidadeBean> unidadeDepartamento = deptPessoa.join(DepartamentoBean_.fkUnidade);

            Predicate predInstituicao = builder.equal(unidadeDepartamento.get(UnidadeBean_.fkInstituicao), instituicao);
            predicados.add(predInstituicao);

            if (unidade != null) {
                Predicate predUnidade = builder.equal(deptPessoa.get(DepartamentoBean_.fkUnidade), unidade);
                predicados.add(predUnidade);

                if (departamento != null) {
                    Predicate predDepart = builder.equal(root.get(PessoaBean_.fkDepartamento), departamento);
                    predicados.add(predDepart);
                }
            }
        }

        if (papel != null) {
            Predicate paper = builder.isMember(papel, root.get(PessoaBean_.papeis));
            predicados.add(paper);
        }

        criteria.where(builder.and(predicados.toArray(new Predicate[]{})));

        criteria.orderBy(builder.asc(root.get(PessoaBean_.nome)));

        Query queryPessoas = em.createQuery(criteria);
        lstPessoas = queryPessoas.getResultList();

        return lstPessoas;
    }

    public PessoaBean findByNome(String nome) {
        PessoaBean pessoa = null;
        EntityManager em = getEntityManager();
        try {
            Query queryPessoaByUsername = em.createNamedQuery("PessoaBean.findByNome");
            queryPessoaByUsername.setParameter("nome", nome);
            pessoa = (PessoaBean) queryPessoaByUsername.getSingleResult();
        } catch (NoResultException nre) {
            logger.debug("Usuário não cadastrado");
        } catch (Exception e) {
            logger.error(e);
        }

        return pessoa;
    }
    
    public void criarPessoaComId(String nome, Integer id){
        String sqlInsert = "INSERT INTO pessoa (nome, id_pessoa) VALUES( ?, ?)";
        Session session = getEntityManager().unwrap(Session.class);
        Connection conn = session.connection();
        PreparedStatement ps;
        try{
            ps = conn.prepareStatement(sqlInsert);
            ps.setString(1, nome);
            ps.setInt(2, id);
            ps.executeUpdate();
            ps.close();
        } catch (Exception ex) {
            logger.debug(ex);
        }
    }
}
