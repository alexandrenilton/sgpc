/*
 * Sistema de Gestão de Projetos Científicos - SGPC
 * 
 * 
 * Copyright  (C)  2010-2014  Instituto  de  Ciências  Matemáticas  e  de
 *                            Computação - ICMC/USP
 * 
 * 
 * This program is  free software; you can redistribute  it and/or modify
 * it under the  terms of the GNU General Public  License as published by
 * the Free Software Foundation; either  version 2 of the License, or (at
 * your option) any later version.
 * 
 * This program  is distributed in the  hope that it will  be useful, but
 * WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
 * MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
 * General Public License for more details.
 * 
 * You  should have received  a copy  of the  GNU General  Public License
 * along  with  this  program;  if   not,  write  to  the  Free  Software
 * Foundation,  Inc.,  51  Franklin   Street,  Fifth  Floor,  Boston,  MA
 * 02110-1301, USA.
 * 
 * 
 * EXCEPTION TO THE TERMS AND CONDITIONS OF GNU GENERAL PUBLIC LICENSE
 * 
 * The  LICENSE  file may  contain  an  additional  clause as  a  special
 * exception  to the  terms  and  conditions of  the  GNU General  Public
 * License. This  clause, if  present, gives you  the permission  to link
 * this  program with  certain third-part  software and  to obtain,  as a
 * result, a  work based  on this program  that can be  distributed using
 * other license than the GNU General Public License.  If you modify this
 * program, you may extend this exception to your version of the program,
 * but you  are not obligated  to do so.   If you do  not wish to  do so,
 * delete this exception statement from your version.
 * 
 */

package br.usp.icmc.sgpc.dao;

import br.usp.icmc.sgpc.beans.DespesaBean;
import br.usp.icmc.sgpc.beans.DespesaBean_;
import br.usp.icmc.sgpc.beans.DocumentoComprovacaoDespesaBean;
import br.usp.icmc.sgpc.beans.DocumentoComprovacaoDespesaBean_;
import br.usp.icmc.sgpc.beans.FornecedorBean;
import br.usp.icmc.sgpc.beans.FornecedorBean_;
import br.usp.icmc.sgpc.fmw.GenericJpaDao;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

/**
 *
 * @author herick
 */
public class DocumentoComprovacaoDespesaJpaDao extends GenericJpaDao<DocumentoComprovacaoDespesaBean> {

   public DocumentoComprovacaoDespesaJpaDao() {
        super(DocumentoComprovacaoDespesaBean.class);
    }
   
   public List<DocumentoComprovacaoDespesaBean> pesquisarCriteriaRegistrosDuplicados(Date dataEmissao, String numero, FornecedorBean fornecedor){
       List<DocumentoComprovacaoDespesaBean> listaRetorno = null;
       EntityManager em = getEntityManager();
       CriteriaBuilder builder = em.getCriteriaBuilder();
       CriteriaQuery<DocumentoComprovacaoDespesaBean> criteria = builder.createQuery(DocumentoComprovacaoDespesaBean.class);
       Root<DocumentoComprovacaoDespesaBean> root = criteria.from(DocumentoComprovacaoDespesaBean.class);
       Join<DocumentoComprovacaoDespesaBean, DespesaBean> despesaJ = root.join(DocumentoComprovacaoDespesaBean_.fkDespesa);
       Join<DespesaBean, FornecedorBean> fornecedorJ = despesaJ.join(DespesaBean_.fkFornecedor);
       
       List<Predicate> predicados = new ArrayList<Predicate>();
       
       criteria.where(builder.and(predicados.toArray(new Predicate[]{})));
       
       if(dataEmissao != null){
            Calendar calendar=Calendar.getInstance();
            calendar.setTime(dataEmissao);
            
            calendar.set(Calendar.HOUR_OF_DAY, 00);
            calendar.set(Calendar.MINUTE, 00);
            Date dataInicial=calendar.getTime();
            
            calendar.set(Calendar.HOUR_OF_DAY, 23);
            calendar.set(Calendar.MINUTE, 59);
            Date dataFinal=calendar.getTime();
           
           Predicate dataEmissaoInicioP = builder.greaterThanOrEqualTo(root.get(DocumentoComprovacaoDespesaBean_.dataEmissao),dataInicial);
           Predicate dataEmissaoFimP = builder.lessThanOrEqualTo(root.get(DocumentoComprovacaoDespesaBean_.dataEmissao),dataFinal);
           
           Predicate durante = builder.and(dataEmissaoInicioP, dataEmissaoFimP);
           
           predicados.add(durante);
       }
       
       if(numero!=null){
           Predicate numeroP = builder.equal(root.get(DocumentoComprovacaoDespesaBean_.numero), numero);
           predicados.add(numeroP);
       }
       
       if(fornecedor!=null){
           Predicate fornecedorP = builder.equal(fornecedorJ.get(FornecedorBean_.id), fornecedor.getId());
           predicados.add(fornecedorP);
       }
       
       criteria.where(builder.and(predicados.toArray(new Predicate[]{})));
       
       Query queryDocumentosFiscais = em.createQuery(criteria);
       
       listaRetorno = queryDocumentosFiscais.getResultList();
       return listaRetorno;
   }

}
