/*
 * Sistema de Gestão de Projetos Científicos - SGPC
 * 
 * 
 * Copyright  (C)  2010-2014  Instituto  de  Ciências  Matemáticas  e  de
 *                            Computação - ICMC/USP
 * 
 * 
 * This program is  free software; you can redistribute  it and/or modify
 * it under the  terms of the GNU General Public  License as published by
 * the Free Software Foundation; either  version 2 of the License, or (at
 * your option) any later version.
 * 
 * This program  is distributed in the  hope that it will  be useful, but
 * WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
 * MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
 * General Public License for more details.
 * 
 * You  should have received  a copy  of the  GNU General  Public License
 * along  with  this  program;  if   not,  write  to  the  Free  Software
 * Foundation,  Inc.,  51  Franklin   Street,  Fifth  Floor,  Boston,  MA
 * 02110-1301, USA.
 * 
 * 
 * EXCEPTION TO THE TERMS AND CONDITIONS OF GNU GENERAL PUBLIC LICENSE
 * 
 * The  LICENSE  file may  contain  an  additional  clause as  a  special
 * exception  to the  terms  and  conditions of  the  GNU General  Public
 * License. This  clause, if  present, gives you  the permission  to link
 * this  program with  certain third-part  software and  to obtain,  as a
 * result, a  work based  on this program  that can be  distributed using
 * other license than the GNU General Public License.  If you modify this
 * program, you may extend this exception to your version of the program,
 * but you  are not obligated  to do so.   If you do  not wish to  do so,
 * delete this exception statement from your version.
 * 
 */

package br.usp.icmc.sgpc.dao;

import br.usp.icmc.sgpc.beans.CategoriaFornecedorBean;
import br.usp.icmc.sgpc.beans.CategoriaFornecedorBean_;
import br.usp.icmc.sgpc.beans.FornecedorBean;
import br.usp.icmc.sgpc.beans.FornecedorBean_;
import br.usp.icmc.sgpc.beans.GrupoFornecimentoBean;
import br.usp.icmc.sgpc.fmw.GenericJpaDao;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
/**
 *
 * @author herick
 */
public class CategoriaFornecedorJpaDao extends GenericJpaDao<CategoriaFornecedorBean>{
    public CategoriaFornecedorJpaDao(){
        super(CategoriaFornecedorBean.class);
    }


     public List<CategoriaFornecedorBean> pesquisarCriteria(String termoPesquisa, GrupoFornecimentoBean grupo) {
        List<CategoriaFornecedorBean> categorias = null;
        EntityManager em = getEntityManager();
        CriteriaBuilder builder = em.getCriteriaBuilder();

        CriteriaQuery<CategoriaFornecedorBean> criteria = builder.createQuery(CategoriaFornecedorBean.class);
        Root<CategoriaFornecedorBean> root = criteria.from(CategoriaFornecedorBean.class);        
        Join<CategoriaFornecedorBean, FornecedorBean> fornecedor = root.join(CategoriaFornecedorBean_.fkFornecedor);
        //Join<CategoriaFornecedorBean, RepresentanteVendasBean> representante = root.join(CategoriaFornecedorBean_.fkRepresentante);
        //Join com representante só deve ser usado quando o mesmo não for nullable
        List<Predicate> predicados = new ArrayList<Predicate>();

        if (!"".equals(termoPesquisa) && termoPesquisa != null) {
            //Predicate rep = builder.like(builder.upper(representante.get(RepresentanteVendasBean_.nome)), ("%" + termoPesquisa + "%").toUpperCase());
            Predicate forn = builder.like(builder.upper(fornecedor.get(FornecedorBean_.razaoSocial)), ("%" + termoPesquisa + "%").toUpperCase());

            Predicate soma = builder.or(forn);
            predicados.add(soma);
        }
        if (grupo != null) {
            Predicate group = builder.equal(root.get(CategoriaFornecedorBean_.fkGrupoFornecimento), grupo);
            predicados.add(group);
        }

        criteria.where(builder.and(predicados.toArray(new Predicate[]{})));
        
        Query queryProjetoByOwner = em.createQuery(criteria);
        categorias = queryProjetoByOwner.getResultList();

        return categorias;
    }

}
