/*
 * Sistema de Gestão de Projetos Científicos - SGPC
 * 
 * 
 * Copyright  (C)  2010-2014  Instituto  de  Ciências  Matemáticas  e  de
 *                            Computação - ICMC/USP
 * 
 * 
 * This program is  free software; you can redistribute  it and/or modify
 * it under the  terms of the GNU General Public  License as published by
 * the Free Software Foundation; either  version 2 of the License, or (at
 * your option) any later version.
 * 
 * This program  is distributed in the  hope that it will  be useful, but
 * WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
 * MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
 * General Public License for more details.
 * 
 * You  should have received  a copy  of the  GNU General  Public License
 * along  with  this  program;  if   not,  write  to  the  Free  Software
 * Foundation,  Inc.,  51  Franklin   Street,  Fifth  Floor,  Boston,  MA
 * 02110-1301, USA.
 * 
 * 
 * EXCEPTION TO THE TERMS AND CONDITIONS OF GNU GENERAL PUBLIC LICENSE
 * 
 * The  LICENSE  file may  contain  an  additional  clause as  a  special
 * exception  to the  terms  and  conditions of  the  GNU General  Public
 * License. This  clause, if  present, gives you  the permission  to link
 * this  program with  certain third-part  software and  to obtain,  as a
 * result, a  work based  on this program  that can be  distributed using
 * other license than the GNU General Public License.  If you modify this
 * program, you may extend this exception to your version of the program,
 * but you  are not obligated  to do so.   If you do  not wish to  do so,
 * delete this exception statement from your version.
 * 
 */

package br.usp.icmc.sgpc.dao;

import br.usp.icmc.sgpc.beans.CentroDespesaBean;
import br.usp.icmc.sgpc.beans.CentroDespesaBean_;
import br.usp.icmc.sgpc.beans.UnidadeBean;
import br.usp.icmc.sgpc.beans.UnidadeBean_;
import br.usp.icmc.sgpc.fmw.GenericJpaDao;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

/**
 *
 * @author ademilson
 */
public class CentroDespesaJpaDao extends GenericJpaDao<CentroDespesaBean> {

    public CentroDespesaJpaDao() {
        super(CentroDespesaBean.class);
    }

    public List<CentroDespesaBean> listarOrdenadaPorNome() {
        List<CentroDespesaBean> centrosDespesa = null;
        EntityManager em = getEntityManager();
        try {
            Query queryCentroDespesaOrdenada = em.createQuery("SELECT c FROM CentroDespesaBean c ORDER BY c.nome");
            centrosDespesa = queryCentroDespesaOrdenada.getResultList();
        } catch (NoResultException nre) {
            logger.debug("Centro não cadastrado");
        } catch (Exception e) {
            logger.error(e);
        }

        return centrosDespesa;
    }

    public List<CentroDespesaBean> pesquisar(String termoPesquisa) {
        List<CentroDespesaBean> centrosDespesa = null;
        EntityManager em = getEntityManager();
        try {
            Query queryCentroDespesaByKeyword = em.createNamedQuery("CentroDespesaBean.findByKeyword");
            queryCentroDespesaByKeyword.setParameter("keyword", "%" + termoPesquisa + "%");
            centrosDespesa = queryCentroDespesaByKeyword.getResultList();
        } catch (NoResultException nre) {
            logger.debug("Usuário não cadastrado");
//            throw new USPException("Usuário não cadastrado");
        } catch (Exception e) {
            logger.error(e);
//            throw new USPException(e);
        }

        return centrosDespesa;
    }

    public CentroDespesaBean buscarPorEstruturaHierarquica(String estruturaPesquisa) {
        CentroDespesaBean centroRetorno = null;
        EntityManager em = getEntityManager();
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT c FROM CentroDespesaBean c");
            sql.append(" WHERE c.estruturaHierarquica = :estruturaHierarquica");

            Query queryCentroDespesaByEstrutura = em.createQuery(sql.toString());
            queryCentroDespesaByEstrutura.setParameter("estruturaHierarquica", estruturaPesquisa);

            centroRetorno = (CentroDespesaBean) queryCentroDespesaByEstrutura.getSingleResult();

        } catch (NoResultException nre) {
            logger.error(nre);
        } catch (Exception e) {
            logger.error(e);
        }

        return centroRetorno;
    }

    public List<CentroDespesaBean> pesquisar(String termoPesquisa, UnidadeBean unidade, CentroDespesaBean centroPai) {
        List<CentroDespesaBean> centrosDespesa = null;
        EntityManager em = getEntityManager();
        CriteriaBuilder builder = em.getCriteriaBuilder();
        CriteriaQuery<CentroDespesaBean> criteria = builder.createQuery(CentroDespesaBean.class);
        Root<CentroDespesaBean> root = criteria.from(CentroDespesaBean.class);
        Join<CentroDespesaBean, UnidadeBean> unidadeJoin = root.join(CentroDespesaBean_.fkUnidade);
        Join<CentroDespesaBean, CentroDespesaBean> filhosJoin = root.join(CentroDespesaBean_.centrosDespesaFilhos, JoinType.LEFT);
        Join<CentroDespesaBean, CentroDespesaBean> netosJoin = filhosJoin.join(CentroDespesaBean_.centrosDespesaFilhos, JoinType.LEFT);
        Join<CentroDespesaBean, CentroDespesaBean> bisNetosJoin = netosJoin.join(CentroDespesaBean_.centrosDespesaFilhos, JoinType.LEFT);
        Join<CentroDespesaBean, CentroDespesaBean> tataraNetosJoin = bisNetosJoin.join(CentroDespesaBean_.centrosDespesaFilhos, JoinType.LEFT);

        List<Predicate> predicados = new ArrayList<Predicate>();
        if (termoPesquisa != null && !"".equals(termoPesquisa)) {
            Predicate nomeP = builder.like(builder.upper(root.get(CentroDespesaBean_.nome)), ("%" + termoPesquisa + "%").toUpperCase());
            Predicate nomeFilhosP = builder.like(builder.upper(filhosJoin.get(CentroDespesaBean_.nome)), ("%" + termoPesquisa + "%").toUpperCase());
            Predicate nomeNetosP = builder.like(builder.upper(netosJoin.get(CentroDespesaBean_.nome)), ("%" + termoPesquisa + "%").toUpperCase());
            Predicate nomeBisNetosP = builder.like(builder.upper(bisNetosJoin.get(CentroDespesaBean_.nome)), ("%" + termoPesquisa + "%").toUpperCase());
            Predicate nometataraNetosP = builder.like(builder.upper(tataraNetosJoin.get(CentroDespesaBean_.nome)), ("%" + termoPesquisa + "%").toUpperCase());
            predicados.add(builder.or(nomeP, nomeFilhosP, nomeNetosP, nomeBisNetosP, nometataraNetosP));
        }

        if (unidade != null) {
            Predicate unidadeP = builder.equal(unidadeJoin.get(UnidadeBean_.id), unidade.getId());
            predicados.add(unidadeP);
        }

        if (centroPai != null) {
            Join<CentroDespesaBean, CentroDespesaBean> centroPaiJoin = root.join(CentroDespesaBean_.centroDespesasPai);
            Predicate centroPaiP = builder.equal(centroPaiJoin.get(CentroDespesaBean_.id), centroPai.getId());
            predicados.add(centroPaiP);
        }

        criteria.where(builder.and(predicados.toArray(new Predicate[]{})));
        criteria.distinct(true);
        criteria.orderBy(builder.asc(root.get(CentroDespesaBean_.centroDespesasPai)), builder.asc(root.get(CentroDespesaBean_.nome)));
//        Order ordenacao = null;
//        Path path = root.get(CentroDespesaBean_.estruturaHierarquica);
//        ordenacao = builder.asc(path);
//        criteria.orderBy(ordenacao);
        Query queryConsulta = em.createQuery(criteria);
        centrosDespesa = queryConsulta.getResultList();

        return centrosDespesa;
    }
}
