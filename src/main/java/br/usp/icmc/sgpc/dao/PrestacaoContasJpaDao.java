/*
 * Sistema de Gestão de Projetos Científicos - SGPC
 * 
 * 
 * Copyright  (C)  2010-2014  Instituto  de  Ciências  Matemáticas  e  de
 *                            Computação - ICMC/USP
 * 
 * 
 * This program is  free software; you can redistribute  it and/or modify
 * it under the  terms of the GNU General Public  License as published by
 * the Free Software Foundation; either  version 2 of the License, or (at
 * your option) any later version.
 * 
 * This program  is distributed in the  hope that it will  be useful, but
 * WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
 * MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
 * General Public License for more details.
 * 
 * You  should have received  a copy  of the  GNU General  Public License
 * along  with  this  program;  if   not,  write  to  the  Free  Software
 * Foundation,  Inc.,  51  Franklin   Street,  Fifth  Floor,  Boston,  MA
 * 02110-1301, USA.
 * 
 * 
 * EXCEPTION TO THE TERMS AND CONDITIONS OF GNU GENERAL PUBLIC LICENSE
 * 
 * The  LICENSE  file may  contain  an  additional  clause as  a  special
 * exception  to the  terms  and  conditions of  the  GNU General  Public
 * License. This  clause, if  present, gives you  the permission  to link
 * this  program with  certain third-part  software and  to obtain,  as a
 * result, a  work based  on this program  that can be  distributed using
 * other license than the GNU General Public License.  If you modify this
 * program, you may extend this exception to your version of the program,
 * but you  are not obligated  to do so.   If you do  not wish to  do so,
 * delete this exception statement from your version.
 * 
 */

package br.usp.icmc.sgpc.dao;

import br.usp.icmc.sgpc.beans.AuxilioBean;
import br.usp.icmc.sgpc.beans.AuxilioBean_;
import br.usp.icmc.sgpc.beans.FinanciadorBean;
import br.usp.icmc.sgpc.beans.ModalidadeBean;
import br.usp.icmc.sgpc.beans.ModalidadeBean_;
import br.usp.icmc.sgpc.beans.PessoaBean;
import br.usp.icmc.sgpc.beans.PessoaBean_;
import br.usp.icmc.sgpc.fmw.GenericJpaDao;
import br.usp.icmc.sgpc.beans.PrestacaoContasBean;
import br.usp.icmc.sgpc.beans.PrestacaoContasBean_;
import br.usp.icmc.sgpc.beans.ProjetoBean;
import br.usp.icmc.sgpc.beans.ProjetoBean_;
import br.usp.icmc.sgpc.beans.TipoAuxilioBean;
import br.usp.icmc.sgpc.beans.TipoAuxilioBean_;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

/**
 *
 * @author Artur
 */
public class PrestacaoContasJpaDao extends GenericJpaDao<PrestacaoContasBean> {

    public PrestacaoContasJpaDao() {
        super(PrestacaoContasBean.class);
    }

    public List<PrestacaoContasBean> getPrestacoesContas(int intervaloDias) {
        List<PrestacaoContasBean> prestacoes = new ArrayList<PrestacaoContasBean>();
        EntityManager em = getEntityManager();

        Calendar c = new GregorianCalendar();
        c.add(Calendar.DAY_OF_MONTH, intervaloDias);
        Date dataFinal = c.getTime();
        Date dataInicial = new Date();

        try {
            Query queryPrestacoesByDataPeriodo = em.createNamedQuery("PrestacaoContasBean.findByDataPeriodo");
            queryPrestacoesByDataPeriodo.setParameter("dataInicial", dataInicial);
            queryPrestacoesByDataPeriodo.setParameter("dataFinal", dataFinal);
            prestacoes = queryPrestacoesByDataPeriodo.getResultList();
        } catch (NoResultException nre) {
            logger.debug("Nenhum registro encontrado");
        } catch (Exception e) {
            logger.error(e);
        }
        return prestacoes;
    }

    public List<PrestacaoContasBean> getPrestacoesContas(PessoaBean pessoa, Date dataLimite) {
        List<PrestacaoContasBean> prestacoes = new ArrayList<PrestacaoContasBean>();
        EntityManager em = getEntityManager();

        try {
            Query queryPrestacoesByDataPeriodo = em.createNamedQuery("PrestacaoContasBean.findByDataLimiteEPessoa");
            queryPrestacoesByDataPeriodo.setParameter("dataLimite", dataLimite);
            queryPrestacoesByDataPeriodo.setParameter("responsavel", pessoa);
            prestacoes = queryPrestacoesByDataPeriodo.getResultList();
        } catch (NoResultException nre) {
            logger.debug("Nenhum registro encontrado");
        } catch (Exception e) {
            logger.error(e);
        }
        return prestacoes;
    }

    public List<PrestacaoContasBean> getPrestacoesContas(Date dataInicial, Date dataFinal) {
        List<PrestacaoContasBean> prestacoes = new ArrayList<PrestacaoContasBean>();
        EntityManager em = getEntityManager();

        try {
            Query queryPrestacoesByDataPeriodo = em.createNamedQuery("PrestacaoContasBean.findByDataPeriodo");
            queryPrestacoesByDataPeriodo.setParameter("dataInicial", dataInicial);
            queryPrestacoesByDataPeriodo.setParameter("dataFinal", dataFinal);
            prestacoes = queryPrestacoesByDataPeriodo.getResultList();
        } catch (NoResultException nre) {
            logger.debug("Nenhum registro encontrado");
        } catch (Exception e) {
            logger.error(e);
        }
        return prestacoes;
    }

    public List<PrestacaoContasBean> getPrestacoesContas(PessoaBean pessoa, Date dataInicial, Date dataFinal) {
        List<PrestacaoContasBean> prestacoes = new ArrayList<PrestacaoContasBean>();
        EntityManager em = getEntityManager();

        try {
            Query queryPrestacoesByDataPeriodo = em.createNamedQuery("PrestacaoContasBean.findByDataPeriodoEPessoa");
            queryPrestacoesByDataPeriodo.setParameter("responsavel", pessoa);
            queryPrestacoesByDataPeriodo.setParameter("dataInicial", dataInicial);
            queryPrestacoesByDataPeriodo.setParameter("dataFinal", dataFinal);
            prestacoes = queryPrestacoesByDataPeriodo.getResultList();
        } catch (NoResultException nre) {
            logger.debug("Nenhum registro encontrado");
        } catch (Exception e) {
            logger.error(e);
        }
        return prestacoes;
    }

    public List<PrestacaoContasBean> pesquisarCriteria(PessoaBean usuarioLogado, String termoPesquisa, Date dataInicio, Date dataFim, FinanciadorBean financiador, String status) {
        List<PrestacaoContasBean> prestacaoContas = null;
        EntityManager em = getEntityManager();
        CriteriaBuilder builder = em.getCriteriaBuilder();

        CriteriaQuery<PrestacaoContasBean> criteria = builder.createQuery(PrestacaoContasBean.class);
        Root<PrestacaoContasBean> root = criteria.from(PrestacaoContasBean.class);
        Join<PrestacaoContasBean, AuxilioBean> auxilioP = root.join(PrestacaoContasBean_.fkAuxilio);
        Join<AuxilioBean, ProjetoBean> projetoP = auxilioP.join(AuxilioBean_.fkProjeto);
        Join<ProjetoBean, PessoaBean> pessoaP = projetoP.join(ProjetoBean_.fkResponsavel);

        Join<AuxilioBean, ModalidadeBean> modalidade = auxilioP.join(AuxilioBean_.fkModalidade);
        Join<ModalidadeBean, TipoAuxilioBean> tipoAuxilio = modalidade.join(ModalidadeBean_.fkTipoAuxilio);

        List<Predicate> predicados = new ArrayList<Predicate>();

        if (usuarioLogado != null) {
            Predicate nomeResponsavel = builder.equal(projetoP.get(ProjetoBean_.fkResponsavel), usuarioLogado);
            predicados.add(nomeResponsavel);
        }

        if (!"".equals(termoPesquisa) && termoPesquisa != null) {
            Predicate descricao = builder.like(builder.upper(root.get(PrestacaoContasBean_.descricao)), ("%" + termoPesquisa + "%").toUpperCase());
            Predicate nomeResponsavel = builder.like(builder.upper(pessoaP.get(PessoaBean_.nome)), ("%" + termoPesquisa + "%").toUpperCase());
            Predicate numMercurio = builder.like(builder.upper(auxilioP.get(AuxilioBean_.numeroMercurio)), ("%" + termoPesquisa + "%").toUpperCase());
            Predicate procUsp = builder.like(builder.upper(auxilioP.get(AuxilioBean_.protocolo)), ("%" + termoPesquisa + "%").toUpperCase());
            Predicate procFinanc = builder.like(builder.upper(auxilioP.get(AuxilioBean_.protocoloFinanciador)), ("%" + termoPesquisa + "%").toUpperCase());

            Predicate soma = builder.or(descricao, nomeResponsavel, numMercurio, procFinanc, procUsp);
            predicados.add(soma);
        }

        if (dataInicio != null) {
            Predicate dataL = builder.greaterThanOrEqualTo(root.get(PrestacaoContasBean_.dataLimite), dataInicio);
            predicados.add(dataL);
        }

        if (dataFim != null) {
            Predicate dataL = builder.lessThanOrEqualTo(root.get(PrestacaoContasBean_.dataLimite), dataFim);
            predicados.add(dataL);
        }

        if (financiador != null) {
            Predicate finan = builder.equal(tipoAuxilio.get(TipoAuxilioBean_.fkFinanciador), financiador);
            predicados.add(finan);
        }

        if (!"".equals(status) && status != null) {
            Predicate statusP = builder.equal(root.get(PrestacaoContasBean_.status), status);
            predicados.add(statusP);
        }

        //sql.append(" ORDER BY p.descricao");
        criteria.where(builder.and(predicados.toArray(new Predicate[]{})));
        criteria.orderBy(builder.asc(root.get(PrestacaoContasBean_.descricao)));

        Query queryContas = em.createQuery(criteria);
        prestacaoContas = queryContas.getResultList();


        return prestacaoContas;
    }
}
