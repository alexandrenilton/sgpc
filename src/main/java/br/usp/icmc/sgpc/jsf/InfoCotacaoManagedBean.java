/*
 * Sistema de Gestão de Projetos Científicos - SGPC
 * 
 * 
 * Copyright  (C)  2010-2014  Instituto  de  Ciências  Matemáticas  e  de
 *                            Computação - ICMC/USP
 * 
 * 
 * This program is  free software; you can redistribute  it and/or modify
 * it under the  terms of the GNU General Public  License as published by
 * the Free Software Foundation; either  version 2 of the License, or (at
 * your option) any later version.
 * 
 * This program  is distributed in the  hope that it will  be useful, but
 * WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
 * MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
 * General Public License for more details.
 * 
 * You  should have received  a copy  of the  GNU General  Public License
 * along  with  this  program;  if   not,  write  to  the  Free  Software
 * Foundation,  Inc.,  51  Franklin   Street,  Fifth  Floor,  Boston,  MA
 * 02110-1301, USA.
 * 
 * 
 * EXCEPTION TO THE TERMS AND CONDITIONS OF GNU GENERAL PUBLIC LICENSE
 * 
 * The  LICENSE  file may  contain  an  additional  clause as  a  special
 * exception  to the  terms  and  conditions of  the  GNU General  Public
 * License. This  clause, if  present, gives you  the permission  to link
 * this  program with  certain third-part  software and  to obtain,  as a
 * result, a  work based  on this program  that can be  distributed using
 * other license than the GNU General Public License.  If you modify this
 * program, you may extend this exception to your version of the program,
 * but you  are not obligated  to do so.   If you do  not wish to  do so,
 * delete this exception statement from your version.
 * 
 */

package br.usp.icmc.sgpc.jsf;

import br.usp.icmc.sgpc.beans.AlineaBean;
import br.usp.icmc.sgpc.beans.AuxilioBean;
import br.usp.icmc.sgpc.beans.CotacaoBean;
import br.usp.icmc.sgpc.beans.DespesaBean;
import br.usp.icmc.sgpc.beans.GrupoFornecimentoBean;
import br.usp.icmc.sgpc.beans.ItemBean;
import br.usp.icmc.sgpc.beans.OrcamentoBean;
import br.usp.icmc.sgpc.beans.ProdutoBean;
import br.usp.icmc.sgpc.beans.ProjetoBean;
import br.usp.icmc.sgpc.beans.SolicitacaoCotacaoBean;
import br.usp.icmc.sgpc.security.RbacConstantes;
import br.usp.icmc.sgpc.security.service.SecurityService;
import br.usp.icmc.sgpc.service.CotacaoService;
import br.usp.icmc.sgpc.service.Service;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import org.apache.log4j.Logger;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;

/**
 *
 * @author herick
 */
@ManagedBean(name = "infoCotacaoMB")
@ViewScoped
public class InfoCotacaoManagedBean implements Serializable {

    private static final Logger logger = Logger.getLogger(InfoCotacaoManagedBean.class);
    public static final String PESQUISAR_STATE = "pesquisar";
    public static final String ADICIONAR_STATE = "adicionar";
    public static final String EDITAR_STATE = "editar";
    private String currentState = PESQUISAR_STATE;
    private String imagemEditarRegistro = "/images/icons/editar.png";
    private String imagemExcluirRegistro = "/images/icons/delbw.png";
    private String imagemCriarRegistro = "/images/icons/add1.png";
    private CotacaoBean cotacao;
    private ItemBean item;
    private List<String> listaStatusItem = new ArrayList<String>();
    @ManagedProperty(value = "#{userSessionMB}")
    private UserSessionManagedBean userSessionMB;
    private String pesquisa;
    private List<ProdutoBean> listaProdutos = new ArrayList<ProdutoBean>();
    private ProdutoBean produto;
    private List<GrupoFornecimentoBean> listaGruposFornecimento = new ArrayList<GrupoFornecimentoBean>();
    private GrupoFornecimentoBean grupo;
    private SolicitacaoCotacaoBean solicitacao;
    private boolean podeSolicitarCotacao = false;
    private List<OrcamentoBean> listaOrcamentos = new ArrayList<OrcamentoBean>();
    OrcamentoBean orcamento = new OrcamentoBean();
    OrcamentoBean orcamentoSelecionado = new OrcamentoBean();
    private AuxilioBean auxilio = new AuxilioBean();
    private AlineaBean alinea = new AlineaBean();
    private List<AlineaBean> listaAlineas = new ArrayList<AlineaBean>();

    @PostConstruct
    public void postConstruct() {
        this.cotacao = (CotacaoBean) userSessionMB.getInterPageParameter();
        pesquisar();
        listaGruposFornecimento = Service.getInstance().listarGruposFornecimento();
        podeSolicitarCotacao = SecurityService.getInstance().verificarPermissao(userSessionMB.getLoggedUser(), RbacConstantes.SOLICITA_COTACAO);
    }

    public boolean isPodeSolicitarCotacao() {
        return podeSolicitarCotacao;
    }

    public void setPodeSolicitarCotacao(boolean podeSolicitarCotacao) {
        this.podeSolicitarCotacao = podeSolicitarCotacao;
    }

    public OrcamentoBean getOrcamentoSelecionado() {
        return orcamentoSelecionado;
    }

    public void setOrcamentoSelecionado(OrcamentoBean orcamentoSelecionado) {
        this.orcamentoSelecionado = orcamentoSelecionado;
    }

    public AlineaBean getAlinea() {
        return alinea;
    }

    public void setAlinea(AlineaBean alinea) {
        this.alinea = alinea;
    }

    public AuxilioBean getAuxilio() {
        return auxilio;
    }

    public void setAuxilio(AuxilioBean auxilio) {
        this.auxilio = auxilio;
    }

    public List<AlineaBean> getListaAlineas() {
        return listaAlineas;
    }

    public void setListaAlineas(List<AlineaBean> listaAlineas) {
        this.listaAlineas = listaAlineas;
    }

    public OrcamentoBean getOrcamento() {
        return orcamento;
    }

    public void setOrcamento(OrcamentoBean orcamento) {
        this.orcamento = orcamento;
    }

    public List<OrcamentoBean> getListaOrcamentos() {
        return listaOrcamentos;
    }

    public void setListaOrcamentos(List<OrcamentoBean> listaOrcamentos) {
        this.listaOrcamentos = listaOrcamentos;
    }

    public SolicitacaoCotacaoBean getSolicitacao() {
        return solicitacao;
    }

    public void setSolicitacao(SolicitacaoCotacaoBean solicitacao) {
        this.solicitacao = solicitacao;
    }

    public GrupoFornecimentoBean getGrupo() {
        return grupo;
    }

    public void setGrupo(GrupoFornecimentoBean grupo) {
        this.grupo = grupo;
    }

    public List<GrupoFornecimentoBean> getListaGruposFornecimento() {
        return listaGruposFornecimento;
    }

    public void setListaGruposFornecimento(List<GrupoFornecimentoBean> listaGruposFornecimento) {
        this.listaGruposFornecimento = listaGruposFornecimento;
    }

    public ProdutoBean getProduto() {
        return produto;
    }

    public void setProduto(ProdutoBean produto) {
        this.produto = produto;
    }

    public List<ProdutoBean> getListaProdutos() {
        return listaProdutos;
    }

    public void setListaProdutos(List<ProdutoBean> listaProdutos) {
        this.listaProdutos = listaProdutos;
    }

    public String getPesquisa() {
        return pesquisa;
    }

    public void setPesquisa(String pesquisa) {
        this.pesquisa = pesquisa;
    }

    public String getImagemCriarRegistro() {
        return imagemCriarRegistro;
    }

    public void setImagemCriarRegistro(String imagemCriarRegistro) {
        this.imagemCriarRegistro = imagemCriarRegistro;
    }

    public String getImagemEditarRegistro() {
        return imagemEditarRegistro;
    }

    public void setImagemEditarRegistro(String imagemEditarRegistro) {
        this.imagemEditarRegistro = imagemEditarRegistro;
    }

    public String getImagemExcluirRegistro() {
        return imagemExcluirRegistro;
    }

    public void setImagemExcluirRegistro(String imagemExcluirRegistro) {
        this.imagemExcluirRegistro = imagemExcluirRegistro;
    }

    public CotacaoBean getCotacao() {
        return cotacao;
    }

    public void setCotacao(CotacaoBean cotacao) {
        this.cotacao = cotacao;
    }

    public String getCurrentState() {
        return currentState;
    }

    public void setCurrentState(String currentState) {
        this.currentState = currentState;
    }

    public ItemBean getItem() {
        return item;
    }

    public void setItem(ItemBean item) {
        this.item = item;
    }

    public List<String> getListaStatusItem() {
        this.listaStatusItem.clear();
        this.listaStatusItem.add("Em Elaboração");
        this.listaStatusItem.add("Aprovado");
        this.listaStatusItem.add("Anulado");
        this.listaStatusItem.add("Concluído");
        this.listaStatusItem.add("Cotado");
        this.listaStatusItem.add("Não Aprovado");
        this.listaStatusItem.add("Para Cotação");
        this.listaStatusItem.add("Solicitação Enviada");
        return listaStatusItem;
    }

    public void setListaStatusItem(List<String> listaStatusItem) {
        this.listaStatusItem = listaStatusItem;
    }

    public UserSessionManagedBean getUserSessionMB() {
        return userSessionMB;
    }

    public void setUserSessionMB(UserSessionManagedBean userSessionMB) {
        this.userSessionMB = userSessionMB;
    }

    public boolean podeEditarItem(ItemBean item) {

        if (podeSolicitarCotacao) {

            if (!item.getStatus().equals("Em Elaboração")) {
                imagemEditarRegistro = "/images/icons/editarbw.png";
                imagemExcluirRegistro = "/images/icons/delbw.png";
                return false;
            } else {
                imagemEditarRegistro = "/images/icons/editar.png";
                imagemExcluirRegistro = "/images/icons/del.png";
                return true;
            }

        } else {

            if (!cotacao.getStatus().equals("Em Elaboração")) {
                imagemEditarRegistro = "/images/icons/editarbw.png";
                imagemExcluirRegistro = "/images/icons/delbw.png";
                return false;
            } else {
                imagemEditarRegistro = "/images/icons/editar.png";
                imagemExcluirRegistro = "/images/icons/del.png";
                return true;
            }

        }

    }

    public boolean podeSolicitarFinanceiro() {
        if (cotacao.getStatus().equals("Em Elaboração")) {
            return true;
        } else {
            return false;
        }
    }

    public boolean podeSolicitarCotacao() {
        if (podeSolicitarCotacao) {
            return true;
        } else {
            return false;
        }
    }

    public boolean podeAdicionarItem() {
        if (cotacao.getStatus().equals("Em Elaboração") || podeSolicitarCotacao) {
            return true;
        } else {
            return false;
        }
    }

    public void pesquisar() {
        logger.debug("PESQUISAR");
        if (("".equals(this.pesquisa) || this.pesquisa == null) && grupo == null) {
            logger.debug("PESQUISAR TODOS");
            this.listaProdutos = Service.getInstance().listarProdutos();
        } else {
            logger.debug("PESQUISAR COM TERMO");
            this.listaProdutos = Service.getInstance().pesquisarProdutosCriteria(pesquisa, grupo);
        }
    }

    public void prepareEditar() {
        this.setCurrentState(EDITAR_STATE);
    }

    public void prepareAdicionar() {
        this.setCurrentState(ADICIONAR_STATE);
        item = new ItemBean();
    }

    public void prepareAdicionarSolicitacao() {
        this.setCurrentState(ADICIONAR_STATE);
        solicitacao = new SolicitacaoCotacaoBean();
    }

    public void gravar() {
        if (ADICIONAR_STATE.equals(this.currentState)) {
            this.item.setFkProduto(produto);
            this.item.setFkCotacao(cotacao);
            this.item.setStatus("Em Elaboração");
            Service.getInstance().cadastraItem(item);
        } else if (EDITAR_STATE.equals(this.currentState)) {
            Service.getInstance().atualizarItem(item);
        }
        this.item = new ItemBean();
        atualizaCotacao();
        RequestContext.getCurrentInstance().execute("PF('addEditPopupItem').hide();");
    }

    public void excluir() {
        logger.debug("EXCLUIR REGISTRO");
        cotacao.getItens().remove(item);
        Service.getInstance().excluirItem(item);
        atualizaCotacao();
        RequestContext.getCurrentInstance().execute("PF('deletePopupItem').hide();");
    }

    public void atualizaCotacao() {
        this.cotacao = Service.getInstance().buscarCotacao(this.cotacao.getId());
        userSessionMB.setInterPageParameter(cotacao);
    }

    public String returnInfoProjeto() {
        atualizaProjeto();
        return "infoProjeto?faces-redirect=true";
    }

    public void atualizaProjeto() {
        ProjetoBean projeto = Service.getInstance().buscarProjeto(cotacao.getFkProjeto().getId());
        userSessionMB.setProjeto(projeto);
    }

    public void selecionarProduto(SelectEvent event) {
        prepareAdicionar();
    }

    public void solicitarCotacao() {
        CotacaoService.getInstance().enviaCotacaoFinanceiro(cotacao);
        cotacao = Service.getInstance().buscarCotacao(cotacao.getId());
    }

    public void solicitarOrcamento() {
        CotacaoService.getInstance().solicitaCotacaoFornecedores(item, solicitacao);
        cotacao = Service.getInstance().buscarCotacao(cotacao.getId());
        RequestContext.getCurrentInstance().execute("PF('addEditPopupSolicitacao').hide();");
    }

    public boolean podeAprovarItem(ItemBean item) {
        listaOrcamentos.clear();
        if (item.getStatus().equals("Cotado")) {
            return true;
        } else {
            return false;
        }
    }

    public void aprovarCotacao() {
        logger.debug("--> Aprovar Cotação");

        DespesaBean despesa = new DespesaBean();
        despesa.setObservacao("Referente a compra de: "+ item.getQuantidade()+" "+item.getUnidade()+" de "+item.getFkProduto().getDescricao());

        Date data = new Date();
        despesa.setDataRealizada(data);

        despesa.setStatus("Em Elaboração");

        
        despesa.setFkAlinea(alinea);

        if (alinea.getFkCategoriaAlinea().isCapital()) {
            despesa.setTipoDespesa("capital");
        } else {
            despesa.setTipoDespesa("custeio");
        }

        despesa.setValor(orcamentoSelecionado.getValor());
        Service.getInstance().cadastraDespesa(despesa);
        item.setFkDespesa(despesa);
        item.setStatus("Aprovado");
        Service.getInstance().atualizarItem(item);
        RequestContext.getCurrentInstance().execute("PF('addEditPopupCotacao').hide();");
    }

    public void prepareEditarAprovacao() {

        List<SolicitacaoCotacaoBean> listaSolicitacao = new ArrayList<SolicitacaoCotacaoBean>();
        SolicitacaoCotacaoBean solicit = new SolicitacaoCotacaoBean();
        listaSolicitacao = item.getSolicitacoesCotacoes();

        for (int i = 0; i < listaSolicitacao.size(); i++) {
            solicit = listaSolicitacao.get(i);
            listaOrcamentos.addAll(solicit.getOrcamentos());
        }
        orcamento = getMenorValor();
    }

    public OrcamentoBean getMenorValor() {
        OrcamentoBean orcMenor = new OrcamentoBean();
        if (listaOrcamentos != null) {
            orcMenor = listaOrcamentos.get(0);
            for (int i = 1; i < listaOrcamentos.size(); i++) {
                if (listaOrcamentos.get(i).getValor().compareTo(orcMenor.getValor()) == -1) {
                    orcMenor = listaOrcamentos.get(i);
                }
            }
        }
        return orcMenor;
    }

    public void pesquisarAlineas() {
        listaAlineas.clear();
        if (auxilio != null) {
            listaAlineas = auxilio.getAlineas();
        }
    }
}
