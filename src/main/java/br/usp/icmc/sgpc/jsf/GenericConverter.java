/*
 * Sistema de Gestão de Projetos Científicos - SGPC
 * 
 * 
 * Copyright  (C)  2010-2014  Instituto  de  Ciências  Matemáticas  e  de
 *                            Computação - ICMC/USP
 * 
 * 
 * This program is  free software; you can redistribute  it and/or modify
 * it under the  terms of the GNU General Public  License as published by
 * the Free Software Foundation; either  version 2 of the License, or (at
 * your option) any later version.
 * 
 * This program  is distributed in the  hope that it will  be useful, but
 * WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
 * MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
 * General Public License for more details.
 * 
 * You  should have received  a copy  of the  GNU General  Public License
 * along  with  this  program;  if   not,  write  to  the  Free  Software
 * Foundation,  Inc.,  51  Franklin   Street,  Fifth  Floor,  Boston,  MA
 * 02110-1301, USA.
 * 
 * 
 * EXCEPTION TO THE TERMS AND CONDITIONS OF GNU GENERAL PUBLIC LICENSE
 * 
 * The  LICENSE  file may  contain  an  additional  clause as  a  special
 * exception  to the  terms  and  conditions of  the  GNU General  Public
 * License. This  clause, if  present, gives you  the permission  to link
 * this  program with  certain third-part  software and  to obtain,  as a
 * result, a  work based  on this program  that can be  distributed using
 * other license than the GNU General Public License.  If you modify this
 * program, you may extend this exception to your version of the program,
 * but you  are not obligated  to do so.   If you do  not wish to  do so,
 * delete this exception statement from your version.
 * 
 */

package br.usp.icmc.sgpc.jsf;

import br.usp.icmc.sgpc.fmw.ApplicationContextProvider;
import br.usp.icmc.sgpc.interfaces.SgpcBeanInterface;
import java.util.ArrayList;
import java.util.List;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.orm.jpa.EntityManagerFactoryUtils;

/**
 *
 * @author Artur
 */
public class GenericConverter {
    private static final Logger logger = Logger.getLogger(GenericConverter.class);
    //public static List<SelectItem> createSelectItems(List<? extends SgpcBeanInterface> listaEntrada) {
    //public static List<SelectItem> createSelectItems(List<SgpcBeanInterface> listaEntrada){
    public static List<SelectItem> createSelectItems(List listaEntrada){
        List<SelectItem> listaSelectItens = new ArrayList<SelectItem>();
        //logger.debug("listaEntrada.size(): "+listaEntrada.size());
        for(Object obj : listaEntrada){
            if(obj instanceof SgpcBeanInterface)
            listaSelectItens.add(new SelectItem(((SgpcBeanInterface)obj).getId(), obj.toString()));
        }
        //logger.debug("listaSelectItens.size(): "+listaSelectItens.size());
        return listaSelectItens;
    }
    
//    public static List<SelectItem> createSelectItems(List listaEntrada){
//        List<SelectItem> listaSelectItens = new ArrayList<SelectItem>();
//        for(Object obj : listaEntrada){
//            listaSelectItens.add(new SelectItem(obj, obj.toString()));
//        }
//        return listaSelectItens;
//    }
    
}
//public class GenericConverter <tipo> {
//
//    private Class<? extends tipo> clazz = null;
//    protected Logger logger = null;
//
//    public GenericConverter(Class<? extends tipo> clazz) {
//        this.clazz = clazz;
//        logger = Logger.getLogger(this.getClass());
//    }
//    
////    public Object getAsObject(FacesContext fc, UIComponent uic, String string) {
////        try {
////            Integer id = new Integer(string);//clazz.
//////            tipo retorno = new GenericJpaDao().findEntity(id);
//////            return retorno;
////        } catch (Exception e) {
////            return null;
////        }
////    }
////
////    public String getAsString(FacesContext fc, UIComponent uic, Object o) {
////        AlineaBean alinea = (AlineaBean) o;
////        if (alinea != null) {
////            return alinea.getId().toString();
////        } else {
////            return null;
////        }
////    }
////    
////    private void init(Class<? extends tipo> clazz, String EMF){
////        this.clazz = clazz;
////        FacesContext fc = FacesContext.getCurrentInstance();
////        ApplicationContext ctx = ApplicationContextProvider.getApplicationContext();
////        EntityManagerFactory emf = (EntityManagerFactory) ctx.getBean(EMF);
////        entityManager = emf.createEntityManager();
////        EntityManagerFactoryUtils.getTransactionalEntityManager(emf);
////        logger = Logger.getLogger(this.getClass());
////    }
//}
