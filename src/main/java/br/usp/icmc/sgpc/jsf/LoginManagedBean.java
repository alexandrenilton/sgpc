/*
 * Sistema de Gestão de Projetos Científicos - SGPC
 * 
 * 
 * Copyright  (C)  2010-2014  Instituto  de  Ciências  Matemáticas  e  de
 *                            Computação - ICMC/USP
 * 
 * 
 * This program is  free software; you can redistribute  it and/or modify
 * it under the  terms of the GNU General Public  License as published by
 * the Free Software Foundation; either  version 2 of the License, or (at
 * your option) any later version.
 * 
 * This program  is distributed in the  hope that it will  be useful, but
 * WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
 * MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
 * General Public License for more details.
 * 
 * You  should have received  a copy  of the  GNU General  Public License
 * along  with  this  program;  if   not,  write  to  the  Free  Software
 * Foundation,  Inc.,  51  Franklin   Street,  Fifth  Floor,  Boston,  MA
 * 02110-1301, USA.
 * 
 * 
 * EXCEPTION TO THE TERMS AND CONDITIONS OF GNU GENERAL PUBLIC LICENSE
 * 
 * The  LICENSE  file may  contain  an  additional  clause as  a  special
 * exception  to the  terms  and  conditions of  the  GNU General  Public
 * License. This  clause, if  present, gives you  the permission  to link
 * this  program with  certain third-part  software and  to obtain,  as a
 * result, a  work based  on this program  that can be  distributed using
 * other license than the GNU General Public License.  If you modify this
 * program, you may extend this exception to your version of the program,
 * but you  are not obligated  to do so.   If you do  not wish to  do so,
 * delete this exception statement from your version.
 * 
 */

package br.usp.icmc.sgpc.jsf;

import br.usp.icmc.sgpc.beans.PessoaBean;
import br.usp.icmc.sgpc.fmw.USPException;
import br.usp.icmc.sgpc.security.ConfigConstantes;
import br.usp.icmc.sgpc.security.RbacConstantes;
import br.usp.icmc.sgpc.security.service.SecurityService;
import br.usp.icmc.sgpc.service.AuditoriaService;
import br.usp.icmc.sgpc.service.AutenticacaoService;
import br.usp.icmc.sgpc.service.MailService;
import br.usp.icmc.sgpc.service.Service;
import java.io.Serializable;
import java.util.Map;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import org.primefaces.context.RequestContext;

/**
 *
 * @author Artur
 */
@ManagedBean(name = "loginMB")
@RequestScoped
public class LoginManagedBean implements Serializable {

    private static final Logger logger = Logger.getLogger(LoginManagedBean.class);
    private String username;
    private String password;
    private String email;
    private boolean acessaProjetoLogin;
    private String mensagensAtualizacao;

    public LoginManagedBean() {
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String doLogin() {
        if (isCamposValidos()) {
            try {
                PessoaBean pessoa = AutenticacaoService.getInstance().realizaLogin(this.username, this.password);

                FacesContext ctx = FacesContext.getCurrentInstance();
                Map sessionMap = ctx.getExternalContext().getSessionMap();
                UserSessionManagedBean userSessionMB = (UserSessionManagedBean) sessionMap.get("userSessionMB");
                if (userSessionMB == null) {
                    userSessionMB = new UserSessionManagedBean();
                    sessionMap.put("userSessionMB", userSessionMB);
                }
                userSessionMB.setLoggedUser(pessoa);                

                acessaProjetoLogin = SecurityService.getInstance().verificarPermissao(pessoa, RbacConstantes.ACESSA_PROJETO_LOGIN);
                
                if(acessaProjetoLogin){
                    return "projetos?faces-redirect=true";
                }else{
                    return "auxilios?faces-redirect=true";
                }
                //return Constantes.SUCESSO;                
            } catch (USPException e) {
                this.username = "";
                this.password = "";
                FacesContext.getCurrentInstance().addMessage("invalido", new FacesMessage(e.getMessage()));
                return "login";
            }
        } else {
            return "login";
        }
    }

    private boolean isCamposValidos() {
        boolean camposValidos = true;
        if ("".equals(username)) {
            FacesContext.getCurrentInstance().addMessage("invalido", new FacesMessage("* O Campo Usuário deve ser preenchido."));
            camposValidos = false;
        }
        if ("".equals(password)) {
            FacesContext.getCurrentInstance().addMessage("invalido", new FacesMessage("* O Campo Senha deve ser preenchido."));
            camposValidos = false;
        }
        return camposValidos;
    }

    public String doLogout() {
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("userSessionMB");
        //Contexto da Aplicação  
        FacesContext conext = FacesContext.getCurrentInstance();  
        //Verifica a sessao e a grava na variavel  
        HttpSession session = (HttpSession) conext.getExternalContext().getSession(false);  
        //Fecha/Destroi sessao  
        session.invalidate(); 
        return "login?faces-redirect=true";
    }

    public void recuperarSenha() {
        try {
            PessoaBean pessoa = Service.getInstance().pesquisarPessoaEmail(email);
            FacesContext.getCurrentInstance().addMessage("invalido", new FacesMessage("* Um e-mail foi enviado com a sua nova senha."));
            String password = AutenticacaoService.getInstance().getRandomPassword(8);
            pessoa.setPassword(AutenticacaoService.getInstance().encryptPassword(password));

            Service.getInstance().atualizarPessoa(pessoa);

            String subject = "Recuperação de Senha - SGPC";
            String setTo = pessoa.getEmail();
            String message = "Sua senha foi alterada pelo sistema SGPC. \n"
                    + "Sua nova senha encontra-se abaixo: \n"
                    + "Nome de Usuário - " + pessoa.getUsername() + " \n"
                    + "Senha - " + password;
            MailService.getInstance().sendMail(subject, setTo, message);

            AuditoriaService.getInstance().gravarAcaoUsuario(pessoa,"Envio de Senha", "Login", "Envio de nova senha por e-mail.", ConfigConstantes.CONFIG_AUDITORIA_SENHA_EMAIL);
            
            RequestContext.getCurrentInstance().execute("PF('lostMailPanel').hide();");

        } catch (USPException ex) {
            FacesContext.getCurrentInstance().addMessage("invalido", new FacesMessage(ex.getMessage()));
        }
    }
    
}
