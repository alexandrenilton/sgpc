/*
 * Sistema de Gestão de Projetos Científicos - SGPC
 * 
 * 
 * Copyright  (C)  2010-2014  Instituto  de  Ciências  Matemáticas  e  de
 *                            Computação - ICMC/USP
 * 
 * 
 * This program is  free software; you can redistribute  it and/or modify
 * it under the  terms of the GNU General Public  License as published by
 * the Free Software Foundation; either  version 2 of the License, or (at
 * your option) any later version.
 * 
 * This program  is distributed in the  hope that it will  be useful, but
 * WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
 * MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
 * General Public License for more details.
 * 
 * You  should have received  a copy  of the  GNU General  Public License
 * along  with  this  program;  if   not,  write  to  the  Free  Software
 * Foundation,  Inc.,  51  Franklin   Street,  Fifth  Floor,  Boston,  MA
 * 02110-1301, USA.
 * 
 * 
 * EXCEPTION TO THE TERMS AND CONDITIONS OF GNU GENERAL PUBLIC LICENSE
 * 
 * The  LICENSE  file may  contain  an  additional  clause as  a  special
 * exception  to the  terms  and  conditions of  the  GNU General  Public
 * License. This  clause, if  present, gives you  the permission  to link
 * this  program with  certain third-part  software and  to obtain,  as a
 * result, a  work based  on this program  that can be  distributed using
 * other license than the GNU General Public License.  If you modify this
 * program, you may extend this exception to your version of the program,
 * but you  are not obligated  to do so.   If you do  not wish to  do so,
 * delete this exception statement from your version.
 * 
 */

package br.usp.icmc.sgpc.jsf;

import br.usp.icmc.sgpc.beans.AtividadeBean;
import br.usp.icmc.sgpc.beans.PessoaBean;
import br.usp.icmc.sgpc.service.Service;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.apache.log4j.Logger;
import org.primefaces.context.RequestContext;

/**
 *
 * @author herick
 */
@ManagedBean(name = "atividadeMB")
@ViewScoped
public class AtividadeManagedBean implements Serializable {

    private static final Logger logger = Logger.getLogger(AtividadeManagedBean.class);
    public static final String PESQUISAR_STATE = "pesquisar";
    public static final String ADICIONAR_STATE = "adicionar";
    public static final String EDITAR_STATE = "editar";
    private String currentState = PESQUISAR_STATE;
    private String pesquisa;
    private String pesquisaPessoa;
    private List<AtividadeBean> listaAtividade = new ArrayList<AtividadeBean>();
    private AtividadeBean atividade;
    private String imagemEditarRegistro = "/images/icons/editar.png";
    private String imagemExcluirRegistro = "/images/icons/delbw.png";
    private String imagemCriarRegistro = "/images/icons/add1.png";
    private String imagemInscricao = "/images/icons/add_user.png";
    private String imagemExcluirInscricao = "/images/icons/del.png";
    private PessoaBean[] pessoas;
    private List<PessoaBean> listaPessoas = new ArrayList<PessoaBean>();

    public AtividadeManagedBean() {
        pesquisar();

        listaPessoas = Service.getInstance().listarPessoas();
    }

    public String getImagemExcluirInscricao() {
        return imagemExcluirInscricao;
    }

    public void setImagemExcluirInscricao(String imagemExcluirInscricao) {
        this.imagemExcluirInscricao = imagemExcluirInscricao;
    }

    public String getPesquisaPessoa() {
        return pesquisaPessoa;
    }

    public void setPesquisaPessoa(String pesquisaPessoa) {
        this.pesquisaPessoa = pesquisaPessoa;
    }

    public List<PessoaBean> getListaPessoas() {
        return listaPessoas;
    }

    public void setListaPessoas(List<PessoaBean> listaPessoas) {
        this.listaPessoas = listaPessoas;
    }

    public PessoaBean[] getPessoas() {
        return pessoas;
    }

    public void setPessoas(PessoaBean[] pessoas) {
        this.pessoas = pessoas;
    }

    public String getImagemInscricao() {
        return imagemInscricao;
    }

    public void setImagemInscricao(String imagemInscricao) {
        this.imagemInscricao = imagemInscricao;
    }

    public String getImagemCriarRegistro() {
        return imagemCriarRegistro;
    }

    public void setImagemCriarRegistro(String imagemCriarRegistro) {
        this.imagemCriarRegistro = imagemCriarRegistro;
    }

    public String getImagemEditarRegistro() {
        return imagemEditarRegistro;
    }

    public void setImagemEditarRegistro(String imagemEditarRegistro) {
        this.imagemEditarRegistro = imagemEditarRegistro;
    }

    public String getImagemExcluirRegistro() {
        return imagemExcluirRegistro;
    }

    public void setImagemExcluirRegistro(String imagemExcluirRegistro) {
        this.imagemExcluirRegistro = imagemExcluirRegistro;
    }

    public AtividadeBean getAtividade() {
        return atividade;
    }

    public void setAtividade(AtividadeBean atividade) {
        this.atividade = atividade;
    }

    public String getCurrentState() {
        return currentState;
    }

    public void setCurrentState(String currentState) {
        this.currentState = currentState;
    }

    public List<AtividadeBean> getListaAtividade() {
        return listaAtividade;
    }

    public void setListaAtividade(List<AtividadeBean> listaAtividade) {
        this.listaAtividade = listaAtividade;
    }

    public String getPesquisa() {
        return pesquisa;
    }

    public void setPesquisa(String pesquisa) {
        this.pesquisa = pesquisa;
    }

    public void prepareAdicionar() {
        this.atividade = new AtividadeBean();
        this.setCurrentState(ADICIONAR_STATE);
        RequestContext.getCurrentInstance().execute("PF('addEditPopup').show();");
    }

    public void prepareEditar(AtividadeBean atividade) {
        this.setCurrentState(EDITAR_STATE);
        this.atividade = atividade;
        RequestContext.getCurrentInstance().execute("PF('addEditPopup').show();");
    }

    public void pesquisar() {
        logger.debug("PESQUISAR");
        if ("".equals(this.pesquisa) || this.pesquisa == null) {
            logger.debug("PESQUISAR TODOS");
            this.listaAtividade = Service.getInstance().listarAtividade();
        } else {
            logger.debug("PESQUISAR COM TERMO");
            this.listaAtividade = Service.getInstance().pesquisarAtividade(pesquisa);
        }
    }

    public void excluir() {
        RequestContext.getCurrentInstance().execute("PF('deletePopup').hide();");
    }

    public void gravar() {
        if (ADICIONAR_STATE.equals(this.currentState)) {
            logger.debug("ADICIONAR REGISTRO");
            Service.getInstance().cadastraAtividade(atividade);
        } else if (EDITAR_STATE.equals(this.currentState)) {
            logger.debug("EDITAR REGISTRO");
            Service.getInstance().atualizarAtividade(atividade);
        }
        this.pesquisar();
        RequestContext.getCurrentInstance().execute("PF('addEditPopup').hide();");
    }

    public void pesquisarPessoa() {
        if ((pesquisaPessoa == null || pesquisaPessoa.trim().length() == 0)) {
            logger.debug("PESQUISAR TODOS");
            this.listaPessoas = Service.getInstance().listarPessoas();
        } else {
            logger.debug("PESQUISAR COM TERMO");
            this.listaPessoas = Service.getInstance().pesquisarPessoas(pesquisaPessoa);
        }
    }

    public void adicionaParticipantes() {
        logger.debug("quantidade selecionada -------------" + pessoas.length);
        List<PessoaBean> listaParticipantes = Arrays.asList(pessoas);

        for (int i = 0; i < listaParticipantes.size(); i++) {
            if (this.atividade.getPessoasInscritas().contains(listaParticipantes.get(i))) {
                FacesContext.getCurrentInstance().addMessage("participanteAtividade", new FacesMessage("Pessoa: " + listaParticipantes.get(i).getNome() + " já participa dessa Atividade!"));
            } else {
                this.atividade.getPessoasInscritas().add(listaParticipantes.get(i));
            }
        }

        Service.getInstance().atualizarAtividade(atividade);
        
        RequestContext.getCurrentInstance().execute("PF('addInscricao').hide();");
    }

    public void removerInscrito(AtividadeBean ativ, PessoaBean part) {
        logger.debug("remove Participante -------------" + part.getNome());
        this.atividade = ativ;

        this.atividade.getPessoasInscritas().remove(part);
        Service.getInstance().atualizarAtividade(atividade);
        this.atividade = Service.getInstance().pesquisarAtividade(atividade.getId());

    }
}
