/*
 * Sistema de Gestão de Projetos Científicos - SGPC
 * 
 * 
 * Copyright  (C)  2010-2014  Instituto  de  Ciências  Matemáticas  e  de
 *                            Computação - ICMC/USP
 * 
 * 
 * This program is  free software; you can redistribute  it and/or modify
 * it under the  terms of the GNU General Public  License as published by
 * the Free Software Foundation; either  version 2 of the License, or (at
 * your option) any later version.
 * 
 * This program  is distributed in the  hope that it will  be useful, but
 * WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
 * MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
 * General Public License for more details.
 * 
 * You  should have received  a copy  of the  GNU General  Public License
 * along  with  this  program;  if   not,  write  to  the  Free  Software
 * Foundation,  Inc.,  51  Franklin   Street,  Fifth  Floor,  Boston,  MA
 * 02110-1301, USA.
 * 
 * 
 * EXCEPTION TO THE TERMS AND CONDITIONS OF GNU GENERAL PUBLIC LICENSE
 * 
 * The  LICENSE  file may  contain  an  additional  clause as  a  special
 * exception  to the  terms  and  conditions of  the  GNU General  Public
 * License. This  clause, if  present, gives you  the permission  to link
 * this  program with  certain third-part  software and  to obtain,  as a
 * result, a  work based  on this program  that can be  distributed using
 * other license than the GNU General Public License.  If you modify this
 * program, you may extend this exception to your version of the program,
 * but you  are not obligated  to do so.   If you do  not wish to  do so,
 * delete this exception statement from your version.
 * 
 */

package br.usp.icmc.sgpc.jsf;

/**
 *
 * @author david
 */
import br.usp.icmc.sgpc.beans.FinanciadorBean;
import br.usp.icmc.sgpc.beans.PessoaBean;
import br.usp.icmc.sgpc.beans.PrestacaoContasBean;
import br.usp.icmc.sgpc.service.ReportService;
import br.usp.icmc.sgpc.service.Service;
import java.io.InputStream;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import org.apache.log4j.Logger;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

@ManagedBean(name = "prestacaoMB")
@ViewScoped
public class PrestacaoContasManagedBean implements Serializable {

    public static final String PESQUISAR_STATE = "pesquisar";
    public static final String EDITAR_STATE = "editar";
    public static final String ADICIONAR_STATE = "adicionar";
    private String currentState = PESQUISAR_STATE;
    private static final Logger logger = Logger.getLogger(PrestacaoContasManagedBean.class);
    private String pesquisa;
    private PrestacaoContasBean prestacao;
    private List<PrestacaoContasBean> listaPrestacaoContas = new ArrayList<PrestacaoContasBean>();    
    private List<String> listaStatus = new ArrayList<String>();
    String status;
    FinanciadorBean financiador;
    private List<FinanciadorBean> listaFinanciadores = new ArrayList<FinanciadorBean>();
    @ManagedProperty(value = "#{userSessionMB}")
    private UserSessionManagedBean userSessionMB;
    private Date dataInicio;
    private Date dataFim;

    //Construtor
    public PrestacaoContasManagedBean() {
        logger.debug("Iniciando a classe......");
    }

    @PostConstruct
    public void postConstruct() {
        this.pesquisar();
        listaFinanciadores = Service.getInstance().listarFinanciadores();
    }

    public Date getDataFim() {
        return dataFim;
    }

    public void setDataFim(Date dataFim) {
        this.dataFim = dataFim;
    }

    public Date getDataInicio() {
        return dataInicio;
    }

    public void setDataInicio(Date dataInicio) {
        this.dataInicio = dataInicio;
    }

    public FinanciadorBean getFinanciador() {
        return financiador;
    }

    public void setFinanciador(FinanciadorBean financiador) {
        this.financiador = financiador;
    }

    public List<FinanciadorBean> getListaFinanciadores() {
        return listaFinanciadores;
    }

    public void setListaFinanciadores(List<FinanciadorBean> listaFinanciadores) {
        this.listaFinanciadores = listaFinanciadores;
    }

    public List<String> getListaStatus() {
        listaStatus.clear();
        listaStatus.add("Agendada");
        listaStatus.add("Concluída");
        listaStatus.add("Atrasada");
        return listaStatus;
    }

    public void setListaStatus(List<String> listaStatus) {
        this.listaStatus = listaStatus;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public UserSessionManagedBean getUserSessionMB() {
        return userSessionMB;
    }

    public void setUserSessionMB(UserSessionManagedBean userSessionMB) {
        this.userSessionMB = userSessionMB;
    }

    public String getCurrentState() {
        return currentState;
    }

    public void setCurrentState(String currentState) {
        this.currentState = currentState;
    }

    //Getters and Setters
    public List<PrestacaoContasBean> getListaPrestacaoContas() {
        return listaPrestacaoContas;
    }
   

    public void setListaPrestacaoContas(List<PrestacaoContasBean> listaPrestacaoContas) {
        this.listaPrestacaoContas = listaPrestacaoContas;
    }
    

    public PrestacaoContasBean getPrestacao() {
        return prestacao;
    }

    public void setPrestacao(PrestacaoContasBean prestacao) {
        this.prestacao = prestacao;
    }

    public String getPesquisa() {
        return pesquisa;
    }

    public void setPesquisa(String pesquisa) {
        this.pesquisa = pesquisa;
    }

    public void pesquisar() {
        logger.debug("PESQUISAR");
        this.prestacao = new PrestacaoContasBean();

        PessoaBean user = null;

        if (("".equals(this.pesquisa) || this.pesquisa == null) && this.dataInicio == null && this.dataFim == null && financiador == null && ("".equals(this.status) || this.status == null)) {
            logger.debug("PESQUISAR TODOS");
            this.listaPrestacaoContas = Service.getInstance().listarPrestacoesContas();
        } else {
            logger.debug("PESQUISAR COM TERMO");
            this.listaPrestacaoContas = Service.getInstance().pesquisarPrestacoesContas(user, pesquisa, dataInicio, dataFim, financiador, status);
        }
    }

    public StreamedContent downloadReportPdf() {
        logger.debug("GERAR RELATORIO PDF");
        InputStream stream = ReportService.getInstance().emiteRelatorioPrestacao(listaPrestacaoContas, ReportService.FORMATO_PDF, listarFiltrosAtivos());
        StreamedContent file = new DefaultStreamedContent(stream, "application/pdf", "report.pdf");
        return file;
    }

    public StreamedContent downloadReportXls() {
        logger.debug("GERAR RELATORIO XLS");
        InputStream stream = ReportService.getInstance().emiteRelatorioPrestacao(listaPrestacaoContas, ReportService.FORMATO_XLS, listarFiltrosAtivos());
        StreamedContent file = new DefaultStreamedContent(stream, "application/vnd.ms-excel", "report.xls");
        return file;
    }

    public String detalharPrestacao() {
        userSessionMB.setProjeto(prestacao.getFkAuxilio().getFkProjeto());
        userSessionMB.setAuxilio(prestacao.getFkAuxilio());
        return "infoAuxilio?faces-redirect=true";
    }

    public void bindDataInicio(SelectEvent event) {
        dataInicio = (Date) event.getObject();
        pesquisar();
    }

    public void bindDataFim(SelectEvent event) {
        dataFim = (Date) event.getObject();
        pesquisar();
    }
    
    private List<String> listarFiltrosAtivos(){
        List<String> retorno = new ArrayList<String>();
        
        // Palavra chave
        if(pesquisa!=null && !pesquisa.isEmpty()) retorno.add("Palavra-chave: " + pesquisa);
        
        // Datas
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

        if(dataInicio!=null){
            retorno.add("Data inicial: " + sdf.format(dataInicio));
        }
        if(dataFim!=null){
            retorno.add("Data final: " + sdf.format(dataFim));
        }
        
        //departamento
        if(financiador!=null) retorno.add("Financiador: " + financiador.getSigla());
        
        //status
        if(status!=null && !status.isEmpty()) retorno.add("Status: " + status);
        
        return retorno;
    }
}
