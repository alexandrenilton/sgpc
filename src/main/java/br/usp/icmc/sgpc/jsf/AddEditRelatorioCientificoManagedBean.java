/*
 * Sistema de Gestão de Projetos Científicos - SGPC
 * 
 * 
 * Copyright  (C)  2010-2014  Instituto  de  Ciências  Matemáticas  e  de
 *                            Computação - ICMC/USP
 * 
 * 
 * This program is  free software; you can redistribute  it and/or modify
 * it under the  terms of the GNU General Public  License as published by
 * the Free Software Foundation; either  version 2 of the License, or (at
 * your option) any later version.
 * 
 * This program  is distributed in the  hope that it will  be useful, but
 * WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
 * MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
 * General Public License for more details.
 * 
 * You  should have received  a copy  of the  GNU General  Public License
 * along  with  this  program;  if   not,  write  to  the  Free  Software
 * Foundation,  Inc.,  51  Franklin   Street,  Fifth  Floor,  Boston,  MA
 * 02110-1301, USA.
 * 
 * 
 * EXCEPTION TO THE TERMS AND CONDITIONS OF GNU GENERAL PUBLIC LICENSE
 * 
 * The  LICENSE  file may  contain  an  additional  clause as  a  special
 * exception  to the  terms  and  conditions of  the  GNU General  Public
 * License. This  clause, if  present, gives you  the permission  to link
 * this  program with  certain third-part  software and  to obtain,  as a
 * result, a  work based  on this program  that can be  distributed using
 * other license than the GNU General Public License.  If you modify this
 * program, you may extend this exception to your version of the program,
 * but you  are not obligated  to do so.   If you do  not wish to  do so,
 * delete this exception statement from your version.
 * 
 */

package br.usp.icmc.sgpc.jsf;

import br.usp.icmc.sgpc.beans.AuxilioBean;
import br.usp.icmc.sgpc.beans.RelatorioCientificoBean;
import br.usp.icmc.sgpc.service.Service;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import org.apache.log4j.Logger;
import org.primefaces.context.RequestContext;

/**
 *
 * @author Artur
 */
@ManagedBean(name = "addEditRelatorioCientificoMB")
@ViewScoped
public class AddEditRelatorioCientificoManagedBean implements Serializable {

    private static final Logger logger = Logger.getLogger(AddEditRelatorioCientificoManagedBean.class);
    public static final String PESQUISAR_STATE = "pesquisar";
    public static final String ADICIONAR_STATE = "adicionar";
    public static final String EDITAR_STATE = "editar";
    private String currentState = PESQUISAR_STATE;
    @ManagedProperty(value = "#{userSessionMB}")
    private UserSessionManagedBean userSessionMB;
    private List<String> listaRelatorioCientifico = new ArrayList<String>();
    private RelatorioCientificoBean relatorioCientifico = new RelatorioCientificoBean();

    @PostConstruct
    public void postConstruct() {
        listaRelatorioCientifico.add(RelatorioCientificoBean.STATUS_AGENDADO);
        listaRelatorioCientifico.add(RelatorioCientificoBean.STATUS_ENTREGUE);
        listaRelatorioCientifico.add(RelatorioCientificoBean.STATUS_ATRASADO);
        listaRelatorioCientifico.add(RelatorioCientificoBean.STATUS_APROVADO);
        listaRelatorioCientifico.add(RelatorioCientificoBean.STATUS_NAO_APROVADO);
        
        relatorioCientifico = new RelatorioCientificoBean();

    }

    public UserSessionManagedBean getUserSessionMB() {
        return userSessionMB;
    }

    public void setUserSessionMB(UserSessionManagedBean userSessionMB) {
        this.userSessionMB = userSessionMB;
    }

    public String getCurrentState() {
        return currentState;
    }

    public void setCurrentState(String currentState) {
        this.currentState = currentState;
    }
    
    public void prepareAdicionar() {
        this.setCurrentState(ADICIONAR_STATE);
        this.relatorioCientifico = new RelatorioCientificoBean();
        RequestContext.getCurrentInstance().execute("PF('editRelatorioCientificoPanel').show();");
    }
    
    public void prepareEditar(RelatorioCientificoBean relat) {
        this.setCurrentState(EDITAR_STATE);
        this.relatorioCientifico = relat;
        RequestContext.getCurrentInstance().execute("PF('editRelatorioCientificoPanel').show();");
    }

    public List<String> getListaRelatorioCientifico() {
        return listaRelatorioCientifico;
    }

    public void setListaRelatorioCientifico(List<String> listaRelatorioCientifico) {
        this.listaRelatorioCientifico = listaRelatorioCientifico;
    }

    public RelatorioCientificoBean getRelatorioCientifico() {
        return relatorioCientifico;
    }

    public void setRelatorioCientifico(RelatorioCientificoBean relatorioCientifico) {
        this.relatorioCientifico = relatorioCientifico;
    }

    public void adicionaRelatorioCientifico() {
        logger.debug("-------- Info AUX 2 ----add relatorio--");
        if (ADICIONAR_STATE.equals(this.currentState)) {
            
            AuxilioBean auxTemp = userSessionMB.getAuxilio();
            auxTemp.getRelatorioCientificos().add(this.relatorioCientifico);
            this.relatorioCientifico.setFkAuxilio(auxTemp);
            auxTemp = Service.getInstance().atualizarAuxilio(auxTemp);
            userSessionMB.setAuxilio(auxTemp);
            this.relatorioCientifico = new RelatorioCientificoBean();

        } else if (EDITAR_STATE.equals(this.currentState)) {
            Service.getInstance().atualizarRelatorioCientifico(this.relatorioCientifico);
            //Service.getInstance().atualizarAuxilio(auxilio);
            //this.auxilio = Service.getInstance().buscarAuxilios(auxilio.getId());
            this.relatorioCientifico = new RelatorioCientificoBean();
        }
        //atualizaAuxilio();
        RequestContext.getCurrentInstance().execute("PF('editRelatorioCientificoPanel').hide();");
    }
    
    public void excluirRelatorioCientifico() {
        AuxilioBean auxTemp = userSessionMB.getAuxilio();
        auxTemp.getRelatorioCientificos().remove(this.relatorioCientifico);
        Service.getInstance().excluirRelatorioCientifico(this.relatorioCientifico);
        //Service.getInstance().atualizarAuxilio(auxilio);
        //this.auxilio = Service.getInstance().buscarAuxilios(auxilio.getId());
        this.relatorioCientifico = new RelatorioCientificoBean();
        //atualizaAuxilio();
        RequestContext.getCurrentInstance().execute("PF('deletePopupRelatorio').hide();");
    }
}
