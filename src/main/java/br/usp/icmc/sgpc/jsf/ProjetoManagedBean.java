/*
 * Sistema de Gestão de Projetos Científicos - SGPC
 * 
 * 
 * Copyright  (C)  2010-2014  Instituto  de  Ciências  Matemáticas  e  de
 *                            Computação - ICMC/USP
 * 
 * 
 * This program is  free software; you can redistribute  it and/or modify
 * it under the  terms of the GNU General Public  License as published by
 * the Free Software Foundation; either  version 2 of the License, or (at
 * your option) any later version.
 * 
 * This program  is distributed in the  hope that it will  be useful, but
 * WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
 * MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
 * General Public License for more details.
 * 
 * You  should have received  a copy  of the  GNU General  Public License
 * along  with  this  program;  if   not,  write  to  the  Free  Software
 * Foundation,  Inc.,  51  Franklin   Street,  Fifth  Floor,  Boston,  MA
 * 02110-1301, USA.
 * 
 * 
 * EXCEPTION TO THE TERMS AND CONDITIONS OF GNU GENERAL PUBLIC LICENSE
 * 
 * The  LICENSE  file may  contain  an  additional  clause as  a  special
 * exception  to the  terms  and  conditions of  the  GNU General  Public
 * License. This  clause, if  present, gives you  the permission  to link
 * this  program with  certain third-part  software and  to obtain,  as a
 * result, a  work based  on this program  that can be  distributed using
 * other license than the GNU General Public License.  If you modify this
 * program, you may extend this exception to your version of the program,
 * but you  are not obligated  to do so.   If you do  not wish to  do so,
 * delete this exception statement from your version.
 * 
 */

package br.usp.icmc.sgpc.jsf;

import br.usp.icmc.sgpc.beans.AuxilioBean;
import br.usp.icmc.sgpc.beans.DepartamentoBean;
import br.usp.icmc.sgpc.beans.EventoBean;
import br.usp.icmc.sgpc.beans.ModalidadeBean;
import br.usp.icmc.sgpc.beans.PesquisaBean;
import br.usp.icmc.sgpc.beans.PessoaBean;
import br.usp.icmc.sgpc.beans.PessoaProjetoBean;
import br.usp.icmc.sgpc.beans.ProjetoBean;
import br.usp.icmc.sgpc.security.ConfigConstantes;
import br.usp.icmc.sgpc.security.RbacConstantes;
import br.usp.icmc.sgpc.security.service.SecurityService;
import br.usp.icmc.sgpc.service.AuditoriaService;
import br.usp.icmc.sgpc.service.MailService;
import br.usp.icmc.sgpc.service.ReportService;
import br.usp.icmc.sgpc.service.Service;
import java.io.InputStream;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIComponent;
import javax.faces.component.UIPanel;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;
import org.apache.log4j.Logger;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

/**
 *
 * @author artur
 */
@ManagedBean(name = "projetoMB")
@ViewScoped
public class ProjetoManagedBean implements Serializable {

    private static final Logger logger = Logger.getLogger(ProjetoManagedBean.class);
    public static final String PESQUISAR_STATE = "pesquisar";
    public static final String ADICIONAR_STATE = "adicionar";
    public static final String EDITAR_STATE = "editar";
    private String currentState = PESQUISAR_STATE;
    private ProjetoBean projeto;
    private List<ProjetoBean> listaProjetoBean = new ArrayList<ProjetoBean>();
    private UIPanel panelForm;
    private Integer currentRow;
    private String pesquisa;
    private List<DepartamentoBean> listaDepartamentos = new ArrayList<DepartamentoBean>();
    private List<PessoaBean> listaPessoas = new ArrayList<PessoaBean>();
    private List<String> listaStatus = new ArrayList<String>();
    private List<String> listaStatusPesquisa = new ArrayList<String>();
    private List<String> listaRelacionamentos = new ArrayList<String>();
    private boolean exibirColunaOwner;
    private boolean criarRegistrosTerceiros;
    private boolean criarRegistroProprio;
    private boolean excluirRegistrosTerceiros;
    private boolean editarRegistrosTerceiros;
    private String imagemEditarRegistro = "/images/icons/editar.png";
    private String imagemExcluirRegistro = "/images/icons/delbw.png";
    private String imagemCriarRegistro = "/images/icons/add1.png";
    private String relacionamento;
    private Date dataInicial;
    private Date dataFinal;
    DepartamentoBean departamentoPesquisa;
    String status;
    //Lista de projetos que não se altera durante pesquisa - é a lista que é carregada
    //no combo de Projeto PAI, na edição e cadastro de projeto.
    private List<ProjetoBean> listaProjetoPai = new ArrayList<ProjetoBean>();
    private List<String> listaTipoProjeto = new ArrayList<String>();
    private String tipoProjeto;
    private String statusAntigo;
    @ManagedProperty(value = "#{userSessionMB}")
    private UserSessionManagedBean userSessionMB;
    private PessoaProjetoBean participanteProjeto;
    private boolean encaminharInfoProjeto = false;    
    private boolean podeAlterarResponsavel = false;
    private List<PessoaBean> listaResponsaveis;
    private PessoaBean responsavelPesquisa;

    public UserSessionManagedBean getUserSessionMB() {
        return userSessionMB;
    }

    public boolean isPodeAlterarResponsavel() {
        return podeAlterarResponsavel;
    }

    public void setPodeAlterarResponsavel(boolean podeAlterarResponsavel) {
        this.podeAlterarResponsavel = podeAlterarResponsavel;
    }

    public void setUserSessionMB(UserSessionManagedBean userSessionMB) {
        this.userSessionMB = userSessionMB;
    }

    public boolean isEncaminharInfoProjeto() {
        return encaminharInfoProjeto;
    }

    public void setEncaminharInfoProjeto(boolean encaminharInfoProjeto) {
        this.encaminharInfoProjeto = encaminharInfoProjeto;
    }

    public List<String> getListaTipoProjeto() {
        listaTipoProjeto.clear();
        listaTipoProjeto.add("Pesquisa e Desenvolvimento");
        listaTipoProjeto.add("Evento Científico");
        return listaTipoProjeto;
    }

    public void setListaTipoProjeto(List<String> listaTipoProjeto) {
        this.listaTipoProjeto = listaTipoProjeto;
    }

    public String getTipoProjeto() {
        return tipoProjeto;
    }

    public void setTipoProjeto(String tipoProjeto) {
        this.tipoProjeto = tipoProjeto;
    }

    public List<String> getListaStatusPesquisa() {
        listaStatusPesquisa.add(ProjetoBean.STATUS_EM_PLANEJAMENTO);
        listaStatusPesquisa.add(ProjetoBean.STATUS_EM_ANDAMENTO);
        listaStatusPesquisa.add(ProjetoBean.STATUS_FINALIZADO);
        listaStatusPesquisa.add(ProjetoBean.STATUS_CANCELADO);
        listaStatusPesquisa.add(ProjetoBean.STATUS_ABORTADO);
        listaStatusPesquisa.add(ProjetoBean.STATUS_DESISTENCIA);
        listaStatusPesquisa.add(ProjetoBean.STATUS_ATRASADO);
        return listaStatusPesquisa;
    }

    public void setListaStatusPesquisa(List<String> listaStatusPesquisa) {
        this.listaStatusPesquisa = listaStatusPesquisa;
    }

    public List<ProjetoBean> getListaProjetoPai() {
        this.listaProjetoPai = Service.getInstance().listarProjetos();
        return listaProjetoPai;
    }

    public void setListaProjetoPai(List<ProjetoBean> listaProjetoPai) {
        this.listaProjetoPai = listaProjetoPai;
    }

    public Date getDataFinal() {
        return dataFinal;
    }

    public void setDataFinal(Date dataFinal) {
        this.dataFinal = dataFinal;
    }

    public Date getDataInicial() {
        return dataInicial;
    }

    public void setDataInicial(Date dataInicial) {
        this.dataInicial = dataInicial;
    }

    public DepartamentoBean getDepartamentoPesquisa() {
        return departamentoPesquisa;
    }

    public void setDepartamentoPesquisa(DepartamentoBean departamentoPesquisa) {
        this.departamentoPesquisa = departamentoPesquisa;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCurrentState() {
        return currentState;
    }

    public void setCurrentState(String currentState) {
        this.currentState = currentState;
    }

    @PostConstruct
    public void postConstruct() {
        logger.debug("Instanciando classe");

        exibirColunaOwner = SecurityService.getInstance().verificarPermissao(userSessionMB.getLoggedUser(), RbacConstantes.PROJETO_CONSULTAR_TERCEIROS);
        editarRegistrosTerceiros = SecurityService.getInstance().verificarPermissao(userSessionMB.getLoggedUser(), RbacConstantes.PROJETO_EDITAR_TERCEIROS);
        excluirRegistrosTerceiros = SecurityService.getInstance().verificarPermissao(userSessionMB.getLoggedUser(), RbacConstantes.PROJETO_EXCLUIR_TERCEIROS);
        criarRegistroProprio = SecurityService.getInstance().verificarPermissao(userSessionMB.getLoggedUser(), RbacConstantes.PROJETO_CRIAR_PROPRIO);
        criarRegistrosTerceiros = SecurityService.getInstance().verificarPermissao(userSessionMB.getLoggedUser(), RbacConstantes.PROJETO_CRIAR_TERCEIROS);

        this.pesquisarProjetos(true);
        this.pesquisarDepartamentos();
        this.pesquisarPessoas();
        this.listaResponsaveis = Service.getInstance().pesquisarPessoasReponsaveis();

    }

    public String getRelacionamento() {
        return relacionamento;
    }

    public void setRelacionamento(String relacionamento) {
        this.relacionamento = relacionamento;
    }

    public List<DepartamentoBean> getListaDepartamentos() {
        return listaDepartamentos;
    }

    public void setListaDepartamentos(List<DepartamentoBean> listaDepartamentos) {
        this.listaDepartamentos = listaDepartamentos;
    }

    public List<ProjetoBean> getListaProjetoBean() {
        return listaProjetoBean;
    }

    public void setListaProjetoBean(List<ProjetoBean> listaProjetoBean) {
        this.listaProjetoBean = listaProjetoBean;
    }

    public void setListaStatus(List<String> listaStatus) {
        this.listaStatus = listaStatus;
    }

    public ProjetoBean getProjeto() {
        return projeto;
    }

    public void setProjeto(ProjetoBean projeto) {
        this.projeto = projeto;
    }

    public Integer getCurrentRow() {
        return currentRow;
    }

    public void setCurrentRow(Integer currentRow) {
        this.currentRow = currentRow;
    }

    public UIPanel getPanelForm() {
        return panelForm;
    }

    public void setPanelForm(UIPanel panelForm) {
        this.panelForm = panelForm;
    }

    public String getPesquisa() {
        return pesquisa;
    }

    public void setPesquisa(String pesquisa) {
        this.pesquisa = pesquisa;
    }

    public List<PessoaBean> getListaPessoas() {
        return listaPessoas;
    }

    public void setListaPessoas(List<PessoaBean> listaPessoas) {
        this.listaPessoas = listaPessoas;
    }

    /**
     *
     * @param pesquisaTodos
     * Se true retorna todos os projetos.
     * Se false, retorna lista de projetos de acordo com os parâmetros de pesquisa selecionados na interface visual.
     */
    public void pesquisarProjetos(boolean pesquisaTodos) {
        logger.debug("PESQUISAR");

        PessoaBean user = null;

        if (pesquisaTodos) {
            logger.debug("PESQUISAR TODOS");
            if (exibirColunaOwner) {
                logger.debug("listar terceiros");
                this.listaProjetoBean = Service.getInstance().listarProjetos();
            } else {
                logger.debug("listar owner");
                this.listaProjetoBean = Service.getInstance().pesquisarProjetos(userSessionMB.getLoggedUser());
            }
        } else {
            logger.debug("PESQUISAR COM TERMO");
            if (!exibirColunaOwner) {
                logger.debug("listar terceiros keyword");
                user = userSessionMB.getLoggedUser();
                ;
            }
            this.listaProjetoBean = Service.getInstance().pesquisarProjetos(user, pesquisa, departamentoPesquisa, status, dataInicial, dataFinal, responsavelPesquisa);
        }
    }

    public void prepareAdicionar() {
        logger.debug("=================== PREPARE ADICIONAR ===================");

        if (criarRegistrosTerceiros && criarRegistroProprio) {
            listaPessoas.clear();
            pesquisarPessoas();
        } else if (criarRegistroProprio && !criarRegistrosTerceiros) {
            listaPessoas.clear();
            listaPessoas.add(userSessionMB.getLoggedUser());
        } else if (!criarRegistroProprio && criarRegistrosTerceiros) {
            listaPessoas.clear();
            pesquisarPessoas();
            listaPessoas.remove(userSessionMB.getLoggedUser());
        }

        podeAlterarResponsavel = true;

        this.setCurrentState(ADICIONAR_STATE);
        if (tipoProjeto.equals("Evento Científico")) {
            this.projeto = new EventoBean();
        } else {
            this.projeto = new PesquisaBean();
        }
        
        RequestContext.getCurrentInstance().execute("PF('selectTipoProjeto').hide();");
        RequestContext.getCurrentInstance().execute("PF('addEditPopup').show();");

    }

    public boolean isAdicionarState() {
        return ADICIONAR_STATE.equals(this.getCurrentState());
    }

    public void prepareEditar(ProjetoBean proj) {
        this.projeto = proj;
        statusAntigo = projeto.getStatus();
        podeAlterarResponsavel = false;
        this.setCurrentState(EDITAR_STATE);
        RequestContext.getCurrentInstance().execute("PF('addEditPopup').show();");
    }

    public boolean isEditarState() {
        return EDITAR_STATE.equals(this.getCurrentState());
    }

    public boolean isPesquisarState() {
        String state = this.getCurrentState();
        return (state == null || PESQUISAR_STATE.equals(state));
    }

    public String ativar() {
        userSessionMB.setProjeto(this.projeto);
        return "infoProjeto?faces-redirect=true";
    }

    /*
     * Exclui registro
     */
    public void excluir() {
        logger.debug("EXCLUIR REGISTRO");
        List<PessoaProjetoBean> listaParticipantes = Service.getInstance().buscarPessoasProjeto(projeto);
        for(PessoaProjetoBean partic : listaParticipantes){
            Service.getInstance().excluirPessoaProjeto(partic);
        }
        Service.getInstance().excluirProjeto(projeto);
        this.pesquisarProjetos(false);
        RequestContext.getCurrentInstance().execute("PF('deletePopup').hide();");
    }

    public boolean validarDatas(FacesContext facesContext, UIComponent uiComponent, Object newValue) {
        Date minhaData = (Date) newValue;
        if (1 == 2) {
            throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Data inválida", "Data Inválida"));
        }
        return true;
    }

    public void pesquisarDepartamentos() {
        logger.debug("PESQUISAR Departamentos");
        this.listaDepartamentos = Service.getInstance().pesquisarDepartamentos();
    }

    public void pesquisarPessoas() {
        logger.debug("PESQUISAR Pessoas");
        this.listaPessoas = Service.getInstance().pesquisarPessoasReponsaveis();
    }

    public List<String> getListaRelacionamentos() {
        listaRelacionamentos.clear();
        listaRelacionamentos.add("Subordinado");
        listaRelacionamentos.add("Relacionado");
        return listaRelacionamentos;
    }

    public void setListaRelacionamentos(List<String> listaRelacionamentos) {
        this.listaRelacionamentos = listaRelacionamentos;
    }

    public List<String> getListaStatus() {
        listaStatus.clear();
        if (ADICIONAR_STATE.equals(this.currentState)) {
            listaStatus.add("Em Planejamento");
        } else {
            listaStatus.add("Em Planejamento");
            listaStatus.add("Em Andamento");
            listaStatus.add("Finalizado");
            listaStatus.add("Cancelado");
            listaStatus.add("Abortado");
            listaStatus.add("Desistência");
            listaStatus.add("Atrasado");
        }
        return listaStatus;
    }

    public String gravar() {
        if (this.projeto.getDataInicial().after(this.projeto.getDataFinal())) {
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR,"Data Inicial não pode ser depois da Data Final","Data Inicial não pode ser depois da Data Final");
            FacesContext.getCurrentInstance().addMessage("erroProjeto", message);
        } else {

            if (ADICIONAR_STATE.equals(this.currentState)) {
                logger.debug("ADICIONAR REGISTRO");

                projeto = Service.getInstance().cadastraProjeto(projeto);
                AuditoriaService.getInstance().gravarAcaoUsuario(userSessionMB.getLoggedUser(), "Adicionar Registro", "Projeto", "Adicionou um novo projeto ao sistema - Título: " + projeto.getTitulo(), ConfigConstantes.CONFIG_AUDITORIA_ADICIONAR_PROJETO);
                
                participanteProjeto = new PessoaProjetoBean();
                participanteProjeto.setFkProjeto(projeto);
                participanteProjeto.setFkPessoa(projeto.getFkResponsavel());
                participanteProjeto.setTipo("Responsável");
                userSessionMB.setProjeto(projeto);
                Service.getInstance().cadastraPessoaProjeto(participanteProjeto);
                return "infoProjeto?faces-redirect=true";
            } else if (EDITAR_STATE.equals(this.currentState)) {
                logger.debug("EDITAR REGISTRO");
                if (!projeto.getStatus().equals(statusAntigo)) {
                    AuditoriaService.getInstance().gravarAcaoUsuario(userSessionMB.getLoggedUser(), "Editar Registro", "Projeto",
                            "Alterou Status do projeto de id: " + this.projeto.getId() + " - Título: " + projeto.getTitulo()
                            + " - Foi alterado de:  " + statusAntigo + " para: " + projeto.getStatus(), ConfigConstantes.CONFIG_AUDITORIA_STATUS_PROJETO);

                    String subject = "Alteração de Status em Projeto - SGPC";
                    String setTo = projeto.getFkResponsavel().getEmail();
                    String message = "O Status do projeto de título - " + projeto.getTitulo() + " \n"
                            + "Sofreu alteração de Status. Foi alterado de:  " + statusAntigo + " para: " + projeto.getStatus();
                    MailService.getInstance().sendMail(subject, setTo, message);
                }

                Service.getInstance().atualizarProjeto(projeto);
                AuditoriaService.getInstance().gravarAcaoUsuario(userSessionMB.getLoggedUser(), "Editar Registro", "Projeto", "Editou o projeto de id: " + projeto.getId() + " - Título: " + projeto.getTitulo(), ConfigConstantes.CONFIG_AUDITORIA_EDITAR_PROJETO);
            }
            this.pesquisarProjetos(false);
        }
        RequestContext.getCurrentInstance().execute("PF('addEditPopup').hide();");
        return null;
    }

    public boolean isEditarRegistrosTerceiros() {
        return editarRegistrosTerceiros;
    }

    public void setEditarRegistrosTerceiros(boolean editarRegistrosTerceiros) {
        this.editarRegistrosTerceiros = editarRegistrosTerceiros;
    }

    public boolean isExcluirRegistrosTerceiros() {
        return excluirRegistrosTerceiros;
    }

    public void setExcluirRegistrosTerceiros(boolean excluirRegistrosTerceiros) {
        this.excluirRegistrosTerceiros = excluirRegistrosTerceiros;
    }

    public boolean isExibirColunaOwner() {
        return exibirColunaOwner;
    }

    public void setExibirColunaOwner(boolean exibirColunaOwner) {
        this.exibirColunaOwner = exibirColunaOwner;
    }

    public String getImagemEditarRegistro() {
        return imagemEditarRegistro;
    }

    public void setImagemEditarRegistro(String imagemEditarRegistro) {
        this.imagemEditarRegistro = imagemEditarRegistro;
    }

    public String getImagemExcluirRegistro() {
        return imagemExcluirRegistro;
    }

    public void setImagemExcluirRegistro(String imagemExcluirRegistro) {
        this.imagemExcluirRegistro = imagemExcluirRegistro;
    }

    public boolean isCriarRegistroProprio() {
        return criarRegistroProprio;
    }

    public void setCriarRegistroProprio(boolean criarRegistroProprio) {
        this.criarRegistroProprio = criarRegistroProprio;
    }

    public boolean isCriarRegistrosTerceiros() {
        return criarRegistrosTerceiros;
    }

    public void setCriarRegistrosTerceiros(boolean criarRegistrosTerceiros) {
        this.criarRegistrosTerceiros = criarRegistrosTerceiros;
    }

    public String getImagemCriarRegistro() {
        return imagemCriarRegistro;
    }

    public void setImagemCriarRegistro(String imagemCriarRegistro) {
        this.imagemCriarRegistro = imagemCriarRegistro;
    }

    public boolean podeEditarRegistro(ProjetoBean projeto) {
        if (projeto.getFkResponsavel().equals(userSessionMB.getLoggedUser())) {
            imagemEditarRegistro = "/images/icons/editar.png";
            return false;
        } else if (!projeto.getFkResponsavel().equals(userSessionMB.getLoggedUser()) && editarRegistrosTerceiros) {
            imagemEditarRegistro = "/images/icons/editar.png";
            return false;
        }
        imagemEditarRegistro = "/images/icons/editarbw.png";
        return true;
    }

    public boolean podeExcluirRegistro(ProjetoBean projeto) {

        /*Projeto não poderá ser excluido por enquanto.
         * Descomente o código caso queira usar  a regra de acesso.
         */

        if ((projeto.getFkResponsavel().equals(userSessionMB.getLoggedUser())) && !(projeto.getStatus().equals("Finalizado"))) {
            imagemExcluirRegistro = "/images/icons/del.png";
            return false;
        } else if ((!projeto.getFkResponsavel().equals(userSessionMB.getLoggedUser()) && excluirRegistrosTerceiros) && !(projeto.getStatus().equals("Finalizado"))) {
            imagemExcluirRegistro = "/images/icons/del.png";
            return false;
        } else {
            imagemExcluirRegistro = "/images/icons/delbw.png";
            return true;
        }
    }

    public boolean podeCriarRegistro() {
        if (criarRegistroProprio || criarRegistrosTerceiros) {
            imagemCriarRegistro = "/images/icons/add1.png";
            return false;
        } else {
            imagemCriarRegistro = "/images/icons/add1bw.png";
            return true;
        }
    }

    public StreamedContent downloadReportPdf() {
        logger.debug("GERAR RELATORIO PDF");
        InputStream stream = ReportService.getInstance().emiteRelatorioProjetos(listaProjetoBean, ReportService.FORMATO_PDF, listarFiltrosAtivos());
        StreamedContent file = new DefaultStreamedContent(stream, "application/pdf", "report.pdf");
        return file;
    }

    public StreamedContent downloadReportXls() {
        logger.debug("GERAR RELATORIO XLS");
        InputStream stream = ReportService.getInstance().emiteRelatorioProjetos(listaProjetoBean, ReportService.FORMATO_XLS, listarFiltrosAtivos());
        StreamedContent file = new DefaultStreamedContent(stream, "application/vnd.ms-excel", "report.xls");
        return file;
    }

    public void bindDataInicial(SelectEvent event) {
        dataInicial = (Date) event.getObject();
        pesquisarProjetos(false);
    }

    public void bindDataFinal(SelectEvent event) {
        dataFinal = (Date) event.getObject();
        pesquisarProjetos(false);
    }

    public void atualizaDepartamento() {
        this.projeto.setFkDepartamento(this.projeto.getFkResponsavel().getFkDepartamento());
    }
    
    private List<String> listarFiltrosAtivos(){
        List<String> retorno = new ArrayList<String>();

        // Palavra chave
        if(pesquisa!=null && !pesquisa.isEmpty()) retorno.add("Palavra-chave: " + pesquisa);
        
        // Datas
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

        if(dataInicial!=null){
            retorno.add("Data inicial: " + sdf.format(dataInicial));
        }
        if(dataFinal!=null){
            retorno.add("Data final: " + sdf.format(dataFinal));
        }
        
        //departamento
        if(departamentoPesquisa!=null) retorno.add("Departamento: " + departamentoPesquisa.getSigla());
        
        //status
        if(status!=null && !status.isEmpty()) retorno.add("Status: " + status);
        
        return retorno;
    }

    public List<PessoaBean> getListaResponsaveis() {
        return listaResponsaveis;
    }

    public void setListaResponsaveis(List<PessoaBean> listaResponsaveis) {
        this.listaResponsaveis = listaResponsaveis;
    }

    public PessoaBean getResponsavelPesquisa() {
        return responsavelPesquisa;
    }

    public void setResponsavelPesquisa(PessoaBean responsavelPesquisa) {
        this.responsavelPesquisa = responsavelPesquisa;
    }
}
