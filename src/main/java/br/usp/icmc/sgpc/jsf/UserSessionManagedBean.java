/*
 * Sistema de Gestão de Projetos Científicos - SGPC
 * 
 * 
 * Copyright  (C)  2010-2014  Instituto  de  Ciências  Matemáticas  e  de
 *                            Computação - ICMC/USP
 * 
 * 
 * This program is  free software; you can redistribute  it and/or modify
 * it under the  terms of the GNU General Public  License as published by
 * the Free Software Foundation; either  version 2 of the License, or (at
 * your option) any later version.
 * 
 * This program  is distributed in the  hope that it will  be useful, but
 * WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
 * MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
 * General Public License for more details.
 * 
 * You  should have received  a copy  of the  GNU General  Public License
 * along  with  this  program;  if   not,  write  to  the  Free  Software
 * Foundation,  Inc.,  51  Franklin   Street,  Fifth  Floor,  Boston,  MA
 * 02110-1301, USA.
 * 
 * 
 * EXCEPTION TO THE TERMS AND CONDITIONS OF GNU GENERAL PUBLIC LICENSE
 * 
 * The  LICENSE  file may  contain  an  additional  clause as  a  special
 * exception  to the  terms  and  conditions of  the  GNU General  Public
 * License. This  clause, if  present, gives you  the permission  to link
 * this  program with  certain third-part  software and  to obtain,  as a
 * result, a  work based  on this program  that can be  distributed using
 * other license than the GNU General Public License.  If you modify this
 * program, you may extend this exception to your version of the program,
 * but you  are not obligated  to do so.   If you do  not wish to  do so,
 * delete this exception statement from your version.
 * 
 */

package br.usp.icmc.sgpc.jsf;

import br.usp.icmc.sgpc.beans.AuxilioBean;
import br.usp.icmc.sgpc.beans.FornecedorBean;
import br.usp.icmc.sgpc.beans.PessoaBean;
import br.usp.icmc.sgpc.beans.ProjetoBean;
import br.usp.icmc.sgpc.security.beans.PapelBean;
import java.io.Serializable;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import org.apache.log4j.Logger;

/**
 *
 * @author Artur
 */
@ManagedBean(name = "userSessionMB")
@SessionScoped
public class UserSessionManagedBean implements Serializable {

    private static final Logger logger = Logger.getLogger(UserSessionManagedBean.class);
    private PessoaBean loggedUser;
    private ProjetoBean projeto;
    private FornecedorBean fornecedor;
    private AuxilioBean auxilio;
    private Object interPageParameter;

    public UserSessionManagedBean() {
        logger.debug("Instanciando classe");
    }

    public PessoaBean getLoggedUser() {
        return loggedUser;
    }

    public void setLoggedUser(PessoaBean loggedUser) {
        this.loggedUser = loggedUser;
    }

    public FornecedorBean getFornecedor() {
        return fornecedor;
    }

    public void setFornecedor(FornecedorBean fornecedor) {
        this.fornecedor = fornecedor;
    }

    public ProjetoBean getProjeto() {
        return projeto;
    }

    public void setProjeto(ProjetoBean projeto) {
        this.projeto = projeto;
    }

    public String getProjetoAtivo() {
        if (this.projeto == null) {
            return "Nenhum projeto ativo no sistema";
        } else {
            String nome = "";
            if (this.projeto.toString().length() > 35) {
                nome = this.projeto.toString().substring(0, 35) + "...";
            } else {
                nome = this.projeto.toString();
            }
            return nome;
        }
    }

    public String getPerfil() {
        String perfil = "";
        List<PapelBean> papeis = this.loggedUser.getPapeis();
        for (int i = 0; i < papeis.size(); i++) {
            PapelBean p = papeis.get(i);
            perfil = perfil + p.getNome();
            return perfil;
        }
        return perfil;
    }

    public AuxilioBean getAuxilio() {
        return auxilio;
    }

    public void setAuxilio(AuxilioBean auxilio) {
        this.auxilio = auxilio;
    }

    public Object getInterPageParameter() {
        return interPageParameter;
    }

    public void setInterPageParameter(Object interPageParameter) {
        this.interPageParameter = interPageParameter;
    }
}
