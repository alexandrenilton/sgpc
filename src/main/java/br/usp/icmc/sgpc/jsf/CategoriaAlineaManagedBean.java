/*
 * Sistema de Gestão de Projetos Científicos - SGPC
 * 
 * 
 * Copyright  (C)  2010-2014  Instituto  de  Ciências  Matemáticas  e  de
 *                            Computação - ICMC/USP
 * 
 * 
 * This program is  free software; you can redistribute  it and/or modify
 * it under the  terms of the GNU General Public  License as published by
 * the Free Software Foundation; either  version 2 of the License, or (at
 * your option) any later version.
 * 
 * This program  is distributed in the  hope that it will  be useful, but
 * WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
 * MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
 * General Public License for more details.
 * 
 * You  should have received  a copy  of the  GNU General  Public License
 * along  with  this  program;  if   not,  write  to  the  Free  Software
 * Foundation,  Inc.,  51  Franklin   Street,  Fifth  Floor,  Boston,  MA
 * 02110-1301, USA.
 * 
 * 
 * EXCEPTION TO THE TERMS AND CONDITIONS OF GNU GENERAL PUBLIC LICENSE
 * 
 * The  LICENSE  file may  contain  an  additional  clause as  a  special
 * exception  to the  terms  and  conditions of  the  GNU General  Public
 * License. This  clause, if  present, gives you  the permission  to link
 * this  program with  certain third-part  software and  to obtain,  as a
 * result, a  work based  on this program  that can be  distributed using
 * other license than the GNU General Public License.  If you modify this
 * program, you may extend this exception to your version of the program,
 * but you  are not obligated  to do so.   If you do  not wish to  do so,
 * delete this exception statement from your version.
 * 
 */

package br.usp.icmc.sgpc.jsf;

import br.usp.icmc.sgpc.beans.CategoriaAlineaBean;
import br.usp.icmc.sgpc.security.RbacConstantes;
import br.usp.icmc.sgpc.security.service.SecurityService;
import br.usp.icmc.sgpc.service.ReportService;
import br.usp.icmc.sgpc.service.Service;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.apache.log4j.Logger;
import org.primefaces.context.RequestContext;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

/**
 *
 * @author Herick
 */
@ManagedBean(name = "categoriaAlineaMB")
@ViewScoped
public class CategoriaAlineaManagedBean implements Serializable {

    private static final Logger logger = Logger.getLogger(AtividadeManagedBean.class);
    public static final String PESQUISAR_STATE = "pesquisar";
    public static final String ADICIONAR_STATE = "adicionar";
    public static final String EDITAR_STATE = "editar";
    private String currentState = PESQUISAR_STATE;
    private String pesquisa;
    private List<CategoriaAlineaBean> listaCategoriasAlineas = new ArrayList<CategoriaAlineaBean>();
    private CategoriaAlineaBean categoriaAlinea;
    private boolean podeCriarAlinea;
    private boolean podeEditarAlinea;
    private boolean podeExcluirAlinea;
    private String imagemEditarRegistro = "/images/icons/editar.png";
    private String imagemExcluirRegistro = "/images/icons/delbw.png";
    private String imagemCriarRegistro = "/images/icons/add1.png";
    @ManagedProperty(value = "#{userSessionMB}")
    private UserSessionManagedBean userSessionMB;

    public UserSessionManagedBean getUserSessionMB() {
        return userSessionMB;
    }

    public void setUserSessionMB(UserSessionManagedBean userSessionMB) {
        this.userSessionMB = userSessionMB;
    }

    public String getImagemCriarRegistro() {
        if (podeCriarAlinea) {
            imagemCriarRegistro = "/images/icons/add1.png";
        } else {
            imagemCriarRegistro = "/images/icons/add1bw.png";
        }
        return imagemCriarRegistro;
    }

    public void setImagemCriarRegistro(String imagemCriarRegistro) {
        this.imagemCriarRegistro = imagemCriarRegistro;
    }

    public String getImagemEditarRegistro() {
        if (podeEditarAlinea) {
            imagemEditarRegistro = "/images/icons/editar.png";
        } else {
            imagemEditarRegistro = "/images/icons/editarbw.png";
        }
        return imagemEditarRegistro;
    }

    public void setImagemEditarRegistro(String imagemEditarRegistro) {
        this.imagemEditarRegistro = imagemEditarRegistro;
    }

    public String getImagemExcluirRegistro() {
        if(podeExcluirAlinea){
            imagemExcluirRegistro = "/images/icons/del.png";
        }else{
            imagemExcluirRegistro = "/images/icons/delbw.png";
        }
        return imagemExcluirRegistro;
    }

    public void setImagemExcluirRegistro(String imagemExcluirRegistro) {
        this.imagemExcluirRegistro = imagemExcluirRegistro;
    }

    public boolean isPodeCriarAlinea() {
        return podeCriarAlinea;
    }

    public void setPodeCriarAlinea(boolean podeCriarAlinea) {
        this.podeCriarAlinea = podeCriarAlinea;
    }

    public boolean isPodeEditarAlinea() {
        return podeEditarAlinea;
    }

    public void setPodeEditarAlinea(boolean podeEditarAlinea) {
        this.podeEditarAlinea = podeEditarAlinea;
    }

    public boolean isPodeExcluirAlinea() {
        return podeExcluirAlinea;
    }

    public void setPodeExcluirAlinea(boolean podeExcluirAlinea) {
        this.podeExcluirAlinea = podeExcluirAlinea;
    }

    public CategoriaAlineaBean getCategoriaAlinea() {
        return categoriaAlinea;
    }

    public void setCategoriaAlinea(CategoriaAlineaBean categoriaAlinea) {
        this.categoriaAlinea = categoriaAlinea;
    }

    public List<CategoriaAlineaBean> getListaCategoriasAlineas() {
        return listaCategoriasAlineas;
    }

    public void setListaCategoriasAlineas(List<CategoriaAlineaBean> listaCategoriasAlineas) {
        this.listaCategoriasAlineas = listaCategoriasAlineas;
    }

    public String getPesquisa() {
        return pesquisa;
    }

    public void setPesquisa(String pesquisa) {
        this.pesquisa = pesquisa;
    }

    public String getCurrentState() {
        return currentState;
    }

    public void setCurrentState(String currentState) {
        this.currentState = currentState;
    }

    public void prepareAdicionar() {
        this.setCurrentState(this.ADICIONAR_STATE);
        this.categoriaAlinea = new CategoriaAlineaBean();
        RequestContext.getCurrentInstance().execute("PF('addEditPopup').show();");
    }

    public void prepareEditar(CategoriaAlineaBean catAlinea) {
        this.setCurrentState(this.EDITAR_STATE);
        this.categoriaAlinea = catAlinea;
        RequestContext.getCurrentInstance().execute("PF('addEditPopup').show();");
    }

    public CategoriaAlineaManagedBean() {
    }

    @PostConstruct
    public void postConstruct() {
        logger.debug("Instanciando classe");

        podeEditarAlinea = SecurityService.getInstance().verificarPermissao(userSessionMB.getLoggedUser(), RbacConstantes.ALINEA_EDITAR);
        podeCriarAlinea = SecurityService.getInstance().verificarPermissao(userSessionMB.getLoggedUser(), RbacConstantes.ALINEA_CRIAR);
        podeExcluirAlinea = SecurityService.getInstance().verificarPermissao(userSessionMB.getLoggedUser(), RbacConstantes.ALINEA_EXCLUIR);

        this.pesquisar();
    }

    public void pesquisar() {
        logger.debug("PESQUISAR");
        this.categoriaAlinea = new CategoriaAlineaBean();
        if ("".equals(this.pesquisa) || this.pesquisa == null) {
            logger.debug("PESQUISAR TODOS");
            this.listaCategoriasAlineas = Service.getInstance().listarCategoriaAlinea();
        } else {
            logger.debug("PESQUISAR COM TERMO");
            this.listaCategoriasAlineas = Service.getInstance().pesquisarCategoriaAlinea(pesquisa);
        }
    }

    public void excluir() {
        logger.debug("EXCLUIR REGISTRO");
        try{
            Service.getInstance().excluirCategoriaAlinea(categoriaAlinea);
        }catch(Exception e){
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Impossível excluir registro", "A Categoria de Alínea tem auxílios vinculados.");
            FacesContext.getCurrentInstance().addMessage("", message);
        }
        this.pesquisar();
        RequestContext.getCurrentInstance().execute("PF('deletePopup').hide();");
    }

    public void gravar() {
        if (ADICIONAR_STATE.equals(this.currentState)) {
            logger.debug("ADICIONAR REGISTRO");
            Service.getInstance().cadastraCategoriaAlinea(categoriaAlinea);
        } else if (EDITAR_STATE.equals(this.currentState)) {
            logger.debug("EDITAR REGISTRO");
            Service.getInstance().atualizarCategoriaAlinea(categoriaAlinea);
        }
        this.pesquisar();
        RequestContext.getCurrentInstance().execute("PF('addEditPopup').hide();");
    }

    public StreamedContent downloadReportPdf() {
        logger.debug("GERAR RELATORIO PDF");
        InputStream stream = ReportService.getInstance().emiteRelatorioCategoriasAlineas(listaCategoriasAlineas, ReportService.FORMATO_PDF);
        StreamedContent file = new DefaultStreamedContent(stream, "application/pdf", "report.pdf");
        return file;
    }

    public StreamedContent downloadReportXls() {
        logger.debug("GERAR RELATORIO XLS");
        InputStream stream = ReportService.getInstance().emiteRelatorioCategoriasAlineas(listaCategoriasAlineas, ReportService.FORMATO_XLS);
        StreamedContent file = new DefaultStreamedContent(stream, "application/vnd.ms-excel", "report.xls");
        return file;
    }
    
    public void prepareExcluir(CategoriaAlineaBean catAlinea){
        this.categoriaAlinea = catAlinea;
        RequestContext.getCurrentInstance().execute("PF('deletePopup').show();");
    }
}
