/*
 * Sistema de Gestão de Projetos Científicos - SGPC
 * 
 * 
 * Copyright  (C)  2010-2014  Instituto  de  Ciências  Matemáticas  e  de
 *                            Computação - ICMC/USP
 * 
 * 
 * This program is  free software; you can redistribute  it and/or modify
 * it under the  terms of the GNU General Public  License as published by
 * the Free Software Foundation; either  version 2 of the License, or (at
 * your option) any later version.
 * 
 * This program  is distributed in the  hope that it will  be useful, but
 * WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
 * MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
 * General Public License for more details.
 * 
 * You  should have received  a copy  of the  GNU General  Public License
 * along  with  this  program;  if   not,  write  to  the  Free  Software
 * Foundation,  Inc.,  51  Franklin   Street,  Fifth  Floor,  Boston,  MA
 * 02110-1301, USA.
 * 
 * 
 * EXCEPTION TO THE TERMS AND CONDITIONS OF GNU GENERAL PUBLIC LICENSE
 * 
 * The  LICENSE  file may  contain  an  additional  clause as  a  special
 * exception  to the  terms  and  conditions of  the  GNU General  Public
 * License. This  clause, if  present, gives you  the permission  to link
 * this  program with  certain third-part  software and  to obtain,  as a
 * result, a  work based  on this program  that can be  distributed using
 * other license than the GNU General Public License.  If you modify this
 * program, you may extend this exception to your version of the program,
 * but you  are not obligated  to do so.   If you do  not wish to  do so,
 * delete this exception statement from your version.
 * 
 */

package br.usp.icmc.sgpc.jsf;

import br.usp.icmc.sgpc.beans.InstituicaoBean;
import br.usp.icmc.sgpc.beans.PessoaBean;
import br.usp.icmc.sgpc.security.RbacConstantes;
import br.usp.icmc.sgpc.security.service.SecurityService;
import br.usp.icmc.sgpc.service.ReportService;
import br.usp.icmc.sgpc.service.Service;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIPanel;
import javax.faces.context.FacesContext;
import org.apache.log4j.Logger;
import org.primefaces.context.RequestContext;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

/**
 *
 * @author Artur
 */
@ManagedBean(name = "instituicaoMB")
@ViewScoped
public class InstituicaoManagedBean implements Serializable {

    private static final Logger logger = Logger.getLogger(InstituicaoManagedBean.class);
    private InstituicaoBean instituicao;
    private List<InstituicaoBean> listaInstituicoes = new ArrayList<InstituicaoBean>();
    private UIPanel panelForm;
    private Integer currentRow;
    private String pesquisa;
    public static final String PESQUISAR_STATE = "pesquisar";
    public static final String ADICIONAR_STATE = "adicionar";
    public static final String EDITAR_STATE = "editar";
    private String currentState = PESQUISAR_STATE;
    private boolean podeCriarInstituicao;
    private boolean podeEditarInstituicao;
    private boolean podeExcluirInstituicao;
    private String imagemEditarRegistro = "/images/icons/editar.png";
    private String imagemExcluirRegistro = "/images/icons/del.png";
    private String imagemCriarRegistro = "/images/icons/add1.png";
    @ManagedProperty(value = "#{userSessionMB}")
    private UserSessionManagedBean userSessionMB;

    public boolean isPodeCriarInstituicao() {
        return podeCriarInstituicao;
    }

    public void setPodeCriarInstituicao(boolean podeCriarInstituicao) {
        this.podeCriarInstituicao = podeCriarInstituicao;
    }

    public boolean isPodeEditarInstituicao() {
        return podeEditarInstituicao;
    }

    public void setPodeEditarInstituicao(boolean podeEditarInstituicao) {
        this.podeEditarInstituicao = podeEditarInstituicao;
    }

    public boolean isPodeExcluirInstituicao() {
        return podeExcluirInstituicao;
    }

    public void setPodeExcluirInstituicao(boolean podeExcluirInstituicao) {
        this.podeExcluirInstituicao = podeExcluirInstituicao;
    }

    public String getCurrentState() {
        return currentState;
    }

    public void setCurrentState(String currentState) {
        this.currentState = currentState;
    }

    public InstituicaoManagedBean() {
        logger.debug("Instanciando classe");

    }

    public String getImagemCriarRegistro() {
        if (podeCriarInstituicao) {
            imagemCriarRegistro = "/images/icons/add1.png";
        } else {
            imagemCriarRegistro = "/images/icons/add1bw.png";
        }
        return imagemCriarRegistro;
    }

    public void setImagemCriarRegistro(String imagemCriarRegistro) {
        this.imagemCriarRegistro = imagemCriarRegistro;
    }

    public String getImagemEditarRegistro() {
        if (podeEditarInstituicao) {
            imagemEditarRegistro = "/images/icons/editar.png";
        } else {
            imagemEditarRegistro = "/images/icons/editarbw.png";
        }
        return imagemEditarRegistro;
    }

    public void setImagemEditarRegistro(String imagemEditarRegistro) {
        this.imagemEditarRegistro = imagemEditarRegistro;
    }

    public String getImagemExcluirRegistro() {
        if (podeExcluirInstituicao) {
            imagemExcluirRegistro = "/images/icons/del.png";
        } else {
            imagemExcluirRegistro = "/images/icons/delbw.png";
        }
        return imagemExcluirRegistro;
    }

    public void setImagemExcluirRegistro(String imagemExcluirRegistro) {
        this.imagemExcluirRegistro = imagemExcluirRegistro;
    }

    public Integer getCurrentRow() {
        return currentRow;
    }

    public void setCurrentRow(Integer currentRow) {
        this.currentRow = currentRow;
    }

    public InstituicaoBean getInstituicao() {
        return instituicao;
    }

    public void setInstituicao(InstituicaoBean instituicao) {
        this.instituicao = instituicao;
    }

    public List<InstituicaoBean> getListaInstituicoes() {
        return listaInstituicoes;
    }

    public void setListaInstituicoes(List<InstituicaoBean> listaInstituicoes) {
        this.listaInstituicoes = listaInstituicoes;
    }

    public UIPanel getPanelForm() {
        return panelForm;
    }

    public void setPanelForm(UIPanel panelForm) {
        this.panelForm = panelForm;
    }

    public String getPesquisa() {
        return pesquisa;
    }

    public void setPesquisa(String pesquisa) {
        this.pesquisa = pesquisa;
    }

    public void pesquisar() {
        logger.debug("PESQUISAR");
        this.instituicao = new InstituicaoBean();
        if ("".equals(this.pesquisa) || this.pesquisa == null) {
            logger.debug("PESQUISAR TODOS");
            this.listaInstituicoes = Service.getInstance().listarInstituicoes();
        } else {
            logger.debug("PESQUISAR COM TERMO");
            this.listaInstituicoes = Service.getInstance().pesquisarInstituicoes(pesquisa);
        }
    }

    public void prepareAdicionar() {
        this.clear();
        this.setCurrentState(ADICIONAR_STATE);
        RequestContext.getCurrentInstance().execute("PF('addEditPopup').show();");
    }

    public boolean isAdicionarState() {
        return ADICIONAR_STATE.equals(this.getCurrentState());
    }

    public void prepareEditar(InstituicaoBean inst) {
        this.setCurrentState(EDITAR_STATE);
        this.instituicao = inst;
        RequestContext.getCurrentInstance().execute("PF('addEditPopup').show();");
    }
    public void prepareExcluir(InstituicaoBean inst) {
        this.setCurrentState(EDITAR_STATE);
        this.instituicao = inst;
        RequestContext.getCurrentInstance().execute("PF('deletePopup').show();");
    }

    public boolean isEditarState() {
        return EDITAR_STATE.equals(this.getCurrentState());
    }

    public boolean isPesquisarState() {
        String state = this.getCurrentState();
        return (state == null || PESQUISAR_STATE.equals(state));
    }

    public void clear() {
        this.instituicao = new InstituicaoBean();
        //this.cleanSubmittedValues(this.panelForm);
    }

    /*
     * Exclui registro
     */
    public void excluir() {
        logger.debug("EXCLUIR REGISTRO");
        try{
            Service.getInstance().excluirInstituicao(instituicao);
        }catch(Exception e){
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Impossível excluir registro", "A Instituição tem unidades vinculadas.");
            FacesContext.getCurrentInstance().addMessage("", message);
        }
        this.pesquisar();
        RequestContext.getCurrentInstance().execute("PF('deletePopup').hide();");
    }

    public void gravar() {
        if (ADICIONAR_STATE.equals(this.currentState)) {
            logger.debug("ADICIONAR REGISTRO");
            Service.getInstance().cadastraInstituicao(instituicao);
        } else if (EDITAR_STATE.equals(this.currentState)) {
            logger.debug("EDITAR REGISTRO");
            Service.getInstance().atualizarInstituicao(instituicao);
        }
        this.pesquisar();
        RequestContext.getCurrentInstance().execute("PF('addEditPopup').hide();");
    }

    public StreamedContent downloadReportPdf() {
        logger.debug("GERAR RELATORIO PDF");
        InputStream stream = ReportService.getInstance().emiteRelatorioInstituicoes(listaInstituicoes, ReportService.FORMATO_PDF);
        StreamedContent file = new DefaultStreamedContent(stream, "application/pdf", "report.pdf");
        return file;
    }

    public StreamedContent downloadReportXls() {
        logger.debug("GERAR RELATORIO XLS");
        InputStream stream = ReportService.getInstance().emiteRelatorioInstituicoes(listaInstituicoes, ReportService.FORMATO_XLS);
        StreamedContent file = new DefaultStreamedContent(stream, "application/vnd.ms-excel", "report.xls");
        return file;
    }

    @PostConstruct
    public void postConstruct(){
        PessoaBean usuarioLogado = userSessionMB.getLoggedUser();

        podeCriarInstituicao = SecurityService.getInstance().verificarPermissao(usuarioLogado, RbacConstantes.INSTITUICAO_CRIAR);
        podeEditarInstituicao = SecurityService.getInstance().verificarPermissao(usuarioLogado, RbacConstantes.INSTITUICAO_EDITAR);
        podeExcluirInstituicao = SecurityService.getInstance().verificarPermissao(usuarioLogado, RbacConstantes.INSTITUICAO_EXCLUIR);

        this.pesquisar();
    }

    public UserSessionManagedBean getUserSessionMB() {
        return userSessionMB;
    }

    public void setUserSessionMB(UserSessionManagedBean userSessionMB) {
        this.userSessionMB = userSessionMB;
    }

    public String abreUnidades(InstituicaoBean instituicao) {
        userSessionMB.setInterPageParameter(instituicao);
        return "unidades?faces-redirect=true";
    }
}
