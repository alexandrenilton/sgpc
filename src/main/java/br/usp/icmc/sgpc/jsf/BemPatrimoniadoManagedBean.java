/*
 * Sistema de Gestão de Projetos Científicos - SGPC
 * 
 * 
 * Copyright  (C)  2010-2014  Instituto  de  Ciências  Matemáticas  e  de
 *                            Computação - ICMC/USP
 * 
 * 
 * This program is  free software; you can redistribute  it and/or modify
 * it under the  terms of the GNU General Public  License as published by
 * the Free Software Foundation; either  version 2 of the License, or (at
 * your option) any later version.
 * 
 * This program  is distributed in the  hope that it will  be useful, but
 * WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
 * MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
 * General Public License for more details.
 * 
 * You  should have received  a copy  of the  GNU General  Public License
 * along  with  this  program;  if   not,  write  to  the  Free  Software
 * Foundation,  Inc.,  51  Franklin   Street,  Fifth  Floor,  Boston,  MA
 * 02110-1301, USA.
 * 
 * 
 * EXCEPTION TO THE TERMS AND CONDITIONS OF GNU GENERAL PUBLIC LICENSE
 * 
 * The  LICENSE  file may  contain  an  additional  clause as  a  special
 * exception  to the  terms  and  conditions of  the  GNU General  Public
 * License. This  clause, if  present, gives you  the permission  to link
 * this  program with  certain third-part  software and  to obtain,  as a
 * result, a  work based  on this program  that can be  distributed using
 * other license than the GNU General Public License.  If you modify this
 * program, you may extend this exception to your version of the program,
 * but you  are not obligated  to do so.   If you do  not wish to  do so,
 * delete this exception statement from your version.
 * 
 */

package br.usp.icmc.sgpc.jsf;

import br.usp.icmc.sgpc.beans.BemPatrimoniadoBean;
import br.usp.icmc.sgpc.beans.FinanciadorBean;
import br.usp.icmc.sgpc.security.RbacConstantes;
import br.usp.icmc.sgpc.security.service.SecurityService;
import br.usp.icmc.sgpc.service.Service;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import org.apache.log4j.Logger;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;

/**
 *
 * @author Artur
 */
@ManagedBean(name = "bemPatrimoniadoMB")
@ViewScoped
public class BemPatrimoniadoManagedBean implements Serializable {

    private static final Logger logger = Logger.getLogger(BemPatrimoniadoManagedBean.class);
    private String pesquisa;
    private List<BemPatrimoniadoBean> listaBemPatrimoniado = new ArrayList<BemPatrimoniadoBean>();
    public static final String PESQUISAR_STATE = "pesquisar";
    public static final String ADICIONAR_STATE = "adicionar";
    public static final String EDITAR_STATE = "editar";
    private String currentState = PESQUISAR_STATE;
    private BemPatrimoniadoBean bemPatrimoniado;
    @ManagedProperty(value = "#{userSessionMB}")
    private UserSessionManagedBean userSessionMB;
    private FinanciadorBean financiador;
    private Date dataInicio;
    private Date dataFim;
    private List<FinanciadorBean> listaFinanciadores = new ArrayList<FinanciadorBean>();
    private boolean podeEditarBemPatrimoniado;
    private boolean podeCriarBemPatrimoniado;
    private String imagemEditarRegistro = "/images/icons/editar.png";
    private String imagemCriarRegistro = "/images/icons/add1.png";

    public UserSessionManagedBean getUserSessionMB() {
        return userSessionMB;
    }

    public void setUserSessionMB(UserSessionManagedBean userSessionMB) {
        this.userSessionMB = userSessionMB;
    }

    public List<FinanciadorBean> getListaFinanciadores() {
        return listaFinanciadores;
    }

    public String getImagemCriarRegistro() {
        if (podeCriarBemPatrimoniado) {
            imagemCriarRegistro = "/images/icons/add1.png";
        } else {
            imagemCriarRegistro = "/images/icons/add1bw.png";
        }
        return imagemCriarRegistro;
    }

    public void setImagemCriarRegistro(String imagemCriarRegistro) {
        this.imagemCriarRegistro = imagemCriarRegistro;
    }

    public String getImagemEditarRegistro() {
        if (podeEditarBemPatrimoniado) {
            imagemEditarRegistro = "/images/icons/editar.png";
        } else {
            imagemEditarRegistro = "/images/icons/editarbw.png";
        }
        return imagemEditarRegistro;
    }

    public void setImagemEditarRegistro(String imagemEditarRegistro) {
        this.imagemEditarRegistro = imagemEditarRegistro;
    }

    public boolean isPodeCriarBemPatrimoniado() {
        return podeCriarBemPatrimoniado;
    }

    public void setPodeCriarBemPatrimoniado(boolean podeCriarBemPatrimoniado) {
        this.podeCriarBemPatrimoniado = podeCriarBemPatrimoniado;
    }

    public boolean isPodeEditarBemPatrimoniado() {
        return podeEditarBemPatrimoniado;
    }

    public void setPodeEditarBemPatrimoniado(boolean podeEditarBemPatrimoniado) {
        this.podeEditarBemPatrimoniado = podeEditarBemPatrimoniado;
    }

    public void setListaFinanciadores(List<FinanciadorBean> listaFinanciadores) {
        this.listaFinanciadores = listaFinanciadores;
    }

    public Date getDataFim() {
        return dataFim;
    }

    public void setDataFim(Date dataFim) {
        this.dataFim = dataFim;
    }

    public Date getDataInicio() {
        return dataInicio;
    }

    public void setDataInicio(Date dataInicio) {
        this.dataInicio = dataInicio;
    }

    public FinanciadorBean getFinanciador() {
        return financiador;
    }

    public void setFinanciador(FinanciadorBean financiador) {
        this.financiador = financiador;
    }

    public BemPatrimoniadoBean getBemPatrimoniado() {
        return bemPatrimoniado;
    }

    public void setBemPatrimoniado(BemPatrimoniadoBean bemPatrimoniado) {
        this.bemPatrimoniado = bemPatrimoniado;
    }

    public BemPatrimoniadoManagedBean() {
    }

    @PostConstruct
    public void postConstruct() {
        logger.debug("Instanciando classe");
        podeEditarBemPatrimoniado = SecurityService.getInstance().verificarPermissao(userSessionMB.getLoggedUser(), RbacConstantes.BEM_PATRIMONIADO_EDITAR);
        podeCriarBemPatrimoniado = SecurityService.getInstance().verificarPermissao(userSessionMB.getLoggedUser(), RbacConstantes.BEM_PATRIMONIADO_CRIAR);

        pesquisar();
        listaFinanciadores = Service.getInstance().listarFinanciadores();
    }

    public String getPesquisa() {
        return pesquisa;
    }

    public void setPesquisa(String pesquisa) {
        this.pesquisa = pesquisa;
    }

    public void pesquisar() {
        logger.debug("Pesquisando Bem Patrimoniado");
        this.bemPatrimoniado = new BemPatrimoniadoBean();
        if (("".equals(this.pesquisa) || this.pesquisa == null) && this.dataInicio == null && this.dataFim == null && financiador == null) {
            logger.debug("PESQUISAR TODOS");
            this.listaBemPatrimoniado = Service.getInstance().listarBensPatrimoniados();
        } else {
            logger.debug("PESQUISAR COM TERMO");
            this.listaBemPatrimoniado = Service.getInstance().pesquisarBensPatrimoniados(pesquisa, dataInicio, dataFim, financiador);
        }
    }

    public List<BemPatrimoniadoBean> getListaBemPatrimoniado() {
        return listaBemPatrimoniado;
    }

    public void setListaBemPatrimoniado(List<BemPatrimoniadoBean> listaBemPatrimoniado) {
        this.listaBemPatrimoniado = listaBemPatrimoniado;
    }

    public void prepareEditar(BemPatrimoniadoBean bemP) {
        this.setCurrentState(EDITAR_STATE);
        this.bemPatrimoniado = bemP;
        RequestContext.getCurrentInstance().execute("PF('addEditPopup').show();");
    }
    public void prepareExcluir(BemPatrimoniadoBean bemP) {
        this.bemPatrimoniado = bemP;
        RequestContext.getCurrentInstance().execute("PF('deletePopup').show();");
    }

    public String getCurrentState() {
        return currentState;
    }

    public void prepareAdicionar() {
        this.setCurrentState(ADICIONAR_STATE);
        bemPatrimoniado = new BemPatrimoniadoBean();
        RequestContext.getCurrentInstance().execute("PF('addEditPopup').show();");
    }

    public void setCurrentState(String currentState) {
        this.currentState = currentState;
    }

    public void gravar() {
        if (ADICIONAR_STATE.equals(this.currentState)) {
            logger.debug("ADICIONAR REGISTRO");
            Service.getInstance().cadastraBemPatrimoniado(bemPatrimoniado);
        } else if (EDITAR_STATE.equals(this.currentState)) {
            logger.debug("EDITAR REGISTRO");
            Service.getInstance().atualizarBemPatrimoniado(bemPatrimoniado);
        }
        this.pesquisar();
        RequestContext.getCurrentInstance().execute("PF('addEditPopup').hide();");
    }

    public void excluir() {
        logger.debug("EXCLUIR REGISTRO");
        Service.getInstance().excluirBemPatrimoniado(bemPatrimoniado);
        this.pesquisar();
        RequestContext.getCurrentInstance().execute("PF('deletePopup').hide();");
    }

    public String detalharBemPatrimoniado() {
        userSessionMB.setProjeto(bemPatrimoniado.getFkDespesa().getFkAlinea().getFkAuxilio().getFkProjeto());
        userSessionMB.setAuxilio(bemPatrimoniado.getFkDespesa().getFkAlinea().getFkAuxilio());
        userSessionMB.setInterPageParameter(bemPatrimoniado.getFkDespesa());
        return "infoDespesa?faces-redirect=true";
    }

    public void bindDataInicio(SelectEvent event) {
        dataInicio = (Date) event.getObject();
        pesquisar();
    }

    public void bindDataFim(SelectEvent event) {
        dataFim = (Date) event.getObject();
        pesquisar();
    }
}
