/*
 * Sistema de Gestão de Projetos Científicos - SGPC
 * 
 * 
 * Copyright  (C)  2010-2014  Instituto  de  Ciências  Matemáticas  e  de
 *                            Computação - ICMC/USP
 * 
 * 
 * This program is  free software; you can redistribute  it and/or modify
 * it under the  terms of the GNU General Public  License as published by
 * the Free Software Foundation; either  version 2 of the License, or (at
 * your option) any later version.
 * 
 * This program  is distributed in the  hope that it will  be useful, but
 * WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
 * MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
 * General Public License for more details.
 * 
 * You  should have received  a copy  of the  GNU General  Public License
 * along  with  this  program;  if   not,  write  to  the  Free  Software
 * Foundation,  Inc.,  51  Franklin   Street,  Fifth  Floor,  Boston,  MA
 * 02110-1301, USA.
 * 
 * 
 * EXCEPTION TO THE TERMS AND CONDITIONS OF GNU GENERAL PUBLIC LICENSE
 * 
 * The  LICENSE  file may  contain  an  additional  clause as  a  special
 * exception  to the  terms  and  conditions of  the  GNU General  Public
 * License. This  clause, if  present, gives you  the permission  to link
 * this  program with  certain third-part  software and  to obtain,  as a
 * result, a  work based  on this program  that can be  distributed using
 * other license than the GNU General Public License.  If you modify this
 * program, you may extend this exception to your version of the program,
 * but you  are not obligated  to do so.   If you do  not wish to  do so,
 * delete this exception statement from your version.
 * 
 */

package br.usp.icmc.sgpc.jsf;

import br.usp.icmc.sgpc.beans.OrcamentoBean;
import br.usp.icmc.sgpc.beans.SolicitaOrcamentoBean;
import br.usp.icmc.sgpc.service.Service;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import org.apache.log4j.Logger;

/**
 *
 * @author herick
 */
@ManagedBean(name = "orcamentoMB")
@ViewScoped
public class OrcamentoManagedBean implements Serializable {

    private static final Logger logger = Logger.getLogger(OrcamentoManagedBean.class);
    private SolicitaOrcamentoBean solicitaOrcamento;
    private OrcamentoBean orcamento;
    public static final String PESQUISAR_STATE = "pesquisar";
    public static final String ADICIONAR_STATE = "adicionar";
    public static final String EDITAR_STATE = "editar";
    private String currentState = PESQUISAR_STATE;
    private String imagemEditarRegistro = "/images/icons/editar.png";
    private String imagemExcluirRegistro = "/images/icons/delbw.png";
    private String imagemCriarRegistro = "/images/icons/add1.png";
    private String imagemSalvarRegistro = "/images/icons/save.png";
    private String imagemCancelarRegistro = "/images/icons/cancel.png";
    @ManagedProperty(value = "#{userSessionMB}")
    private UserSessionManagedBean userSessionMB;
    private boolean exibePanel = false;
    private boolean exibePanelEntrada = false;

    public UserSessionManagedBean getUserSessionMB() {
        return userSessionMB;
    }

    public void setUserSessionMB(UserSessionManagedBean userSessionMB) {
        this.userSessionMB = userSessionMB;
    }

    public String getImagemCancelarRegistro() {
        return imagemCancelarRegistro;
    }

    public void setImagemCancelarRegistro(String imagemCancelarRegistro) {
        this.imagemCancelarRegistro = imagemCancelarRegistro;
    }

    public String getImagemSalvarRegistro() {
        return imagemSalvarRegistro;
    }

    public void setImagemSalvarRegistro(String imagemSalvarRegistro) {
        this.imagemSalvarRegistro = imagemSalvarRegistro;
    }

    public boolean isExibePanel() {
        return exibePanel;
    }

    public boolean isExibePanelEntrada() {
        return exibePanelEntrada;
    }

    public void setExibePanelEntrada(boolean exibePanelEntrada) {
        this.exibePanelEntrada = exibePanelEntrada;
    }

    public void setExibePanel(boolean exibePanel) {
        this.exibePanel = exibePanel;
    }

    public String getImagemCriarRegistro() {
        return imagemCriarRegistro;
    }

    public String getCurrentState() {
        return currentState;
    }

    public void setCurrentState(String currentState) {
        this.currentState = currentState;
    }

    public void setImagemCriarRegistro(String imagemCriarRegistro) {
        this.imagemCriarRegistro = imagemCriarRegistro;
    }

    public String getImagemEditarRegistro() {
        return imagemEditarRegistro;
    }

    public void setImagemEditarRegistro(String imagemEditarRegistro) {
        this.imagemEditarRegistro = imagemEditarRegistro;
    }

    public String getImagemExcluirRegistro() {
        return imagemExcluirRegistro;
    }

    public void setImagemExcluirRegistro(String imagemExcluirRegistro) {
        this.imagemExcluirRegistro = imagemExcluirRegistro;
    }

    public OrcamentoBean getOrcamento() {
        return orcamento;
    }

    public void setOrcamento(OrcamentoBean orcamento) {
        this.orcamento = orcamento;
    }

    public SolicitaOrcamentoBean getSolicitaOrcamento() {
        return solicitaOrcamento;
    }

    public void setSolicitaOrcamento(SolicitaOrcamentoBean solicitaOrcamento) {
        this.solicitaOrcamento = solicitaOrcamento;
    }

    public OrcamentoManagedBean() {
    }

    @PostConstruct
    public void postConstruct() {
        solicitaOrcamento = (SolicitaOrcamentoBean) userSessionMB.getInterPageParameter();
        atualizar();
    }

    public void prepareEditar() {
        this.setCurrentState(EDITAR_STATE);
        exibePanelEntrada = true;
    }

    public void prepareAdicionar() {
        orcamento = new OrcamentoBean();
        this.setCurrentState(ADICIONAR_STATE);
        exibePanelEntrada = true;
    }

    public void gravar() {
        logger.debug("Metodo gravar");
        if (ADICIONAR_STATE.equals(this.currentState)) {
            logger.debug("ADICIONAR REGISTRO");
            orcamento.setFkFornecedor(solicitaOrcamento.getFkFornecedor());
            orcamento.setFkSolicitacaoCotacao(solicitaOrcamento.getFkSolicitacaoCotacao());
            orcamento.setFkSolicitaOrcamento(solicitaOrcamento);
            Service.getInstance().cadastrarOrcamento(orcamento);
        } else if (EDITAR_STATE.equals(this.currentState)) {
            logger.debug("EDITAR REGISTRO");
            orcamento.setFkFornecedor(solicitaOrcamento.getFkFornecedor());
            orcamento.setFkSolicitacaoCotacao(solicitaOrcamento.getFkSolicitacaoCotacao());
            orcamento.setFkSolicitaOrcamento(solicitaOrcamento);
            Service.getInstance().atualizarOrcamento(orcamento);
        }
        exibePanelEntrada = false;
        this.atualizar();
    }

    public void excluir() {
        logger.debug("EXCLUIR REGISTRO");
        Service.getInstance().excluirOrcamento(orcamento);
        this.atualizar();
    }

    public void atualizar() {
        solicitaOrcamento = Service.getInstance().buscarSolicitacaoOrcamento(solicitaOrcamento.getId());
        orcamento = solicitaOrcamento.getFkOrcamento();

        if (orcamento != null) {
            exibePanel = true;
            imagemCriarRegistro = "/images/icons/add1bw.png";
        } else {
            exibePanel = false;
            imagemCriarRegistro = "/images/icons/add1.png";
        }
    }

    public void cancelar(){
        exibePanelEntrada = false;
    }
}
