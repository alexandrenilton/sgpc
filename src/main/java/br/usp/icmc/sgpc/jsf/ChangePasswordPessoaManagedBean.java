/*
 * Sistema de Gestão de Projetos Científicos - SGPC
 * 
 * 
 * Copyright  (C)  2010-2014  Instituto  de  Ciências  Matemáticas  e  de
 *                            Computação - ICMC/USP
 * 
 * 
 * This program is  free software; you can redistribute  it and/or modify
 * it under the  terms of the GNU General Public  License as published by
 * the Free Software Foundation; either  version 2 of the License, or (at
 * your option) any later version.
 * 
 * This program  is distributed in the  hope that it will  be useful, but
 * WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
 * MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
 * General Public License for more details.
 * 
 * You  should have received  a copy  of the  GNU General  Public License
 * along  with  this  program;  if   not,  write  to  the  Free  Software
 * Foundation,  Inc.,  51  Franklin   Street,  Fifth  Floor,  Boston,  MA
 * 02110-1301, USA.
 * 
 * 
 * EXCEPTION TO THE TERMS AND CONDITIONS OF GNU GENERAL PUBLIC LICENSE
 * 
 * The  LICENSE  file may  contain  an  additional  clause as  a  special
 * exception  to the  terms  and  conditions of  the  GNU General  Public
 * License. This  clause, if  present, gives you  the permission  to link
 * this  program with  certain third-part  software and  to obtain,  as a
 * result, a  work based  on this program  that can be  distributed using
 * other license than the GNU General Public License.  If you modify this
 * program, you may extend this exception to your version of the program,
 * but you  are not obligated  to do so.   If you do  not wish to  do so,
 * delete this exception statement from your version.
 * 
 */

package br.usp.icmc.sgpc.jsf;

import br.usp.icmc.sgpc.beans.PessoaBean;
import br.usp.icmc.sgpc.security.ConfigConstantes;
import br.usp.icmc.sgpc.service.AuditoriaService;
import br.usp.icmc.sgpc.service.AutenticacaoService;
import br.usp.icmc.sgpc.service.MailService;
import br.usp.icmc.sgpc.service.Service;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.apache.log4j.Logger;
import org.primefaces.context.RequestContext;

/**
 *
 * @author Artur
 */
@ManagedBean(name = "changePasswordPessoaMB")
@ViewScoped
public class ChangePasswordPessoaManagedBean implements Serializable {
    private static final Logger logger = Logger.getLogger(ChangePasswordPessoaManagedBean.class);
    private PessoaBean pessoa;
    private String passwordAntigo;
    private String password;
    private String rePassword;
    
    @ManagedProperty(value = "#{userSessionMB}")
    private UserSessionManagedBean userSessionMB;
    
    @PostConstruct
    public void postConstruct() {
        logger.debug("Instanciando classe");
    }

    public PessoaBean getPessoa() {
        return pessoa;
    }

    public void setPessoa(PessoaBean pessoa) {
        this.pessoa = pessoa;
    }

    public String getPasswordAntigo() {
        return passwordAntigo;
    }

    public void setPasswordAntigo(String passwordAntigo) {
        this.passwordAntigo = passwordAntigo;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRePassword() {
        return rePassword;
    }

    public void setRePassword(String rePassword) {
        this.rePassword = rePassword;
    }

    public UserSessionManagedBean getUserSessionMB() {
        return userSessionMB;
    }

    public void setUserSessionMB(UserSessionManagedBean userSessionMB) {
        this.userSessionMB = userSessionMB;
    }
    
    public void prepareEditar() {
        password = "";
        rePassword = "";
    }
    
    public void alterarSenha() {
        String senhaAtual = pessoa.getPassword();

        passwordAntigo = AutenticacaoService.getInstance().encryptPassword(passwordAntigo);
        if (passwordAntigo.equals(senhaAtual)) {
            if (password.equals(rePassword)) {

                pessoa.setPassword(AutenticacaoService.getInstance().encryptPassword(password));
                Service.getInstance().atualizarPessoa(pessoa);
                if(pessoa.getEmail()!=null && !pessoa.getEmail().isEmpty() && pessoa.getUsername()!=null && !pessoa.getUsername().isEmpty()){
                    String subject = "Confirmação de Alteração de Senha - SGPC";
                    String setTo = pessoa.getEmail();
                    String message = "Sua senha foi alterada recentemente no sistema SGPC. \n"
                            + "Nome de Usuário - " + pessoa.getUsername() + " \n"
                            + "Senha - " + password;
                    MailService.getInstance().sendMail(subject, setTo, message);
                }

                AuditoriaService.getInstance().gravarAcaoUsuario(userSessionMB.getLoggedUser(), "Editar Registro", "Pessoa", "Alteração de senha de usuário. Id Usuário: " + pessoa.getId() + " - Nome: " + pessoa.getNome() + " - Username: " + pessoa.getUsername(), ConfigConstantes.CONFIG_AUDITORIA_SENHA_PESSOA);
                RequestContext.getCurrentInstance().execute("PF('addEditPassword').hide();");

            } else {
                passwordAntigo = "";
                password = "";
                rePassword = "";
                FacesContext.getCurrentInstance().addMessage("erroSenha", new FacesMessage(FacesMessage.SEVERITY_ERROR,"Nova senha não confere!!", "Nova senha não confere!!"));
            }
        } else {
            passwordAntigo = "";
            password = "";
            rePassword = "";
            FacesContext.getCurrentInstance().addMessage("erroSenha", new FacesMessage(FacesMessage.SEVERITY_ERROR,"Senha atual inválida!!", "Senha atual inválida!!" ));
        }
        
    }
}
