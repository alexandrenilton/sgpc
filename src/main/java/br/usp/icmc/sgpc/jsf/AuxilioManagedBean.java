/*
 * Sistema de Gestão de Projetos Científicos - SGPC
 * 
 * 
 * Copyright  (C)  2010-2014  Instituto  de  Ciências  Matemáticas  e  de
 *                            Computação - ICMC/USP
 * 
 * 
 * This program is  free software; you can redistribute  it and/or modify
 * it under the  terms of the GNU General Public  License as published by
 * the Free Software Foundation; either  version 2 of the License, or (at
 * your option) any later version.
 * 
 * This program  is distributed in the  hope that it will  be useful, but
 * WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
 * MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
 * General Public License for more details.
 * 
 * You  should have received  a copy  of the  GNU General  Public License
 * along  with  this  program;  if   not,  write  to  the  Free  Software
 * Foundation,  Inc.,  51  Franklin   Street,  Fifth  Floor,  Boston,  MA
 * 02110-1301, USA.
 * 
 * 
 * EXCEPTION TO THE TERMS AND CONDITIONS OF GNU GENERAL PUBLIC LICENSE
 * 
 * The  LICENSE  file may  contain  an  additional  clause as  a  special
 * exception  to the  terms  and  conditions of  the  GNU General  Public
 * License. This  clause, if  present, gives you  the permission  to link
 * this  program with  certain third-part  software and  to obtain,  as a
 * result, a  work based  on this program  that can be  distributed using
 * other license than the GNU General Public License.  If you modify this
 * program, you may extend this exception to your version of the program,
 * but you  are not obligated  to do so.   If you do  not wish to  do so,
 * delete this exception statement from your version.
 * 
 */

package br.usp.icmc.sgpc.jsf;

import br.usp.icmc.sgpc.beans.*;
import br.usp.icmc.sgpc.security.RbacConstantes;
import br.usp.icmc.sgpc.security.service.SecurityService;
import br.usp.icmc.sgpc.service.ContabilidadeService;
import br.usp.icmc.sgpc.service.ReportService;
import br.usp.icmc.sgpc.service.Service;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.Serializable;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.model.SelectItem;
import org.apache.log4j.Logger;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.primefaces.model.StreamedContent;

/**
 *
 * @author artur
 */
@ManagedBean(name = "auxilioMB")
@SessionScoped
public class AuxilioManagedBean implements Serializable {

    private static final Logger logger = Logger.getLogger(AuxilioManagedBean.class);
    public static final String PESQUISAR_STATE = "pesquisar";
    public static final String ADICIONAR_STATE = "adicionar";
    public static final String EDITAR_STATE = "editar";
    private String currentState = PESQUISAR_STATE;
    private AuxilioBean auxilio;
    private ProjetoBean projeto;
    private List<FinanciadorBean> listaFinanciadores = new ArrayList<FinanciadorBean>();
    private List<String> listaStatus = new ArrayList<String>();
    private List<String> listaTipoArquivo = new ArrayList<String>();
    private String pesquisa;
    private Date dataInicial;
    private Date dataFinal;
    FinanciadorBean financiador;
    String status = "Em Andamento";
    private String tipoArquivoUpload;
    ArquivoAnexoBean arquivoAnexo;
    private String imagemExcluirArquivo = "/images/icons/del.png";
    private String imagemArquivoAnexo = "/images/icons/attach.png";
    private String imagemCriarRegistro = "/images/icons/add1.png";
    private String imagemExcluirRegistro = "/images/icons/delbw.png";
    @ManagedProperty(value = "#{userSessionMB}")
    private UserSessionManagedBean userSessionMB;
    private boolean criarRegistrosTerceiros;
    private boolean criarRegistroProprio;
    private boolean podeAlterarStatusAuxilioFinanceiro = false;
    private boolean alterarStatusAuxilio = false;
    private boolean existeProjetoAtivo = false;
    private boolean insereProjetoAtivo = false;
    private boolean exibirColunaOwner;
    private boolean podeEditarRegistro = false;
    private boolean podeExcluirAuxilio = false;

    private BigDecimal valorTotalAuxiliosRs;
    private BigDecimal valorTotalAuxiliosUs;
    private LazyDataModel<AuxilioBean> lazyModel;
    private int pageSize = 10;
    private String imagemEditarRegistro = "/images/icons/editar.png";
    private String sortField2;
    private String sortOrder2;
    private String dadosAdicionaisCC;
    private String pesquisaModalidade;
    private List<PessoaBean> listaResponsaveis;
    private PessoaBean responsavelPesquisa;
    private List<SelectItem> listaInstituicoes;
    private Integer idInstituicao;
    private List<SelectItem> listaUnidades;
    private Integer idUnidade;
    private List<SelectItem> listaDepartamentos;
    private Integer idDepartamento;
    
    private String pesquisaVigenciaEscopo = "durante";
    private String pesquisaTipoBolsaAuxilio = "auxilios";
    
    public UserSessionManagedBean getUserSessionMB() {
        return userSessionMB;
    }

    public void setUserSessionMB(UserSessionManagedBean userSessionMB) {
        this.userSessionMB = userSessionMB;
    }

    public boolean isExisteProjetoAtivo() {
        if (userSessionMB.getProjeto() != null) {
            existeProjetoAtivo = true;
        }
        return existeProjetoAtivo;
    }

    public void setExisteProjetoAtivo(boolean existeProjetoAtivo) {
        this.existeProjetoAtivo = existeProjetoAtivo;
    }

    public boolean isInsereProjetoAtivo() {
        return insereProjetoAtivo;
    }

    public void setInsereProjetoAtivo(boolean insereProjetoAtivo) {
        this.insereProjetoAtivo = insereProjetoAtivo;
    }

    public boolean isAlterarStatusAuxilio() {
        return alterarStatusAuxilio;
    }

    public void setAlterarStatusAuxilio(boolean alterarStatusAuxilio) {
        this.alterarStatusAuxilio = alterarStatusAuxilio;
    }

    public boolean isPodeAlterarStatusAuxilioFinanceiro() {
        return podeAlterarStatusAuxilioFinanceiro;
    }

    public void setPodeAlterarStatusAuxilioFinanceiro(boolean podeAlterarStatusAuxilioFinanceiro) {
        this.podeAlterarStatusAuxilioFinanceiro = podeAlterarStatusAuxilioFinanceiro;
    }

    public boolean isCriarRegistroProprio() {
        return criarRegistroProprio;
    }

    public void setCriarRegistroProprio(boolean criarRegistroProprio) {
        this.criarRegistroProprio = criarRegistroProprio;
    }

    public boolean isCriarRegistrosTerceiros() {
        return criarRegistrosTerceiros;
    }

    public void setCriarRegistrosTerceiros(boolean criarRegistrosTerceiros) {
        this.criarRegistrosTerceiros = criarRegistrosTerceiros;
    }

    public String getImagemCriarRegistro() {
        return imagemCriarRegistro;
    }

    public void setImagemCriarRegistro(String imagemCriarRegistro) {
        this.imagemCriarRegistro = imagemCriarRegistro;
    }

    public String getImagemExcluirArquivo() {
        return imagemExcluirArquivo;
    }

    public void setImagemExcluirArquivo(String imagemExcluirArquivo) {
        this.imagemExcluirArquivo = imagemExcluirArquivo;
    }

    public String getImagemArquivoAnexo() {
        return imagemArquivoAnexo;
    }

    public void setImagemArquivoAnexo(String imagemArquivoAnexo) {
        this.imagemArquivoAnexo = imagemArquivoAnexo;
    }

    public ArquivoAnexoBean getArquivoAnexo() {
        return arquivoAnexo;
    }

    public void setArquivoAnexo(ArquivoAnexoBean arquivoAnexo) {
        this.arquivoAnexo = arquivoAnexo;
    }

    public List<String> getListaTipoArquivo() {
        this.listaTipoArquivo.clear();
        this.listaTipoArquivo.add("Despacho");
        this.listaTipoArquivo.add("Parecer");
        this.listaTipoArquivo.add("Projeto Original");
        this.listaTipoArquivo.add("Termo Aditivo");
        this.listaTipoArquivo.add("Termo de Outorga");
        this.listaTipoArquivo.add("Outros Documentos");

        return listaTipoArquivo;
    }

    public void setListaTipoArquivo(List<String> listaTipoArquivo) {
        this.listaTipoArquivo = listaTipoArquivo;
    }

    public String getTipoArquivoUpload() {
        return tipoArquivoUpload;
    }

    public void setTipoArquivoUpload(String tipoArquivoUpload) {
        this.tipoArquivoUpload = tipoArquivoUpload;
    }

    public Date getDataFinal() {
        return dataFinal;
    }

    public void setDataFinal(Date dataFinal) {
        this.dataFinal = dataFinal;
    }

    public Date getDataInicial() {
        return dataInicial;
    }

    public void setDataInicial(Date dataInicial) {
        this.dataInicial = dataInicial;
    }

    public FinanciadorBean getFinanciador() {
        return financiador;
    }

    public void setFinanciador(FinanciadorBean financiador) {
        this.financiador = financiador;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public List<FinanciadorBean> getListaFinanciadores() {
        listaFinanciadores.clear();
        listaFinanciadores = Service.getInstance().listarFinanciadores();

        return listaFinanciadores;
    }

    public void setListaFinanciadores(List<FinanciadorBean> listaFinanciadores) {
        this.listaFinanciadores = listaFinanciadores;
    }

    public List<String> getListaStatus() {
        listaStatus.clear();
        listaStatus.add(AuxilioBean.STATUS_EM_ELABORACAO);
        //listaStatus.add(AuxilioBean.STATUS_EM_ANALISE);
        //listaStatus.add(AuxilioBean.STATUS_APROVADO);
        //listaStatus.add(AuxilioBean.STATUS_NAO_APROVADO);
        listaStatus.add(AuxilioBean.STATUS_EM_ANDAMENTO);
        //listaStatus.add(AuxilioBean.STATUS_SUSPENSO);
        //listaStatus.add(AuxilioBean.STATUS_DESISTENCIA);
        //listaStatus.add(AuxilioBean.STATUS_TRANSFERIDO);
        //listaStatus.add(AuxilioBean.STATUS_CANCELADO);
        listaStatus.add(AuxilioBean.STATUS_FINALIZADO);
        //listaStatus.add(AuxilioBean.STATUS_ATRASADO);

        return listaStatus;
    }

    public void setListaStatus(List<String> listaStatus) {
        this.listaStatus = listaStatus;
    }

    public String getCurrentState() {
        return currentState;
    }

    public void setCurrentState(String currentState) {
        this.currentState = currentState;
    }

    public AuxilioBean getAuxilio() {
        return auxilio;
    }

    public void setAuxilio(AuxilioBean auxilio) {
        this.auxilio = auxilio;
    }

    public String getPesquisa() {
        return pesquisa;
    }

    public void setPesquisa(String pesquisa) {
        this.pesquisa = pesquisa;
    }

    public ProjetoBean getProjeto() {
        return projeto;
    }

    public void setProjeto(ProjetoBean projeto) {
        this.projeto = projeto;
    }

    public void bindDataInicial(SelectEvent event) {
        dataInicial = (Date) event.getObject();
        pesquisarAuxilios();
    }

    public void bindDataFinal(SelectEvent event) {
        dataFinal = (Date) event.getObject();
        pesquisarAuxilios();
    }

    @PostConstruct
    public void postConstruct() {
        logger.debug("Instanciando classe");
        exibirColunaOwner = SecurityService.getInstance().verificarPermissao(userSessionMB.getLoggedUser(), RbacConstantes.AUXILIO_CONSULTAR_TERCEIROS);

        criarRegistroProprio = SecurityService.getInstance().verificarPermissao(userSessionMB.getLoggedUser(), RbacConstantes.PROJETO_CRIAR_PROPRIO);
        criarRegistrosTerceiros = SecurityService.getInstance().verificarPermissao(userSessionMB.getLoggedUser(), RbacConstantes.PROJETO_CRIAR_TERCEIROS);
        podeAlterarStatusAuxilioFinanceiro = SecurityService.getInstance().verificarPermissao(userSessionMB.getLoggedUser(), RbacConstantes.AUXILIO_ALTERAR_STATUS_FINANCEIRO);
        podeEditarRegistro = SecurityService.getInstance().verificarPermissao(userSessionMB.getLoggedUser(), RbacConstantes.AUXILIO_EDITAR);
        podeExcluirAuxilio = SecurityService.getInstance().verificarPermissao(userSessionMB.getLoggedUser(), RbacConstantes.AUXILIO_EXCLUIR);
        if(podeExcluirAuxilio){
            imagemExcluirRegistro = "/images/icons/del.png";
        } else {
            imagemExcluirRegistro = "/images/icons/delbw.png";
        }
        this.listaFinanciadores = Service.getInstance().listarFinanciadores();
        //this.listaDepartamentos = GenericConverter.createSelectItems(Service.getInstance().pesquisarDepartamentos());
        this.listaInstituicoes = GenericConverter.createSelectItems(Service.getInstance().listarInstituicoes());
        this.listaResponsaveis = Service.getInstance().pesquisarPessoasReponsaveis();

        this.pesquisarAuxilios();
    }

    public void pesquisarAuxilios() {
        logger.debug("PESQUISAR");
        this.auxilio = new AuxilioBean();
        if(this.idInstituicao!=null){
            InstituicaoBean instituicao = Service.getInstance().buscarInstituicao(idInstituicao);
            this.listaUnidades = GenericConverter.createSelectItems(instituicao.getUnidades());
        }
        if(this.idUnidade!=null){
            UnidadeBean unidade = Service.getInstance().buscarUnidade(idUnidade);
            this.listaDepartamentos = GenericConverter.createSelectItems(unidade.getDepartamentos());
        }

        this.loadData();
        //this.lazyModel.load(0, 100, "", SortOrder.UNSORTED, null);
    }

    public boolean isAdicionarState() {
        return ADICIONAR_STATE.equals(this.getCurrentState());
    }

    public boolean isEditarState() {
        return EDITAR_STATE.equals(this.getCurrentState());
    }

    public boolean isPesquisarState() {
        String state = this.getCurrentState();
        return (state == null || PESQUISAR_STATE.equals(state));
    }

    public String ativarAuxilio() {
        userSessionMB.setProjeto(auxilio.getFkProjeto());
        userSessionMB.setAuxilio(auxilio);
        return "infoAuxilio?faces-redirect=true";
        //return "infoAuxilio?faces-redirect=true&idAuxilioSelecionado="+auxilio.getId();
    }

    public boolean isExibirColunaOwner() {
        return exibirColunaOwner;
    }

    public void setExibirColunaOwner(boolean exibirColunaOwner) {
        this.exibirColunaOwner = exibirColunaOwner;
    }

    public StreamedContent downloadReportPdf() {
        logger.debug("GERAR RELATORIO PDF");
        List<AuxilioBean> listaRelatorio;
        Date dataPesquisaInicioDentro = null;
        Date dataPesquisaInicioDurante = null;
        Date dataPesquisaFimDentro = null;
        Date dataPesquisaFimDurante = null;
        Date dataPesquisaInicioIniciadoEm = null;
        Date dataPesquisaFimIniciadoEm = null;

        if("durante".equalsIgnoreCase(pesquisaVigenciaEscopo)){
            dataPesquisaInicioDurante = dataInicial;
            dataPesquisaFimDurante = dataFinal;
        }else if("dentro".equalsIgnoreCase(pesquisaVigenciaEscopo)){
            dataPesquisaInicioDentro = dataInicial;
            dataPesquisaFimDentro = dataFinal;                    
        }else if("iniciado".equalsIgnoreCase(pesquisaVigenciaEscopo)){
            dataPesquisaInicioIniciadoEm = dataInicial;
            dataPesquisaFimIniciadoEm = dataFinal;
        }
        
        DepartamentoBean departamento = null;
        if(idDepartamento != null){
            departamento = Service.getInstance().buscarDepartamento(idDepartamento);
        }
        
        UnidadeBean unidade = null;
        if(idUnidade != null){
            unidade = Service.getInstance().buscarUnidade(idUnidade);
        }
        
        if (exibirColunaOwner) {
            listaRelatorio = Service.getInstance().pesquisarAuxilios(null, pesquisa, financiador, status, dataPesquisaInicioDentro, dataPesquisaFimDentro, 0, 0, sortField2, sortOrder2, null, dataPesquisaInicioDurante, dataPesquisaFimDurante, departamento, pesquisaModalidade, unidade, responsavelPesquisa, null, pesquisaTipoBolsaAuxilio, dataPesquisaInicioIniciadoEm, dataPesquisaFimIniciadoEm);//getJpaController().findCarEntities(pageSize, first);
        } else {
            listaRelatorio = Service.getInstance().pesquisarAuxilios(userSessionMB.getLoggedUser(), pesquisa, financiador, status, dataPesquisaInicioDentro, dataPesquisaFimDentro, 0, 0, sortField2, sortOrder2, null, dataPesquisaInicioDurante, dataPesquisaFimDurante, departamento, pesquisaModalidade, unidade, responsavelPesquisa, null, pesquisaTipoBolsaAuxilio, dataPesquisaInicioIniciadoEm, dataPesquisaFimIniciadoEm);//getJpaController().findCarEntities(pageSize, first);
        }
        InputStream stream = ReportService.getInstance().emiteRelatorioAuxilios(listaRelatorio, ReportService.FORMATO_PDF, listarFiltrosAtivos());
        StreamedContent file = new DefaultStreamedContent(stream, "application/pdf", "report.pdf");
        return file;
    }

    public StreamedContent downloadReportXls() {
        logger.debug("GERAR RELATORIO XLS");
        List<AuxilioBean> listaRelatorio;
        Date dataPesquisaInicioDentro = null;
        Date dataPesquisaInicioDurante = null;
        Date dataPesquisaFimDentro = null;
        Date dataPesquisaFimDurante = null;
        Date dataPesquisaInicioIniciadoEm = null;
        Date dataPesquisaFimIniciadoEm = null;

        if("durante".equalsIgnoreCase(pesquisaVigenciaEscopo)){
            dataPesquisaInicioDurante = dataInicial;
            dataPesquisaFimDurante = dataFinal;
        }else if("dentro".equalsIgnoreCase(pesquisaVigenciaEscopo)){
            dataPesquisaInicioDentro = dataInicial;
            dataPesquisaFimDentro = dataFinal;                    
        }else if("iniciado".equalsIgnoreCase(pesquisaVigenciaEscopo)){
            dataPesquisaInicioIniciadoEm = dataInicial;
            dataPesquisaFimIniciadoEm = dataFinal;
        }
        
        DepartamentoBean departamento = null;
        if(idDepartamento != null){
            departamento = Service.getInstance().buscarDepartamento(idDepartamento);
        }
        
        UnidadeBean unidade = null;
        if(idUnidade != null){
            unidade = Service.getInstance().buscarUnidade(idUnidade);
        }
        
        if (exibirColunaOwner) {
            listaRelatorio = Service.getInstance().pesquisarAuxilios(null, pesquisa, financiador, status, dataPesquisaInicioDentro, dataPesquisaFimDentro, 0, 0, sortField2, sortOrder2, null, dataPesquisaInicioDurante, dataPesquisaFimDurante, departamento, pesquisaModalidade, unidade, responsavelPesquisa, null, pesquisaTipoBolsaAuxilio, dataPesquisaInicioIniciadoEm, dataPesquisaFimIniciadoEm);//getJpaController().findCarEntities(pageSize, first);
        } else {
            listaRelatorio = Service.getInstance().pesquisarAuxilios(userSessionMB.getLoggedUser(), pesquisa, financiador, status, dataPesquisaInicioDentro, dataPesquisaFimDentro, 0, 0, sortField2, sortOrder2, null, dataPesquisaInicioDurante, dataPesquisaFimDurante, departamento, pesquisaModalidade, unidade, responsavelPesquisa, null, pesquisaTipoBolsaAuxilio, dataPesquisaInicioIniciadoEm, dataPesquisaFimIniciadoEm);//getJpaController().findCarEntities(pageSize, first);
        }
        InputStream stream = ReportService.getInstance().emiteRelatorioAuxilios(listaRelatorio, ReportService.FORMATO_XLS,listarFiltrosAtivos());
        StreamedContent file = new DefaultStreamedContent(stream, "application/vnd.ms-excel", "report.xls");
        return file;
    }

    public BigDecimal somaVerbaAprovada(AuxilioBean auxilio, int moeda) {
        return ContabilidadeService.getInstance().verbaAprovadaAuxilio(auxilio,moeda);
    }

    public StreamedContent downloadArquivoAnexoAuxilio(ArquivoAnexoBean arquivo) {
        logger.debug("ArquivoAnexo: " + arquivo.getNome());
        ByteArrayInputStream stream = new ByteArrayInputStream(arquivo.getConteudo());
        StreamedContent file = new DefaultStreamedContent(stream, arquivo.getContentType(), arquivo.getNome());
        return file;
    }

    public boolean possuiArquivoAnexo(AuxilioBean auxilio) {
        if (auxilio.getArquivosAnexos() == null || auxilio.getArquivosAnexos().isEmpty()) {
            return false;
        } else {
            return true;
        }
    }

    public void handleFileUpload(FileUploadEvent event) {
        arquivoAnexo = new ArquivoAnexoBean();
        arquivoAnexo.setNome(event.getFile().getFileName());
        arquivoAnexo.setConteudo(event.getFile().getContents());
        arquivoAnexo.setContentType(event.getFile().getContentType());
        
        auxilio.getArquivosAnexos().add(arquivoAnexo);
        
        auxilio = Service.getInstance().atualizarAuxilio(auxilio);

//        FacesMessage msg = new FacesMessage("Succesfull ", event.getFile().getFileName() + " is uploaded.");
//        FacesContext.getCurrentInstance().addMessage("uploadDeArquivo", msg);
    }

    public void excluirArquivoUpload() {
        auxilio.getArquivosAnexos().remove(arquivoAnexo);
        Service.getInstance().atualizarAuxilio(auxilio);
        RequestContext.getCurrentInstance().execute("PF('deletePopupArquivo').hide();");
    }

    public boolean podeCriarRegistro() {
        if (criarRegistroProprio || criarRegistrosTerceiros) {
            imagemCriarRegistro = "/images/icons/add1.png";
            return false;
        } else {
            imagemCriarRegistro = "/images/icons/add1bw.png";
            return true;
        }
    }

    public void atualizarProjetoSessao() {
        projeto = Service.getInstance().pesquisaProjeto(projeto.getId());

        userSessionMB.setProjeto(this.projeto);
    }

    public BigDecimal getValorTotalAuxiliosRs() {
        return valorTotalAuxiliosRs;
    }

    public void setValorTotalAuxiliosRs(BigDecimal valorTotalAuxiliosRs) {
        this.valorTotalAuxiliosRs = valorTotalAuxiliosRs;
    }
    
    public BigDecimal getValorTotalAuxiliosUs() {
        return valorTotalAuxiliosUs;
    }

    public void setValorTotalAuxiliosUs(BigDecimal valorTotalAuxiliosUs) {
        this.valorTotalAuxiliosUs = valorTotalAuxiliosUs;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void loadData() {
        lazyModel = new LazyDataModel<AuxilioBean>() {

            @Override
            public List<AuxilioBean> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
                sortField2 = sortField;
                sortOrder2 = sortOrder.name();

                List<AuxilioBean> result = new ArrayList<AuxilioBean>();
                int qtdTotalRegistros = 0;
                double valorTot = 0d;
                
                Date dataPesquisaInicioDentro = null;
                Date dataPesquisaInicioDurante = null;
                Date dataPesquisaFimDentro = null;
                Date dataPesquisaFimDurante = null;
                Date dataPesquisaInicioIniciadoEm = null;
                Date dataPesquisaFimIniciadoEm = null;
                
                if("durante".equalsIgnoreCase(pesquisaVigenciaEscopo)){
                    dataPesquisaInicioDurante = dataInicial;
                    dataPesquisaFimDurante = dataFinal;
                }else if("dentro".equalsIgnoreCase(pesquisaVigenciaEscopo)){
                    dataPesquisaInicioDentro = dataInicial;
                    dataPesquisaFimDentro = dataFinal;                    
                }else if("iniciado".equalsIgnoreCase(pesquisaVigenciaEscopo)){
                    dataPesquisaInicioIniciadoEm = dataInicial;
                    dataPesquisaFimIniciadoEm = dataFinal;
                }

                try {
                    DepartamentoBean departamento = null;
                    if(idDepartamento != null){
                        departamento = Service.getInstance().buscarDepartamento(idDepartamento);
                    }
                    
                    UnidadeBean unidade = null;
                    if(idUnidade != null){
                        unidade = Service.getInstance().buscarUnidade(idUnidade);
                    }
                    
                    if (exibirColunaOwner) {
                        logger.debug("listar terceiros");
                        
                        result = Service.getInstance().pesquisarAuxilios(null, pesquisa, financiador, status, dataPesquisaInicioDentro, dataPesquisaFimDentro, first, pageSize, sortField, sortOrder.name(), null, dataPesquisaInicioDurante, dataPesquisaFimDurante, departamento, pesquisaModalidade, unidade, responsavelPesquisa, null, pesquisaTipoBolsaAuxilio, dataPesquisaInicioIniciadoEm, dataPesquisaFimIniciadoEm);//getJpaController().findCarEntities(pageSize, first);
                        qtdTotalRegistros = Service.getInstance().contarAuxilios(null, pesquisa, financiador, status, dataPesquisaInicioDentro, dataPesquisaFimDentro, dataPesquisaInicioDurante, dataPesquisaFimDurante, departamento, pesquisaModalidade, unidade, responsavelPesquisa, null, pesquisaTipoBolsaAuxilio, dataPesquisaInicioIniciadoEm, dataPesquisaFimIniciadoEm);
                        valorTotalAuxiliosRs = Service.getInstance().valorTotalAuxilios(null, pesquisa, financiador, status, dataPesquisaInicioDentro, dataPesquisaFimDentro, dataPesquisaInicioDurante, dataPesquisaFimDurante, departamento, pesquisaModalidade, unidade, responsavelPesquisa, null,1, pesquisaTipoBolsaAuxilio, dataPesquisaInicioIniciadoEm, dataPesquisaFimIniciadoEm);
                        valorTotalAuxiliosUs = Service.getInstance().valorTotalAuxilios(null, pesquisa, financiador, status, dataPesquisaInicioDentro, dataPesquisaFimDentro, dataPesquisaInicioDurante, dataPesquisaFimDurante, departamento, pesquisaModalidade, unidade, responsavelPesquisa, null,2, pesquisaTipoBolsaAuxilio, dataPesquisaInicioIniciadoEm, dataPesquisaFimIniciadoEm);
                    } else {
                        logger.debug("listar owner");
                        result = Service.getInstance().pesquisarAuxilios(userSessionMB.getLoggedUser(), pesquisa, financiador, status, dataPesquisaInicioDentro, dataPesquisaFimDentro, first, pageSize, sortField, sortOrder.name(), null, dataPesquisaInicioDurante, dataPesquisaFimDurante, departamento, pesquisaModalidade, unidade, responsavelPesquisa, null, pesquisaTipoBolsaAuxilio, dataPesquisaInicioIniciadoEm, dataPesquisaFimIniciadoEm);//getJpaController().findCarEntities(pageSize, first);
                        qtdTotalRegistros = Service.getInstance().contarAuxilios(userSessionMB.getLoggedUser(), pesquisa, financiador, status, dataPesquisaInicioDentro, dataPesquisaFimDentro, dataPesquisaInicioDurante, dataPesquisaFimDurante, departamento, pesquisaModalidade, unidade, responsavelPesquisa, null, pesquisaTipoBolsaAuxilio, dataPesquisaInicioIniciadoEm, dataPesquisaFimIniciadoEm);
                        valorTotalAuxiliosRs = Service.getInstance().valorTotalAuxilios(userSessionMB.getLoggedUser(), pesquisa, financiador, status, dataPesquisaInicioDentro, dataPesquisaFimDentro, dataPesquisaInicioDurante, dataPesquisaFimDurante, departamento, pesquisaModalidade, unidade, responsavelPesquisa, null, 1, pesquisaTipoBolsaAuxilio, dataPesquisaInicioIniciadoEm, dataPesquisaFimIniciadoEm);
                        valorTotalAuxiliosUs = Service.getInstance().valorTotalAuxilios(userSessionMB.getLoggedUser(), pesquisa, financiador, status, dataPesquisaInicioDentro, dataPesquisaFimDentro, dataPesquisaInicioDurante, dataPesquisaFimDurante, departamento, pesquisaModalidade, unidade, responsavelPesquisa, null, 2, pesquisaTipoBolsaAuxilio, dataPesquisaInicioIniciadoEm, dataPesquisaFimIniciadoEm);
                    }

                } catch (Exception ex) {
                    logger.debug(ex);
                }

                //int dataSize = result.size();
                this.setRowCount(qtdTotalRegistros);
                this.setPageSize(pageSize);
                
                return result;
            }
        };
    }

    public LazyDataModel<AuxilioBean> getLazyModel() {
        return lazyModel;
    }

    public boolean isPodeEditarRegistro() {
        return podeEditarRegistro;
    }

    public void setPodeEditarRegistro(boolean podeEditarRegistro) {
        this.podeEditarRegistro = podeEditarRegistro;
    }

    public boolean podeEditarRegistro() {
        if (podeEditarRegistro) {
            imagemEditarRegistro = "/images/icons/editar.png";
            return false;
        } else {
            imagemEditarRegistro = "/images/icons/editarbw.png";
            return true;
        }
    }

    public String getImagemEditarRegistro() {
        return imagemEditarRegistro;
    }

    public void setImagemEditarRegistro(String imagemEditarRegistro) {
        this.imagemEditarRegistro = imagemEditarRegistro;
    }

    public String getDadosAdicionaisCC() {
        return dadosAdicionaisCC;
    }

    public void setDadosAdicionaisCC(String dadosAdicionaisCC) {
        this.dadosAdicionaisCC = dadosAdicionaisCC;
    }

    public String getPesquisaModalidade() {
        return pesquisaModalidade;
    }

    public void setPesquisaModalidade(String pesquisaModalidade) {
        this.pesquisaModalidade = pesquisaModalidade;
    }

    public String getPesquisaVigenciaEscopo() {
        return pesquisaVigenciaEscopo;
    }

    public void setPesquisaVigenciaEscopo(String pesquisaVigenciaEscopo) {
        this.pesquisaVigenciaEscopo = pesquisaVigenciaEscopo;
    }

    private List<String> listarFiltrosAtivos(){
        List<String> retorno = new ArrayList<String>();

        // Palavra chave
        if(pesquisa!=null && !pesquisa.isEmpty()) retorno.add("Palavra-chave: " + pesquisa);
        
        // Datas
        Date dataPesquisaInicioDentro = null;
        Date dataPesquisaInicioDurante = null;
        Date dataPesquisaFimDentro = null;
        Date dataPesquisaFimDurante = null;
        Date dataPesquisaInicioIniciadoEm = null;
        Date dataPesquisaFimIniciadoEm = null;
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

        if("durante".equalsIgnoreCase(pesquisaVigenciaEscopo)){
            dataPesquisaInicioDurante = dataInicial;
            dataPesquisaFimDurante = dataFinal;
            if(dataPesquisaInicioDurante!=null && dataPesquisaFimDurante!=null){
                retorno.add("Data inicial durante: " + sdf.format(dataPesquisaInicioDurante));
                retorno.add("Data final durante: " + sdf.format(dataPesquisaFimDurante));
            }
        }else if("dentro".equalsIgnoreCase(pesquisaVigenciaEscopo)){
            dataPesquisaInicioDentro = dataInicial;
            dataPesquisaFimDentro = dataFinal;
            if(dataPesquisaInicioDentro!=null) retorno.add("Data inicial: " + sdf.format(dataPesquisaInicioDentro));
            if(dataPesquisaFimDentro!=null) retorno.add("Data final: " + sdf.format(dataPesquisaFimDentro));
        }else if("iniciado".equalsIgnoreCase(pesquisaVigenciaEscopo)){
            dataPesquisaInicioIniciadoEm = dataInicial;
            dataPesquisaFimIniciadoEm = dataFinal;
            if(dataPesquisaInicioIniciadoEm!=null && dataPesquisaFimIniciadoEm!=null){
                retorno.add("Iniciado entre: " + sdf.format(dataPesquisaInicioIniciadoEm) + " e " + sdf.format(dataPesquisaFimIniciadoEm));
            }
        }
        
        if("bolsas".equalsIgnoreCase(pesquisaTipoBolsaAuxilio)){
            retorno.add("Tipo: Bolsas");
        }else if ("auxilios".equalsIgnoreCase(pesquisaTipoBolsaAuxilio)){
            retorno.add("Tipo: Auxílios");
        }
        
        //financiador
        if(financiador!=null) retorno.add("Financiador: " + financiador.getSigla());
        
        //status
        if(status!=null && !status.isEmpty()) retorno.add("Status: " + status);
        
        //Departamento
        DepartamentoBean departamento = null;
        if(idDepartamento != null){
            departamento = Service.getInstance().buscarDepartamento(idDepartamento);
        }
        if(departamento!=null) retorno.add("Departamento: " + departamento.getSigla());
        
        //Modalidade
        if(pesquisaModalidade!=null && !pesquisaModalidade.isEmpty()) retorno.add("Modalidade: " + pesquisaModalidade);
              
        return retorno;
    }

    public List<PessoaBean> getListaResponsaveis() {
        return listaResponsaveis;
    }

    public void setListaResponsaveis(List<PessoaBean> listaResponsaveis) {
        this.listaResponsaveis = listaResponsaveis;
    }

    public PessoaBean getResponsavelPesquisa() {
        return responsavelPesquisa;
    }

    public void setResponsavelPesquisa(PessoaBean responsavelPesquisa) {
        this.responsavelPesquisa = responsavelPesquisa;
    }

    public Integer getIdDepartamento() {
        return idDepartamento;
    }

    public void setIdDepartamento(Integer idDepartamento) {
        this.idDepartamento = idDepartamento;
    }

    public Integer getIdInstituicao() {
        return idInstituicao;
    }

    public void setIdInstituicao(Integer idInstituicao) {
        this.idInstituicao = idInstituicao;
    }

    public Integer getIdUnidade() {
        return idUnidade;
    }

    public void setIdUnidade(Integer idUnidade) {
        this.idUnidade = idUnidade;
    }

    public List<SelectItem> getListaDepartamentos() {
        return listaDepartamentos;
    }

    public void setListaDepartamentos(List<SelectItem> listaDepartamentos) {
        this.listaDepartamentos = listaDepartamentos;
    }

    public List<SelectItem> getListaInstituicoes() {
        return listaInstituicoes;
    }

    public void setListaInstituicoes(List<SelectItem> listaInstituicoes) {
        this.listaInstituicoes = listaInstituicoes;
    }

    public List<SelectItem> getListaUnidades() {
        return listaUnidades;
    }

    public void setListaUnidades(List<SelectItem> listaUnidades) {
        this.listaUnidades = listaUnidades;
    }
    
    public boolean isPodeExcluirAuxilio() {
        return podeExcluirAuxilio;
    }

    public void setPodeExcluirAuxilio(boolean podeExcluirAuxilio) {
        this.podeExcluirAuxilio = podeExcluirAuxilio;
    }

    public String getImagemExcluirRegistro() {
        return imagemExcluirRegistro;
    }

    public void setImagemExcluirRegistro(String imagemExcluirRegistro) {
        this.imagemExcluirRegistro = imagemExcluirRegistro;
    }
    
    public boolean podeExcluirRegistro(AuxilioBean auxilio) {
        if(podeExcluirAuxilio){
            imagemExcluirRegistro = "/images/icons/del.png";
        } else {
            imagemExcluirRegistro = "/images/icons/delbw.png";
        }
        return podeExcluirAuxilio;
    }
    
    public void excluir() {
        logger.debug("EXCLUIR REGISTRO");
        Service.getInstance().excluirAuxilio(auxilio);
        this.pesquisarAuxilios();
        RequestContext.getCurrentInstance().execute("PF('deletePopup').hide();");
    }

    public String getPesquisaTipoBolsaAuxilio() {
        return pesquisaTipoBolsaAuxilio;
    }

    public void setPesquisaTipoBolsaAuxilio(String pesquisaTipoBolsaAuxilio) {
        this.pesquisaTipoBolsaAuxilio = pesquisaTipoBolsaAuxilio;
    }
    
}
