/*
 * Sistema de Gestão de Projetos Científicos - SGPC
 * 
 * 
 * Copyright  (C)  2010-2014  Instituto  de  Ciências  Matemáticas  e  de
 *                            Computação - ICMC/USP
 * 
 * 
 * This program is  free software; you can redistribute  it and/or modify
 * it under the  terms of the GNU General Public  License as published by
 * the Free Software Foundation; either  version 2 of the License, or (at
 * your option) any later version.
 * 
 * This program  is distributed in the  hope that it will  be useful, but
 * WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
 * MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
 * General Public License for more details.
 * 
 * You  should have received  a copy  of the  GNU General  Public License
 * along  with  this  program;  if   not,  write  to  the  Free  Software
 * Foundation,  Inc.,  51  Franklin   Street,  Fifth  Floor,  Boston,  MA
 * 02110-1301, USA.
 * 
 * 
 * EXCEPTION TO THE TERMS AND CONDITIONS OF GNU GENERAL PUBLIC LICENSE
 * 
 * The  LICENSE  file may  contain  an  additional  clause as  a  special
 * exception  to the  terms  and  conditions of  the  GNU General  Public
 * License. This  clause, if  present, gives you  the permission  to link
 * this  program with  certain third-part  software and  to obtain,  as a
 * result, a  work based  on this program  that can be  distributed using
 * other license than the GNU General Public License.  If you modify this
 * program, you may extend this exception to your version of the program,
 * but you  are not obligated  to do so.   If you do  not wish to  do so,
 * delete this exception statement from your version.
 * 
 */

package br.usp.icmc.sgpc.jsf;

import br.usp.icmc.sgpc.beans.DepartamentoBean;
import br.usp.icmc.sgpc.beans.InstituicaoBean;
import br.usp.icmc.sgpc.beans.PessoaBean;
import br.usp.icmc.sgpc.beans.PessoaProjetoBean;
import br.usp.icmc.sgpc.beans.UnidadeBean;
import br.usp.icmc.sgpc.security.RbacConstantes;
import br.usp.icmc.sgpc.security.beans.PapelBean;
import br.usp.icmc.sgpc.security.service.SecurityService;
import br.usp.icmc.sgpc.service.ReportService;
import br.usp.icmc.sgpc.service.Service;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import org.apache.log4j.Logger;
import org.primefaces.context.RequestContext;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

/**
 *
 * @author edwin
 * Classe encarregada do gerenciamento dos usuarios do sistema
 */
@ManagedBean(name = "pessoaMB")
@ViewScoped
public class PessoaManagedBean implements Serializable {

    private static final Logger logger = Logger.getLogger(PessoaManagedBean.class);
    public static final String PESQUISAR_STATE = "pesquisar";
    public static final String ADICIONAR_STATE = "adicionar";
    public static final String EDITAR_STATE = "editar";
    private String currentState = PESQUISAR_STATE;
    private String pesquisa;
    private List<PessoaBean> listaPessoas = new ArrayList<PessoaBean>();
    private PessoaBean pessoa;
    private boolean excluirRegistrosTerceiros;
    private boolean editarRegistrosTerceiros;
    private boolean criarRegistro;
    private boolean exibirRegistrosTerceiros;
    private String imagemPassword = "/images/icons/password.png";
    private String imagemEditarRegistro = "/images/icons/editar.png";
    private String imagemExcluirRegistro = "/images/icons/del.png";
    private String imagemCriarRegistro = "/images/icons/add1.png";
    private InstituicaoBean instituicao;
    private UnidadeBean unidade;
    private DepartamentoBean departamento;
    private List<InstituicaoBean> listaInstituicoes = new ArrayList<InstituicaoBean>();
    private List<UnidadeBean> listaUnidades = new ArrayList<UnidadeBean>();
    private List<DepartamentoBean> listaDepartamentos = new ArrayList<DepartamentoBean>();
    private PapelBean papel;
    private List<PapelBean> listaPapeis = new ArrayList<PapelBean>();
    @ManagedProperty(value = "#{userSessionMB}")
    private UserSessionManagedBean userSessionMB;

    public UserSessionManagedBean getUserSessionMB() {
        return userSessionMB;
    }

    public void setUserSessionMB(UserSessionManagedBean userSessionMB) {
        this.userSessionMB = userSessionMB;
    }

    public String getImagemPassword() {
        return imagemPassword;
    }

    public void setImagemPassword(String imagemPassword) {
        this.imagemPassword = imagemPassword;
    }

    public List<PapelBean> getListaPapeis() {
        listaPapeis.clear();
        listaPapeis = SecurityService.getInstance().listarPapeis();
        return listaPapeis;
    }

    public void setListaPapeis(List<PapelBean> listaPapeis) {
        this.listaPapeis = listaPapeis;
    }

    public PapelBean getPapel() {
        return papel;
    }

    public void setPapel(PapelBean papel) {
        this.papel = papel;
    }

    public boolean isCriarRegistro() {
        return criarRegistro;
    }

    public void setCriarRegistro(boolean criarRegistro) {
        this.criarRegistro = criarRegistro;
    }

    public String getImagemCriarRegistro() {
        return imagemCriarRegistro;
    }

    public void setImagemCriarRegistro(String imagemCriarRegistro) {
        this.imagemCriarRegistro = imagemCriarRegistro;
    }

    public String getCurrentState() {
        return currentState;
    }

    public void setCurrentState(String currentState) {
        this.currentState = currentState;
    }

    public List<PessoaBean> getListaPessoas() {
        return listaPessoas;
    }

    public void setListaPessoas(List<PessoaBean> listaPessoas) {
        this.listaPessoas = listaPessoas;
    }

    public String getPesquisa() {
        return pesquisa;
    }

    public void setPesquisa(String pesquisa) {
        this.pesquisa = pesquisa;
    }

    public PessoaBean getPessoa() {
        return pessoa;
    }

    public void setPessoa(PessoaBean pessoa) {
        this.pessoa = pessoa;
    }

    @PostConstruct
    public void postConstruct() {
        logger.debug("Instanciando classe");
        //this.listaPessoas = Service.getInstance().listarPessoas();

        editarRegistrosTerceiros = SecurityService.getInstance().verificarPermissao(userSessionMB.getLoggedUser(), RbacConstantes.PESSOA_EDITAR_TERCEIROS);
        excluirRegistrosTerceiros = SecurityService.getInstance().verificarPermissao(userSessionMB.getLoggedUser(), RbacConstantes.PESSOA_EXCLUIR_TERCEIROS);
        criarRegistro = SecurityService.getInstance().verificarPermissao(userSessionMB.getLoggedUser(), RbacConstantes.PESSOA_CRIAR_REGISTRO);
        exibirRegistrosTerceiros = SecurityService.getInstance().verificarPermissao(userSessionMB.getLoggedUser(), RbacConstantes.PESSOA_EXIBIR_TERCEIROS);
        

        pesquisarUsuarios();
    }


    public void pesquisarUsuarios() {
        logger.debug("PESQUISAR");
        //this.setCurrentState(PESQUISAR_STATE);
        this.pessoa = new PessoaBean();
        if (exibirRegistrosTerceiros) {
            this.listaPessoas = Service.getInstance().pesquisarPessoas(pesquisa, instituicao, unidade, departamento, papel);
        } else {
            this.listaPessoas.clear();
            this.listaPessoas.add(Service.getInstance().pesquisarPessoa(userSessionMB.getLoggedUser()));
        }
    }

    /*
     * Exclui registro
     */
    public void excluir() {
        logger.debug("EXCLUIR REGISTRO");
        for(PessoaProjetoBean pesPrj : Service.getInstance().buscarPessoasProjeto(pessoa)){
            Service.getInstance().excluirPessoaProjeto(pesPrj);
        }
        Service.getInstance().excluirPessoa(pessoa);
        pessoa = null;
        this.pesquisarUsuarios();
        RequestContext.getCurrentInstance().execute("PF('deletePopup').hide();");
    }
    /*
     * Limpa atributo
     */

    public void clear() {
        this.pessoa = new PessoaBean();
    }

    public boolean isPesquisarState() {
        String state = this.getCurrentState();
        return (state == null || PESQUISAR_STATE.equals(state));
    }

    public boolean isAdicionarState() {
        return ADICIONAR_STATE.equals(this.getCurrentState());
    }

    public boolean isEditarState() {
        return EDITAR_STATE.equals(this.getCurrentState());
    }

    public boolean isEditarRegistrosTerceiros() {
        return editarRegistrosTerceiros;
    }

    public void setEditarRegistrosTerceiros(boolean editarRegistrosTerceiros) {
        this.editarRegistrosTerceiros = editarRegistrosTerceiros;
    }

    public boolean isExcluirRegistrosTerceiros() {
        return excluirRegistrosTerceiros;
    }

    public void setExcluirRegistrosTerceiros(boolean excluirRegistrosTerceiros) {
        this.excluirRegistrosTerceiros = excluirRegistrosTerceiros;
    }

    public String getImagemEditarRegistro() {
        return imagemEditarRegistro;
    }

    public void setImagemEditarRegistro(String imagemEditarRegistro) {
        this.imagemEditarRegistro = imagemEditarRegistro;
    }

    public String getImagemExcluirRegistro() {
        return imagemExcluirRegistro;
    }

    public void setImagemExcluirRegistro(String imagemExcluirRegistro) {
        this.imagemExcluirRegistro = imagemExcluirRegistro;
    }

    public boolean podeEditarRegistro(PessoaBean pessoa) {
        if (pessoa.equals(userSessionMB.getLoggedUser())) {
            imagemEditarRegistro = "/images/icons/editar.png";
            return false;
        } else if (!pessoa.equals(userSessionMB.getLoggedUser()) && editarRegistrosTerceiros) {
            imagemEditarRegistro = "/images/icons/editar.png";
            return false;
        }
        imagemEditarRegistro = "/images/icons/editarbw.png";
        return true;
    }

    public boolean podeExcluirRegistro(PessoaBean pessoa) {
        if (!pessoa.equals(userSessionMB.getLoggedUser()) && excluirRegistrosTerceiros) {
            imagemExcluirRegistro = "/images/icons/del.png";
            return false;
        }
        imagemExcluirRegistro = "/images/icons/delbw.png";
        return true;
    }

    public boolean podeCriarRegistro() {
        if (criarRegistro) {
            imagemCriarRegistro = "/images/icons/add1.png";
            return false;
        } else {
            imagemCriarRegistro = "/images/icons/add1bw.png";
            return true;
        }
    }

    public StreamedContent downloadReportPdf() {
        logger.debug("GERAR RELATORIO PDF");
        InputStream stream = ReportService.getInstance().emiteRelatorioPessoas(listaPessoas, ReportService.FORMATO_PDF, listarFiltrosAtivos());
        StreamedContent file = new DefaultStreamedContent(stream, "application/pdf", "report.pdf");
        return file;
    }

    public StreamedContent downloadReportXls() {
        logger.debug("GERAR RELATORIO XLS");
        InputStream stream = ReportService.getInstance().emiteRelatorioPessoas(listaPessoas, ReportService.FORMATO_XLS, listarFiltrosAtivos());
        StreamedContent file = new DefaultStreamedContent(stream, "application/vnd.ms-excel", "report.xls");
        return file;
    }

    public InstituicaoBean getInstituicao() {
        return instituicao;
    }

    public void setInstituicao(InstituicaoBean instituicao) {
        this.instituicao = instituicao;
    }

    public List<InstituicaoBean> getListaInstituicoes() {
        listaInstituicoes.clear();
        listaInstituicoes = Service.getInstance().listarInstituicoes();
        return listaInstituicoes;
    }

    public void setListaInstituicoes(List<InstituicaoBean> listaInstituicoes) {
        this.listaInstituicoes = listaInstituicoes;
    }

    public List<UnidadeBean> getListaUnidades() {
        return listaUnidades;
    }

    public void setListaUnidades(List<UnidadeBean> listaUnidades) {
        this.listaUnidades = listaUnidades;
    }

    public UnidadeBean getUnidade() {
        return unidade;
    }

    public void setUnidade(UnidadeBean unidade) {
        this.unidade = unidade;
    }

    public DepartamentoBean getDepartamento() {
        return departamento;
    }

    public void setDepartamento(DepartamentoBean departamento) {
        this.departamento = departamento;
    }

    public List<DepartamentoBean> getListaDepartamentos() {
        return listaDepartamentos;
    }

    public void setListaDepartamentos(List<DepartamentoBean> listaDepartamentos) {
        this.listaDepartamentos = listaDepartamentos;
    }

    public void pesquisarUnidades() {
        listaUnidades.clear();
        if (instituicao != null) {
            listaUnidades = instituicao.getUnidades();
        }
        listaDepartamentos.clear();
        unidade = null;
        pesquisarDepartamentos();
        pesquisarUsuarios();
    }

    public void pesquisarDepartamentos() {
        listaDepartamentos.clear();
        if (unidade != null) {
            listaDepartamentos = unidade.getDepartamentos();
        }
        departamento = null;
        pesquisarUsuarios();
    }

    public String detalharPessoa(PessoaBean pessoa) {
        userSessionMB.setInterPageParameter(pessoa);
        return "infoPessoa?faces-redirect=true";
    }
    
    private List<String> listarFiltrosAtivos(){
        List<String> retorno = new ArrayList<String>();

        // Palavra chave
        if(pesquisa!=null && !pesquisa.isEmpty()) retorno.add("Palavra-chave: " + pesquisa);
        
        //instituicao
        if(instituicao!=null) retorno.add("Instituição: " + instituicao.getSigla());
        
        //unidade
        if(unidade!=null) retorno.add("Unidade: " + unidade.getSigla());
        
        //departamento
        if(instituicao!=null) retorno.add("Departamento: " + instituicao.getSigla());
        
        //papel
        if(papel!=null) retorno.add("Papel: " + papel.getNome());
        
        return retorno;
    }
}
