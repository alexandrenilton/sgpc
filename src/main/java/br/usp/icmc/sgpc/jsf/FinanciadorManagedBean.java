/*
 * Sistema de Gestão de Projetos Científicos - SGPC
 * 
 * 
 * Copyright  (C)  2010-2014  Instituto  de  Ciências  Matemáticas  e  de
 *                            Computação - ICMC/USP
 * 
 * 
 * This program is  free software; you can redistribute  it and/or modify
 * it under the  terms of the GNU General Public  License as published by
 * the Free Software Foundation; either  version 2 of the License, or (at
 * your option) any later version.
 * 
 * This program  is distributed in the  hope that it will  be useful, but
 * WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
 * MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
 * General Public License for more details.
 * 
 * You  should have received  a copy  of the  GNU General  Public License
 * along  with  this  program;  if   not,  write  to  the  Free  Software
 * Foundation,  Inc.,  51  Franklin   Street,  Fifth  Floor,  Boston,  MA
 * 02110-1301, USA.
 * 
 * 
 * EXCEPTION TO THE TERMS AND CONDITIONS OF GNU GENERAL PUBLIC LICENSE
 * 
 * The  LICENSE  file may  contain  an  additional  clause as  a  special
 * exception  to the  terms  and  conditions of  the  GNU General  Public
 * License. This  clause, if  present, gives you  the permission  to link
 * this  program with  certain third-part  software and  to obtain,  as a
 * result, a  work based  on this program  that can be  distributed using
 * other license than the GNU General Public License.  If you modify this
 * program, you may extend this exception to your version of the program,
 * but you  are not obligated  to do so.   If you do  not wish to  do so,
 * delete this exception statement from your version.
 * 
 */

package br.usp.icmc.sgpc.jsf;

import br.usp.icmc.sgpc.beans.FinanciadorBean;
import br.usp.icmc.sgpc.beans.ModeloRelatorioPrestacaoContasBean;
import br.usp.icmc.sgpc.beans.PessoaBean;
import br.usp.icmc.sgpc.security.RbacConstantes;
import br.usp.icmc.sgpc.security.service.SecurityService;
import br.usp.icmc.sgpc.service.ReportService;
import br.usp.icmc.sgpc.service.Service;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIPanel;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import org.apache.log4j.Logger;
import org.primefaces.context.RequestContext;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

/**
 *
 * @author Artur
 */
@ManagedBean(name = "financiadorMB")
@ViewScoped
public class FinanciadorManagedBean implements Serializable {

    private static final Logger logger = Logger.getLogger(FinanciadorManagedBean.class);
    public static final String PESQUISAR_STATE = "pesquisar";
    public static final String ADICIONAR_STATE = "adicionar";
    public static final String EDITAR_STATE = "editar";
    private String currentState = PESQUISAR_STATE;
    private UIPanel panelForm;
    private Integer currentRow;
    private String pesquisa;
    private boolean podeCriarFinanciador;
    private boolean podeEditarFinanciador;
    private boolean podeExcluirFinanciador;
    private String imagemEditarRegistro = "/images/icons/editar.png";
    private String imagemExcluirRegistro = "/images/icons/del.png";
    private String imagemCriarRegistro = "/images/icons/add1.png";
    
    private FinanciadorBean financiador;
    private List<FinanciadorBean> listaFinanciadores = new ArrayList<FinanciadorBean>();
    
    private Integer idModeloRelatorio;
    private List<SelectItem> listaModelosRelatoriosSelectItem = new ArrayList<SelectItem>();
    
    @ManagedProperty(value = "#{userSessionMB}")
    private UserSessionManagedBean userSessionMB;

    public FinanciadorBean getFinanciador() {
        return financiador;
    }

    public void setFinanciador(FinanciadorBean financiador) {
        this.financiador = financiador;
    }

    public List<FinanciadorBean> getListaFinanciadores() {
        return listaFinanciadores;
    }

    public void setListaFinanciadores(List<FinanciadorBean> listaFinanciadores) {
        this.listaFinanciadores = listaFinanciadores;
    }

    public String getCurrentState() {
        return currentState;
    }

    public void setCurrentState(String currentState) {
        this.currentState = currentState;
    }

    public String getImagemCriarRegistro() {
        if (podeCriarFinanciador) {
            imagemCriarRegistro = "/images/icons/add1.png";
        } else {
            imagemCriarRegistro = "/images/icons/add1bw.png";
        }
        return imagemCriarRegistro;
    }

    public void setImagemCriarRegistro(String imagemCriarRegistro) {
        this.imagemCriarRegistro = imagemCriarRegistro;
    }

    public String getImagemEditarRegistro() {
        if (podeEditarFinanciador) {
            imagemEditarRegistro = "/images/icons/editar.png";
        } else {
            imagemEditarRegistro = "/images/icons/editarbw.png";
        }
        return imagemEditarRegistro;
    }

    public void setImagemEditarRegistro(String imagemEditarRegistro) {
        this.imagemEditarRegistro = imagemEditarRegistro;
    }

    public String getImagemExcluirRegistro() {
        if(podeExcluirFinanciador){
            imagemExcluirRegistro = "/images/icons/del.png";
        }else{
            imagemExcluirRegistro = "/images/icons/delbw.png";
        }
        return imagemExcluirRegistro;
    }

    public void setImagemExcluirRegistro(String imagemExcluirRegistro) {
        this.imagemExcluirRegistro = imagemExcluirRegistro;
    }

    public boolean isPodeCriarFinanciador() {
        return podeCriarFinanciador;
    }

    public void setPodeCriarFinanciador(boolean podeCriarFinanciador) {
        this.podeCriarFinanciador = podeCriarFinanciador;
    }

    public boolean isPodeEditarFinanciador() {
        return podeEditarFinanciador;
    }

    public void setPodeEditarFinanciador(boolean podeEditarFinanciador) {
        this.podeEditarFinanciador = podeEditarFinanciador;
    }

    public boolean isPodeExcluirFinanciador() {
        return podeExcluirFinanciador;
    }

    public void setPodeExcluirFinanciador(boolean podeExcluirFinanciador) {
        this.podeExcluirFinanciador = podeExcluirFinanciador;
    }

    public FinanciadorManagedBean() {
        logger.debug("Instanciando classe");
    }

    public Integer getCurrentRow() {
        return currentRow;
    }

    public void setCurrentRow(Integer currentRow) {
        this.currentRow = currentRow;
    }

    public UIPanel getPanelForm() {
        return panelForm;
    }

    public void setPanelForm(UIPanel panelForm) {
        this.panelForm = panelForm;
    }

    public String getPesquisa() {
        return pesquisa;
    }

    public void setPesquisa(String pesquisa) {
        this.pesquisa = pesquisa;
    }

    public void pesquisar() {
        logger.debug("PESQUISAR");
        this.financiador = new FinanciadorBean();
        if ("".equals(this.pesquisa) || this.pesquisa == null) {
            logger.debug("PESQUISAR TODOS");
            this.listaFinanciadores = Service.getInstance().listarFinanciadores();
        } else {
            logger.debug("PESQUISAR COM TERMO");
            this.listaFinanciadores = Service.getInstance().pesquisarFinanciadores(pesquisa);
        }
    }

    public void prepareAdicionar() {
        financiador = new FinanciadorBean();
        idModeloRelatorio = null;
        this.setCurrentState(ADICIONAR_STATE);
        RequestContext.getCurrentInstance().execute("PF('addEditPopup').show();");
    }

    public void prepareEditar(FinanciadorBean fin) {
        this.setCurrentState(EDITAR_STATE);
        this.financiador = fin;
        idModeloRelatorio = null;
        if(financiador.getFkModeloRelatorio()!=null){
            idModeloRelatorio = financiador.getFkModeloRelatorio().getId();
        }
        RequestContext.getCurrentInstance().execute("PF('addEditPopup').show();");
    }
    public void prepareExcluir(FinanciadorBean fin) {
        this.setCurrentState(EDITAR_STATE);
        this.financiador = fin;
        RequestContext.getCurrentInstance().execute("PF('deletePopup').show();");
    }

    public void excluir() {
        logger.debug("EXCLUIR REGISTRO");
        try{
            Service.getInstance().excluirFinanciador(financiador);
        }catch(Exception e){
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Impossível excluir registro", "O financiador tem auxílios vinculados.");
            FacesContext.getCurrentInstance().addMessage("", message);
        }
        this.pesquisar();
        RequestContext.getCurrentInstance().execute("PF('deletePopup').hide();");
    }

    public boolean isPesquisarState() {
        String state = this.getCurrentState();
        return (state == null || PESQUISAR_STATE.equals(state));
    }

    public boolean isAdicionarState() {
        return ADICIONAR_STATE.equals(this.getCurrentState());
    }

    public boolean isEditarState() {
        return EDITAR_STATE.equals(this.getCurrentState());
    }

    public void gravar() {
        if(idModeloRelatorio!=null){
            financiador.setFkModeloRelatorio(Service.getInstance().buscarModeloRelatorio(idModeloRelatorio));
        }else{
            financiador.setFkModeloRelatorio(null);
        }
        if (ADICIONAR_STATE.equals(this.currentState)) {
            logger.debug("ADICIONAR REGISTRO");
            Service.getInstance().cadastraFinanciador(financiador);
        } else if (EDITAR_STATE.equals(this.currentState)) {
            logger.debug("EDITAR REGISTRO");
            Service.getInstance().atualizarFinanciador(financiador);
        }
        this.pesquisar();
        RequestContext.getCurrentInstance().execute("PF('addEditPopup').hide();");
    }

    public String abreLinhas(FinanciadorBean financiador) {
        userSessionMB.setInterPageParameter(financiador);
        return "tiposAuxilios?faces-redirect=true";
    }

    @PostConstruct
    public void postConstruct(){
        PessoaBean usuarioLogado = userSessionMB.getLoggedUser();

        podeCriarFinanciador = SecurityService.getInstance().verificarPermissao(usuarioLogado, RbacConstantes.FINACIADOR_CRIAR);
        podeEditarFinanciador = SecurityService.getInstance().verificarPermissao(usuarioLogado, RbacConstantes.FINANCIADOR_EDITAR);
        podeExcluirFinanciador = SecurityService.getInstance().verificarPermissao(usuarioLogado, RbacConstantes.FINANCIADOR_EXCLUIR);
        
        List<ModeloRelatorioPrestacaoContasBean> listaModelosRelatorios =  Service.getInstance().listarModelosRelatorios();
        listaModelosRelatoriosSelectItem = GenericConverter.createSelectItems(listaModelosRelatorios);

        this.pesquisar();
    }

    public UserSessionManagedBean getUserSessionMB() {
        return userSessionMB;
    }

    public void setUserSessionMB(UserSessionManagedBean userSessionMB) {
        this.userSessionMB = userSessionMB;
    }

    public StreamedContent downloadReportPdf() {
        logger.debug("GERAR RELATORIO PDF");
        InputStream stream = ReportService.getInstance().emiteRelatorioFinanciadores(listaFinanciadores, ReportService.FORMATO_PDF);
        StreamedContent file = new DefaultStreamedContent(stream, "application/pdf", "report.pdf");
        return file;
    }

    public StreamedContent downloadReportXls() {
        logger.debug("GERAR RELATORIO XLS");
        InputStream stream = ReportService.getInstance().emiteRelatorioFinanciadores(listaFinanciadores, ReportService.FORMATO_XLS);
        StreamedContent file = new DefaultStreamedContent(stream, "application/vnd.ms-excel", "report.xls");
        return file;
    }

    public Integer getIdModeloRelatorio() {
        return idModeloRelatorio;
    }

    public void setIdModeloRelatorio(Integer idModeloRelatorio) {
        this.idModeloRelatorio = idModeloRelatorio;
    }

    public List<SelectItem> getListaModelosRelatoriosSelectItem() {
        return listaModelosRelatoriosSelectItem;
    }

    public void setListaModelosRelatoriosSelectItem(List<SelectItem> listaModelosRelatoriosSelectItem) {
        this.listaModelosRelatoriosSelectItem = listaModelosRelatoriosSelectItem;
    }

}
