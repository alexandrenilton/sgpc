/*
 * Sistema de Gestão de Projetos Científicos - SGPC
 * 
 * 
 * Copyright  (C)  2010-2014  Instituto  de  Ciências  Matemáticas  e  de
 *                            Computação - ICMC/USP
 * 
 * 
 * This program is  free software; you can redistribute  it and/or modify
 * it under the  terms of the GNU General Public  License as published by
 * the Free Software Foundation; either  version 2 of the License, or (at
 * your option) any later version.
 * 
 * This program  is distributed in the  hope that it will  be useful, but
 * WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
 * MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
 * General Public License for more details.
 * 
 * You  should have received  a copy  of the  GNU General  Public License
 * along  with  this  program;  if   not,  write  to  the  Free  Software
 * Foundation,  Inc.,  51  Franklin   Street,  Fifth  Floor,  Boston,  MA
 * 02110-1301, USA.
 * 
 * 
 * EXCEPTION TO THE TERMS AND CONDITIONS OF GNU GENERAL PUBLIC LICENSE
 * 
 * The  LICENSE  file may  contain  an  additional  clause as  a  special
 * exception  to the  terms  and  conditions of  the  GNU General  Public
 * License. This  clause, if  present, gives you  the permission  to link
 * this  program with  certain third-part  software and  to obtain,  as a
 * result, a  work based  on this program  that can be  distributed using
 * other license than the GNU General Public License.  If you modify this
 * program, you may extend this exception to your version of the program,
 * but you  are not obligated  to do so.   If you do  not wish to  do so,
 * delete this exception statement from your version.
 * 
 */
package br.usp.icmc.sgpc.jsf;

import br.usp.icmc.sgpc.beans.FornecedorBean;
import br.usp.icmc.sgpc.beans.RepresentanteVendasBean;
import br.usp.icmc.sgpc.service.Service;
import br.usp.icmc.sgpc.service.ValidaDocumentosService;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;
import org.apache.log4j.Logger;
import org.primefaces.context.RequestContext;

/**
 *
 * @author Artur
 */
@ManagedBean(name = "addEditFornecedorMB")
@ViewScoped
public class AddEditFornecedorManagedBean implements Serializable {

    private static final Logger logger = Logger.getLogger(AddEditAlineaManagedBean.class);
    public static final String PESQUISAR_STATE = "pesquisar";
    public static final String ADICIONAR_STATE = "adicionar";
    public static final String EDITAR_STATE = "editar";
    private String currentState = PESQUISAR_STATE;
    @ManagedProperty(value = "#{userSessionMB}")
    private UserSessionManagedBean userSessionMB;

    private FornecedorBean fornecedor;
    private String nacionalidade;
    private boolean esconderCnpj;
    private RepresentanteVendasBean representante;
    private List<String> listaStatus = new ArrayList<String>();

    @PostConstruct
    public void postConstruct() {
        fornecedor = new FornecedorBean();
        nacionalidade = "nacional";
        esconderCnpj = false;
    }

    public UserSessionManagedBean getUserSessionMB() {
        return userSessionMB;
    }

    public void setUserSessionMB(UserSessionManagedBean userSessionMB) {
        this.userSessionMB = userSessionMB;
    }

    public String getCurrentState() {
        return currentState;
    }

    public void setCurrentState(String currentState) {
        this.currentState = currentState;
    }

    public FornecedorBean getFornecedor() {
        return fornecedor;
    }

    public void setFornecedor(FornecedorBean fornecedor) {
        this.fornecedor = fornecedor;
    }

    public RepresentanteVendasBean getRepresentante() {
        return representante;
    }

    public void setRepresentante(RepresentanteVendasBean representante) {
        this.representante = representante;
    }

    public void prepareEditar(FornecedorBean forn) {
        this.setCurrentState(EDITAR_STATE);
        this.fornecedor = forn;
        if (this.fornecedor.getCnpj() == null || this.fornecedor.getCnpj().length() == 0) {
            this.nacionalidade = "internacional";
            this.esconderCnpj = true;
        } else {
            this.nacionalidade = "nacional";
            this.esconderCnpj = false;
        }
        RequestContext.getCurrentInstance().execute("PF('addEditFornecedorPopup').show();");
    }

    public void prepareExcluir(FornecedorBean forn) {
        this.setCurrentState(EDITAR_STATE);
        this.fornecedor = forn;
        RequestContext.getCurrentInstance().execute("PF('deleteFornecedorPopup').show();");
    }

    public void prepareEditar(RepresentanteVendasBean repr, FornecedorBean forn) {
        this.setCurrentState(EDITAR_STATE);
        this.fornecedor = forn;
        this.representante = repr;
        if (this.fornecedor.getCnpj() == null || this.fornecedor.getCnpj().length() == 0) {
            this.nacionalidade = "internacional";
            this.esconderCnpj = true;
        } else {
            this.nacionalidade = "nacional";
            this.esconderCnpj = false;
        }
        RequestContext.getCurrentInstance().execute("PF('addEditPopupRepresentante').show();");
    }

    public void prepareExcluir(RepresentanteVendasBean repr, FornecedorBean forn) {
        this.setCurrentState(EDITAR_STATE);
        this.fornecedor = forn;
        this.representante = repr;
        RequestContext.getCurrentInstance().execute("PF('deletePopupRepresentante').show();");
    }

    public void prepareAdicionar() {
        this.clear();
        this.setCurrentState(ADICIONAR_STATE);
        RequestContext.getCurrentInstance().execute("PF('addEditFornecedorPopup').show();");
    }

    public void prepareAdicionarRepresentante(RepresentanteVendasBean rep, FornecedorBean forn) {
        this.representante = new RepresentanteVendasBean();
        this.representante = rep;
        this.fornecedor = forn;
        this.setCurrentState(ADICIONAR_STATE);
        RequestContext.getCurrentInstance().execute("PF('addEditPopupRepresentante').show();");
    }

    public void gravar() {
        if(fornecedor.getCnpj() != null && !fornecedor.getCnpj().isEmpty()){
            List<FornecedorBean> listaDuplicados = Service.getInstance().pesquisarFornecedorCriteria(fornecedor.getCnpj(), null);
            for(FornecedorBean fornTemp : listaDuplicados){
                if(fornecedor.getCnpj().equalsIgnoreCase(fornTemp.getCnpj()) && ((fornecedor.getId()==null) || (fornecedor.getId().intValue() != fornTemp.getId().intValue()))){
                    FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "CNPJ inválido", "O CNPJ informado já está cadastrado no sistema.");
                    FacesContext.getCurrentInstance().addMessage("cnpj", message);
                    return;
                }
            }
        }
        if (ADICIONAR_STATE.equals(this.currentState)) {
            logger.debug("ADICIONAR REGISTRO");
            //this.fornecedor.setFkEndereco(fornecedor.getFkEndereco());
            Service.getInstance().cadastraFornecedor(fornecedor);
        } else if (EDITAR_STATE.equals(this.currentState)) {
            logger.debug("EDITAR REGISTRO");
            Service.getInstance().atualizarFornecedor(fornecedor);
        }
        RequestContext.getCurrentInstance().execute("PF('addEditFornecedorPopup').hide();");
    }

    public void excluir() {
        logger.debug("EXCLUIR REGISTRO");
        Service.getInstance().excluirFornecedor(fornecedor);
        RequestContext.getCurrentInstance().execute("PF('deleteFornecedorPopup').hide();");
    }

    public void gravarRepresentante() {
        if (ADICIONAR_STATE.equals(this.currentState)) {
            logger.debug("ADICIONAR REGISTRO");
            fornecedor.getRepresentantes().add(representante);
            Service.getInstance().atualizarFornecedor(fornecedor);
            //representante.setFkFornecedor(fornecedor);
            //Service.getInstance().cadastrarRepresentanteVendas(representante);
        } else if (EDITAR_STATE.equals(this.currentState)) {
            logger.debug("EDITAR REGISTRO");
            Service.getInstance().atualizarRepresentanteVendas(representante);
        }
        RequestContext.getCurrentInstance().execute("PF('addEditPopupRepresentante').hide();");
    }

    public void excluirRepresentante() {
        logger.debug("EXCLUIR REGISTRO");
        fornecedor.getRepresentantes().remove(representante);
        Service.getInstance().excluirRepresentanteVendas(representante);
        Service.getInstance().atualizarFornecedor(fornecedor);
        RequestContext.getCurrentInstance().execute("PF('deletePopupRepresentante').hide();");
    }

    public void clear() {
        this.fornecedor = new FornecedorBean();
        this.nacionalidade = "nacional";
        this.esconderCnpj = false;
        //this.cleanSubmittedValues(this.panelForm);
    }

    public String getNacionalidade() {
        return nacionalidade;
    }

    public void setNacionalidade(String nacionalidade) {
        this.nacionalidade = nacionalidade;
        if ("nacional".equalsIgnoreCase(this.nacionalidade)) {
            esconderCnpj = false;
        } else {
            esconderCnpj = true;
            this.fornecedor.setCnpj("");
            this.fornecedor.setIe("");
        }
    }

    public boolean getEsconderCnpj() {
        return esconderCnpj;
    }

    public void setEsconderCnpj(boolean esconderCnpj) {
        this.esconderCnpj = esconderCnpj;
    }
    
    public List<String> getListaStatus() {
        listaStatus.clear();
        listaStatus.add("Ativo");
        listaStatus.add("Inativo");
        listaStatus.add("Suspenso");
        return listaStatus;
    }

    public void setListaStatus(List<String> listaStatus) {
        this.listaStatus = listaStatus;
    }
    
    public void validateCNPJ(FacesContext facesContext, UIComponent component, Object newValue) throws ValidatorException {
        boolean cnpjValido = true;
        if (newValue!=null && !((String)newValue).isEmpty()) {
            cnpjValido = ValidaDocumentosService.getInstance().isValidCNPJ(newValue.toString());
            if(!cnpjValido){
                ((UIInput) component).setValid(false);
                FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "CNPJ inválido", "O dígito verificador não permite validar esse numero de CNPJ");
                facesContext.addMessage(component.getClientId(facesContext), message);
            }
        }
    }

}
